<?php

function create_voucher_pdf($filename, $user_header, $user_text, $code, $value, $currency_code, $lang, $img)
{
	$template_dir = $GLOBALS['CONFIG']['app']['voucher_templates_dir'];

	$lang = strtolower($lang);

	$pdf_template = $template_dir . '/template_' . $lang . '.pdf';
	$tex_template = file_get_contents($template_dir . '/template.tex');
	$frame_template = $template_dir . '/frame_' . $lang . '.pdf';

	$value = price_format($value, $currency_code);
	$image = $template_dir . '/themes/' . $img . '.jpg';
	$image = tex_insert_jpeg($image);
	$text = str_ireplace('<br>', '\\hfill\\break ', trim($user_text));			// TeX-Zeilenumbrüche einfügen
	$header = mb_strtoupper(trim($user_header), 'UTF-8');
	$header = str_ireplace('<br>', '\\hfill\\break ', $header);
	$code = strtoupper($code);

	$values = array(
		'IMAGE' => array($image, false),
		'USER_TEXT' => array($text, false),
		'USER_HEADER' => array($header, false),
		'CODE' => array($code, true),
		'VALUE' => array($value, true),
		'LEFT_CURRENCY_SYMBOL' => array($left_curr_symbol, true),
		'RIGHT_CURRENCY_SYMBOL' => array($right_curr_symbol, true),
	);
	$tex = insert_values_into_tex($values, $tex_template);

	// Die PDF-Datei mit den dynamischen Inhalten erzeugen
	$pdf_path = df_tex_render_xetex_to_file($tex);

	// Vorlage, dynamische Inhalte und Rahmen und "Gutschein"-Schriftzug übereinanderlegen
	df_tex_overlay_pdfs(array($pdf_template, $pdf_path, $frame_template), $filename);
}

// Format von $values: array(STRING $variable_name => array(STRING $value, BOOLEAN $escape_value) [, ...])
function insert_values_into_tex($values, $tex)
{
	foreach ($values as $var => $value)
		$tex = str_replace('[[' . strtoupper(trim($var)) . ']]', $value[1] ? df_tex_escape_for_xetex($value[0]) : $value[0], $tex);
	return $tex;
}

function price_format($amount, $currency_code)
{
	if ($currency_code == 'AUD')
		return utf8_encode('$').number_format($amount,2,'.',' ');
	if ($currency_code == 'CAD')
		return utf8_encode('$').number_format($amount,2,'.',' ');
	if ($currency_code == 'CHF')
		return 'CHF '.number_format($amount,2,',',' ');
	if ($currency_code == 'DKK')
		return number_format($amount,2,',',' ').' kr.';
	if ($currency_code == 'EUR')
		return number_format($amount,2,',',' ')." \xE2\x82\xAC"; // Eurozeichen ist zwar in CP1252, aber nicht in ISO-8859-1
	if ($currency_code == 'GBP')
		return utf8_encode('£').number_format($amount,2,'.',' ');
	if ($currency_code == 'JPY')
		return number_format($amount,0,'.',' ').utf8_encode(' ¥');
	if ($currency_code == 'NOK')
		return number_format($amount,2,',',' ').' kr';
	if ($currency_code == 'SEK')
		return number_format($amount,2,',',' ').' kr';
	if ($currency_code == 'USD')
		return utf8_encode('$').number_format($amount,2,'.',' ');
}

function tex_insert_jpeg($filepath, $valuelist = null)
{
	$tempdir = df_get_cache_dir('tex');

	// Dateien im Tempdir zur Verfügung stellen. Dort verweilen sie dann hoffentlich so lange, bis df_tex_render_to_file aufgerufen wird
	$tempfilepath = $tempdir.DIRECTORY_SEPARATOR.df_scriptwide_prefix().'_'.md5($filepath . filemtime($filepath)).'.jpg';
	$tempfilename = df_scriptwide_prefix().'_'.md5($filepath . filemtime($filepath)) . '.jpg';
	copy($filepath,$tempfilepath);

	$output = '\\XeTeXpicfile "' . $tempfilename . '"';
	if (isset($valuelist['width']))
		$output .= ' width ' . $valuelist['width'] . ' ';
	if (isset($valuelist['height']))
		$output .= 'height ' . $valuelist['height'] . ' ';
	return $output;
}

function initialize_df()
{
	df_init();

	df_component_load('database');
	df_component_load('tex');

	df_register_callback('database', 'df_db_connection', 'on_connect', app_set_db_charset);

	function app_set_db_charset($meta, $connection, $identifier = null)
	{
		if ($identifier == 'default')
			mysql_query('SET NAMES \'utf8\'', $connection);
	}
}
