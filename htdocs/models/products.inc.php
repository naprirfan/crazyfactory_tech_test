<?php

require_once(dirname(__FILE__).'/../includes/database.inc.php');

/*
digraph g{
	activate_new_producs_of_the_day -> products_new -> products_status
	activate_new_producs_of_the_day -> products_new_activated -> products_status
	"Artikel-Bearbeiten-Maske" -> manually_deactivated
	"Artikel-Bearbeiten-Maske" -> products_new
	Produktimport -> manually_deactivated -> products_status
	Produktimport -> products_new
	ERP -> "maint_optimize_formulas.php" -> out_of_stock -> products_status
	"specials-Tabelle" -> "Artikel des Tages" -> products_status
	"specials-Tabelle" -> "Morgen Artikel des Tages" -> products_status
}

digraph g{
	"Manuell\ndeaktiviert?" [shape=diamond]
	"Manuell\ndeaktiviert?" -> Inaktiv [label=Ja]
	"Manuell\ndeaktiviert?" -> "Heute oder\nmorgen Artikel des\nTages?" [label=Nein]
	"Heute oder\nmorgen Artikel des\nTages?" [shape=diamond]
	"Heute oder\nmorgen Artikel des\nTages?" -> Aktiv [label=Ja]
	"Heute oder\nmorgen Artikel des\nTages?" -> "Neues\nProdukt?" [label=Nein]
	"Neues\nProdukt?" [shape=diamond]
	"Neues\nProdukt?" -> "Schon aktiviert?" [label=Ja]
	"Schon aktiviert?" [shape=diamond]
	"Schon aktiviert?" -> "Aktiv" [label=Ja]
	"Schon aktiviert?" -> "Inaktiv" [label=Nein]
	"Neues\nProdukt?" -> "Out of Stock?" [label=Nein]
	"Out of Stock?" [shape=diamond]
	"Out of Stock?" -> "Inaktiv" [label=Ja]
	"Out of Stock?" -> "Aktiv" [label=Nein]
}

*/

function update_products_status($product_ids = null)
{
	$where = 'TRUE';

	if ($product_ids)
	{
		$where = 'products_id IN (';
		foreach ((array)$product_ids as $product_id)
			$where .= sqlval($product_id).',';
		$where = substr($where, 0, -1);
		$where .= ')';
	}

	query('
		UPDATE products SET products_status =
			!manually_deactivated AND (
				EXISTS (SELECT TRUE FROM specials WHERE specials.products_id = products.products_id AND start_date <= DATE(NOW() + INTERVAL 1 DAY) AND expires_date >= DATE(NOW())) OR -- ob sich ein Special f�r das Produkt mit Heute oder Morgen �berlappt
				products_new AND products_new_activated OR
				!products_new AND !out_of_stock
			)
			WHERE '.$where.'
	');
}