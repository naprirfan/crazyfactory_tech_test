<?php

// Alles was sich um die Produktattribute dreht

function reconstruct_attributes_from_article_number($products_model,$full_article_number)
{
	$product_data = array_pop(query('SELECT formula_main, formula_deactivation, konstruktor FROM products WHERE products_model = '.sqlval($products_model).' LIMIT 1'));
	$effective_formula = $product_data['formula_main']."\n".$product_data['formula_deactivation'];

	// Der alte Schei� ;)
	$konstruktor_array = array_merge( //Hier kriegen wir alle Felder die das Produkt hat
  	array_filter(
  		preg_split('/(\[|\]|\]\[)/',$product_data['konstruktor']),
  		create_function('$val', 'return !empty($val) && $val != "*";')
  	)
  );

	// Die Liste aller m�glichen Attributkombinationen f�llt als Nebenprodukt beim Erstellen des Formel-Cache-Arrays an:
	$combinations = prepare_formula($product_data['formula_main'], $konstruktor_array, $metadata, true);

	// Der triviale Ansatz... wir probieren alle durch bis einer pa�t
	unset($matching_combination);
	if ($combinations) foreach ($combinations as $combination)
	{
		$test_article_number = $products_model.$product_data['konstruktor'];
		foreach ($combination as $attribute_name => $attribute_value)
			$test_article_number = str_replace('['.$attribute_name.']',str_replace(array('.','/',','),'',$attribute_value),$test_article_number);

		if ($test_article_number == $full_article_number)
		{
			$matching_combination = $combination;
			break;
		}
	}
	return $matching_combination;
}

function refresh_formula_array($products_property, $query_by = 'products_id')
{
	query('UPDATE products SET formula_needs_processing = 1, export_to_erp = 1 WHERE '.$query_by.' = '.sqlval($products_property).'');
}

function f_sort($array)
{
	sort($array);
	return $array;
}

// Liest die Formel ein und speichert sie wieder als serialisiertes Array. Dabei werden die Minus-Zeilen so zusammengefasst,
// dass die Funktionen f�r die Produktdetailseite funktionieren

function prepare_formula($formula, $attributes, &$metadata, $return_combinations = false)
{
	$metadata = array();
	$start_time = microtime(true);
	echo number_format (microtime(true)-$start_time,3,',',' ').' - ';

	$formula_lines = explode("\n", $formula);

	// ==========
	// Zuerst parsen wir alle Zeilen und machen daraus einerseits maschinenlesbare Regeln und andererseits eine Liste aller Werte der Attribute

	$plus_rules = array();
	$plus_rules_sort = array();
	$all_attribute_values = array();
	$original_minus_rules = array();
	$especialnesses = array();

	foreach ($formula_lines as $line)
	{
		$line = trim($line);

		$operator = substr($line, 0, 1);

		if ($operator == '+' || $operator == '-')
		{

			$rule = array();

			$parts = explode('|', substr($line, 2, -1));
			foreach ($parts as $part)
			{
				list($attribute_name, $values_list) = explode(':', $part, 2);
				$values = explode(';', $values_list);

				foreach ($values as $value)
				{
					// Das hier ist pro Regel:
					$rule[$attribute_name][$value] = true;                 // Indexiert f�r schnelle Pr�fung

					// Das hier ist global:
					$all_attribute_values[$attribute_name][$value] = true; // Gleiches Pattern, anderer Zweck: Wir wollen jeden Wert nur einmal
				}
			}

			if ($operator == '+')
			{
				$plus_rules []= $rule;
				$plus_rules_sort []= count($parts);
			}

			if ($operator == '-')
				$original_minus_rules []= $rule;
		}

		if ($operator == '=')
		{
			$uber_parts = array();
			preg_match_all('/^=\[([^]]+)\]\(([^)]+)\)$/',$line,$uber_parts);

			foreach (explode('|',$uber_parts[2][0]) as $especialness)
			{
				list($key, $value) = explode(':',$especialness,2);
				if ($key == 'Preis')
				{
					$value = str_replace(',', '.', $value); // Englische Preise und so
					$extra_charge = floatval($value);
				}
			}

			list($attribute_name, $values_list) = explode(':', $uber_parts[1][0], 2);
			$values = explode(';', $values_list);

			foreach ($values as $value)
				$especialnesses[$attribute_name][$value] = array('extra_charge' => $extra_charge);
		}
	}

	echo number_format (microtime(true)-$start_time,3,',',' ').' - ';

	// ==========

	// Kurze �berpr�fung, weil in den aktuellen Produkten durchaus vorkommt, da� es anders ist:
	if (f_sort(array_keys($all_attribute_values)) != f_sort($attributes))
		return false;

	// Nice-to-have:
	// Die Regeln mit weniger Attributen f�hren schneller zu einem Treffer, wir m�ssen also weniger
	// Regeln durchlaufen. Daher sortieren wir die Regeln so, da� die mit wenig Attributen vorne stehen:
	array_multisort($plus_rules_sort,SORT_NUMERIC,$plus_rules,SORT_STRING);

	$metadata += compact('formula_lines','plus_rules','plus_rules_sort','all_attribute_values','original_minus_rules','especialnesses');

	echo number_format (microtime(true)-$start_time,3,',',' ').' - ';

	// ==========
	// Um weiter unten herauszufinden, welche Kombinationen durch Minus-Regeln verboten werden,
	// bauen wir uns nun alle Kombinationen aller Werte.
	// Das sind bekanntlich n1 * n2 * n... (wobei ni die Anzahl der Werte des i-ten Attributs
	// darstellt) und damit nicht sooo schrecklich viele, jedenfalls weit weniger als als
	// Zwischenprodukte entstehen w�rden, wenn man alle Permutationen aller Regeln bilden w�rde.

	// Wir beginnen das kartesische Produkt mit einem leeren Array, weil das das neutrale
	// Element der Vereinigung ist:
	$all_combinations = array(array());

	// Wir m�ssen das $order-Array umgekehrt nehmen, damit die m�glichen Kombinationen in der
	// richtigen Reihenfolge stehen. Das ist wichtig, damit die Reihenfolge der Attributwerte auch
	// dann noch stimmt, wenn f�r ein rechteres Attribut ein Wert eines linkeren Attributs verboten
	// wird:
	foreach (array_reverse($attributes) as $attribute_name)
	{
		$new_combinations = array();
		foreach ($all_attribute_values[$attribute_name] as $value => $dummy) // |
			foreach ($all_combinations as $row)                                // | kartesisches Produkt
				$new_combinations []= array($attribute_name => $value) + $row;   // |
		$all_combinations = $new_combinations;
	}

	// Als n�chstes pr�fen wir jede dieser Kombinationen darauf, ob sie durch eine "+"-Regel legitimiert wird.

	// Eine Regel trifft auf eine Kombination zu, wenn alle Regelattribute mit der Kombination �bereinstimmen.
	// Eine Kombination ist legitimiert, wenn es f�r *jedes* ihrer Attribute eine Regel gibt, die zutrifft.

	$combinations = array();
	foreach ($all_combinations as $combination)            // F�r jede Kombination...
	{
		$to_check = $combination;

		foreach ($plus_rules as $rule)                       // ... durchlaufen wir alle Regeln...
		{
			$rule_applies = true;
			foreach ($rule as $attribute_name => $values)      // ... wenn eine Regel ein Attribut betrifft...
				if ($to_check[$attribute_name])                  // ... was auch bei der Kombination noch nicht legitimiert ist...
					if (!$values[$to_check[$attribute_name]])      // ... aber es einen Wert hat, den die Regel nicht legitimiert...
						$rule_applies = false;                       // ... trifft die Regel nicht zu.

			if ($rule_applies)                                 // Wenn sie zutrifft...
				foreach (array_keys($rule) as $attribute_name)   // ...entfernen wir alle Attribute aus den noch zu pr�fenden
					unset($to_check[$attribute_name]);             //    Attributen, denn sie sind damit erfolgreich legitimiert.

			if (!$to_check)                                    // Wenn alle Attribute der Kombination legitimiert sind...
				break;                                           // ...k�nnen wir die Suche vorzeitig abbrechen und brauchen
			                                                   //    uns die restlichen Regeln nicht anzuschauen.
		}

		if (!$to_check)                                      // Wenn wir f�r alle Attribute passende (legitimierende)
		                                                     // Regeln gefunden haben...
			$combinations []= $combination;                    // ... k�nnen wir die Kombination �bernehmen.
	}

	$metadata += compact('all_combinations','combinations');

	echo number_format (microtime(true)-$start_time,3,',',' ').' - ';

	if ($return_combinations)
		return $combinations;

	// ==========
	// Nun sortieren wir die Kombinationen danach, ob sie verboten oder erlaubt sind

	// Eine Regel trifft auf eine Kombination zu, wenn alle Regelattribute mit der Kombination �bereinstimmen. (genau wie oben)
	// Eine Kombination ist verboten, wenn es f�r *eines* ihrer Attribute eine Regel gibt, die zutrifft. (anders als oben)

	$forbidden_combinations = array();
	$allowed_combinations = array();

	foreach ($combinations as $combination)                                      // F�r jede Kombination...
	{
		$forbidden = false;

		foreach ($original_minus_rules as $rule)                                       // ... durchlaufen wir alle Regeln...
		{
			$rule_applies = true;
			foreach ($combination as $attribute_name => $attribute_value)               // ... wenn ein Attribut der Kombination...
				if ($rule[$attribute_name] && !$rule[$attribute_name][$attribute_value])  // ... in der Regel vorkommt, aber einen anderen Wert hat...
					$rule_applies = false;                                                  // ... trifft die Regel nicht zu.

			if ($rule_applies)                                                          // Wenn sie zutrifft...
			{
				$forbidden = true;                                                        // ... ist die Kombination verboten...
				break;                                                                    // ... und wir brauchen die restlichen Regeln nicht mehr anzuschauen.
			}
		}

		if ($forbidden)
			$forbidden_combinations []= $combination;
		else
			$allowed_combinations []= $combination;
	}

	$metadata += compact('forbidden_combinations','allowed_combinations');

	echo number_format (microtime(true)-$start_time,3,',',' ').' - ';

	// ==========
	// Nun bauen wir aus den verbotenen Kombinationen neue Minus-Regeln

	$minus_rules = array();

	foreach ($forbidden_combinations as $combination)
	{
		$rule = array();
		foreach ($combination as $attribute_name => $attribute_value)
			$rule[$attribute_name][$attribute_value] = true;
		$minus_rules []= $rule;
	}

	$metadata['minus_rules_virgin'] = $minus_rules;

	echo number_format (microtime(true)-$start_time,3,',',' ').' - ';

	// ==========
	// Anschlie�end reduzieren wir

	foreach (array_reverse($attributes) as $merge_attribute_name)
	{
		$matches = array();
		$involved_rules = array();

		foreach ($minus_rules as $a_idx => $rule_a)
		{
			if ($involved_rules[$a_idx])
				continue;

			foreach ($minus_rules as $b_idx => $rule_b)
			{
				if ($a_idx == $b_idx)
					continue;
				if ($involved_rules[$b_idx])
					continue;

				$identical = true;
				foreach ($attributes as $attribute_name)
				{
					if ($attribute_name == $merge_attribute_name)
						continue;

					foreach ($rule_a[$attribute_name] as $value => $dummy)
						if (!$rule_b[$attribute_name][$value])
							$identical = false;
					foreach ($rule_b[$attribute_name] as $value => $dummy)
						if (!$rule_a[$attribute_name][$value])
							$identical = false;
				}

				if ($identical) // Die beiden Regeln sind bis auf das merge_attribute identisch -> zusammenfassen
				{
					$matches[$a_idx] []= $b_idx; // Direkt nach einem Partner zu gruppieren vereinfacht die Zusammenf�hrung sp�ter

					$involved_rules[$a_idx] = true; // Die betroffenen Regeln werden nicht mehr weiter auf �bereinstimmungen untereinander untersucht
					$involved_rules[$b_idx] = true;
				}
			}
		}

		foreach ($matches as $a_idx => $partners)
		{
			// Bis auf das eine Attribut sind die gleich, also kann ich die erste als Vorlage f�r die neue Regel nehmen:
			$new_rule = $minus_rules[$a_idx];
			unset($minus_rules[$a_idx]);

			foreach ($partners as $partner)
			{
				foreach ($minus_rules[$partner][$merge_attribute_name] as $value => $dummy)
					$new_rule[$merge_attribute_name][$value] = true;

				unset($minus_rules[$partner]);
			}

			$minus_rules []= $new_rule;
		}

	}

	$metadata['minus_rules_reduced'] = $minus_rules;

	echo number_format (microtime(true)-$start_time,3,',',' ').' - ';

	// ==========
	// Jetzt schauen wir noch bei jeder Regel, ob wir nicht von rechts nach links Attribute weglassen k�nnen (das Kernst�ck
	// der Formelvorbereitung). Dazu schauen wir f�r jede erlaubte Kombination, ob sie von der Minus-Regel verboten w�rde.
	// Wenn eine verk�rzte Minus-Regel eine erlaubte Kombination verbieten w�rde, dann k�nnen wir nicht verk�rzen.

	foreach ($minus_rules as &$rule)
	{
		$match_counts = array();
		foreach ($allowed_combinations as $combination)
		{
			foreach ($attributes as $attribute_name)
			{
				if ($rule[$attribute_name][$combination[$attribute_name]])
					$match_counts[$attribute_name]++;
				else
					break; // Sobald ein Attribut nicht pa�t, gehen wir direkt zur n�chsten Kombination, denn dann trifft diese Regel nicht auf die Kombination zu
			}
		}

		$new_rule = array();
		foreach ($attributes as $attribute_name)
		{
			$new_rule[$attribute_name] = $rule[$attribute_name];   // Das erste Attribut, das nicht mehr pa�t, wird noch mit r�bergenommen...

			if (!$match_counts[$attribute_name])
				break;                                               // ... aber keine weiteren.
		}
		$rule = $new_rule; // Regel �berschreiben
	}
	unset($rule);

	$metadata['minus_rules'] = $minus_rules;

	echo number_format (microtime(true)-$start_time,3,',',' ').' - ';

	// ==========
	// Zum Schlu� fassen wir die Ergebnisse zu einem Array dieser Form zusammen:
	//
	// array(
	// 	'rules' => array(
	// 		array(
	// 			'operator' => '+' oder '-',
	// 			'combinations' => array(
	// 				'gauge' => array('1.6' => true, ...),
	// 				'length' => array('1.6' => true, ...),
	// 				...
	// 			),
	// 		),
	// 		...
	// 	),
	// 	'especialnesses' => array(
	// 		'gauge' => array(
	// 			'1.6' => array('extra_charge' => 0.97),
	// 			...
	// 		),
	// 		...
	// 	)
	// )

	$result = array();

	foreach ($plus_rules as $rule)
	{
		$result['rules'] []= array(
			'operator' => '+',
			'combinations' => $rule
		);
	}

	foreach ($minus_rules as $rule)
	{
		$result['rules'] []= array(
			'operator' => '-',
			'combinations' => $rule
		);
	}

	$result['especialnesses'] = $especialnesses;

	foreach ($all_attribute_values as $attribute_name => $data)
		$result['all_involved_values'][$attribute_name] = array_keys($data);

	$walltime = microtime(true)-$start_time;
	$metadata += compact('walltime');
	// var_dump($metadata);

	return $result;
}
