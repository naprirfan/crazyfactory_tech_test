<?php

define('MODULE_ORDER_TOTAL_CASHPOINTS_TITLE', 'Crazy Cash Points');
define('MODULE_ORDER_TOTAL_CASHPOINTS_DESCRIPTION', 'Crazy Cash Points auf Bestellung');

define('MODULE_ORDER_TOTAL_CASHPOINTS_STATUS_TITLE', 'Crazy Cash Points');
define('MODULE_ORDER_TOTAL_CASHPOINTS_STATUS_DESC', 'Anzeige der Cash Points?');

define('MODULE_ORDER_TOTAL_CASHPOINTS_SORT_ORDER_TITLE', 'Sortierreihenfolge');
define('MODULE_ORDER_TOTAL_CASHPOINTS_SORT_ORDER_DESC', 'Anzeigereihenfolge');