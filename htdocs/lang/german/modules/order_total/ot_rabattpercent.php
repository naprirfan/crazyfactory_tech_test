<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_loworderfee.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ot_loworderfee.php,v 1.2 2002/04/17); www.oscommerce.com
   (c) 2003	 nextcommerce (ot_loworderfee.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  define('MODULE_ORDER_TOTAL_RABATTPERCENT_ORDERTOTAL', '%s %% Rabatt ab %s');

  define('MODULE_ORDER_TOTAL_RABATTPERCENT_TITLE', 'Prozentuale Rabatte');
  define('MODULE_ORDER_TOTAL_RABATTPERCENT_DESCRIPTION', 'Prozentualer Rabatt nach Gesamtbetrag');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_STATUS_TITLE', 'Rabatte anzeigen');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_STATUS_DESC', 'M&ouml;chten Sie sich die Rabatte ansehen?');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_SORT_ORDER_TITLE', 'Sortierreihenfolge');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_SORT_ORDER_DESC', 'Anzeigereihenfolge.');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER1_TITLE', '1. Bestellungen &uuml;ber');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER1_DESC', 'Rabatte werden für Bestellungen &uuml;ber diesem Wert hinzugef&uuml;gt.');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT1_TITLE', '1. prozentualer Rabatt');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT1_DESC', 'prozentualer Rabatt für entsprechende Bestellungen');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER2_TITLE', '2. Bestellungen &uuml;ber');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER2_DESC', 'Rabatte werden für Bestellungen &uuml;ber diesem Wert hinzugef&uuml;gt.');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT2_TITLE', '2. prozentualer Rabatt');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT2_DESC', 'prozentualer Rabatt für entsprechende Bestellungen');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER3_TITLE', '3. Bestellungen &uuml;ber');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER3_DESC', 'Rabatte werden für Bestellungen &uuml;ber diesem Wert hinzugef&uuml;gt.');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT3_TITLE', '3. prozentualer Rabatt');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT3_DESC', 'prozentualer Rabatt für entsprechende Bestellungen');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER4_TITLE', '4. Bestellungen &uuml;ber');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER4_DESC', 'Rabatte werden für Bestellungen &uuml;ber diesem Wert hinzugef&uuml;gt.');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT4_TITLE', '4. prozentualer Rabatt');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT4_DESC', 'prozentualer Rabatt für entsprechende Bestellungen');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER5_TITLE', '5. Bestellungen &uuml;ber');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER5_DESC', 'Rabatte werden für Bestellungen &uuml;ber diesem Wert hinzugef&uuml;gt.');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT5_TITLE', '5. prozentualer Rabatt');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT5_DESC', 'prozentualer Rabatt für entsprechende Bestellungen');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER6_TITLE', '6. Bestellungen &uuml;ber');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER6_DESC', 'Rabatte werden für Bestellungen &uuml;ber diesem Wert hinzugef&uuml;gt.');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT6_TITLE', '6. prozentualer Rabatt');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT6_DESC', 'prozentualer Rabatt für entsprechende Bestellungen');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER7_TITLE', '7. Bestellungen &uuml;ber');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER7_DESC', 'Rabatte werden für Bestellungen &uuml;ber diesem Wert hinzugef&uuml;gt.');

	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT7_TITLE', '7. prozentualer Rabatt');
	define('MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT7_DESC', 'prozentualer Rabatt für entsprechende Bestellungen');

  define('MODULE_ORDER_TOTAL_RABATTPERCENT_DESTINATION_TITLE','Rabatte nach Zonen berechnen');
  define('MODULE_ORDER_TOTAL_RABATTPERCENT_DESTINATION_DESC','Rabatte f&uuml;r Bestellungen, die an diesen Ort versandt werden.');

//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_TITLE','Mindermengenzuschlag anzeigen');
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_DESC','M&ouml;chten Sie sich den Mindermengenzuschlag ansehen?');
//
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_SORT_ORDER_TITLE','Sortierreihenfolge');
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_SORT_ORDER_DESC','Anzeigereihenfolge.');
//
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_TITLE','Mindermengenzuschlag erlauben');
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_DESC','M&ouml;chten Sie Mindermengenzuschl&auml;ge erlauben?');
//
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_TITLE','Mindermengenzuschlag f&uuml;r Bestellungen unter');
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_DESC','Mindermengenzuschlag wird für Bestellungen unter diesem Wert hinzugef&uuml;gt.');
//
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_TITLE','Zuschlag');
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_DESC','Mindermengenzuschlag.');
//
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_TITLE','Mindestmengenzuschlag nach Zonen berechnen');
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_DESC','Mindestmengenzuschlag f&uuml;r Bestellungen, die an diesen Ort versandt werden.');
//
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_TITLE','Steuerklasse');
//  define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_DESC','Folgende Steuerklasse für den Mindermengenzuschlag verwenden.');