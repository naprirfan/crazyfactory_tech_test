<?php

/* -----------------------------------------------------------------------------------------
   $Id: moneybookers_wlt.php 40 2009-01-22 15:54:43Z mzanier $

   xt:Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2007 xt:Commerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_MONEYBOOKERS_WLT_TEXT_TITLE', 'Skrill (Moneybookers) Guthaben');
$_var = 'Moneybookers E-Wallet';
if (_PAYMENT_MONEYBOOKERS_EMAILID=='') {
	$_var.='<br /><br /><b><font color="red">Bitte nehmen Sie zuerst die Einstellungen unter<br /> Konfiguration->xt:C Partner -> Moneybookers.com vor!</font></b>';
}
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_TEXT_DESCRIPTION', $_var);
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_NOCURRENCY_ERROR', 'Es ist keine von Skrill (Moneybookers) akzeptierte W&auml;hrung installiert!');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_ERRORTEXT1', 'payment_error=');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_TEXT_EMAIL', 'Zahlen Sie direkt über Ihr Guthaben bei Skrill (Moneybookers). Hierfür ist ein Konto bei Skrill (Moneybookers) erforderlich.<br>Bitte beachten Sie, dass bei dieser Zahlungsart der Name des Bestellers mit dem Namen des Karteninhabers/Kontoinhabers übereinstimmen muss. Dies ist bei Skrill (Moneybookers) leider aus Sicherheitsgründen Pflicht, Ihre Zahlung wird sonst abgelehnt.');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_TEXT_INFO', 'Zahlen Sie direkt über Ihr Guthaben bei Skrill (Moneybookers) . Hierfür ist ein Konto bei Skrill (Moneybookers) erforderlich.<br>Bitte beachten Sie, dass bei dieser Zahlungsart der Name des Bestellers mit dem Namen des Karteninhabers/Kontoinhabers übereinstimmen muss. Dies ist bei Skrill (Moneybookers) leider aus Sicherheitsgründen Pflicht, Ihre Zahlung wird sonst abgelehnt.');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_ERRORTEXT2', '&error=Fehler w&auml;hrend Ihrer Bezahlung bei Skrill (Moneybookers)!');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_ORDER_TEXT', 'Bestelldatum: ');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_TEXT_ERROR', 'Fehler bei Zahlung!');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_CONFIRMATION_TEXT', 'Danke f&uuml;r Ihre Bestellung!');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_TRANSACTION_FAILED_TEXT', 'Ihre Zahlungstransaktion bei Skrill (Moneybookers) ist fehlgeschlagen. Bitte versuchen Sie es nochmal, oder w&auml;hlen Sie eine andere Zahlungsm&ouml;glichkeit!');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_TEXT_SUM', 'Summe: ');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_TEXT_DATE', 'Datum: ');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_TEXT_TAX', 'MwSt: ');

define('MODULE_PAYMENT_MONEYBOOKERS_WLT_STATUS_TITLE', 'Skrill (Moneybookers) aktivieren');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_STATUS_DESC', 'M&ouml;chten Sie Zahlungen per Skrill (Moneybookers) akzeptieren?');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_SORT_ORDER_TITLE', 'Anzeigereihenfolge');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_SORT_ORDER_DESC', 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_ZONE_TITLE', 'Zahlungszone');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_ZONE_DESC', 'Wenn eine Zone ausgew&auml;hlt ist, gilt die Zahlungsmethode nur f&uuml;r diese Zone.');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_ALLOWED_TITLE', 'Erlaubte Zonen');
define('MODULE_PAYMENT_MONEYBOOKERS_WLT_ALLOWED_DESC', 'Geben Sie <b>einzeln</b> die Zonen an, welche f&uuml;r dieses Modul erlaubt sein sollen. (z. B. AT, DE (wenn leer, werden alle Zonen erlaubt))');