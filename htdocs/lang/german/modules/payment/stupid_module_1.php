<?php

	// $name = substr(basename(__FILE__),0,-4);

	// Wird benutzt im Bestellprozeß
	define('MODULE_PAYMENT_STUPID_MODULE_1_TEXT_TITLE', 'Schweizer Bankverbindung');
	define('MODULE_PAYMENT_STUPID_MODULE_1_TEXT_INFO',' Überweisen Sie auf unser Konto in der Schweiz bei der UBS. Die Kontodaten werden Ihnen nach Abschluss der Bestellung angezeigt.');
	define('MODULE_PAYMENT_STUPID_MODULE_1_TEXT_CAPTION','Schweizer Bankverbindung');
 	define('MODULE_PAYMENT_STUPID_MODULE_1_TEXT_INFO_CONFIRM','Nachdem Sie alle Angaben überprüft haben, klicken Sie bitte auf "Bestellen", um die Bestellung abzuschliessen. Auf der folgenden Seite werden Sie dann die Bankdaten erhalten, damit Sie Ihre Zahlung ausführen können.');

  define('MODULE_PAYMENT_STUPID_MODULE_1_TEXT_DESCRIPTION_BANK_TO_CURRENCY','<br />Kontonummer: 0208-926860.60B<br />IBAN CH170020820892686060B');
 	define('MODULE_PAYMENT_STUPID_MODULE_1_TEXT_DESCRIPTION_BANK_TO_CURRENCY_CHF','<br />Kontonummer: 0208-926860.01Q<br />IBAN CH260020820892686001Q');
                                                    								
 	define('MODULE_PAYMENT_STUPID_MODULE_1_TEXT_DESCRIPTION_CH', '<p>Bitte verwenden Sie folgende Daten für die Überweisung des Gesamtbetrages:<br />
 <br />Crazy Factory GmbH<br />Kontonummer: 0208-926860.60B<br />Clearing-No. 208<br />IBAN CH170020820892686060B <br />  BIC UBSWCHZH80A <br />  Die Ware wird ausgeliefert, wenn der Betrag auf unserem Konto eingegangen ist.<br /></p>
');
	define('MODULE_PAYMENT_STUPID_MODULE_1_TEXT_DESCRIPTION', 'Bitte verwenden Sie folgende Daten für die Überweisung des Gesamtbetrages:<br /><br />Kontoinhaber:<br />Crazy Factory GmbH<br />Quaderstrasse 15<br />Postfach 47<br />7002 Chur<br />Schweiz %s <br />BIC/SWIFT: UBSWCHZH80A <br /><br />Name der Bank: <br />UBS AG <br />Poststrasse 1 <br />7002 Chur <br />Schweiz <br />Die Ware wird ausgeliefert, wenn der Betrag auf unserem Konto eingegangen ist.<br />');
  
  define('MODULE_PAYMENT_STUPID_MODULE_1_TRANSFER_REASON','Verwendungszweck: ');
	

	// Konfigurationsschlüssel
  define('MODULE_PAYMENT_STUPID_MODULE_1_STATUS_TITLE' , 'Modul aktivieren');
  define('MODULE_PAYMENT_STUPID_MODULE_1_STATUS_DESC' , '');
  define('MODULE_PAYMENT_STUPID_MODULE_1_SORT_ORDER_TITLE' , 'Anzeigereihenfolge');
  define('MODULE_PAYMENT_STUPID_MODULE_1_SORT_ORDER_DESC' , 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.');
  define('MODULE_PAYMENT_STUPID_MODULE_1_ZONE_TITLE' , 'Zahlungszone');
  define('MODULE_PAYMENT_STUPID_MODULE_1_ZONE_DESC' , 'Wenn eine Zone ausgew&auml;hlt ist, gilt die Zahlungsmethode nur f&uuml;r diese Zone.');
  define('MODULE_PAYMENT_STUPID_MODULE_1_CONTENTLINK_TITLE' , 'Content-Link');
  define('MODULE_PAYMENT_STUPID_MODULE_1_CONTENTLINK_DESC' , 'Link des Contents in dem die ... geladen wird.');
  define('MODULE_PAYMENT_STUPID_MODULE_1_ALLOWED_TITLE' , 'Erlaubte Zonen');
  define('MODULE_PAYMENT_STUPID_MODULE_1_ALLOWED_DESC' , 'Die Zonen angeben, die für dieses Modul erlaubt sein sollen. Wenn leer werden alle Zonen erlaubt.');