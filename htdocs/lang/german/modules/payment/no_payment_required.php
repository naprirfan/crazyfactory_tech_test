<?php

// $name = substr(basename(__FILE__),0,-4);

// Wird benutzt im Bestellprozeß
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_TEXT_TITLE', 'Bereits beglichen');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_TEXT_INFO', 'Ihre Bestellung wurde bereits durch Cash Points beglichen und muss nicht weiter bezahlt werden.');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_TEXT_CAPTION', 'Bereits beglichen');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_TEXT_INFO_CONFIRM', 'Ihre Bestellung wurde bereits durch Cash Points beglichen und muss nicht weiter bezahlt werden.');

define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_TEXT_DESCRIPTION', 'Ihre Bestellung wurde bereits durch Cash Points beglichen und muss nicht weiter bezahlt werden.');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_TRANSFER_REASON', '');

// Konfigurationsschlüssel
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_STATUS_TITLE', 'Modul aktivieren');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_STATUS_DESC', '');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_SORT_ORDER_TITLE', 'Anzeigereihenfolge');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_SORT_ORDER_DESC', 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_ZONE_TITLE', 'Zahlungszone');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_ZONE_DESC', 'Wenn eine Zone ausgew&auml;hlt ist, gilt die Zahlungsmethode nur f&uuml;r diese Zone.');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_CONTENTLINK_TITLE', 'Content-Link');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_CONTENTLINK_DESC', 'Link des Contents in dem die ... geladen wird.');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_ALLOWED_TITLE', 'Erlaubte Zonen');
define('MODULE_PAYMENT_NO_PAYMENT_REQUIRED_ALLOWED_DESC', 'Die Zonen angeben, die für dieses Modul erlaubt sein sollen. Wenn leer werden alle Zonen erlaubt.');