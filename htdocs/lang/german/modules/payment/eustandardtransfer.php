<?php
/* -----------------------------------------------------------------------------------------
   $Id: eustandardtransfer.php 998 2005-07-07 14:18:20Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ptebanktransfer.php,v 1.4.1 2003/09/25 19:57:14); www.oscommerce.com

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/


  define('MODULE_PAYMENT_EUTRANSFER_TEXT_TITLE', 'int. Banküberweisung');
  define('MODULE_PAYMENT_EUSTANDARDTRANSFER_TEXT_TITLE', 'int. Banküberweisung');
  define('MODULE_PAYMENT_EUTRANSFER_TEXT_DESCRIPTION_DE', 'Bitte verwenden Sie folgende Daten f&uuml;r die &Uuml;berweisung des Gesamtbetrages:<br />' .
                                                       '<br />Kontoname: ' . MODULE_PAYMENT_EUTRANSFER_ACCNAM .
                                                       '<br />Kontonummer: ' . MODULE_PAYMENT_EUTRANSFER_ACCNUM .
                                                       '<br />Bankleitzahl: ' . MODULE_PAYMENT_EUTRANSFER_ACCBLZ .
                                                       '<br />Name der Bank: ' . MODULE_PAYMENT_EUTRANSFER_BANKNAM .
                                                       '<br /><br />Die Ware wird ausgeliefert, wenn der Betrag auf unserem Konto eingegangen ist.<br />');

  define('MODULE_PAYMENT_EUTRANSFER_TEXT_DESCRIPTION_AT_CH', 'Bitte verwenden Sie folgende Daten f&uuml;r die &Uuml;berweisung des Gesamtbetrages:<br />' .
                                                       '<br />Kontoname: ' . MODULE_PAYMENT_EUTRANSFER_ACCNAM .
                                                       '<br />IBAN: ' . MODULE_PAYMENT_EUTRANSFER_ACCIBAN .
                                                       '<br />BIC/SWIFT: ' . MODULE_PAYMENT_EUTRANSFER_BANKBIC .
                                                       '<br />Name der Bank: ' . MODULE_PAYMENT_EUTRANSFER_BANKNAM .
                                                       '<br /><br />Adresse:' .
                                                       '<br />' . nl2br(STORE_NAME_ADDRESS) .
                                                       '<br /><br />Die Ware wird ausgeliefert, wenn der Betrag auf unserem Konto eingegangen ist.<br />');

  define('MODULE_PAYMENT_EUTRANSFER_TEXT_DESCRIPTION', 'Bitte verwenden Sie folgende Daten f&uuml;r die &Uuml;berweisung des Gesamtbetrages:<br />' .
                                                       '<br />' . nl2br(STORE_NAME_ADDRESS) .
                                                       '<br /><br /> ' . MODULE_PAYMENT_EUTRANSFER_ACCNUM .
                                                       '<br />'.MODULE_PAYMENT_EUTRANSFER_ACCBLZ.
                                                       '<br />IBAN: ' . MODULE_PAYMENT_EUTRANSFER_ACCIBAN .
                                                       '<br />BIC/SWIFT: ' . MODULE_PAYMENT_EUTRANSFER_BANKBIC .
                                                       '<br /><br />Bank Adresse:' .
                                                       '<br /><br />' . MODULE_PAYMENT_EUTRANSFER_BANKNAM .
                                                       '<br />' . MODULE_PAYMENT_EUTRANSFER_BRANCH .
                                                       '<br /><br />Die Ware wird ausgeliefert, wenn der Betrag auf unserem Konto eingegangen ist.<br />');

  define('MODULE_PAYMENT_EUTRANSFER_TEXT_INFO','&Uuml;berweisen Sie auf unser internationales Bankkonto in Deutschland. Die Kontodaten inklusive IBAN- und BIC/SWIFT-Code werden Ihnen direkt nach der Bestellung angezeigt.');
  define('MODULE_PAYMENT_EUTRANSFER_TRANSFER_REASON','Verwendungszweck: ');
  define('MODULE_PAYMENT_EUTRANSFER_STATUS_TITLE','Allow Bank Transfer Payment');
  define('MODULE_PAYMENT_EUTRANSFER_TEXT_INFO_CONFIRM','Nachdem Sie alle Angaben überprüft haben, klicken Sie bitte auf "Bestellen", um die Bestellung abzuschliessen. Auf der folgenden Seite werden Sie dann die Bankdaten erhalten, damit Sie Ihre Zahlung ausführen können.');
 
  define('MODULE_PAYMENT_EUTRANSFER_STATUS_DESC','Do you want to accept bank transfer order payments?');

  define('MODULE_PAYMENT_EUTRANSFER_BRANCH_TITLE','Branch Location');
  define('MODULE_PAYMENT_EUTRANSFER_BRANCH_DESC','The brach where you have your account.');

  define('MODULE_PAYMENT_EUTRANSFER_BANKNAM_TITLE','Bank Name');
  define('MODULE_PAYMENT_EUTRANSFER_BANKNAM_DESC','Your full bank name');

  define('MODULE_PAYMENT_EUTRANSFER_ACCNAM_TITLE','Bank Account Name');
  define('MODULE_PAYMENT_EUTRANSFER_ACCNAM_DESC','The name associated with the account.');

  define('MODULE_PAYMENT_EUTRANSFER_ACCNUM_TITLE','Bank Account No.');
  define('MODULE_PAYMENT_EUTRANSFER_ACCNUM_DESC','Your account number.');

	define('MODULE_PAYMENT_EUTRANSFER_ACCBLZ_TITLE', 'BLZ');
	define('MODULE_PAYMENT_EUTRANSFER_ACCBLZ_DESC', 'Bankleitzahl');

  define('MODULE_PAYMENT_EUTRANSFER_ACCIBAN_TITLE','Bank Account IBAN');
  define('MODULE_PAYMENT_EUTRANSFER_ACCIBAN_DESC','International account id.<br />(ask your bank if you don\'t know it)');

  define('MODULE_PAYMENT_EUTRANSFER_BANKBIC_TITLE','Bank Bic');
  define('MODULE_PAYMENT_EUTRANSFER_BANKBIC_DESC','International bank id.<br />(ask your bank if you don\'t know it)');

  define('MODULE_PAYMENT_EUTRANSFER_SORT_ORDER_TITLE','Module Sort order of display.');
  define('MODULE_PAYMENT_EUTRANSFER_SORT_ORDER_DESC','Sort order of display. Lowest is displayed first.');

  define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ALLOWED_TITLE' , 'Erlaubte Zonen');
 define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ALLOWED_DESC' , 'Geben Sie <b>einzeln</b> die Zonen an, welche f&uuml;r dieses Modul erlaubt sein sollen. (z.B. AT,DE (wenn leer, werden alle Zonen erlaubt))');