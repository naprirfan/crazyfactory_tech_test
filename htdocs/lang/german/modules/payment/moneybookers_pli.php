<?php

/* -----------------------------------------------------------------------------------------
   $Id: moneybookers_cc.php 36 2009-01-22 12:31:32Z mzanier $

   xt:Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2007 xt:Commerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_MONEYBOOKERS_PLI_TEXT_TITLE', 'Poli &uuml;ber Skrill (Moneybookers)');
$_var = 'Poli &uuml;ber Moneybookers';
if (_PAYMENT_MONEYBOOKERS_EMAILID=='') {
	$_var.='<br /><br /><b><font color="red">Bitte nehmen Sie zuerst die Einstellungen unter<br /> Konfiguration->xt:C Partner -> Moneybookers.com vor!</font></b>';
}
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_TEXT_DESCRIPTION', $_var);
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_TEXT_EMAIL', 'Aus Sicherheitsgr&uuml;nden werden Ihre Kreditkartendaten grunds&auml;tzlich nicht auf unseren Systemen gespeichert. Die Abwicklung Ihrer Zahlung erfolgt direkt durch Skrill (Moneybookers), Ihre Kreditkarte wurde daher unmittelbar mit Ihrer endg&uuml;ltigen Best&auml;tigung der Bestellung belastet.');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_NOCURRENCY_ERROR', 'Es ist keine von Skrill (Moneybookers) akzeptierte Währung installiert!');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_ERRORTEXT1', 'payment_error=');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_TEXT_INFO', 'Mit Poli zahlen Sie Ihre Eink&auml;ufe im Internet einfach, schnell und sicher - per Online-&Uuml;berweisung. <br><font color="red">Achtung: IE 6 oder h&ouml;her ist Vorrausetzung für die Zahlung mit Poli!</font>');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_ERRORTEXT2', '&error=Fehler w&auml;hrend Ihrer Bezahlung bei Skrill (Moneybookers)!');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_ORDER_TEXT', 'Bestelldatum: ');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_TEXT_ERROR', 'Fehler bei Zahlung!');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_CONFIRMATION_TEXT', 'Danke f&uuml;r Ihre Bestellung!');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_TRANSACTION_FAILED_TEXT', 'Ihre Zahlungstransaktion bei Skrill (Moneybookers) ist fehlgeschlagen. Bitte versuchen Sie es nochmal, oder w&auml;hlen Sie eine andere Zahlungsm&ouml;glichkeit!');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_TEXT_SUM', 'Summe: ');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_TEXT_DATE', 'Datum: ');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_TEXT_TAX', 'MwSt: ');


define('MODULE_PAYMENT_MONEYBOOKERS_PLI_STATUS_TITLE', 'Skrill (Moneybookers) aktivieren');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_STATUS_DESC', 'M&ouml;chten Sie Zahlungen per Skrill (Moneybookers) akzeptieren?');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_SORT_ORDER_TITLE', 'Anzeigereihenfolge');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_SORT_ORDER_DESC', 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_ZONE_TITLE', 'Zahlungszone');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_ZONE_DESC', 'Wenn eine Zone ausgew&auml;hlt ist, gilt die Zahlungsmethode nur f&uuml;r diese Zone.');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_ALLOWED_TITLE', 'Erlaubte Zonen');
define('MODULE_PAYMENT_MONEYBOOKERS_PLI_ALLOWED_DESC', 'Geben Sie <b>einzeln</b> die Zonen an, welche f&uuml;r dieses Modul erlaubt sein sollen. (z. B. AT, DE (wenn leer, werden alle Zonen erlaubt))');