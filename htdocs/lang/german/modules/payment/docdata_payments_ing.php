<?php

/* -----------------------------------------------------------------------------------------
   $Id: Docdata Payments_BANKTRANSFER.php 36 2009-01-22 12:31:32Z mzanier $

   xt:Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2007 xt:Commerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_TEXT_TITLE', 'ING Home Bank &uuml;ber Docdata Payments');
$_var = 'ING Home Bank  &uuml;ber Docdata Payments';
if (_PAYMENT_DOCDATA_PAYMENTS_EMAILID=='') {
	$_var.='<br /><br /><b><font color="red">Bitte nehmen Sie zuerst die Einstellungen unter<br /> Konfiguration->xt:C Partner -> Docdata Payments vor!</font></b>';
}
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_TEXT_DESCRIPTION', $_var);
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_TEXT_EMAIL', 'Mit ING Home Bank zahlen Sie Ihre Einkäufe im Internet einfach, schnell und sicher. Ihre Zahlung wird sofort in unserem System verbucht.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_NOCURRENCY_ERROR', 'Es ist keine von Docdata Payments akzeptierte Währung installiert!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_ERRORTEXT1', 'payment_error=');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_TEXT_INFO', 'Mit ING Home Bank zahlen Sie Ihre Einkäufe im Internet einfach, schnell und sicher. Ihre Zahlung wird sofort in unserem System verbucht.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_ERRORTEXT2', '&error=Fehler w&auml;hrend Ihrer Bezahlung bei Docdata Payments!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_ORDER_TEXT', 'Bestelldatum: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_TEXT_ERROR', 'Fehler bei Zahlung!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_TEXT_SUM', 'Summe: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_TEXT_DATE', 'Datum: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_TEXT_TAX', 'MwSt: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_CONFIRMATION_TEXT', 'Danke f&uuml;r Ihre Bestellung!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_TRANSACTION_FAILED_TEXT', 'Ihre Zahlungstransaktion bei Docdata Payments.com ist fehlgeschlagen. Bitte versuchen Sie es nochmal, oder w&auml;hlen Sie eine andere Zahlungsm&ouml;glichkeit!');

define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_STATUS_TITLE', 'Docdata Payments aktivieren');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_STATUS_DESC', 'M&ouml;chten Sie Zahlungen per Docdata Payments akzeptieren?');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_SORT_ORDER_TITLE', 'Anzeigereihenfolge');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_SORT_ORDER_DESC', 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_ZONE_TITLE', 'Zahlungszone');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_ZONE_DESC', 'Wenn eine Zone ausgew&auml;hlt ist, gilt die Zahlungsmethode nur f&uuml;r diese Zone.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_ALLOWED_TITLE', 'Erlaubte Zonen');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ING_ALLOWED_DESC', 'Geben Sie <b>einzeln</b> die Zonen an, welche f&uuml;r dieses Modul erlaubt sein sollen. (z. B. AT, DE (wenn leer, werden alle Zonen erlaubt))');