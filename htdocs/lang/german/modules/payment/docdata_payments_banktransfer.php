<?php

/* -----------------------------------------------------------------------------------------
   $Id: Docdata Payments_BANKTRANSFER.php 36 2009-01-22 12:31:32Z mzanier $

   xt:Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2007 xt:Commerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_TEXT_TITLE', 'Banktransfer über Docdata Payments');
$_var = 'Banktransfer über Docdata Payments';
if (_PAYMENT_DOCDATA_PAYMENTS_EMAILID=='') {
	$_var.='<br /><br /><b><font color="red">Bitte nehmen Sie zuerst die Einstellungen unter<br /> Konfiguration->xt:C Partner -> Docdata Payments vor!</font></b>';
}
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_TEXT_DESCRIPTION', $_var);
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_TEXT_EMAIL', 'Überweisen Sie auf ein Bankkonto unseres Partners Docdata in Ihrem Land. Es dauert ca. 2-3 Werktage, bis Ihre Zahlung bei uns verbucht wird. Die Bankdaten werden Ihnen nach Abschluss Ihrer Bestellung angezeigt.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_NOCURRENCY_ERROR', 'Es ist keine von Docdata Payments akzeptierte Währung installiert!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_ERRORTEXT1', 'payment_error=');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_TEXT_INFO', 'Überweisen Sie auf ein Bankkonto unseres Partners Docdata in Ihrem Land. Es dauert ca. 2-3 Werktage, bis Ihre Zahlung bei uns verbucht wird. Die Bankdaten werden Ihnen nach Abschluss Ihrer Bestellung angezeigt.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_ERRORTEXT2', '&error=Fehler während Ihrer Bezahlung bei Docdata Payments!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_ORDER_TEXT', 'Bestelldatum: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_TEXT_ERROR', 'Fehler bei Zahlung!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_TEXT_SUM', 'Summe: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_TEXT_DATE', 'Datum: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_TEXT_TAX', 'MwSt: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_CONFIRMATION_TEXT', 'Danke für Ihre Bestellung!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_TRANSACTION_FAILED_TEXT', 'Ihre Zahlungstransaktion bei Docdata Payments.com ist fehlgeschlagen. Bitte versuchen Sie es nochmal, oder wählen Sie eine andere Zahlungsmöglichkeit!');

define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_STATUS_TITLE', 'Docdata Payments aktivieren');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_STATUS_DESC', 'Möchten Sie Zahlungen per Docdata Payments akzeptieren?');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_SORT_ORDER_TITLE', 'Anzeigereihenfolge');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_SORT_ORDER_DESC', 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_ZONE_TITLE', 'Zahlungszone');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_ZONE_DESC', 'Wenn eine Zone ausgewählt ist, gilt die Zahlungsmethode nur für diese Zone.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_ALLOWED_TITLE', 'Erlaubte Zonen');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_BANKTRANSFER_ALLOWED_DESC', 'Geben Sie <b>einzeln</b> die Zonen an, welche für dieses Modul erlaubt sein sollen. (z. B. AT, DE (wenn leer, werden alle Zonen erlaubt))');