<?php

	// $name = substr(basename(__FILE__),0,-4);

	// Wird benutzt im Bestellprozeß
	define('MODULE_PAYMENT_STUPID_MODULE_2_TEXT_TITLE', 'Banküberweisung Deutschland');
	define('MODULE_PAYMENT_STUPID_MODULE_2_TEXT_INFO','Überweisen Sie auf unser deutsches Konto bei der UBS Deutschland AG. Die Kontodaten inklusive Kontonummer und Bankleitzahl werden Ihnen nach Abschluss der Bestellung angezeigt.');
	define('MODULE_PAYMENT_STUPID_MODULE_2_TEXT_CAPTION','Banküberweisung Deutschland');
 	define('MODULE_PAYMENT_STUPID_MODULE_2_TEXT_INFO_CONFIRM','Nachdem Sie alle Angaben überprüft haben, klicken Sie bitte auf "Bestellen", um die Bestellung abzuschliessen. Auf der folgenden Seite werden Sie dann die Bankdaten erhalten, damit Sie Ihre Zahlung ausführen können.');

  define('MODULE_PAYMENT_STUPID_MODULE_2_TEXT_DESCRIPTION', 'Bitte verwenden Sie folgende Daten f&uuml;r die &Uuml;berweisung des Gesamtbetrages:<br /><br />Zahlungsempfänger: Crazy Factory GmbH<br />Kontonummer: 2541251017<br />Bankleitzahl:  50130600<br />Bankinstitut: UBS Germany<br /><br />Sobald wir Ihre Zahlung erhalten haben, werden wir die Ware ausliefern und Sie über den aktuellen Status Ihrer Bestellung per E-Mail informieren.<br />');    
  define('MODULE_PAYMENT_STUPID_MODULE_2_TRANSFER_REASON','Verwendungszweck: ');
	
	// Konfigurationsschlüssel
  define('MODULE_PAYMENT_STUPID_MODULE_2_STATUS_TITLE' , 'Modul aktivieren');
  define('MODULE_PAYMENT_STUPID_MODULE_2_STATUS_DESC' , '');
  define('MODULE_PAYMENT_STUPID_MODULE_2_SORT_ORDER_TITLE' , 'Anzeigereihenfolge');
  define('MODULE_PAYMENT_STUPID_MODULE_2_SORT_ORDER_DESC' , 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.');
  define('MODULE_PAYMENT_STUPID_MODULE_2_ZONE_TITLE' , 'Zahlungszone');
  define('MODULE_PAYMENT_STUPID_MODULE_2_ZONE_DESC' , 'Wenn eine Zone ausgew&auml;hlt ist, gilt die Zahlungsmethode nur f&uuml;r diese Zone.');
  define('MODULE_PAYMENT_STUPID_MODULE_2_CONTENTLINK_TITLE' , 'Content-Link');
  define('MODULE_PAYMENT_STUPID_MODULE_2_CONTENTLINK_DESC' , 'Link des Contents in dem die ... geladen wird.');
  define('MODULE_PAYMENT_STUPID_MODULE_2_ALLOWED_TITLE' , 'Erlaubte Zonen');
  define('MODULE_PAYMENT_STUPID_MODULE_2_ALLOWED_DESC' , 'Die Zonen angeben, die für dieses Modul erlaubt sein sollen. Wenn leer werden alle Zonen erlaubt.');