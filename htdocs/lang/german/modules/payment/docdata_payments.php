<?php

	// Wird benutzt im Bestellprozeß
	define('MODULE_PAYMENT_DOCDATA_PAYMENTS_TEXT_TITLE', 'Docdata Payments');
	define('MODULE_PAYMENT_DOCDATA_PAYMENTS_TEXT_INFO','Alle Zahlungen werden über docdata Payments abgewickelt. Dort können Sie unter folgenden Zahlungsarten wählen:');
	define('MODULE_PAYMENT_DOCDATA_PAYMENTS_TEXT_BANK_TRANSFER', 'Überweisung');
	define('MODULE_PAYMENT_DOCDATA_PAYMENTS_TEXT_DIRECT_DEBIT', 'Direct Debit (online)');
	define('MODULE_PAYMENT_DOCDATA_PAYMENTS_TEXT_DIRECT_DEBIT_OFFLINE', 'Direct Debit (post/fax)');
	
	// Wird benutzt im Admin
	define('MODULE_PAYMENT_DOCDATA_PAYMENTS_TEXT_DESCRIPTION', 'Zahlungsmodul Docdata Payments');

	// Konfigurationsschlüssel
	define('MODULE_PAYMENT_DOCDATA_PAYMENTS_STATUS_TITLE' , 'Modul aktivieren');
  define('MODULE_PAYMENT_DOCDATA_PAYMENTS_STATUS_DESC' , '');
  define('MODULE_PAYMENT_DOCDATA_PAYMENTS_SORT_ORDER_TITLE' , 'Anzeigereihenfolge');
  define('MODULE_PAYMENT_DOCDATA_PAYMENTS_SORT_ORDER_DESC' , 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezei');
  define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CONTENTLINK_TITLE' , 'Content-Link');
  define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CONTENTLINK_DESC' , 'Link des Contents in dem die media/content/simplepaypal.php geladen wird.');
  define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ZONE_TITLE' , 'Zahlungszone');
  define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ZONE_DESC' , 'Wenn eine Zone ausgew&auml;hlt ist, gilt die Zahlungsmethode nur f&uuml;r diese Zone.');
  define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ALLOWED_TITLE' , 'Erlaubte Zonen');
  define('MODULE_PAYMENT_DOCDATA_PAYMENTS_ALLOWED_DESC' , 'Die Zonen angeben, die für dieses Modul erlaubt sein sollen. Wenn leer werden alle Zonen erlaubt.');