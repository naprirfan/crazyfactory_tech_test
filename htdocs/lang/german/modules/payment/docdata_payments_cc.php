<?php

/* -----------------------------------------------------------------------------------------
   $Id: Docdata Payments_cc.php 36 2009-01-22 12:31:32Z mzanier $

   xt:Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2007 xt:Commerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_TEXT_TITLE', 'Kreditkarte &uuml;ber Docdata Payments');
$_var = 'Kreditkarte &uuml;ber Docdata Payments';
if (_PAYMENT_DOCDATA_PAYMENTS_EMAILID=='') {
	$_var.='<br /><br /><b><font color="red">Bitte nehmen Sie zuerst die Einstellungen unter<br /> Konfiguration->xt:C Partner -> Docdata Payments vor!</font></b>';
}
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_TEXT_DESCRIPTION', $_var);
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_TEXT_EMAIL', 'Zahlen Sie schnell, kostenlos und sicher mit Ihrer Kreditkarte. Die Abwicklung erfolgt über unseren Partner Docdata, die Daten Ihrer Karte werden nicht auf unseren Systemen gespeichert.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_NOCURRENCY_ERROR', 'Es ist keine von Docdata Payments akzeptierte Währung installiert!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_ERRORTEXT1', 'payment_error=');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_TEXT_INFO', 'Zahlen Sie schnell, kostenlos und sicher mit Ihrer Kreditkarte. Die Abwicklung erfolgt über unseren Partner Docdata, die Daten Ihrer Karte werden nicht auf unseren Systemen gespeichert.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_ERRORTEXT2', '&error=Fehler w&auml;hrend Ihrer Bezahlung bei Docdata Payments!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_ORDER_TEXT', 'Bestelldatum: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_TEXT_ERROR', 'Fehler bei Zahlung!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_TEXT_SUM', 'Summe: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_TEXT_DATE', 'Datum: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_TEXT_TAX', 'MwSt: ');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_CONFIRMATION_TEXT', 'Danke f&uuml;r Ihre Bestellung!');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_TRANSACTION_FAILED_TEXT', 'Ihre Zahlungstransaktion bei Docdata Payments.com ist fehlgeschlagen. Bitte versuchen Sie es nochmal, oder w&auml;hlen Sie eine andere Zahlungsm&ouml;glichkeit!');


define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_STATUS_TITLE', 'Docdata Payments aktivieren');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_STATUS_DESC', 'M&ouml;chten Sie Zahlungen per Docdata Payments akzeptieren?');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_SORT_ORDER_TITLE', 'Anzeigereihenfolge');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_SORT_ORDER_DESC', 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_ZONE_TITLE', 'Zahlungszone');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_ZONE_DESC', 'Wenn eine Zone ausgew&auml;hlt ist, gilt die Zahlungsmethode nur f&uuml;r diese Zone.');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_ALLOWED_TITLE', 'Erlaubte Zonen');
define('MODULE_PAYMENT_DOCDATA_PAYMENTS_CC_ALLOWED_DESC', 'Geben Sie <b>einzeln</b> die Zonen an, welche f&uuml;r dieses Modul erlaubt sein sollen. (z. B. AT, DE (wenn leer, werden alle Zonen erlaubt))');
?>