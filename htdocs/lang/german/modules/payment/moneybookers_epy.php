<?php

/* -----------------------------------------------------------------------------------------
   $Id: moneybookers_epy.php 2009-09-06 Bernd Blazynski $

   xt:Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2007 xt:Commerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_MONEYBOOKERS_EPY_TEXT_TITLE', 'ePay &uuml;ber Skrill (Moneybookers)');
$_var = 'ePay &uuml;ber Moneybookers';
if (_PAYMENT_MONEYBOOKERS_EMAILID=='') {
	$_var.='<br /><br /><b><font color="red">Bitte nehmen Sie zuerst die Einstellungen unter<br /> Konfiguration->xt:C Partner -> Moneybookers.com vor!</font></b>';
}
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_TEXT_DESCRIPTION', $_var);
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_TEXT_EMAIL', 'Aus Sicherheitsgr&uuml;nden werden Ihre Kartendaten grunds&auml;tzlich nicht auf unseren Systemen gespeichert. Die Abwicklung Ihrer Zahlung erfolgt direkt durch Skrill (Moneybookers), Ihre Karte wurde daher unmittelbar mit Ihrer endg&uuml;ltigen Best&auml;tigung der Bestellung belastet.');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_NOCURRENCY_ERROR', 'Es ist keine von Skrill (Moneybookers) akzeptierte W&auml;hrung installiert!');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_ERRORTEXT1', 'payment_error=');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_TEXT_INFO', 'Aus Sicherheitsgr&uuml;nden werden Ihre Kartendaten grunds&auml;tzlich nicht auf unseren Systemen gespeichert. Die Abwicklung Ihrer Zahlung erfolgt direkt durch Skrill (Moneybookers), Ihre Karte wird daher unmittelbar mit Ihrer endg&uuml;ltigen Best&auml;tigung der Bestellung belastet.');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_ERRORTEXT2', '&error=Fehler w&auml;hrend Ihrer Bezahlung bei Skrill (Moneybookers)!');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_ORDER_TEXT', 'Bestelldatum: ');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_TEXT_ERROR', 'Fehler bei Zahlung!');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_CONFIRMATION_TEXT', 'Danke f&uuml;r Ihre Bestellung!');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_TRANSACTION_FAILED_TEXT', 'Ihre Zahlungstransaktion bei Skrill (Moneybookers) ist fehlgeschlagen. Bitte versuchen Sie es nochmal, oder w&auml;hlen Sie eine andere Zahlungsm&ouml;glichkeit!');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_TEXT_SUM', 'Summe: ');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_TEXT_DATE', 'Datum: ');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_TEXT_TAX', 'MwSt: ');

define('MODULE_PAYMENT_MONEYBOOKERS_EPY_STATUS_TITLE', 'Skrill (Moneybookers) aktivieren');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_STATUS_DESC', 'M&ouml;chten Sie Zahlungen per Skrill (Moneybookers) akzeptieren?');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_SORT_ORDER_TITLE', 'Anzeigereihenfolge');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_SORT_ORDER_DESC', 'Reihenfolge der Anzeige. Kleinste Ziffer wird zuerst angezeigt.');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_ZONE_TITLE', 'Zahlungszone');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_ZONE_DESC', 'Wenn eine Zone ausgew&auml;hlt ist, gilt die Zahlungsmethode nur f&uuml;r diese Zone.');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_ALLOWED_TITLE', 'Erlaubte Zonen');
define('MODULE_PAYMENT_MONEYBOOKERS_EPY_ALLOWED_DESC', 'Geben Sie <b>einzeln</b> die Zonen an, welche f&uuml;r dieses Modul erlaubt sein sollen. (z. B. AT, DE (wenn leer, werden alle Zonen erlaubt))');