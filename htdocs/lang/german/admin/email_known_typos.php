<?php
/* --------------------------------------------------------------
  Id: email_known_typos.php 1167 2010-02-04 16:27:00Z mz

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommercecoding standards www.oscommerce.com
   (c) 2003	 nextcommerce (accounting.php,v 1.27 2003/08/24); www.nextcommerce.org

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'eMail-Check');
define('TABLE_HEADING_TYPO', 'Schreibfehler');
define('TABLE_HEADING_STATUS', 'Aktiv?');
define('TABLE_HEADING_ACTION', 'Aktion');
define('TEXT_INFO_CHECK_STRING', 'Schreibfehler:');
define('TEXT_INFO_ACTIVE', 'Aktiv:');
define('TEXT_YES', 'Ja');
define('TEXT_NO', 'Nein');
define('TEXT_INFO_HEADING_NEW_TYPO', 'Neuer Schreibfehler');
define('TEXT_INFO_INSERT_INTRO', '');
define('TEXT_INFO_HEADING_EDIT_TYPO', 'Schreibfehler bearbeiten');
define('TEXT_INFO_EDIT_INTRO', 'Bitte f&uuml;hren Sie alle notwendigen &Auml;nderungen durch');
define('TEXT_INFO_HEADING_DELETE_TYPO', 'Schreibfehler l&ouml;schen');
define('TEXT_INFO_DELETE_INTRO', 'Sind Sie sicher, dass Sie diesen Schreibfehler l&ouml;schen m&ouml;chten?');