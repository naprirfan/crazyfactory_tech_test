<?php
define('RMA_HEADING_PACKET', 'RMA-Pakete');
define('FINISH_PACKET', 'Paket abschließen');
define('RMA_IN_PACKET', 'Anzahl RMA im aktuellen Paket: ');
define('PACKET_ID', 'Packetnummer');
define('CREATED_BY', 'Erstellt von');
define('PACKET_AMOUNT', 'Anzahl RMA');
define('CREATED_DATETIME', 'Erstellt am');
define('FINISHED_DATETIME', 'Abgeschlossen am');
define('EXPORT', 'CSV-Export');
define('NONEMPTY_BAG', 'Die aktuelle Tüte enthält Rücksendungen. Paket trotzdem abschließen?');