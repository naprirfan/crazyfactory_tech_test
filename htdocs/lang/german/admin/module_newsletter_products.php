<?php
/* --------------------------------------------------------------
   $Id: module_newsletter_products.php 1142 2007-09-22 16:19:55Z marco krueger

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommercecoding standards www.oscommerce.com 
   (c) 2003 nextcommerce (templates_boxes.php,v 1.14 2003/08/18); www.nextcommerce.org
   (c) 2007 accessoires by sergej stroh

   Released under the GNU General Public License 
   
   Copyright (c) 2007 Marco Krueger
   www.xt-module.de
   --------------------------------------------------------------*/
 
	define('HEADING_TITLE','Artikellisten Manager "Modul: erweiterter Newsletter v2.0"');
	define('CONTENT_NOTE','<p>Mit diesem Modul erstellen Sie Artikellisten, die Sie in einen erweiterten Newsletter einf&uuml;gen k&ouml;nnen.<br>Sie k&ouml;nnen jeder Artikelliste beliebig viele Artikel zuordnen und bereits erstelle Artikellisten im Newsletter einf&uuml;gen</p>');
	
	define('STEP_1','Artikelliste anlegen!');
	define('STEP_1_HELP','<i>Geben Sie eine beliebige Bezeichnung f&uuml;r die Artikelliste ein.</i>');
	define('STEP_2','Artikel suchen!');
	define('STEP_2_HELP','<i>Klicken Sie auf den Button "Suchen", um der Artikelliste Produkte zuzuordnen.</i>');
	define('STEP_2a','Artikel festegen!');
	define('STEP_3_HELP','<i>Folgende Eingaben sind m&ouml;glich:<br> - Artikelname<br> - Artikelnummer</i>');
	define('SEARCH','Suchen');
	define('INPUT_PRODUCT','Hinzufügen');
	
	define('NAME','Artikelliste');
	define('ACCESSORIES','Artikel');
	define('ACTION','Aktion');
	
	define('ACTION_EDIT','Bearbeiten');
	define('ACTION_DEL','Löschen');
	
	define('DOW','y xt-mod');
	define('ED','de');
	
	define('LIST_OVERVIEW','Übersicht');
	define('LIST_NEW','Neue Artikelliste anlegen');
	
	define('INPUT_DEL_ACCPRODUCT','Markierte Artikel löschen');

?>