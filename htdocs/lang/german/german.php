<?php
/* -----------------------------------------------------------------------------------------
   $Id: german.php 1308 2005-10-15 14:22:18Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(german.php,v 1.119 2003/05/19); www.oscommerce.com
   (c) 2003  nextcommerce (german.php,v 1.25 2003/08/25); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

/*
 *
 *  ZEIT / DATUM
 *
 */

define('TITLE', STORE_NAME);
define('HEADER_TITLE_TOP', 'Startseite');
define('HEADER_TITLE_CATALOG', 'Katalog');

define('HTML_PARAMS','dir="ltr" lang="de"');

@setlocale(LC_TIME, 'de_DE@euro', 'de_DE', 'de-DE', 'de', 'ge', 'de_DE.UTF-8', 'German','de_DE.UTF-8');

define('DATE_FORMAT_SHORT', '%d.%m.%Y');  // this is used for strftime()
define('DATE_FORMAT_LONG', '%A, %d. %B %Y'); // this is used for strftime()
define('DATE_FORMAT', 'd.m.Y');  // this is used for strftime()
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');
define('DOB_FORMAT_STRING', 'tt.mm.jjjj');

function xtc_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 0, 2) . substr($date, 3, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 3, 2) . substr($date, 0, 2);
  }
}

// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'EUR');

define('MALE', 'Herr');
define('FEMALE', 'Frau');

/*
 *
 *  BOX TEXT
 *
 */

// text for gift voucher redeeming
define('IMAGE_REDEEM_GIFT','Gutschein einlösen!');

define('BOX_TITLE_STATISTICS','Statistik:');
define('BOX_ENTRY_CUSTOMERS','Kunden');
define('BOX_ENTRY_PRODUCTS','Artikel');
define('BOX_ENTRY_REVIEWS','Bewertungen');
define('TEXT_VALIDATING','Nicht bestätigt');

// manufacturer box text
define('BOX_MANUFACTURER_INFO_HOMEPAGE', '%s Homepage');
define('BOX_MANUFACTURER_INFO_OTHER_PRODUCTS', 'Mehr Artikel');

define('BOX_HEADING_ADD_PRODUCT_ID','In den Korb legen');

define('BOX_LOGINBOX_STATUS','Kundengruppe:');
define('BOX_LOGINBOX_DISCOUNT','Artikelrabatt');
define('BOX_LOGINBOX_DISCOUNT_TEXT','Rabatt');
define('BOX_LOGINBOX_DISCOUNT_OT','');

// reviews box text in includes/boxes/reviews.php
define('BOX_REVIEWS_WRITE_REVIEW', 'Bewerten Sie diesen Artikel!');
define('BOX_REVIEWS_TEXT_OF_5_STARS', '%s von 5 Sternen!');

// pull down default text
define('PULL_DOWN_DEFAULT', 'Bitte wählen');

// javascript messages
define('JS_ERROR', 'Notwendige Angaben fehlen! Bitte richtig ausfüllen.\n\n');

define('JS_REVIEW_TEXT', '* Der Text muss aus mindestens ' . REVIEW_TEXT_MIN_LENGTH . ' Buchstaben bestehen.\n\n');
define('JS_REVIEW_RATING', '* Geben Sie Ihre Bewertung ein.\n\n');
define('JS_ERROR_NO_PAYMENT_MODULE_SELECTED', '* Bitte wählen Sie eine Zahlungsweise für Ihre Bestellung.\n');
define('JS_ERROR_SUBMITTED', 'Diese Seite wurde bereits bestätigt. Klicken Sie bitte OK und warten bis der Prozess durchgeführt wurde.');
define('ERROR_NO_PAYMENT_MODULE_SELECTED', '* Bitte wählen Sie eine Zahlungsweise für Ihre Bestellung.');

/*
 *
 * ACCOUNT FORMS
 *
 */

define('ENTRY_COMPANY_ERROR', '');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_GENDER_ERROR', 'Bitte wählen Sie Ihre Anrede aus.');
define('ENTRY_GENDER_TEXT', '*');
define('ENTRY_FIRST_NAME_ERROR', 'Ihr Vorname muss aus mindestens ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME_ERROR', 'Ihr Nachname muss aus mindestens ' . ENTRY_LAST_NAME_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Ihr Geburtsdatum muss im Format TT.MM.JJJJ (zB. 21.05.1970) eingeben werden');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (zB. 21.05.1970)');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Ihre E-Mail-Adresse muss aus mindestens ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Ihre eingegebene E-Mail-Adresse ist fehlerhaft - bitte überprüfen Sie diese.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Ihre eingegebene E-Mail-Adresse existiert bereits - bitte überprüfen Sie diese.');
define('ENTRY_EMAIL2_ADDRESS_ERROR', 'Ihre E-Mail-Wiederholung muss aus mindestens ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_EMAIL2_ADDRESS_CHECK_ERROR', 'Ihre eingegebene E-Mail-Wiederholung ist fehlerhaft - bitte überprüfen Sie diese.');
define('ENTRY_EMAIL2_ADDRESS_ERROR_EXISTS', 'Ihre eingegebene E-Mail-Wiederholung existiert bereits - bitte überprüfen Sie diese.');
define('ENTRY_EMAIL_ADDRESS_DIFFERENT', 'Die E-Mail-Adresse muss identisch mit der Wiederholung der E-Mail-Adresse sein');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_STREET_ADDRESS_ERROR', 'Strasse/Nr muss aus mindestens ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_STREET_ADDRESS_TEXT', '*');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE_ERROR', 'Ihre Postleitzahl muss aus mindestens ' . ENTRY_POSTCODE_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY_ERROR', 'Ort muss aus mindestens ' . ENTRY_CITY_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE_ERROR', 'Ihr Bundesland muss aus mindestens ' . ENTRY_STATE_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_STATE_ERROR_SELECT', 'Bitte wählen Sie ihr Bundesland aus der Liste aus.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY_ERROR', 'Bitte wählen Sie ihr Land aus der Liste aus.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Ihre Telefonnummer muss aus mindestens ' . ENTRY_TELEPHONE_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_PASSWORD_ERROR', 'Ihr Passwort muss aus mindestens ' . ENTRY_PASSWORD_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'Ihre Passwörter stimmen nicht überein.');
define('ENTRY_PASSWORD_TEXT', '*');
define('ENTRY_PASSWORD_CONFIRMATION_TEXT', '*');
define('ENTRY_PASSWORD_CURRENT_TEXT', '*');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Ihr Passwort muss aus mindestens ' . ENTRY_PASSWORD_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_PASSWORD_NEW_TEXT', '*');
define('ENTRY_PASSWORD_NEW_ERROR', 'Ihr neues Passwort muss aus mindestens ' . ENTRY_PASSWORD_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Ihre Passwörter stimmen nicht überein.');
define('ERROR_DATENSG_NOT_ACCEPTED', 'Sofern Sie die Kenntnisnahme unserer Informationen zu den Datenschutzbestimmung nicht bestätigen, können wir Ihren Account nicht einrichten!');
define('TEXT_REALLY_YOUR_COUNTRY', 'Ist das wirklich Ihr Heimatland?');

/*
 *
 *  RESTULTPAGES
 *
 */

define('TEXT_RESULT_PAGE', 'Seiten:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Zeige %s pro Seite');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Zeige <b>%d</b> bis <b>%d</b> (von insgesamt <b>%d</b> Bestellungen)');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'Zeige <b>%d</b> bis <b>%d</b> (von insgesamt <b>%d</b> Bewertungen)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW', 'Zeige <b>%d</b> bis <b>%d</b> (von insgesamt <b>%d</b> neuen Artikeln)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Zeige <b>%d</b> bis <b>%d</b> (von insgesamt <b>%d</b> Angeboten)');
define('TEXT_DISPLAY_ALL', 'Alle');

/*
 *
 * SITE NAVIGATION
 *
 */

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'vorherige Seite');
define('PREVNEXT_TITLE_NEXT_PAGE', 'nächste Seite');
define('PREVNEXT_TITLE_PAGE_NO', 'Seite %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Vorhergehende %d Seiten');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Nächste %d Seiten');

/*
 *
 * PRODUCT NAVIGATION
 *
 */

define('PREVNEXT_BUTTON_PREV', '[&lt;&lt;&nbsp;vorherige]');
define('PREVNEXT_BUTTON_NEXT', '[nächste&nbsp;&gt;&gt;]');

/*
 *
 * IMAGE BUTTONS
 *
 */

define('IMAGE_BUTTON_ADD_ADDRESS', 'Neue Adresse');
define('IMAGE_BUTTON_BACK', 'Zurück');
define('IMAGE_BUTTON_CHANGE_ADDRESS', 'Adresse ändern');
define('IMAGE_BUTTON_CHECKOUT', 'Zur Kasse');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Bestellung bestätigen');
define('IMAGE_BUTTON_CONTINUE', 'Weiter');
define('IMAGE_BUTTON_DELETE', 'Löschen');
define('IMAGE_BUTTON_LOGIN', 'Anmelden');
define('IMAGE_BUTTON_IN_CART', 'In den Warenkorb');
define('IMAGE_BUTTON_SEARCH', 'Suchen');
define('IMAGE_BUTTON_UPDATE', 'Aktualisieren');
define('IMAGE_BUTTON_UPDATE_CART', 'Warenkorb aktualisieren');
define('IMAGE_BUTTON_WRITE_REVIEW', 'Bewertung schreiben');
define('IMAGE_BUTTON_ADMIN', 'Admin');
define('IMAGE_BUTTON_PRODUCT_EDIT', 'Produkt bearbeiten');
define('IMAGE_BUTTON_CANCEL', 'Abbrechen');
define('IMAGE_BUTTON_CENTRALISED_BUYING_ACTIVE', 'Sammelbestellung aktivieren');
define('IMAGE_BUTTON_CENTRALISED_BUYING_NOTACTIVE', 'Sammelbestellung deaktivieren');
define('IMAGE_BUTTON_ORDER_TO_CART', 'Als Vorlage verwenden');
define('IMAGE_BUTTON_ADD_TO_WATCHLIST', 'Auf den Merkzettel');
define('IMAGE_BUTTON_CANCEL_ORDER', 'Stornieren');
define('IMAGE_BUTTON_EXTEND_ORDER', 'Erweitern');
define('IMAGE_BUTTON_PDF_INVOICE', 'PDF-Rechnung');

define('SMALL_IMAGE_BUTTON_DELETE', 'Löschen');
define('SMALL_IMAGE_BUTTON_EDIT', 'Ändern');
define('SMALL_IMAGE_BUTTON_VIEW', 'Anzeigen');

define('ICON_ARROW_RIGHT', 'Zeige mehr');
define('ICON_CART', 'In den Warenkorb');
define('ICON_SUCCESS', 'Erfolg');
define('ICON_WARNING', 'Warnung');

/*
 *
 *  GREETINGS
 *
 */

define('TEXT_GREETING_PERSONAL', 'Schön, dass Sie wieder da sind, <span class="greetUser">%s!</span> Möchten Sie sich unsere <a style="text-decoration:underline;" href="%s">neuen Artikel</a> ansehen?');
define('TEXT_GREETING_PERSONAL_RELOGON', '<small>Wenn Sie nicht %s sind, melden Sie sich bitte <a style="text-decoration:underline;" href="%s">hier</a> mit Ihren Anmeldedaten an.</small>');
define('TEXT_GREETING_GUEST', 'Herzlich Willkommen <span class="greetUser">Gast!</span> Möchten Sie sich <a style="text-decoration:underline;" href="%s">anmelden</a>? Oder wollen Sie ein <a style="text-decoration:underline;" href="%s">Kundenkonto</a> eröffnen?');

define('TEXT_SORT_PRODUCTS', 'Sortierung der Artikel ist ');
define('TEXT_DESCENDINGLY', 'absteigend');
define('TEXT_ASCENDINGLY', 'aufsteigend');
define('TEXT_BY', ' nach ');

define('TEXT_REVIEW_BY', 'von %s');
define('TEXT_REVIEW_WORD_COUNT', '%s Worte');
define('TEXT_REVIEW_RATING', 'Bewertung: %s [%s]');
define('TEXT_REVIEW_DATE_ADDED', 'Hinzugefügt am: %s');
define('TEXT_NO_REVIEWS', 'Es liegen noch keine Bewertungen vor.');
define('TEXT_NO_NEW_PRODUCTS', 'Zur Zeit gibt es keine neuen Artikel.');
define('TEXT_UNKNOWN_TAX_RATE', 'Unbekannter Steuersatz');

/*
 *
 * WARNINGS
 *
 */

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Warnung: Das Installationverzeichnis ist noch vorhanden auf: ' . dirname($HTTP_SERVER_VARS['SCRIPT_FILENAME']) . '/xtc_installer. Bitte löschen Sie das Verzeichnis aus Gründen der Sicherheit!');
define('WARNING_CONFIG_FILE_WRITEABLE', 'Warnung: XT-Commerce kann in die Konfigurationsdatei schreiben: ' . dirname($HTTP_SERVER_VARS['SCRIPT_FILENAME']) . '/includes/configure.php. Das stellt ein mögliches Sicherheitsrisiko dar - bitte korrigieren Sie die Benutzerberechtigungen zu dieser Datei!');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Warnung: Das Verzeichnis für die Sessions existiert nicht: ' . xtc_session_save_path() . '. Die Sessions werden nicht funktionieren bis das Verzeichnis erstellt wurde!');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Warnung: XT-Commerce kann nicht in das Sessions Verzeichnis schreiben: ' . xtc_session_save_path() . '. Die Sessions werden nicht funktionieren bis die richtigen Benutzerberechtigungen gesetzt wurden!');
define('WARNING_SESSION_AUTO_START', 'Warnung: session.auto_start ist aktiviert (enabled) - Bitte deaktivieren (disabled) Sie dieses PHP Feature in der php.ini und starten Sie den WEB-Server neu!');
define('WARNING_DOWNLOAD_DIRECTORY_NON_EXISTENT', 'Warnung: Das Verzeichnis für den Artikel Download existiert nicht: ' . DIR_FS_DOWNLOAD . '. Diese Funktion wird nicht funktionieren bis das Verzeichnis erstellt wurde!');

define('SUCCESS_ACCOUNT_UPDATED', 'Ihr Konto wurde erfolgreich aktualisiert.');
define('SUCCESS_PASSWORD_UPDATED', 'Ihr Passwort wurde erfolgreich geändert!');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING', 'Das eingegebene Passwort stimmt nicht mit dem gespeichertem Passwort überein. Bitte versuchen Sie es noch einmal.');
define('TEXT_MAXIMUM_ENTRIES', 'Hinweis: Ihnen stehen %s Adressbucheinträge zur Verfügung!');
define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED', 'Der ausgewählte Eintrag wurde erfolgreich gelöscht.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED', 'Ihr Adressbuch wurde erfolgreich aktualisiert!');
define('WARNING_PRIMARY_ADDRESS_DELETION', 'Die Standardadresse kann nicht gelöscht werden. Bitte erst eine andere Standardadresse wählen. Danach kann der Eintrag gelöscht werden.');
define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY', 'Dieser Adressbucheintrag ist nicht vorhanden.');
define('ERROR_ADDRESS_BOOK_FULL', 'Ihr Adressbuch kann keine weiteren Adressen aufnehmen. Bitte löschen Sie eine nicht mehr benötigte Adresse. Danach können Sie einen neuen Eintrag speichern.');
define('ERROR_CDATENSG', 'Bitte Bestätigen Sie die Kenntnisnahme unserer Datenschutzbestimmungen.');
//  conditions check

define('ERROR_CONDITIONS_NOT_ACCEPTED', '* Sofern Sie unsere Allgemeinen Geschäftsbedingungen nicht akzeptieren,\n können wir Ihre Bestellung bedauerlicherweise nicht entgegennehmen!\n\n');

// Datenschutz Check
define('ERROR_DATENSCHUTZ_NOT_ACCEPTED', '* Sofern Sie unsere Datenschutzbelehrung nicht akzeptieren,\n können wir Ihre Bestellung bedauerlicherweise nicht entgegennehmen!\n\n');
define('ERROR_WIDERRUFSRECHT_NOT_ACCEPTED', '* Sofern Sie unser Widerrufsrecht nicht akzeptieren,\n können wir Ihre Bestellung bedauerlicherweise nicht entgegennehmen!\n\n');

define('SUB_TITLE_OT_DISCOUNT','Rabatt:');

define('TAX_ADD_TAX','inkl. ');
define('TAX_NO_TAX','zzgl. ');

define('NOT_ALLOWED_TO_SEE_PRICES','Sie können als Gast (bzw mit Ihrem derzeitigen Status) keine Preise sehen');
define('NOT_ALLOWED_TO_SEE_PRICES_TEXT','Sie haben keine Erlaubnis Preise zu sehen, erstellen Sie bitte ein Kundenkonto.');

define('TEXT_DOWNLOAD','Download');
define('TEXT_VIEW','Ansehen');

define('TEXT_BUY', '1 x \'');
define('TEXT_NOW', '\' bestellen');
define('TEXT_GUEST','Gast');

/*
 *
 * ADVANCED SEARCH
 *
 */

define('TEXT_ALL_CATEGORIES', 'Alle Kategorien');
define('TEXT_ALL_MANUFACTURERS', 'Alle Hersteller');
define('JS_AT_LEAST_ONE_INPUT', 'Bitte grenzen Sie die Suche über ein Suchwort oder mindestens einen Filter ein.');
define('AT_LEAST_ONE_INPUT', 'Eines der folgenden Felder muss ausgefüllt werden:<br />Stichworte mit mindestens drei Zeichen<br />Preis ab<br />Preis bis<br />');
define('JS_INVALID_FROM_DATE', '* ungü Datum (von)\n');
define('JS_INVALID_TO_DATE', '* ungü Datum (bis)\n');
define('JS_TO_DATE_LESS_THAN_FROM_DATE', '* Das Datum(von) muß größer oder gleich sein als das Datum (bis)\n');
define('JS_PRICE_FROM_MUST_BE_NUM', '* \"Preis ab\" muss eine Zahl sein\n\n');
define('JS_PRICE_TO_MUST_BE_NUM', '* \"Preis bis\" muss eine Zahl sein\n\n');
define('JS_PRICE_TO_LESS_THAN_PRICE_FROM', '* Preis bis muss grösser oder gleich Preis ab sein.\n');
define('JS_INVALID_KEYWORDS', '* Suchbegriff unzulässig\n');
define('TEXT_LOGIN_ERROR', 'Keine Übereinstimmung der eingebenen \'E-Mail-Adresse\' und/oder dem \'Passwort\'.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND', 'Die eingegebene E-Mail-Adresse ist nicht registriert. Bitte versuchen Sie es noch einmal.');
define('TEXT_REMOVED_COUPON', 'Der Gutschein %s ist abgelaufen und wurde aus Ihrem Warenkorb entfernt.');
define('TEXT_PASSWORD_SENT', 'Wir haben Ihnen soeben eine E-Mail mit Ihrem neuen Passwort gesendet.');
define('TEXT_MORE_INFORMATION', 'Für weitere Informationen, besuchen Sie bitte die <a style="text-decoration:underline;" href="%s" onclick="window.open(this.href); return false;">Homepage</a> zu diesem Artikel.');
define('TEXT_DATE_ADDED', 'Diesen Artikel haben wir am %s in unseren Katalog aufgenommen.');
define('TEXT_DATE_AVAILABLE', '<font color="#ff0000">Dieser Artikel wird voraussichtlich ab dem %s wieder vorrätig sein.</font>');
define('SUB_TITLE_SUB_TOTAL', 'Zwischensumme');
define('SUB_TITLE_SUB_RABATT', '%s %% Rabatt ab %s:');
define('SUB_TITLE_SUB_RABATT_SHORT', '%s %% Rabatt:');
define('SUB_TITLE_NOT_DISCOUNTABLE', 'Dieser Artikel ist nicht rabattierbar');
define('SUB_TITLE_NOT_DISCOUNTABLE_SHORT', 'nicht rabattierbar');

define('OUT_OF_STOCK_CANT_CHECKOUT', 'Die mit ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' markierten Artikel sind leider nicht in der von Ihnen gewünschten Menge auf Lager.<br />Bitte reduzieren Sie Ihre Bestellmenge für die gekennzeichneten Artikel. Vielen Dank');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Die mit ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' markierten Artikel sind leider nicht in der von Ihnen gewünschten Menge auf Lager.<br />Die bestellte Menge wird kurzfristig von uns geliefert, wenn Sie es wünschen nehmen wir auch eine Teillieferung vor.');

define('MINIMUM_ORDER_VALUE_NOT_REACHED_1', 'Sie haben den Mindestbestellwert von: ');
define('MINIMUM_ORDER_VALUE_NOT_REACHED_2', ' leider noch nicht erreicht.<br />Bitte bestellen Sie für mindestens weitere: ');
define('MAXIMUM_ORDER_VALUE_REACHED_1', 'Sie haben die Höchstbestellsumme von: ');
define('MAXIMUM_ORDER_VALUE_REACHED_2', 'überschritten.<br /> Bitte reduzieren Sie Ihre Bestellung um mindestens: ');

define('ERROR_INVALID_PRODUCT', 'Der von Ihnen gewählte Artikel wurde nicht gefunden!');

/*
 *
 * NAVBAR Titel
 *
 */

define('NAVBAR_TITLE_ACCOUNT', 'Ihr Konto');
define('NAVBAR_TITLE_1_ACCOUNT_EDIT', 'Ihr Konto');
define('NAVBAR_TITLE_2_ACCOUNT_EDIT', 'Ihre persönliche Daten ändern');
define('NAVBAR_TITLE_1_ACCOUNT_HISTORY', 'Ihr Konto');
define('NAVBAR_TITLE_2_ACCOUNT_HISTORY', 'Ihre getätigten Bestellungen');
define('NAVBAR_TITLE_1_ACCOUNT_HISTORY_INFO', 'Ihr Konto');
define('NAVBAR_TITLE_2_ACCOUNT_HISTORY_INFO', 'Getätigte Bestellung');
define('NAVBAR_TITLE_3_ACCOUNT_HISTORY_INFO', 'Bestellnummer %s');
define('NAVBAR_TITLE_1_ACCOUNT_PASSWORD', 'Ihr Konto');
define('NAVBAR_TITLE_2_ACCOUNT_PASSWORD', 'Passwort ändern');
define('NAVBAR_TITLE_1_ADDRESS_BOOK', 'Ihr Konto');
define('NAVBAR_TITLE_2_ADDRESS_BOOK', 'Adressbuch');
define('NAVBAR_TITLE_1_ADDRESS_BOOK_PROCESS', 'Ihr Konto');
define('NAVBAR_TITLE_2_ADDRESS_BOOK_PROCESS', 'Adressbuch');
define('NAVBAR_TITLE_ADD_ENTRY_ADDRESS_BOOK_PROCESS', 'Neuer Eintrag');
define('NAVBAR_TITLE_MODIFY_ENTRY_ADDRESS_BOOK_PROCESS', 'Eintrag ändern');
define('NAVBAR_TITLE_DELETE_ENTRY_ADDRESS_BOOK_PROCESS', 'Eintrag löschen');
define('NAVBAR_TITLE_ADVANCED_SEARCH', 'Erweiterte Suche');
define('NAVBAR_TITLE1_ADVANCED_SEARCH', 'Erweiterte Suche');
define('NAVBAR_TITLE2_ADVANCED_SEARCH', 'Suchergebnisse');
define('NAVBAR_TITLE_1_CHECKOUT_CONFIRMATION', 'Kasse');
define('NAVBAR_TITLE_2_CHECKOUT_CONFIRMATION', 'Bestätigung');
define('NAVBAR_TITLE_1_CHECKOUT_PAYMENT', 'Kasse');
define('NAVBAR_TITLE_2_CHECKOUT_PAYMENT', 'Zahlungsweise');
define('NAVBAR_TITLE_1_PAYMENT_ADDRESS', 'Kasse');
define('NAVBAR_TITLE_2_PAYMENT_ADDRESS', 'Rechnungsadresse ändern');
define('NAVBAR_TITLE_1_CHECKOUT_SHIPPING', 'Kasse');
define('NAVBAR_TITLE_2_CHECKOUT_SHIPPING', 'Versandinformationen');
define('NAVBAR_TITLE_1_CHECKOUT_SHIPPING_ADDRESS', 'Kasse');
define('NAVBAR_TITLE_2_CHECKOUT_SHIPPING_ADDRESS', 'Versandadresse ändern');
define('NAVBAR_TITLE_1_CHECKOUT_SUCCESS', 'Kasse');
define('NAVBAR_TITLE_2_CHECKOUT_SUCCESS', 'Erfolg');
define('NAVBAR_TITLE_CREATE_ACCOUNT', 'Konto erstellen');
define('NAVBAR_TITLE_LOGIN', 'Anmelden');
define('NAVBAR_TITLE_ORDER', 'Bestellen');
define('NAVBAR_TITLE_LOGOFF','Auf Wiedersehen');
define('NAVBAR_TITLE_PRODUCTS_NEW', 'Neue Artikel');
define('NAVBAR_TITLE_SHOPPING_CART', 'Warenkorb');
define('NAVBAR_TITLE_SPECIALS', 'Angebote');
define('NAVBAR_TITLE_COOKIE_USAGE', 'Cookie-Nutzung');
define('NAVBAR_TITLE_PRODUCT_REVIEWS', 'Bewertungen');
define('NAVBAR_TITLE_REVIEWS_WRITE', 'Bewertungen');
define('NAVBAR_TITLE_REVIEWS','Bewertungen');
define('NAVBAR_TITLE_SSL_CHECK', 'Sicherheitshinweis');
define('NAVBAR_TITLE_CREATE_GUEST_ACCOUNT','Konto erstellen');
define('NAVBAR_TITLE_PASSWORD_DOUBLE_OPT','Passwort vergessen?');
define('NAVBAR_TITLE_NEWSLETTER','Newsletter');
define('NAVBAR_GV_REDEEM', 'Gutschein einlösen');
define('NAVBAR_GV_SEND', 'Gutschein versenden');

/*
 *
 *  MISC
 *
 */

define('TEXT_NEWSLETTER','Sie möchten immer auf dem Laufenden bleiben?<br />Kein Problem, tragen Sie sich in unseren Newsletter ein und Sie sind immer auf dem neuesten Stand.');
define('TEXT_EMAIL_INPUT','Ihre E-Mail-Adresse wurde in unser System eingetragen.');

define('TEXT_WRONG_CODE','<font color="FF0000">Ihr eingegebener Sicherheitscode stimmte nicht mit dem angezeigten Code Überein. Bitte versuchen Sie es erneut.</font>');
define('TEXT_EMAIL_EXIST_NO_NEWSLETTER','<font color="FF0000">Diese E-Mail-Adresse existiert bereits in unserer Datenbank ist aber noch nicht für den Empfang des Newsletters freigeschaltet!</font>');
define('TEXT_EMAIL_EXIST_NEWSLETTER','<font color="FF0000">Diese E-Mail-Adresse existiert bereits in unserer Datenbank und ist für den Newsletterempfang bereits freigeschaltet!</font>');
define('TEXT_EMAIL_NOT_EXIST','<font color="FF0000">Diese E-Mail-Adresse existiert nicht in unserer Datenbank!</font>');
define('TEXT_EMAIL_DEL','Ihre E-Mail-Adresse wurde aus unserer Newsletterdatenbank gelöscht.');
define('TEXT_EMAIL_DEL_ERROR','<font color="FF0000">Es ist ein Fehler aufgetreten, Ihre E-Mail-Adresse wurde nicht gelöscht!</font>');
define('TEXT_EMAIL_ACTIVE','<font color="FF0000">Ihre E-Mail-Adresse wurde erfolgreich für den Newsletterempfang freigeschaltet!</font>');
define('TEXT_EMAIL_ACTIVE_ERROR','<font color="FF0000">Es ist ein Fehler aufgetreten, Ihre E-Mail-Adresse wurde nicht freigeschaltet!</font>');
define('TEXT_EMAIL_SUBJECT','Ihre Newsletteranmeldung');

define('TEXT_CUSTOMER_GUEST','Gast');

define('TEXT_LINK_MAIL_SENDED','Ihre Anfrage nach einem neuen Passwort muss von Ihnen erst bestätigt werden.<br />Deshalb wurde Ihnen vom System eine E-Mail mit einem Bestätigungslink gesendet. Bitte klicken Sie nach dem Erhalt der E-Mail auf den Link um eine weitere E-Mail mit Ihrem neuen Passwort zu erhalten. Anderenfalls kann Ihnen aus Sicherheitsgründen das neue Passwort nicht zugestellt werden!');
define('TEXT_PASSWORD_MAIL_SENDED','Eine E-Mail mit einem neuen Anmelde-Passwort wurde Ihnen soeben zugestellt.<br />Bitte ändern Sie nach Ihrer nächsten Anmeldung Ihr Passwort wie gewünscht.');
define('TEXT_CODE_ERROR','Bitte geben Sie Ihre E-Mail-Adresse und den Sicherheitscode erneut ein. <br />Achten Sie dabei auf Tippfehler!');
define('TEXT_EMAIL_ERROR','Bitte geben Sie Ihre E-Mail-Adresse erneut ein. <br />Achten Sie dabei auf Tippfehler!');
define('TEXT_NO_ACCOUNT','Leider müssen wir Ihnen mitteilen, dass Ihre Anfrage für ein neues Anmelde-Passwort entweder ungültig war oder abgelaufen ist.<br />Bitte versuchen Sie es erneut.');
define('HEADING_PASSWORD_FORGOTTEN','Passwort erneuern');
define('TEXT_PASSWORD_FORGOTTEN','Ändern Sie Ihr Passwort.');
define('TEXT_EMAIL_PASSWORD_FORGOTTEN','Bestätigungs-E-Mail für Passwortänderung');
define('TEXT_EMAIL_PASSWORD_NEW_PASSWORD','Ihr neues Passwort');
define('ERROR_MAIL','Bitte überprüfen Sie Ihre eingegebenen Daten im Formular');

define('CATEGORIE_NOT_FOUND','Kategorie wurde nicht gefunden');

define('GV_FAQ', 'Gutschein FAQ');
define('ERROR_NO_REDEEM_CODE', 'Sie haben leider keinen Code eingegeben.');
define('ERROR_NO_INVALID_REDEEM_GV', 'Ungültiger Gutscheincode');
define('TABLE_HEADING_CREDIT', 'Guthaben');
define('EMAIL_GV_TEXT_SUBJECT', 'Ein Geschenk von %s');
define('MAIN_MESSAGE', 'Sie haben sich dazu entschieden, einen Gutschein im Wert von %s an %s versenden, dessen E-Mail-Adresse %s lautet.<br /><br />Folgender Text erscheint in Ihrer E-Mail:<br /><br />Hallo %s<br /><br />Ihnen wurde ein Gutschein im Wert von %s durch %s geschickt.');
define('REDEEMED_AMOUNT','Ihr Gutschein wurde erfolgreich auf Ihr Konto verbucht. Gutscheinwert:');
define('REDEEMED_COUPON','Ihr Coupon wurde erfolgreich eingebucht und wird bei Ihrer nächsten Bestellung automatisch eingelöst.');
define('COUPON', 'Gutschein');

define('ERROR_INVALID_USES_USER_COUPON','Sie können den Coupon nur ');
define('ERROR_INVALID_USES_COUPON','Diesen Coupon können Kunden nur ');
define('TIMES',' mal einlösen.');
define('ERROR_INVALID_STARTDATE_COUPON','Ihr Coupon ist noch nicht verfügbar.');
define('ERROR_INVALID_FINISDATE_COUPON','Ihr Coupon ist bereits abgelaufen.');
define('PERSONAL_MESSAGE', '%s schreibt:');

// Product Variants
define('VARIANT_NOT_POSSIBLE_MESSAGE', 'Kombination nicht möglich');
define('VARIANT_NOT_AVAILABLE_MESSAGE', 'Kombination nicht lieferbar');

//Popup Window
define('TEXT_CLOSE_WINDOW', 'Schließen');

define('ADD_GROUP', 'Gruppe hinzufügen');
define('CONFIRM_CANCEL_ORDER', 'Sind Sie sicher, dass sie diese Bestellung stornieren möchten?');
define('CONFIRM_EXTEND_ORDER', 'Sind Sie sicher, dass sie diese Bestellung erweitern möchten?'."\\n".'Achtung, die alte Bestellung wird dabei storniert!');

define('RECOMMEND_MAIL_TITLE', 'Empfehlung von ');
define('RECOMMEND_MAIL_HELLO', 'Hallo %s,<br />%s hat Ihnen einen Artikel aus unserem Sortiment mit folgendem Kommentar empfohlen:<br /><br />%s');
define('RECOMMEND_MAIL_ARTICLE', 'Artikel: ');
define('RECOMMEND_MAIL_ARTICLE_NR', 'Artikel Nr.:');
define('RECOMMEND_MAIL_PRICE', 'Preis:');
define('RECOMMEND_MAIL_TEXT_HTML', '<div style="text-align: right;">
	<img src="%s" />
</div>
<div style="margin-bottom: 10px;">

</div>
<div style="margin-bottom: 10px;">%s</div>
<div style="width: 475px; border-bottom: 1px solid rgb(204, 204, 204); margin-bottom: 4px;">
	<div style="float: left; width: 300px;">
		<strong>Artikel:</strong>
	</div>
	<div style="float: left; width: 80px; margin-left: 5px;">
		<strong>Artikel Nr.:</strong>
	</div>
	<div style="float: left; width: 80px; text-align: right;">
		<strong>Einzelpreis:</strong>
	</div>
	<br style="clear: both;">
</div>');


define('RECOMMEND_MAIL_TEXT', 'Hallo %s,
%s hat Ihnen einen Artikel aus unserem Sortiment mit folgendem Kommentar empfohlen

%s

%s

Artikel: %s
Artikelnummer: %s
Einzelpreis: %s');

/*
 *
 *  COUPON POPUP
 *
 */

define('TEXT_COUPON_HELP_HEADER', 'Ihr Gutschein wurde erfolgreich verbucht.');
define('TEXT_COUPON_HELP_NAME', '<br /><br />Gutscheinbezeichnung: %s');
define('TEXT_COUPON_HELP_FIXED', '<br /><br />Der Gutscheinwert beträgt %s ');
define('TEXT_COUPON_HELP_MINORDER', '<br /><br />Der Mindestbestellwert beträgt %s ');
define('TEXT_COUPON_HELP_FREESHIP', '<br /><br />Gutschein für kostenlosen Versand');
define('TEXT_COUPON_HELP_DESC', '<br /><br />Couponbeschreibung: %s');
define('TEXT_COUPON_HELP_DATE', '<br /><br />Dieser Coupon ist gültig vom %s bis %s');
define('TEXT_COUPON_HELP_RESTRICT', '<br /><br />Artikel / Kategorie Einschränkungen');
define('TEXT_COUPON_HELP_CATEGORIES', 'Kategorie');
define('TEXT_COUPON_HELP_PRODUCTS', 'Artikel');

// VAT ID
define('ENTRY_VAT_TEXT', 'Nur für Deutschland und EU!');
define('ENTRY_VAT_ERROR', 'Die Eingegebene UstID ist ungültig oder kann derzeit nicht überprüft werden! Bitte geben Sie eine gültige ID ein oder lassen Sie das Feld leer.');
define('MSRP','UVP');
define('YOUR_PRICE','Ihr Preis ');
define('ONLY',' Heute für');
define('FROM','Ab ');
define('YOU_SAVE','Sie Sparen ');
define('INSTEAD','statt ');
define('TXT_PER',' pro ');
define('TAX_INFO_INCL','incl. %s UST');
define('TAX_INFO_EXCL','exkl. %s UST');
define('TAX_INFO_ADD','zzgl. %s UST');
define('SHIPPING_EXCL','exkl.');
define('SHIPPING_COSTS','Versandkosten');

// changes 3.0.4 SP2
define('SHIPPING_TIME','Lieferzeit: ');
define('MORE_INFO','[Mehr]');

define('TEXT_YOUR_MAIL_ADDRESS', 'Ihre E-Mail-Adresse: ');
define('TEXT_MAIL_CHECK', 'Bitte prüfen Sie, ob Ihre E-Mail-Adresse korrekt eingegeben wurde.');
define('TEXT_MAIL_WWW_ERROR', '* E-Mail-Adressen enthalten für gewöhnlich kein www.');
define('TEXT_MAIL_ERROR', '* Es wurde ein häufiger Tippfehler gefunden (z.B.: name@-online.de statt name@t-online.de)');
define('TEXT_SEARCHSTRING_TO_SHORT', 'Der eingegebene Suchbegriff muss mindestens zwei Zeichen haben.');

define('REGISTRATION_MAIL_SUBJECT', 'Willkommen bei Crazy Factory');

define('ORDER_MAIL_SUBJECT', 'Ihre Bestellung Nummer: ');

define('CENTRALISED_BUYING_ENABLED', 'aktiv');
define('CENTRALISED_BUYING_DISABLED', 'inaktiv');
define('CENTRALISED_BUYING_TEXT', 'Klicken Sie einfach auf eine Nummer und dieses Produkt wir bei Ihrer Bestellung in eine gesonderte gekennzeichnete Versandtüte verpackt.');
define('ACTIONSHOT_REPORTED', 'Vielen Dank!'."\n".'Der Hinweis wurde versendet und wird von uns geprüft.');
define('ACTIONSHOT_ALLREADY_REPORTED', 'Der Action Shot wurde von Ihnen bereits gemeldet.');

define('REVIEW_SAVED', 'Bewertung gespeichert.');

define('SHIPPING_FOR_FREE','frei');
define('TEXT_NOT_ALL_ATTRIBUTES_CHOSEN', 'Bitte treffen Sie für alle Varianten eine Auswahl!
(z.B. Länge, Materialstärke, Farbe, Motiv, etc.).');
define('TEXT_PICTURE_NOT_ALL_ATTRIBUTES_CHOSEN', 'Bitte wählen Sie zunächst die Größe des Artikels aus.');
define('TEXT_LOGOUT_LINK', 'Logout');
define('TEXT_ACCOUNT_LINK', 'Mein Konto');
define('TEXT_QUESTION_RESTORE_CART', 'Von Ihrem letzten Shopbesuch befinden sich noch Artikel im Warenkorb. Möchten Sie diese Artikel übernehmen und zu den Artikeln die jetzt im Warenkorb liegen hinzufügen?');
define('LINK_RESTORE_CART', 'Artikel übernehmen');
define('LINK_DISCARD_CART', 'Artikel verwerfen');

define('MINI_CART_HEADING_LAST_ADDED', 'Zuletzt hinzugefügter Artikel:');
define('MINI_CART_LINK_TO_CART', 'Zum Warenkorb');
define('MINI_CART_LINK_CHECKOUT', 'Zur Kasse');


// Cashpoints Aktionen
define('CASHPOINTS_ACTIONSHOT', 'Sie haben einen ActionShot hochgeladen!');
define('CASHPOINTS_USED_TO_ORDER', 'Sie haben Crazy Cash Points f&uuml;r die Bestellung Nr. %order_id% eingetauscht!');
define('CASHPOINTS_COUPON', 'Sie haben einen Crazy Cash Points-Gutschein eingelöst!');
define('CASHPOINTS_COMMENT', 'Kommentar');
define('CASHPOINTS_NEWSLETTER', 'Newsletter');
define('CASHPOINTS_SPECIAL', 'Sonderaktion');
define('CASHPOINTS_ORDER_BONUS', 'Gutschrift für die Bestellung Nr. %order_id%');
define('CASHPOINTS_NOT_ENOUGH', 'Achtung! Sie verfügen nicht über so viele Crazy Cash Points. Es konnten nur %s Crazy Cash Points eingelöst werden. ');
define('CASHPOINTS_VALENTINE2011', 'Valentins-Grüsse 2011');
define('CASHPOINTS_CANCELLATION', 'Rückerstattung für die Stornierung der Bestellung Nr. %order_id%.');
define('CASHPOINTS_RMA', 'Gutschrift für die Rücksendung Nr. %rma_id% zur Bestellung Nr. %order_id%.');
define('CASHPOINTS_RMA_BONUS_REDUCE', 'Abzug durch die Rücksendung Nr. %rma_id% zur Bestellung Nr. %order_id%.');
define('CASHPOINTS_STORE_CREDIT', 'Store Credit');

define('CASHPOINTS_COUPON_NOT_FOUND', 'Dieser Coupon konnte nicht gefunden werden!');
define('CASHPOINTS_COUPON_EXPIRED', 'Dieser Coupon ist leider abgelaufen!');
define('CASHPOINTS_COUPON_ALREADY_USED', 'Dieser Coupon wurde bereits eingel&ouml;st!');

// Actioshotzeug
define('NO_ACTIONSHOT_NICKNAME','Anonymous');
define('NO_ACTIONSHOT_DESC','Keine Beschreibung hinzugefügt.');
define('ALLREADY_UPLOADED_ACTIONSHOT', 'Sie haben bereits einen Action Shot hochgeladen.');

define('FILTER_TEXT', '%s wählen');

// Zeitraum-Pseudofilter bei "Neue Produkte"
define('FILTER_TIME', 'Zeitraum');
define('FILTER_1MONTH', '1 Monat');
define('FILTER_3MONTHS', '3 Monate');
define('FILTER_LASTORDER', 'Seit der letzten Bestellung');

define('FILTER_MATERIAL_THICKNESS', 'Materialstärke');
define('FILTER_MATERIAL', 'Material');
define('FILTER_FOR_WHICH_MATERIAL_THICKNESS', 'Gewindegröße');
define('FILTER_PRODUCT_TYPE', 'Produktart');
define('FILTER_STYLE', 'Stil');
define('FILTER_HAS_DIAMETER', 'Durchmesser');
define('FILTER_THREAD_TYPE', 'Gewindeart');
define('FILTER_VALUE_0_8MM','0.8mm');
define('FILTER_VALUE_1_0MM','1.0mm');
define('FILTER_VALUE_1_2MM','1.2mm');
define('FILTER_VALUE_1_6MM','1.6mm');
define('FILTER_VALUE_2_0MM_AND_BIGGER','2.0mm und größer');
define('FILTER_VALUE_2_5MM','2.5mm');
define('FILTER_VALUE_FOR_1_0MM','für 1.0mm');
define('FILTER_VALUE_FOR_1_2MM','für 1.2mm');
define('FILTER_VALUE_FOR_1_6MM','für 1.6mm');
define('FILTER_VALUE_FOR_2_0MM_AND_BIGGER','für 2.0mm und größer');
define('FILTER_VALUE_925_STERLING_SILVER','925 Sterling Silber');
define('FILTER_VALUE_BALL_CLOSURE_RINGS','Ball Closure Ringe');
define('FILTER_VALUE_BALLS___ATTACHMENTS','Kugeln & Aufsätze');
define('FILTER_VALUE_BANANAS','Bananen');
define('FILTER_VALUE_BARBELLS','Barbells');
define('FILTER_VALUE_BASIC_BANANAS','Standard-Banane');
define('FILTER_VALUE_BELLY_CHAIN_BANANAS','Bauchketten-Banane');
define('FILTER_VALUE_BIOFLEX','Bioflex');
define('FILTER_VALUE_BONE','Knochen');
define('FILTER_VALUE_CHARMS','Charms');
define('FILTER_VALUE_CIRCULAR_BARBELL','Circular Barbells');
define('FILTER_VALUE_CLAWS','Claws');
define('FILTER_VALUE_COCONUT','Kokusnuss');
define('FILTER_VALUE_CURVED_NOSE_STUDS','Gebogene Nasenstecker');
define('FILTER_VALUE_DERMAL_ANCHOR','Dermal Anchors');
define('FILTER_VALUE_EAR_STUDS','Ohrringstecker');
define('FILTER_VALUE_EARRING_PIN','Ohrringstäbe');
define('FILTER_VALUE_EARRINGS','Ohrringe');
define('FILTER_VALUE_EXPANDER','Expander');
define('FILTER_VALUE_FASHION_BANANAS','Modische Bananen');
define('FILTER_VALUE_FOR_BALL_CLOSURE_RINGS','für Ball Closure Ringe');
define('FILTER_VALUE_FOR_INTERNAL_BIOFLEX_PINS','für Internal Bioflex Stäbe');
define('FILTER_VALUE_FOR_INTERNALLY_THREADED_PINS','für Stäbe mit Innengewinde');
define('FILTER_VALUE_FOR_NORMAL_THREADED_PINS','für Stäbe mit normalem Gewinde');
define('FILTER_VALUE_GOLD','Gold');
define('FILTER_VALUE_GOLD_PLATED_STEEL','Vergoldet');
define('FILTER_VALUE_HELIX_SHIELD','Helix Shield');
define('FILTER_VALUE_HOOKS','Hooks');
define('FILTER_VALUE_HORN','Horn');
define('FILTER_VALUE_IMPLANT','Implantate');
define('FILTER_VALUE_INTERNALLY_THREADED','mit Innengewinde');
define('FILTER_VALUE_ITEMS_WITHOUT_THREAD','Artikel ohne Gewinde');
define('FILTER_VALUE_LABRET','Labrets');
define('FILTER_VALUE_NORMAL_THREAD','normales Gewinde');
define('FILTER_VALUE_NOSE_BONES','Nasenknochen');
define('FILTER_VALUE_NOSE_RINGS','Nasenringe');
define('FILTER_VALUE_NOSE_STUD','Nasenstecker');
define('FILTER_VALUE_SEPTUM','Septum');
define('FILTER_VALUE_OTHER_MATERIALS','Andere Materialien');
define('FILTER_VALUE_OTHER_RINGS','Andere Ringe');
define('FILTER_VALUE_OTHERS','Sonstige');
define('FILTER_VALUE_PINS_FOR_CHARMS','Artikel für Charms');
define('FILTER_VALUE_PLUGS','Plugs');
define('FILTER_VALUE_SEGMENT_RINGS','Segment Ringe');
define('FILTER_VALUE_SKIN_DIVER','Skin Diver');
define('FILTER_VALUE_SPIRALS','Spiralen');
define('FILTER_VALUE_STRAIGHT_NOSE_STUDS','Gerade Nasenstecker');
define('FILTER_VALUE_SURFACE_PIERCING','Surface Piercings');
define('FILTER_VALUE_SURGICAL_STEEL_316L','Chirurgenstahl 316L');
define('FILTER_VALUE_TITANIUM','Titan');
define('FILTER_VALUE_TUBES','Tubes');
define('FILTER_VALUE_TUNNELS','Tunnels');
define('FILTER_VALUE_TUNNELS__TUBES__PLUGS','Tunnels, Plugs, Tubes etc.');
define('FILTER_VALUE_UV_ACRYL','UV-Acryl');
define('FILTER_VALUE_WITHOUT_THREAD','ohne Gewinde');
define('FILTER_VALUE_WOOD','Holz');
define('FILTER_VALUE_LABRETS','Labrets');
define('FILTER_VALUE_LOOSE_PINS','Loose Pins');
define('FILTER_VALUE_CIRCULAR_BARBELLS','Circular Barbells');
define('FILTER_VALUE_RINGS','Ringe');
define('FILTER_VALUE_PENDANTS','Anhänger');
define('FILTER_VALUE_BRACELETS','Armbänder');
define('FILTER_VALUE_NECKLACES','Halsketten');
define('FILTER_VALUE_TOE_RINGS','Zehenringe');
define('FILTER_VALUE_FINGER_CLAWS','Fingerkrallen');
define('FILTER_VALUE_BANGLES','Armreifen');
define('FILTER_VALUE_PEWTER','Hartzinn');


define('FILTER_VALUE_OTHER','Andere');

define('NOT_ENEUGH_CASH_POINTS', 'Leider haben Sie für diese Aktion nicht genügend Crazy Cash Points auf Ihrem Konto!');

define('PRICE_DECREASED_SINCE_LAST_VISIT', 'Der Preis für den Artikel %article% hat sich seit Ihrem letzten Besuch verringert! Vorheriger Preis: %price%');
define('PRICE_INCREASED_SINCE_LAST_VISIT', 'Der Preis für den Artikel %article% hat sich seit Ihrem letzten Besuch leider erhöht! Vorheriger Preis: %price%');

define('TILTVIEWER_UPLOADED_BY', 'Hochgeladen von:');
define('TILTVIEWER_ARTICLE_NAME', 'Artikel:');
define('TILTVIEWER_DESCRIPTION', 'Beschreibung:');

define('TEXT_MAIL_SENT', 'Die Empfehlung wurde gesendet.');

define('TEXT_NEW_DESIGN', 'Sagen Sie uns, wie Ihnen das neue Design gefällt');
define('TEXT_REPORT_BUG', 'Fehler melden');
define('TEXT_PROBLEM_ORDER', 'Probleme mit einer Bestellung');
define('TEXT_OTHER', 'Sonstiges');
define('ERROR_TOPIC', 'Bitte wählen Sie ein Thema.');

define('TEXT_CONFIRM_DELETE_ADDRESS', 'Wollen Sie diese Adresse unwiderruflich aus Ihrem Adressbuch entfernen?');

define('TEXT_SESSION_CART_ERROR', 'Achtung, der Artikel konnte nicht in den Warenkob übernommen werden! Bitte laden Sie die Produktseite mit der Tastenkombination „STRG + F5“ neu. (Errorcode: 1)');
define('TEXT_NO_QUANTITY', 'Achtung, keine Bestellmenge angegeben!');
define('TEXT_ALREADY_LOGGED_IN', 'Achtung, Sie sind bereits eingeloggt. Bitte klicken Sie hier: <a href="">hier</a>!');
define('TEXT_NO_PRODUCT_ID', 'Achtung, der Artikel konnte nicht in den Warenkob übernommen werden! Bitte laden Sie die Produktseite mit der Tastenkombination „STRG + F5“ neu. (Errorcode: 2)');

// IE6 kram
define('TEXT_IE6_MSG1', 'Wissen Sie, dass Ihr Internet Explorer nicht mehr aktuell ist?');
define('TEXT_IE6_MSG2', 'Um unsere Webseite bestmöglich zu nutzen, empfehlen wir Ihnen Ihren Browser auf eine aktuellere Version zu aktualisieren oder einen anderen Webbrowser zu nutzen. Eine Liste der populärsten Browser finden Sie weiter unten.');
define('TEXT_IE6_MSG3', 'Klicken Sie auf eines der Symbole um auf die Download-Seite zu gelangen');

define('TEXT_ATTRIBUTE_NOT_FOUND', 'Bitte treffen Sie für alle Varianten eine Auswahl!');
define('TEXT_PRODUCT_NOT_FOUND', 'Das Produkt konnte nicht gefunden werden');

define('TEXT_FOLLOWING_PRODUCTS_DEACTIVATED', 'Die folgenden Produkte sind derzeit nicht lieferbar und konnten nicht wieder hergestellt werden:');
define('TEXT_FIND_PRODUCTS_ON_WATCHLIST', 'Sie finden diese Produkte auf Ihrem Merkzettel.');
define('TEXT_RMA_ORDER', 'Bestellung %s vom %s');
define('TEXT_RMA_ERROR_NONE_SELECTED', 'Bitte wählen Sie mindestens einen Artikel, den Sie zurücksenden möchten.');
define('TEXT_RMA_ERROR_WRONG_AMOUNT', 'Keine oder zu hohe Anzahl angegeben.');
define('TEXT_RMA_ERROR_NO_CAUSE_SELECTED', 'Bitte wählen Sie einen Grund für die Rücksendung.');
define('TEXT_RMA_ERROR_BANK_DATA', 'Bitte tragen Sie Ihre Bankverbindung vollständig ein.');
define('TEXT_RMA_ERROR_MONEYBOOKERS_EMAIL', 'Bitte tragen Sie Ihre Skrill (Moneybookers)-E-Mail-Adresse ein.');
define('TEXT_RMA_ERROR_NO_REPLACEMENT', 'Neulieferung ist für diesen Rücksendegrund nicht verfügbar.');

define('TEXT_ALREADY_IN_LIST', 'Dieser Artikel ist schon auf Ihrem Merkzettel.');

define('QUANTITY_REDUCED_TO_STOCK_AMOUNT', 'Leider haben wir von diesem Artikel momentan zu geringe Stückzahlen auf Lager. Die gewünsche Menge wurde auf %%amount%% reduziert und in Ihren Warenkorb gelegt.');
define('NOT_IN_STOCK', 'Leider ist dieser Artikel momentan nicht auf Lager!');

define('RMA_MAIL_SUBJECT', 'Rücksendung Crazy Factory');