<?php

/* -----------------------------------------------------------------------------------------
   $Id: cart_actions.php 168 2007-02-06 14:11:42Z mzanier $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(application_top.php,v 1.273 2003/05/19); www.oscommerce.com
   (c) 2003         nextcommerce (application_top.php,v 1.54 2003/08/25); www.nextcommerce.org

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contribution:
   Add A Quickie v1.0 Autor  Harald Ponce de Leon

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org


   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

include(DIR_WS_INCLUDES.'modules/order_to_cart.php');
include(DIR_WS_INCLUDES.'functions/cancel_order.php');

// Shopping cart actions
if (isset ($_GET['action'])) {
	// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled
	if ($_GET['action'] != 'remove' && !$_COOKIE['XTCsid']) {
		xtc_redirect(xtc_href_link(FILENAME_COOKIE_USAGE));
	}

	if (DISPLAY_CART == 'true') {
		$goto = FILENAME_SHOPPING_CART;
		$parameters = array (
			'action',
			'cPath',
			'products_id',
			'pid'
		);
	} else {
		if(basename($PHP_SELF) == 'commerce_seo_url.php')
			$goto = $_REQUEST['linkurl'];
		else
		$goto = basename($PHP_SELF);
		if ($_GET['action'] == 'buy_now') {
			$parameters = array (
				'action',
				'pid',
				'products_id',
				'BUYproducts_id'
			);
		} else {
			$parameters = array (
				'action',
				'pid',
				'BUYproducts_id',
				'info',
				'products_id'
			);
		}
	}

	switch ($_GET['action']) {
		// customer wants to update the product quantity in their shopping cart
		case 'update_product' :

			if (is_object($econda))
				$econda->_emptyCart();

			if ($_POST['cart_quantity'] > MAX_PRODUCTS_QTY)
				$_POST['cart_quantity'] = MAX_PRODUCTS_QTY;

			if (is_object($econda)) {
				$old_quantity = $_SESSION['cart']->get_quantity(xtc_get_uprid($_POST['products_id'], $_POST['id']), $_POST['centralised_buying_group_id']);
				$econda->_updateProduct($_POST['products_id'], $_POST['cart_quantity'], $old_quantity);
			}

			$_SESSION['cart']->update_quantity($_POST['products_id'], xtc_remove_non_numeric($_POST['cart_quantity']), $_POST['combinedAttributes'], $_POST['centralised_buying_group_id']);

			xtc_redirect(xtc_href_link($goto, xtc_get_all_get_params($parameters)));
			break;
			// customer adds a product from the products page
		case 'add_product' :

			if (!$_COOKIE['XTCsid']) {
				xtc_redirect(xtc_href_link(FILENAME_COOKIE_USAGE));
			}

			if (isset ($_POST['products_id']) && is_numeric($_POST['products_id'])) {

				if(!is_numeric($_POST['products_qty'])){
					$_POST['products_qty'] = 1;
				}

				if ($_POST['products_qty'] > MAX_PRODUCTS_QTY)
					$_POST['products_qty'] = MAX_PRODUCTS_QTY;

				if (is_object($econda)) {
					$econda->_emptyCart();
					$old_quantity = $_SESSION['cart']->get_quantity(xtc_get_uprid($_POST['products_id'], $_POST['id']));
					$econda->_addProduct($_POST['products_id'], $_POST['products_qty'], $old_quantity);
				}
				if(is_array($_SESSION[tracking][products_history]))
					if(end($_SESSION[tracking][products_history]) == $_POST['products_id'])
						array_pop($_SESSION[tracking][products_history]);
					//Wenn der Artikel gekauft wird, lohnt es sich nicht ihn als "Zuletzt angesehen" zu speichern.

				$_SESSION['cart']->add_cart((int) $_POST['products_id'], $_SESSION['cart']->get_quantity($_POST['products_id'],$_POST['attributes']) + xtc_remove_non_numeric($_POST['products_qty']), $_POST['combinedAttributes'], true, $_POST['group_id']);
			}
			xtc_redirect(xtc_href_link($goto, xtc_get_all_get_params($parameters)));
			break;

		case 'check_gift' :
			require_once (DIR_FS_INC . 'xtc_collect_posts.inc.php');
			xtc_collect_posts();
			break;

			// customer wants to add a quickie to the cart (called from a box)
		case 'add_a_quickie' :
			$quicky = addslashes($_POST['quickie']);
			if (GROUP_CHECK == 'true') {
				$group_check = "and group_permission_" . $_SESSION['customers_status']['customers_status_id'] . "=1 ";
			}

			$quickie_query = xtc_db_query("select
						                                        products_fsk18,
						                                        products_id from " . TABLE_PRODUCTS . "
						                                        where products_model = '" . $quicky . "' " . "AND products_status = '1' " . $group_check);

			if (!xtc_db_num_rows($quickie_query)) {
				if (GROUP_CHECK == 'true') {
					$group_check = "and group_permission_" . $_SESSION['customers_status']['customers_status_id'] . "=1 ";
				}
				$quickie_query = xtc_db_query("select
								                                                 products_fsk18,
								                                                 products_id from " . TABLE_PRODUCTS . "
								                                                 where products_model LIKE '%" . $quicky . "%' " . "AND products_status = '1' " . $group_check);
			}
			if (xtc_db_num_rows($quickie_query) != 1) {
				xtc_redirect(xtc_href_link(FILENAME_ADVANCED_SEARCH_RESULT, 'keywords=' . $quicky, 'NONSSL'));
			}
			$quickie = xtc_db_fetch_array($quickie_query);
			if (xtc_has_product_attributes($quickie['products_id'])) {
				xtc_redirect(xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $quickie['products_id'], 'NONSSL'));
			} else {
				if ($quickie['products_fsk18'] == '1' && $_SESSION['customers_status']['customers_fsk18'] == '1') {
					xtc_redirect(xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $quickie['products_id'], 'NONSSL'));
				}
				if ($_SESSION['customers_status']['customers_fsk18_display'] == '0' && $quickie['products_fsk18'] == '1') {
					xtc_redirect(xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $quickie['products_id'], 'NONSSL'));
				}
				if ($_POST['quickie'] != '') {
					$act_qty = $_SESSION['cart']->get_quantity(xtc_get_uprid($quickie['products_id'], 1));
					if ($act_qty > MAX_PRODUCTS_QTY)
						$act_qty = MAX_PRODUCTS_QTY - 1;
					$_SESSION['cart']->add_cart($quickie['products_id'], $act_qty +1, 1);
					xtc_redirect(xtc_href_link($goto, xtc_get_all_get_params(array (
						'action'
					)), 'NONSSL'));
				} else {
					xtc_redirect(xtc_href_link(FILENAME_ADVANCED_SEARCH_RESULT, 'keywords=' . $quicky, 'NONSSL'));
				}
			}
			break;

			// performed by the 'buy now' button in product listings and review page
		case 'buy_now' :
			if (isset ($_GET['BUYproducts_id'])) {
				// check permission to view product

				$permission_query = xtc_db_query("SELECT group_permission_" . $_SESSION['customers_status']['customers_status_id'] . " as customer_group, products_fsk18 from " . TABLE_PRODUCTS . " where products_id='" . (int) $_GET['BUYproducts_id'] . "'");
				$permission = xtc_db_fetch_array($permission_query);

				// check for FSK18
				if ($permission['products_fsk18'] == '1' && $_SESSION['customers_status']['customers_fsk18'] == '1') {
					xtc_redirect(xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . (int) $_GET['BUYproducts_id'], 'NONSSL'));
				}
				if ($_SESSION['customers_status']['customers_fsk18_display'] == '0' && $permission['products_fsk18'] == '1') {
					xtc_redirect(xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . (int) $_GET['BUYproducts_id'], 'NONSSL'));
				}

				if (GROUP_CHECK == 'true') {
					if ($permission['customer_group'] != '1') {
						xtc_redirect(xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . (int) $_GET['BUYproducts_id']));
					}
				}
				if (xtc_has_product_attributes($_GET['BUYproducts_id'])) {
					xtc_redirect(xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . (int) $_GET['BUYproducts_id']));
				} else {
					if (isset ($_SESSION['cart'])) {

						if (is_object($econda)) {
							$econda->_emptyCart();
							$old_quantity = $_SESSION['cart']->get_quantity((int) $_GET['BUYproducts_id']);
							$econda->_addProduct($_GET['BUYproducts_id'], $old_quantity +1, $old_quantity);
						}
						$_SESSION['cart']->add_cart((int)$_GET['BUYproducts_id'], $_SESSION['cart']->get_quantity((int)$_GET['BUYproducts_id']) + 1);
					} else {
						xtc_redirect(xtc_href_link(FILENAME_DEFAULT));
					}
				}
			}
			xtc_redirect(xtc_href_link($goto, xtc_get_all_get_params(array (
				'action',
				'BUYproducts_id',
				'cPath',
				'page'
			))));
			break;
		case 'cust_order' :
			if (isset ($_SESSION['customer_id']) && isset ($_GET['pid'])) {
				if (xtc_has_product_attributes((int) $_GET['pid'])) {
					xtc_redirect(xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . (int) $_GET['pid']));
				} else {
					$_SESSION['cart']->add_cart((int) $_GET['pid'], $_SESSION['cart']->get_quantity((int) $_GET['pid']) + 1);
				}
			}
			xtc_redirect(xtc_href_link($goto, xtc_get_all_get_params($parameters)));
			break;

		case 'paypal_express_checkout' :
			$o_paypal->paypal_express_auth_call();
			xtc_redirect($o_paypal->payPalURL);
			break;

		case 'switch_centralised_buying':
			$_SESSION['cart']->switch_centralised_buying($_SESSION['customer_id']);
			break;

		case 'add_group':
			if (isset ($_SESSION['customer_id']))
			{
				$_SESSION['cart']->add_group($_GET['product'], $_GET['group_id']);
				//xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, '#details_'.$_GET['product'].'_'.$_GET['group_id']));
			}
			break;
		case 'order_to_cart':
			if (isset($_SESSION['customer_id']))
			{
				order_to_cart($_GET['order_id']);
				xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, xtc_get_all_get_params(array('action', 'order_id'))));
			}
			break;
		case 'cancel_order':
			if (isset($_SESSION['customer_id']) && isset($_GET['order_id']))
			{
				$check_customer_session = xtc_db_fetch_array(xtc_db_query("SELECT orders_id FROM orders WHERE customers_id = '".(int) $_SESSION['customer_id']."' and orders_id = '".(int) $_GET['order_id']."'"));
				if ($_GET['order_id'] == $check_customer_session['orders_id'])
				{
					if (cancel_order($_GET['order_id']))
						xtc_redirect(xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('action', 'order_id')).'order_canceled=1'));
					else
						xtc_redirect(xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('action', 'order_id')).'order_canceled=0'));
				}
				else
					xtc_redirect(xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('action', 'order_id')).'order_canceled=0'));
			}
			break;
		case 'extend_order':
			if (isset($_SESSION['customer_id']))
			{
				order_to_cart($_GET['order_id']);
				cancel_order($_GET['order_id'],'Info: order extendet to a new one - ');
				xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, xtc_get_all_get_params(array('action', 'order_id'))));
			}
			break;
	}
}
?>