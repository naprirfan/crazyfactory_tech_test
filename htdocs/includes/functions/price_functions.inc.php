<?php



/*
 * Disclaimer:
 *
 * Die Methoden in dieser Datei sind so geschrieben, dass sie OHNE die application_top.php auskommen. Das soll bitte auch
 * so bleiben! Wenn hier mal was dazu kommen sollte was unbedingt die a_t.php braucht bitte irgendwie dummy-Methoden
 * einbauen damit das Ding auch weiterhin OHNE laufen kann. Ungetestetes, hypothetisches Bsp.:
 *
 * if (defined(DEFAULT_CURRENCY))
 * {
 *     function schwarzfusskatze() { return 'Miau!'; }
 * }
 * else
 * {
 *     function schwarzfusskatze() { trigger_error('schwarzfusskatze() needs application_top.php!', E_USER_ERROR); }
 * }
 */




require_once(dirname(__FILE__).'/../database.inc.php');
require_once(dirname(__FILE__).'/product_functions.inc.php');
$GLOBALS['price_functions']['currency_cache'] = array();

/**
 * Formatiert einen Preis zur Anzeige
 * Achtung: Diese Funktion ist NUR zur Formatierung gedacht. Der Preis muss VORHER in die W�hrung konvertiert werden!
 * Achtung 2: Manche Produkte haben Attribute mit Aufpreis und m�ssen daf�r ein "ab" vor den Preis schreiben, damit
 *            dies ermittelt werden kann muss im 2. Parameter die Produkt-ID mitgegeben werden.
 *
 * @param float $price Komplett berechneter Preis
 * @param int|NULL $products_id_for_flag_checks Produkt-ID um nach bestimmten Flags zu suchen (ab XXX� bei Attributen, etc.)
 * @param string|NULL $currency W�hrung des Preises. Wenn NULL wird die aktuelle W�hrung des Shops genommen.
 * @return string Mit passenden W�hrungs- und Trennzeichen formatierter Preis
 */
function format_price($price, $products_id_for_flag_checks = NULL, $currency = NULL)
{
	if (!is_string($currency))
		$currency = $_SESSION['currency'];

	$product_has_price_changes = false;
	if (is_numeric($products_id_for_flag_checks))
	{
		$formula = get_cached_formula($products_id_for_flag_checks);

		$product_has_price_changes = ($formula['especialnesses'] ? true : false);
	}

	$currency_data = __get_cached_currency_data($currency);

	// Preis als String mit passenden Trennzeichen der W�hrung formatieren
	$formatted_price = number_format($price, $currency_data['decimal_places'], $currency_data['decimal_point'], $currency_data['thousands_point']);

	// Symbol(e) anh�ngen
	$formatted_price = trim($currency_data['symbol_left'].' '.$formatted_price.' '.$currency_data['symbol_right']);

	if ($product_has_price_changes)
		$formatted_price = '<span class="price_from">'.FROM.'</span> '.$formatted_price;

	return $formatted_price;
}

/**
 * Holt den Preis eines Produktes anhand der ID und ggfs. gew�hlter Attribute in der Standardw�hrung
 *
 * Die Preise der Attribute werden aus der Formel die zum Zeitpunkt des Aufrufs in der Datenbank steht gebildet
 *
 * @param int $pID Produkt-ID
 * @param array|NULL $attributes Gew�hlte Attribute
 * @return float Preis des Produktes in der Standardw�hrung
 */
function get_products_price($pID, $attributes = NULL)
{
	static $cached_product_data = array();
	static $cached_special_prices = array();

	if (!isset($cached_product_data[$pID]))
	{
		$sql = '
			SELECT `products_price`
			FROM `products`
			WHERE `products_id` = '.sqlval($pID).'
		';
		$cached_product_data[$pID] = query($sql);
	}

	$product_data = $cached_product_data[$pID][0];

	if (!$product_data)
	{
		trigger_error('Product ID "'.$pID.'" not found in database!', E_USER_WARNING);
		return false;
	}

	$products_price = floatval($product_data['products_price']);

	// Ggfs. Aufpreise der gew�hlten Attribute aufrechnen
	if (is_array($attributes))
	{
		$formula_array = get_cached_formula($pID);

		foreach ($attributes as $attr_name => $value)
		{
			$extra_charge = get_extra_charge_for_attribute_value($formula_array['especialnesses'], $attr_name, $value);

			if ($extra_charge)
				$products_price+= $extra_charge;
		}
	}

	// Pr�fen ob es f�r dieses Produkt ein Special gibt
	if (!isset($cached_special_prices[$pID]))
	{
		$sql = '
				SELECT specials.discount_in_percent
				FROM specials
				JOIN products USING (products_id)
				WHERE products.products_status = 1
					AND products.products_id = '.sqlval($pID).'
					AND specials.start_date = DATE(NOW())
					AND specials.expires_date = DATE(NOW())
					AND specials.status = 1
		';
		$cached_special_prices[$pID] = query($sql);
	}

	$special_discount = $cached_special_prices[$pID][0]['discount_in_percent'];

	if ($special_discount)
		$products_price*= (100-$special_discount) / 100;

	return $products_price;
}

/**
 * Konvertiert einen Preis von der Standardw�hrung in die Zielw�hrung
 * Bequemlichkeitsfunktion die automatisch den Umrechnungskurs aus der DB holt und {@link convert_price_by_exchange_rate()} aufruft
 *
 * @param float $price Zu konvertierender Preis in der Standardw�hrung
 * @param string|NULL $target_currency Key der Zielw�hrung. Wenn NULL wird die aktuelle W�hrung des Shops genommen.
 * @return float Konvertierter Preis in der Zielw�hrung
 */
function convert_price_to_currency($price, $target_currency = NULL)
{
	if (!is_string($target_currency))
		$target_currency = $_SESSION['currency'];

	$target_currency_data = __get_cached_currency_data($target_currency);

	return convert_price_by_exchange_rate($price, $target_currency_data['value']);
}


/**
 * Konvertiert einen Preis von einer bestimmten W�hrung in die Standardw�hrung
 *
 * @param float $price Zu konvertierender Preis in der Quellw�hrung
 * @param string|NULL $source_currency Key der Quellw�hrung. Wenn NULL wird die aktuelle W�hrung des Shops genommen.
 * @return float Konvertierter Preis in der Standardw�hrung
 */
function convert_price_from_currency($price, $source_currency = NULL)
{
	if (!is_string($source_currency))
		$source_currency = $_SESSION['currency'];

	$source_currency_data = __get_cached_currency_data($source_currency);

	return $price / $source_currency_data['value'];
}

/**
 * Rechnet einen Preis mit dem angegebenen Umrechnungskurs um
 *
 * Ja, das Ding macht einfach nur return $price*rate;
 * @param float $price Preis in der Standardw�hrung
 * @param float $rate Umrechnungskurs der Zielw�hrung zur Standardw�hrung
 * @return string
 */
function convert_price_by_exchange_rate($price, $rate)
{
	return floatval($price) * floatval($rate);
}

/**
 * Rechnet den Preis in der Standardw�hrung in die angegebene W�hrung um und formatiert ihn f�r die Ausgabe
 *
 * Bequemlichkeitsmethode die im Grunde nur {@link convert_price_to_currency()} und {@link format_price()} aufruft
 *
 * @param float $price Preis in der Standardw�hrung des Shops
 * @param int|NULL $products_id Produkt-ID um nach bestimmten Flags zu suchen (ab XXX� bei Attributen, etc.)
 * @param string|NULL $currency W�hrung des Preises. Wenn NULL wird die aktuelle W�hrung des Shops genommen.
 * @return string Mit passenden W�hrungs- und Trennzeichen formatierter und umgerechneter Preis
 */
function convert_and_format_price($price, $products_id = NULL, $currency = NULL)
{
	return format_price(convert_price_to_currency($price, $currency), $products_id, $currency);
}

/**
 * Interne Funktion die die W�hrungsdaten aus der Datenbank zwischenspeichert und zur�ckgibt
 *
 * @param string $currency_code
 * @return array
 */
function __get_cached_currency_data($currency_code)
{
	// W�hrung aus der Datenbank holen
	if (!$GLOBALS['price_functions']['currency_cache'][$currency_code])
	{
		$sql = 'SELECT * FROM `currencies` WHERE `code` = '.sqlval($currency_code);
		$GLOBALS['price_functions']['currency_cache'][$currency_code] = array_pop(query($sql));

		// Wenn die W�hrung in der Datenbank nicht gefunden wurde muss ein Fehler ausgegeben werden
		if (!$GLOBALS['price_functions']['currency_cache'][$currency_code])
		{
			trigger_error('Currency '.$currency_code.' does not exist!', E_USER_ERROR);
			return false;
		}
	}

	return $GLOBALS['price_functions']['currency_cache'][$currency_code];
}

/**
 * Rundet einen Preis entsprechend der Nachkommastellen der W�hrung.
 *
 * @param float $price Preis in der entsprechenden W�hrung
 * @param string|NULL $currency W�hrung des Preises. Wenn NULL wird die aktuelle W�hrung des Shops genommen.
 * @return float Gerundeter Preis
 */
function round_price($price, $currency = NULL)
{
	if (!is_string($currency))
		$currency = $_SESSION['currency'];

	// Preis auf/abrunden
	$currency_data = __get_cached_currency_data($currency);
	return round($price, $currency_data['decimal_places']);
}

/**
 * Rechnet den Preis in der Standardw�hrung in die angegebene W�hrung um und rundet ihn entsprechende der Nachkommastellen der W�hrung.
 *
 * Bequemlichkeitsmethode die im Grunde nur {@link convert_price_to_currency()} und {@link round_price()} aufruft
 *
 * @param float $price Preis in der Standardw�hrung des Shops
 * @param string|NULL $currency W�hrung des Preises. Wenn NULL wird die aktuelle W�hrung des Shops genommen.
 * @return float Konvertierter und gerundeter Preis
 */
function convert_and_round_price($price, $currency = NULL)
{
	// Preis in die W�hrung konvertieren
	$price = convert_price_to_currency($price, $currency);

	// Preis auf/abrunden
	return round_price($price, $currency);
}