<?php

require_once('includes/database.inc.php');
require_once('includes/functions/product_query_logger.php');

function perform_search_and_give_filters_data($q,$language_id,$special=null)
{
	return _search($q,$language_id,'filters_data',$special);
}

function plan_search_and_give_sql_fragments($q,$language_id,$special=null)
{
	return _search($q,$language_id,'sql_fragments',$special);
}

function _search($q,$language_id,$return_mode,$special)
{
	// Dieses Pattern ist vielleicht nicht auf den ersten Blick eindeutig, daher kurze Erkl�rung:
	// Die verschiedenen Teile des SELECTs (Felder, Tabellen, AND-verkn�pfte WHERE-Bedingungen) werden
	// in Arrays geesammelt. Dabei bekommen sie sprechende Keys, so da� sie nicht doppelt hinzugef�gt
	// werden. Anschlie�end werden sie zusammenimplodet. So kann jede Eingabebedingung halbwegs
	// �bersichtlich bestimmen, was dazu im SQL n�tig ist.

	$select_expressions = array('NULL');
	$table_references = array();
	$where_conditions_and = array('TRUE');
	$group_clause = array();
	$order_clause = array();

	$first_table_reference = 'products';
	$where_conditions_and []= 'products_status';

	if ($return_mode == 'filters_data')
	{
		$select_expressions []= 'cm_filters.*';
		$select_expressions []= 'GROUP_CONCAT(DISTINCT filter_value_id) AS value_in_clause';
		$select_expressions []= 'categories_description.categories_name';
		$table_references []= 'JOIN sphinx_intermediate ON (product_id = products_id)';
		$table_references []= 'JOIN cm_filter_values USING (filter_value_id)';
		$table_references []= 'JOIN cm_filters USING (filter_id)';
		$table_references []= 'JOIN categories ON (category_id = categories_id)';
		$table_references []= 'JOIN categories_description USING (categories_id)';
		$where_conditions_and []= 'categories_description.language_id = '.sqlval($language_id);
		$where_conditions_and []= 'cm_filters.show_in_searches'; // Mit dieser Funktion ermitteln eh nur die beiden Such-Modi ihre Filter, das normale Kategorielisting holt sich die Filter selber
		$group_clause []= 'filter_id';
		$order_clause []= 'cm_filters.products_field, categories.sort_order'; // products_field ist wichtig f�r das Einf�gen der Trenner
	}

	// Achtung, das ist ein bi�chen unsauber: Beim Verarbeiten der SQL-Fragmente in ajax_get_product_listing.php
	// werden Annahmen dar�ber getroffen, was hier gemacht wird. Das ist zu beachten, wenn man hier was �ndert.
	if ($special == 'new_products')
	{
		// WELCHE Filter angezeigt werden, bestimmen die Produkte die im l�ngsten der w�hlbaren Zeitr�ume hinzugekommen sind

		$start_datetime = array_pop(array_pop(query('SELECT IF(MAX(date_purchased) < NOW() - INTERVAL 3 MONTH, MAX(date_purchased), NOW() - INTERVAL 3 MONTH) FROM orders WHERE customers_id = '.sqlval($_SESSION['customer_id']).'')));
		$where_conditions_and['period'] = 'products_date_added > '.sqlval($start_datetime).'';
	}
	elseif ($special == 'custom_search')
	{
		if ($q == 'picture')
		{
			$where_conditions_and []= 'filter_is_picture';
		}
		if ($q == 'glow')
		{
			$where_conditions_and []= 'products_model IN ("GDSTN", "GDSPL", "GDSO-RING")';
		}
	}
	elseif (substr($q,0,1) == '#')
		$where_conditions_and []= 'products_model='.sqlval(substr($q,1));
	else
	{
		$where_conditions_and []= '(products_model LIKE "%'.str_replace('%','\\%',addslashes($q)).'%" OR products_name LIKE "%'.str_replace('%','\\%',addslashes($q)).'%")';
		$table_references ['products_description'] = 'JOIN products_description pd USING (products_id)';
		$where_conditions_and['language_product'] = 'pd.language_id = '.sqlval($language_id);
	}

	$fragment_array = array(
		'select_expressions' => $select_expressions,
		'first_table_reference' => $first_table_reference,
		'table_references' => $table_references,
		'where_conditions_and' => $where_conditions_and,
		'group_clause' => $group_clause,
		'order_clause' => $order_clause,
	);

	if ($return_mode == 'sql_fragments')
		return $fragment_array;

	$sql = build_select_from_fragments($fragment_array);

	$start_time = microtime(true);
	$result = query($sql);
	$end_time = microtime(true);
	product_query_logger($start_time,$end_time,'s',$sql);

	return $result;
}

function build_select_from_fragments($fragment_array)
{
	$group_clause_sql = $fragment_array['group_clause'] ? 'GROUP BY '.implode(',',$fragment_array['group_clause']) : '';

	$order_clause_sql = $fragment_array['order_clause'] ? 'ORDER BY '.implode(',',$fragment_array['order_clause']) : '';

	$having_conditions_and_sql = $fragment_array['having_conditions_and'] ? 'HAVING '.implode(' AND ',$fragment_array['having_conditions_and']) : '';

	return '
		SELECT '. implode(',',$fragment_array['select_expressions']) .'
		FROM '.$fragment_array['first_table_reference'].'
		'.implode(' ',$fragment_array['table_references']).'
		WHERE
		'. implode(' AND ',$fragment_array['where_conditions_and']) .'
		'.$group_clause_sql.'
		'.$having_conditions_and_sql.'
		'.$order_clause_sql.'
	';
}

function mode_is($test_mode)
{
	// Normalerweise verarbeiten wir nat�rlich kein POST in ner Funktion, aber hey... is xtCommerce

	if ($_GET['d'] && $_GET['d'] != 'false')
		$mode = 'new_products';
	elseif ($_GET['cs'])
		$mode = 'custom_search';
	elseif ($_GET['q'] && $_GET['q'] != 'false')
		$mode = 'search';
	elseif ($_GET['cat_id'] || $_GET['c'])
		$mode = 'category';
	else
		$mode = 'none';

	return ($test_mode == $mode);
}
