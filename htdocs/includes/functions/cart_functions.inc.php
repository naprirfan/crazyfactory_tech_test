<?php

/**
 * Berechnet den gestaffelten Rabatt anhand der discount-f�higen Produkte im Warenkorb
 *
 * @param bool $text_for_order_total Wenn wahr wird statt SUB_TITLE_SUB_RABATT MODULE_ORDER_TOTAL_RABATTPERCENT_ORDERTOTAL f�r den title genommen.
 * @return array Array mit folgenden Keys: calculated_discount = ausgerechneter Rabatt in der W�hrung des Shops, title = Titel f�r die Darstellung im Template
 */
function calculate_graduated_discount($text_for_order_total = false)
{
	$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

	$discount_threshold = $discount_percentage = 0;
	$array_return = array();

	$shoppingcart_products_price_sum = 0;
	$array_products = $_SESSION['cart']->get_products(); // Holt eine mit Produktdaten angereicherte (siehe attach_old_cart_values_to_shopping_cart_ng_product_arrays) Warenkorbliste

	$discount_percentage = 0;
	foreach (($array_products ? $array_products : array()) as $product_data) // bl�der Workaround gegen das Warning
	{
		if ($product_data['discountable'])
			$shoppingcart_products_price_sum += intval($product_data['quantity']) * floatval($product_data['individualprice']);
	}

	$coupon = $cart->get_coupon_data();
	if ($coupon && $coupon['coupon_type'] == 'P')
	{
		$discount_percentage = $coupon['coupon_percent'] ;
		$discount_threshold = 0;
	}


	$i = 1;
	while (defined("MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER".$i))
	{
		$current_threshold = convert_price_to_currency(constant("MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER".$i));

		if ($current_threshold && $shoppingcart_products_price_sum >= $current_threshold && constant("MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT".$i) > $discount_percentage)
		{
			$discount_percentage = constant("MODULE_ORDER_TOTAL_RABATTPERCENT_PERCENT_RABATT".$i);
			$discount_threshold = constant("MODULE_ORDER_TOTAL_RABATTPERCENT_PRICE_OVER".$i);
		}

		$i++;
	}

	if ($discount_percentage)
	{
		if ($shoppingcart_products_price_sum)
		{
			$text = ($text_for_order_total ? MODULE_ORDER_TOTAL_RABATTPERCENT_ORDERTOTAL : SUB_TITLE_SUB_RABATT);
			$title = sprintf($text, $discount_percentage, format_price(convert_and_round_price($discount_threshold)));
			$calculated_discount = $shoppingcart_products_price_sum * ($discount_percentage / 100);

			$array_return = array(
				'calculated_discount' => round_price($calculated_discount),
				'title' => $title
			);
		}
	}

	return $array_return;
}

/**
 * Berechnet die Lieferkosten f�r einen Gesamtpreis. Erwartet die Preise bereits in der momentan aktiven W�hrung.
 *
 * @param float $total Gesamtpreis
 * @return float|int Lieferkosten oder 0 bei kostenloser Lieferung
 */
function calculate_shipping_costs($total)
{
	$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);
	$cupon = $cart->get_coupon_data();

	// Versandkosten
	$query = array_pop(query('SELECT `configuration_value` FROM `configuration` WHERE `configuration_key` = "MODULE_SHIPPING_FLAT_COST"'));
	$costs = convert_price_to_currency(array_pop($query));

	$query = array_pop(query('SELECT `configuration_value` FROM `configuration` WHERE `configuration_key` = "MODULE_SHIPPING_FLAT_FOR_FREE_THRESHOLD"'));
	$shipping_for_free_threshold = convert_price_to_currency(array_pop($query));

	$cart_contains_regular_products = count($cart->get_all_entries()) != 0; // $cart->get_entry_count() w�rde Gutscheine mitz�hlen

	if ($total > $shipping_for_free_threshold || $cupon['coupon_type'] == 'S' || !$cart_contains_regular_products)
		return 0;
	else
		return $costs;
}