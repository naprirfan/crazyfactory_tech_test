<?php
require_once(dirname(__FILE__).'/../database.inc.php');

function cm_missing_attribute_log()
{
	$sql = '
		INSERT INTO cm_log (
			session,
			server,
			backtrace,
			post,
			customers_id,
			entry_datetime
		) VALUES (
			'.sqlval(serialize($_SESSION)).',
			'.sqlval(serialize($_SERVER)).',
			'.sqlval(serialize(debug_backtrace())).',
			'.sqlval(serialize($_POST)).',
			'.sqlval($_SESSION['customer_id']).',
			NOW()
		)
	';
	// wir deaktivieren das erst mal wieder .. query($sql);
}