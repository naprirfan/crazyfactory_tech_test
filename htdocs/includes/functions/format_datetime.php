<?php
function format_datetime($datetime, $date_only = false, $languages_id = null)
{
	if ($datetime == '0000-00-00' || $datetime == '0000-00-00 00:00:00')
		return '-';
		
	if (!is_object($datetime))
		$datetime = new DateTime($datetime);

	if (!$languages_id)
		$languages_id = $_SESSION['languages_id'];

	$sql = '
		SELECT
			date_format,
			time_format
		FROM languages
		WHERE languages_id = '.sqlval($languages_id).'
	';
	$format_data = query($sql);

	$format = $format_data[0]['date_format'];
	if (!$date_only)
	{
		if ($format_data[0]['time_format'] == 12)
			$format .= ' h:i:s a';
		else
			$format .= 'H:i:s';
	}

	return $datetime->format($format);
}