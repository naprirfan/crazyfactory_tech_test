<?php

function convert_cash_points_to_actual_money($cash_points)
{
	return convert_price_to_currency($cash_points * CASH_POINTS_TO_MONEY_MODIFIER);
}

/**
 * Gibt die Anzahl der CashPoints zur�ck die der Benutzer momentan hat
 *
 * @param int|null $customers_id Kunden ID dessen CashPoints abgefragt werden sollen. Wenn nicht gesetzt wird der momentan angemeldete Kunde genommen.
 * @return int Anzahl der CashPoints im Kundenkonto
 */
function get_user_cash_points($customers_id = NULL)
{
	if (!is_numeric($customers_id))
		$customers_id = $_SESSION['customer_id'];

	$sql = '
		SELECT SUM(`amount`)
		FROM `cm_cash_points`
		WHERE `customers_id` = ' . sqlval($customers_id) . '
	';

	return intval(array_pop(array_pop(query($sql))));
}

/**
 * Generiert einen Gutscheincode f�r Crazy Cash Points. Wenn eine $customers_id �bergeben wird wird der Gutschein an
 * diesen Kunden gebunden.
 *
 * @param int $amount Menge der Cash Points im Gutschein
 * @param int $expire_timestamp Ablaufzeitpunkt des Gutscheins
 * @param int|null $customers_id Kunden-ID an die der Gutschein gebunden werden soll
 * @return string Generierter Code
 */
function generate_cash_points_coupon($amount, $expire_timestamp, $customers_id = NULL)
{
	// Coupon generieren und auf duplikat pr�fen
	$unique_coupon_code_generated = false;
	while (!$unique_coupon_code_generated)
	{
		$code = __generate_coupon_string();

		if (!array_pop(array_pop(query('SELECT COUNT(*) FROM `cm_cash_point_coupons` WHERE `code` = '.sqlval($code)))))
			$unique_coupon_code_generated = true;
	}

	// Sicherstellen, dass das Ablaufdatum korrekt eingetragen wird (Zeitverschiebung und so)
	$expire_date = date('Y-m-d', $expire_timestamp);

	query('
		INSERT INTO `cm_cash_point_coupons`
		SET `code` = '.sqlval($code).',
			`expire_date` = '.sqlval($expire_date).',
			`customers_id` = '.sqlval($customers_id).',
			`amount` = '.sqlval($amount).'
	');

	return $code;
}

function __generate_coupon_string()
{
	// http://stackoverflow.com/questions/3521621/php-code-for-generating-decent-looking-coupon-codes-mix-of-alphabets-and-numbers/3521643#3521643
	$chars = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ'; // Ich hab mal 0, O, 1, und I weggelassen wegen Verwechslungsgefahr
	$chars_len = strlen($chars)-1;
	$code = 'CCP';
	for ($i = 0; $i < 7; $i++)
	{
		$code.= $chars[mt_rand(0, $chars_len)];
	}

	return $code;
}

/**
 * L�st einen CCP Gutschein ein.
 *
 * @param string $code Gutscheincode
 * @param int|null $customers_id Kundennummer f�r die der Gutschein bestimmt ist. Fallback auf aktuell angemeldeten Benutzer wenn NULL.
 * @return array array('error' => true|false, 'msg' => Fehlernachricht, 'amount' => eingel�ste Cash Points)
 */
function redeem_cash_points_coupon($code, $customers_id = NULL)
{
	// Fallback auf den aktuellen Benutzer
	if (!$customers_id)
		$customers_id = $_SESSION['customer_id'];

	// Gutschein Code laden
	$array_coupon_code_data = array_pop(query('
		SELECT *, UNIX_TIMESTAMP(`expire_date`) as `timestamp_expire`, UNIX_TIMESTAMP(DATE(NOW())) as `timestamp_now`
		FROM `cm_cash_point_coupons`
		WHERE UPPER(`code`) = '.sqlval(strtoupper($code)).'
	'));

	// Pr�fen ob der Gutschein gefunden wurde und noch g�ltig ist
	if ($array_coupon_code_data['coupon_id'])
	{
		if ($array_coupon_code_data['timestamp_expire'] && $array_coupon_code_data['timestamp_expire'] < $array_coupon_code_data['timestamp_now'])
			return array('error' => true, 'msg' => CASHPOINTS_COUPON_EXPIRED);
		
		// Pr�fen ob dieser Gutschein schonmal eingel�st wurde
		if (!$array_coupon_code_data['one_time_usage'])
			$coupon_code_used = array_pop(array_pop(query('
				SELECT COUNT(*)
				FROM `cm_cash_points`
				WHERE `customers_id` = '.sqlval($customers_id).'
				  AND `coupon_id` = '.sqlval($array_coupon_code_data['coupon_id']).'
			')));
		else
			$coupon_code_used = ($array_coupon_code_data['one_time_usage'] && $array_coupon_code_data['used_by_customers_id']);

		if (!$coupon_code_used)
		{
			query('
				INSERT INTO `cm_cash_points`
				SET `customers_id` = '.sqlval($customers_id).',
					`action` = "coupon",
					`amount` = '.sqlval($array_coupon_code_data['amount']).',
					`date_create` = NOW(),
					`coupon_id` = '.sqlval($array_coupon_code_data['coupon_id']).'
			');

			if ($array_coupon_code_data['one_time_usage'] && !$array_coupon_code_data['used_by_customers_id'])
				query('
					UPDATE `cm_cash_point_coupons`
					SET `used_by_customers_id` = '.sqlval($customers_id).'
					WHERE `coupon_id` = '.sqlval($array_coupon_code_data['coupon_id']).'
				');

			return array('error' => false, 'amount' => $array_coupon_code_data['amount']);
		}
		else
			return array('error' => true, 'msg' => CASHPOINTS_COUPON_ALREADY_USED);
	}
	else
		return array('error' => true, 'msg' => CASHPOINTS_COUPON_NOT_FOUND);
}

// zum gro�teil von der xtc_set_cashpoints_for_paid_order() �bernommen
function calculate_gained_cashpoints($orders_id)
{
	// Wert holen (nur sinnvoll wenn noch nie welche vergeben wurden, zB. direkt nach der Bestellung)
	$sql = '
		SELECT
			o.orders_id,
			o.currency_value,
			ot.value,
			ot.class
		FROM orders o
		INNER JOIN orders_total ot ON (o.orders_id = ot.orders_id )
		LEFT JOIN cm_cash_points cp ON (o.orders_id = cp.orders_id AND cp.action = "order_bonus")
		WHERE o.orders_id = '.xtc_db_input($orders_id).'
	';
	$check_query = xtc_db_query($sql);
	$raw_value = 0;
	while ($check_data = xtc_db_fetch_array($check_query))
	{
		$currency_value = $check_data['currency_value'];

		if ($check_data['class'] != 'ot_total' AND $check_data['class'] != 'ot_shipping' AND $check_data['class'] != 'ot_tax')
			$raw_value = $raw_value + $check_data['value'];
	}
	if ($raw_value > 0)
		return ceil(($raw_value/$currency_value)*5);  // Vorgabe: Immer aufrunden.
	else
		return false;
}