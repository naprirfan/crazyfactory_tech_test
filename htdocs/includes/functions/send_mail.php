<?php
function df_mail_send($to,$subject,$text = null,$html = null,$options = array())
{
    $default_reply_address = $options['default_reply_address'];
    $default_reply_Name = $options['default_reply_address'];

    // Erzeuge neue PHPMailer Instanz
    $mail = new PHPMailer();

    // Mail SMTP Einstellungen aus der Config in der Mail-Instanz setzen
    $mail->IsSMTP();
    if ($mail_send_config['smtp_auth'])
        $mail->SMTPAuth = true;
    $mail->Host = $mail_send_config['host'];
    $mail->Username = $mail_send_config['username'];
    $mail->Password = $mail_send_config['password'];

    // Absender Eigenschaften festlegen
    $mail->From = $mail_send_config['from'];
    $mail->FromName = $mail_send_config['from_name'];
    $mail->AddReplyTo($default_reply_address, $default_reply_Name);

    $debug_active = $options['debug'];
    $debug_address = $options['debug_address'];
    $debug_name = $options['debug_name'];

    if ($debug_active)
    {
        $mail->AddAddress($debug_address, $debug_name);
    }
    else
    {
        // Angegebenen Empf�nger eintragen
        $mail->AddAddress($to);

        // Standard BCC?
        $default_bcc_address = $options['default_bcc_address'];
        if ($default_bcc_address)
            $mail->AddBCC($default_bcc_address);
    }

    // M�glichkeit einen Zeilenumbruch nach einer bestimmten Buchstabenanzahl einzuf�gen
    if (is_numeric($options['plaintext_word_wrap']))
     $mail->WordWrap = $options['plaintext_word_wrap'];

    // EMail Betreff hinzuf�gen
    $mail->Subject = $subject;

    // Sollte HTML Content �bergeben worden sein, wird der Textinhalt automatisch als alternativer Content eingebunden
    if (isset($html))
    {
        // PHPMailer kann im Email-Inhalt enthaltene img-Tags finden und das referenzierte Bild als Anhang an die Mail anh�ngen.
        // Man kann Bilder aber auch explizit mittels des Images-Arrays in den Optionen gezielt angeben.
        if ($options['parse_images_from_content'])
            $mail->MsgHTML($html);
        else
        {
            // Standard HTML Fall. Eingebette Bilder m�ssen mittels des optionalen ...
            // Images-Arrays in den Optionen behandelt werden (g�ltige 'cid' �bergeben).
            $mail->IsHTML(true);
            $mail->Body = $html;
        }

        $mail->AltBody = $text;
    }
    else
        $mail->Body = $text;

    // Mail-Anh�nge behandeln
    if (is_array($options['attachments']) && count($options['attachments'] > 0))
    {
        foreach($options['attachments'] as $attachment)
        {
            if (file_exists($attachment['file_path']))
            {
                $mail->AddAttachment($attachment['file_path'], $attachment['file_name']);
            }
        }
    }

    // Mail-Bilder behandeln
    if (is_array($options['images']) && count($options['images'] > 0))
    {
        foreach($options['images'] as $image)
        {
            if (file_exists($image['file_path']))
            {
                $fileParts = split("\.", basename($image['file_path']));
          $ext = $fileParts[1];
          $mimeType = $mail->_mime_types($ext);

                $mail->AddEmbeddedImage($image['file_path'], $image['cid'], $image['file_name'], 'base64', $mimeType);
            }
        }
    }

    $error_mail = $options['error_mail'];
    $error_mail_address = $options['error_mail_address'];
    $error_mail_name = $options['error_mail_name'];

    if ($error_mail)
    {
		$mail->ClearReplyTos();
		$mail->From = $error_mail_address;
		$mail->FromName = $error_mail_name;
		$mail->AddReplyTo($error_mail_address, $error_mail_name);
    }

    return $mail->Send();
}