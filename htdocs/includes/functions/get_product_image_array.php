<?php

require_once(DIR_FS_DOCUMENT_ROOT.'/includes/database.inc.php');
require_once(DIR_FS_DOCUMENT_ROOT.'/includes/classes/shopping_cart_ng.php');

// Ich hab KEINE AHNUNG warum der schei� doppelt auftaucht aber ich krieg dauernd redeclare-Fehler
if (!defined('GET_PRODUCT_IMAGE_ARRAY_PHP')) // Das ist von C++ Headern abgeguckt, falls sich einer wundert
{
	define('GET_PRODUCT_IMAGE_ARRAY_PHP', true);

	function add_missing_image_entry($path, $id)
	{
		$sql = '
			INSERT IGNORE INTO `cm_missing_images`
			SET `image` = '.sqlval($path).',
				`product_id` = '.sqlval($id).'
		';

		query($sql);
	}

	function add_missing_variantimage($product_id, $attributes, $path)
	{
		$json_attributes = json_encode($attributes);

		$sql = '
			INSERT IGNORE INTO `cm_missing_variantimages`
			SET `product_id` = ' . sqlval($product_id) . ',
				`attributes` = ' . sqlval($json_attributes) . ',
				`image` = ' . sqlval($path) . '
		';

		query($sql);
 	}

	function __check_file($path)
	{
		if (DISABLE_IMAGE_FS_CHECK === true)
			return true;

		if (file_exists($path))
			return true;
		else
			return false;
	}

	function construct_attribute_array($constructor, $forced_attributes_constructor, $data)
	{
		static $check_func;
		if (!isset($check_func))
			$check_func = create_function('$val', 'return !empty($val) && $val != "*";');

		$array_return = array();

		if (is_string($data))
		{
			$AttributesKeys = array_merge(
				array_filter(
					preg_split('/(\[|\]|\]\[)/', $constructor),
					$check_func
				)
			);
			$AttributesValues = array_merge(
				array_filter(
					preg_split('/(\[|\]|\]\[)/', $data),
					$check_func
				)
			);

			if ($AttributesKeys && count($AttributesKeys) == count($AttributesValues))
			{
				$array_return = array_combine($AttributesKeys, $AttributesValues);
				foreach ($array_return as $key => $val)
					if ($val == '-')
						unset($array_return[$key]);
			}
		}
		else
		{
			// Sauberes Attribut-Array erstellen (sprich: Unbekannte Attribute entfernen)
			$constructor_tmp = __prepare_constructor_array($constructor);

			foreach ($constructor_tmp as $current_attribute)
			{
				// Hier fehlt ein Attribut
				if (!$data[$current_attribute])
					continue;

				$array_return[$current_attribute] = $data[$current_attribute];
			}
		}

		// Erzwungene Attribute parsen
		$array_forced_attributes_tmp = explode('&', $forced_attributes_constructor);
		$array_forced_attributes = array();
		if ($array_forced_attributes_tmp)
			foreach ($array_forced_attributes_tmp as $le)
			{
				$separator_pos = strrpos($le, ':');
				if ($separator_pos !== false)
					$array_forced_attributes[substr($le, 0, $separator_pos)] = substr($le, $separator_pos+1);
			}

		// Erzwungene Attribute anwenden
		foreach ($array_forced_attributes as $key => $val)
			$array_return[$key] = $val;

		return $array_return;
	}

	function build_attribute_img_string($constructor, $array_attributes)
	{
		if (!$array_attributes)
			return false;

		$attr_str = $constructor;

		// Vorhandene Attribute im Konstruktor ersetzen
		foreach ($array_attributes as $key => $attr)
			$attr_str = str_replace('['.$key.']', $attr, $attr_str);

		// Sternchen ersetzen
		$attr_str = str_replace('*', '-', $attr_str);

		// B�se Zeichen rauswerfen
		$attr_str = str_replace(array('/', '\\', '.'), '', $attr_str);

		// Nicht gef�llte Attribute entfernen
		$attr_str = preg_replace('/\[[a-zA-Z]*\]/', '', $attr_str);

		return $attr_str;
	}

	function get_product_image_array($product_id, $attributes = null, $check_missing_variantimages = false)
	{
		static $image_path_webserver;
		if (!isset($image_path_webserver)) $image_path_webserver = DIR_WS_IMAGES.'product_images_2010/';
		static $image_path_filesystem;
		if (!isset($image_path_filesystem)) $image_path_filesystem = DIR_FS_DOCUMENT_ROOT.'images/product_images_2010/';

		if (!$attributes)
			$attributes = array();

		// Standardwerte wenn kein Bild vorhanden ist
		$array_return = array(
			'S' => $image_path_webserver.'noimage_s.gif',
			'M' => $image_path_webserver.'noimage_m.gif',
			'L' => $image_path_webserver.'noimage_l.gif',
			'TH' => $image_path_webserver.'noimage_th.gif'
		);
		// Wird sp�ter mit $array_return kombiniert
	    $array_types = array(
			'S_type' => 'noimage',
			'M_type' => 'noimage',
			'L_type' => 'noimage',
			'TH_type' => 'noimage'
	    );

		// Produktdaten aus der MySQL laden
		$sql = '
			SELECT
				`products_image`,
				`konstruktor`,
				`image_forced_attributes`,
				`products_model` as `model`
			FROM `products`
			WHERE `products_id` = '.sqlval($product_id).'
		';
		$product_data = array_pop(query($sql));

		if ($product_data)
		{
			// Basispfad der Bilder
			$base_path = $image_path_filesystem.$product_data['model'].'/'.str_replace('/', '', $product_data['model']);
			$base_path_web = $image_path_webserver.$product_data['model'].'/'.str_replace('/', '', $product_data['model']);

			// Pr�fen ob es f�r dieses Produkt ein Hauptbild gibt
			foreach (array_keys($array_return) as $le)
				if (__check_file($base_path.'_'.$le.'.JPG'))
				{
					$array_return[$le] = $base_path_web.'_'.$le.'.JPG';
				    $array_types[$le.'_type'] = 'main';
				}
				elseif ($le != 'L')
					add_missing_image_entry($base_path_web.'_'.$le.'.JPG', $product_id);
			// Wenn der Artikel einen Konstruktor hat m�ssen wir die Attribute zusammenschrauben
			if ($product_data['konstruktor'])
			{
				// Attributsachen zusammenbauen
				$array_attributes = construct_attribute_array($product_data['konstruktor'], $product_data['image_forced_attributes'], $attributes);

				// Bilderpfad f�r die Attribute erstellen
				foreach (array_keys($array_return) as $curr_img_type)
				{
					// Attributsuffix erstellen
					$attr_suffix = strtoupper(build_attribute_img_string($product_data['konstruktor'], $array_attributes));

					if ($attr_suffix)
					{
						$image_web_path = $base_path_web.$attr_suffix.'_'.$curr_img_type.'.JPG';
						// Pr�fen ob diese Datei existiert
						if (__check_file($base_path.$attr_suffix.'_'.$curr_img_type.'.JPG'))
						{
							$array_return[$curr_img_type] = $image_web_path;
						    $array_types[$curr_img_type.'_type'] = 'attribute';
						}
						elseif ($check_missing_variantimages && $le != 'L')
							add_missing_variantimage($product_id, $array_attributes, $image_web_path);
					}
				}
			}
		}

		return $array_return+$array_types;
	}
}