<?php

function curl_call($options=null)
{
	static $ch;

	if (!$ch)
		$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $options['url']);

	if (isset($options['timeout']))
		curl_setopt($ch, CURLOPT_TIMEOUT, $options['timeout']);
	else
		curl_setopt($ch, CURLOPT_TIMEOUT, 5); // Default-Timeout: 5 Sekunden

	curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

	// Redirects erlauben
	// Nur wenn open_basedir nicht gesetzt ist, denn sonst w�rde cURL es ohnehin verhindern, als Schutz vor "file://"-URLS
	if (!ini_get('open_basedir'))
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

	curl_setopt($ch, CURLOPT_HTTPGET, 1);

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, ($options['ignore_bad_certificates'] ? 0 : 2));

	if (isset($options['postfields']))
	{
		if (is_array($options['postfields']) && $options['no_multipart'])
		{
			$postfields = '';
			foreach ($options['postfields'] as $p => $v)
				$postfields .= urlencode($p).'='.urlencode($v).'&';
			$postfields = substr($postfields,0,-1);
		}
		else
			$postfields = $options['postfields'];

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
	}

	$result = curl_exec($ch);

	return $result;
}

// So kann man URLs in mehreren Zeilen angeben
function remove_breaks_and_tabs($text)
{
	return str_replace("\n",'',str_replace("\r",'',str_replace("\t",'',$text)));
}

function curl_get($url,$getfields,$options = null)
{
	$options['url'] = $url;

	if (is_array($getfields) && $getfields)
	{
		$options['url'] .= '?';
		foreach ($getfields as $p => $v)
			$options['url'] .= urlencode($p).'='.urlencode($v).'&';
		$options['url'] = substr($options['url'],0,-1);
	}

	return curl_call($options);
}

function curl_post($url,$postfields,$options = null)
{
	$options['url'] = $url;
	$options['postfields'] = $postfields;
	return curl_call($options);
}