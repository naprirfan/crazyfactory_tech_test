<?php

function send_mail($to, $body, $alt_body, $subject, $attachments = array(), $embedded_images = array(), $is_html = true)
{
	// Ich kann keinen PHPMailer includen, weil das offenbar die application_top.php schon macht... naja, hoffen wir mal, da� der es tut...
	$mail = new PHPMailer();

	$mail->CharSet = 'UTF-8';

	// Konfiguriert wird jener aber offenbar nicht... ach fickt euch, dann mach ich das halt hier...
	$mail->IsSMTP();
	$mail->Host = "mailout1.creations.de";
	$mail->SMTPAuth = false;
	$mail->Username = "";
	$mail->Password = "";

	$mail->From = "info@crazy-factory.com";
	$mail->FromName = "Crazy Factory";

	$mail->AddAddress($to, substr($to, 0, strpos($to, '@')));

	foreach ($attachments as $attachment)
		$mail->AddAttachment($attachment['path'], $attachment['name'] ? $attachment['name'] : '', $attachment['encoding'] ? $attachment['encoding'] : 'base64', $attachment['type'] ? $attachment['type'] : 'application/octet-stream');

	foreach ($embedded_images as $image)
		$mail->AddEmbeddedImage($image['path'], $image['cid'], $image['name'] ? $image['name'] : '', $image['encoding'] ? $image['encoding'] : 'base64', $image['type'] ? $image['type'] : 'application/octet-stream');

	$mail->IsHTML($is_html);

	$mail->Subject = $subject;
	$mail->Body    = $body;
	if ($is_html)
		$mail->AltBody = $alt_body;

	$mail->Send();
}

?>