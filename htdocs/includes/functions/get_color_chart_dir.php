<?php
function get_color_chart_dir($chart_type)
{
	$subdirs = array(
		'S' => 'svarowski/',
		'T' => 'titan/',
		'U' => 'uv/',
		'TJ' => 'tj/',
		'C' => 'c/',
		'G' => 'g/',
		'M' => 'm/',
		'X' => 'x/',
	);
	return $subdirs[$chart_type];
}