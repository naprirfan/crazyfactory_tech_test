<?php

// diverse Hilfsfunktionen existieren hier schon (bspw. __prepare_constructor_array)
require_once(dirname(__FILE__).'/../classes/shopping_cart_ng.php');

/**
 * Pr�ft ob eine Attributkombination anhand einer Formel valide ist. Pr�ft auch auf fehlende oder unbekannte Attribute!
 *
 * @param array $formula_rules {@link get_cached_formula()}
 * @param array $chosen_attributes Array mit den zu pr�fenden Attributen - array(attribut => wert, ...)
 * @return bool
 */
function check_attribute_combination($formula_rules, $chosen_attributes)
{
	// Wenn Attribute gew�hlt sind, aber der Artikel keine hat, ist das wie als wenn zuviele Attribute
	// gew�hlt w�ren, also false
	if (!$formula_rules && $chosen_attributes)
		return false;

	// Und wenn keine gew�hlt sind und der Artikel auch keine hat, dann ist das ok
	if (!$formula_rules && !$chosen_attributes)
		return true;

	$all_attribute_names = array();

	foreach ($formula_rules as $rule)
	{
		if ($rule['operator'] == '-')
		{
			$rule_applies = true;
			foreach ($rule['combinations'] as $attribute_name => $values)
				if (!$values[$chosen_attributes[$attribute_name]])
					$rule_applies = false;

			if ($rule_applies)
				return false;
		}

		// Gleichzeitig sammeln wir uns auch schon die Liste der m�glichen Attribute zusammen
		foreach ($rule['combinations'] as $le => $dummy)
			$all_attribute_names[$le] = true;
	}

	// Wenn nicht alle Attribute gesetzt wurden, ist der Artikel auf jeden Fall ung�ltig
	if (array_diff(array_keys($all_attribute_names), array_keys($chosen_attributes)))
		return false;

	$tmp_chosen_attributes = $chosen_attributes;
	foreach ($formula_rules as $rule)
	{
		if ($rule['operator'] == '+')
		{
			$array_combinations = $rule['combinations'];

			$rule_applies = true;
			foreach ($array_combinations as $attribute_name => $values) // ... wenn eine Regel ein Attribut betrifft...
				if ($tmp_chosen_attributes[$attribute_name]) // ... was auch bei der Kombination noch nicht legitimiert ist...
					if (!$values[$tmp_chosen_attributes[$attribute_name]]) // ... aber es einen Wert hat, den die Regel nicht legitimiert...
						$rule_applies = false; // ... trifft die Regel nicht zu.

			if ($rule_applies) // Wenn sie zutrifft...
				foreach (array_keys($array_combinations) as $attribute_name) // ...entfernen wir alle Attribute aus den noch zu pr�fenden
					unset($tmp_chosen_attributes[$attribute_name]); //    Attributen, denn sie sind damit erfolgreich legitimiert.

			if (!$tmp_chosen_attributes)
				return true;
		}
	}

	// Wenn nicht alle Attribute aus $tmp_chosen_attributes entfernt wurden, also ein Attribut einen Wert
	// hat, der in keiner Plus-Regel legitimiert wird, ist diese Attributkombination nicht erlaubt
	return false;
}

/**
 * Gibt den Aufpreis zu einem bestimmten Attribut-Wert zur�ck
 *
 * @param array $formula_especialnesses especialnesses-Teil eines Formel-Arrays {@see get_cached_formula()}
 * @param string $attribute Schl�ssel des Attributs
 * @param string $value Wert des Attributs
 * @return bool|float Aufpreis in der Standardw�hrung bzw. false wenn kein Aufpreis gesetzt
 */
function get_extra_charge_for_attribute_value($formula_especialnesses, $attribute, $value)
{
	return isset($formula_especialnesses[$attribute][$value]['extra_charge']) ? $formula_especialnesses[$attribute][$value]['extra_charge'] : false;
}

function get_cached_formula($product_id)
{
	static $formula_cache = array();

	if (isset($formula_cache[$product_id]))
		return $formula_cache[$product_id];

	$sql = '
		SELECT `formula_array`
		FROM `products`
		WHERE `products_id` = '.sqlval($product_id).'
	';
	$formula_cache[$product_id] = @unserialize(array_pop(array_pop(query($sql))));

	return $formula_cache[$product_id];
}

function get_possible_attribute_values_for_attribute($current_attribute_combination, $target_attribute, $product_id)
{
	$formula = get_cached_formula($product_id);
	$sql = 'SELECT `konstruktor` FROM `products` WHERE `products_id` = '.sqlval($product_id);
	$constructor = @array_pop(array_pop(query($sql)));
	$constructor_array = __prepare_constructor_array($constructor);

	$possible_values = array();

	// Vorherige Attribute sammeln
	$left_attributes = array();
	foreach ($constructor_array as $attribute_name)
	{
		if ($attribute_name == $target_attribute)
			break;

		$left_attributes[] = $attribute_name;
	}

	// Wir verlassen uns darauf, dass die +-Regeln zuerst kommen
	foreach($formula['rules'] as $rule)
	{
		$combinations = $rule['combinations'];

		if ($rule['operator'] == '+')
		{
			$rule_applies = true;

			// Wenn der gesuchte Attribut in der Regel nicht erw�hnt wird ist sie irrelevant
			if (!$combinations[$target_attribute])
				continue;

			// Wenn einer der bereits gew�hlten Attribute in der Regel vorkommt, aber NICHT mit dem gew�hlten Attributwert
			// trifft sie nicht zu
			foreach ($left_attributes as $le)
			{
				$selected_value = $current_attribute_combination[$le];

				if ($combinations[$le] && !$combinations[$le][$selected_value])
				{
					$rule_applies = false;
					break;
				}
			}

			if ($rule_applies)
				$possible_values+= $combinations[$target_attribute];
		}

		if ($rule['operator'] == '-')
		{
			$rule_applies = true;

			// Wenn der gesuchte Attribut in der Regel nicht erw�hnt wird ist sie irrelevant
			if (!$combinations[$target_attribute])
				continue;

			// Es m�ssen alle bereits gew�hlten Attributwerte in der Regel vorkommen damit sie zutreffen kann
			foreach ($combinations as $attribute => $values)
			{
				if ($attribute == $target_attribute)
					continue; // Den gesuchten Attribut k�nnen wir hier ignorieren weil der in den left_attributes nat�rlich NIE vorhanden ist

				if (!in_array($attribute, $left_attributes) ||
					!$values[$current_attribute_combination[$attribute]])
				{
					$rule_applies = false;
					break;
				}
			}

			// Wenn die Regel zutrifft m�ssen die dort spezifizierten Attributwerte wieder aus den m�glichen Werten entfernt werden
			if ($rule_applies)
				foreach ($combinations[$target_attribute] as $key => $dummy)
					unset($possible_values[$key]);
		}
	}

	return $possible_values;
}