<?php
function cancel_order($order_id, $originate = 'Info: customer', $override = 0)
{
	$order_id = (int)$order_id;
	$sql = '
		SELECT orders_status, customers_id, currency_value, currency, used_cashpoints, stock_freed, products_model
		FROM orders
		LEFT JOIN orders_products ON ( orders.orders_id = orders_products.orders_id
		AND products_model = "coupon" )
		WHERE orders.orders_id = '.$order_id.'
	';
	$order = xtc_db_fetch_array(xtc_db_query($sql));
	$order_status = $order['orders_status'];
	if (($order_status == 1
			|| $order_status == 7
			|| $order_status == 9
			|| $order_status == 10
			|| $order_status == 13
			|| $order_status == 14) || $override == 1)
	{

		if ($order['stock_freed'] != 1)
		{
			// alle Produkte auf Lagerbestandsverwaltung pr�fen
			$sql = '
					SELECT orders_products.products_id, products.products_stock_item, orders_products.products_quantity FROM orders_products
						INNER JOIN products ON (orders_products.products_id = products.products_id)
					WHERE orders_products.orders_id = '.$order_id.'
					';
			$res = xtc_db_query($sql);
			$stock_freed = 0;
			while ($order_products = xtc_db_fetch_array($res))
			{
				if ($order_products['products_stock_item'] == '1')
				{
					xtc_db_query("UPDATE products_stock SET stock = stock + '".$order_products['products_quantity']."' where products_id = '".$order_products['products_id']."' LIMIT 1");
					$stock_freed = 1;
				}

			}
			if ($stock_freed == 1)
				xtc_db_query("UPDATE orders SET stock_freed = 1 WHERE orders_id  = '".$order_id."' LIMIT 1");
		}

		if ($order['products_model'] == 'coupon')
		{
			$sql = 'DELETE FROM `coupon_redeem_track` WHERE `order_id` = '.$order_id;
			xtc_db_query($sql);
		}

		// storecredit l�schen.
		$sql = 'DELETE FROM `cm_cash_points` WHERE action = "store_credit" AND `orders_id` = '.$order_id;
		xtc_db_query($sql);

		// cashpoints checken
		$cashpoints = $order['used_cashpoints'];
		if ($cashpoints) // nur wenn mehr als 0
		{
			$sql = '
				UPDATE '.TABLE_ORDERS.'
				SET orders_status = 8,
					last_modified = now(),
					used_cashpoints = 0,
					comments = CONCAT(comments, "\n\n'.$cashpoints.' Cashpoints geloescht, wegen Stornierung der Rechnung.")
				WHERE orders_id = "'.xtc_db_input($order_id).'"
			';
			xtc_db_query($sql);

			$sql = '
				INSERT INTO cm_cash_points (
					customers_id,
					orders_id,
					action,
					amount,
					date_create
				) VALUES (
					'.xtc_db_prepare_input($order['customers_id']).',
					'.xtc_db_prepare_input($order_id).',
					"cancellation",
					'.xtc_db_prepare_input($cashpoints).',
					NOW()
				)
			';
			xtc_db_query($sql);

			$sql = '
				SELECT value * -1 as cp_value
				FROM orders_total
				WHERE orders_id = '.  xtc_db_prepare_input($order_id).'
					AND class="ot_cashpoints"
			';
			$q = xtc_db_fetch_array(xtc_db_query($sql));

			// da die W�hrung nicht umgerechnet werden muss auf dem kleinen Dienstweg mit der W�hrungsformatierung.
			$sql = '
				SELECT *
				FROM `currencies`
				WHERE code = "'.  xtc_db_prepare_input($order['currency']).'"
			';

			$curr_format = xtc_db_fetch_array(xtc_db_query($sql));
			$ordertotal = xtc_db_fetch_array(xtc_db_query('SELECT value FROM orders_total  WHERE orders_id = "'.xtc_db_input($order_id).'" AND class ="ot_total"'));
			$ordertotal_text = '<b>'.$curr_format['symbol_left'].' '.number_format(($ordertotal['value']+$q['cp_value']),2, $curr_format['decimal_point'],($curr_format['thousands_point'] ? $curr_format['thousands_point'] : "")).' '.$curr_format['symbol_right'].'</b>';
			$sql = '
				UPDATE orders_total
					SET value = '.($ordertotal['value']+$q['cp_value']).',
					text = "'.$ordertotal_text.'"
				WHERE orders_id = "'.xtc_db_input($order_id).'"
				AND class ="ot_total"';
			xtc_db_query($sql);


			$sql = '
				DELETE FROM orders_total
				WHERE orders_id = '.xtc_db_prepare_input($order_id).'
					AND class = "ot_cashpoints"
			';
			xtc_db_query($sql);
		}
		else
		{
			$sql = '
				UPDATE '.TABLE_ORDERS.'
				SET orders_status = 8,
					last_modified = now()
				WHERE orders_id = "'.xtc_db_input($order_id).'"
			';
			xtc_db_query($sql);
		}

		// Historyeintrag anlegen.
		if ($order_status != 8)
		{
			$sql = '
				INSERT INTO '.TABLE_ORDERS_STATUS_HISTORY.' (
					orders_id,
					orders_status_id,
					date_added,
					customer_notified,
					comments
				) VALUES (
					"'.xtc_db_input($order_id).'",
					8,
					now(),
					0,
					"'.xtc_db_input($originate.' canceled').'"
				)
			';
			xtc_db_query($sql);
		}
		return true;
	}
	else
		return false;
}

function get_cancel_link($filename, $order_id)
{
	return get_a_real_cancel_link_and_not_a_stupid_a_tag_with_button($filename, $order_id);
}

// Andr� hat gesagt ich darf das: fuck fuck FUCK FUUUUUUUUUUUUUUUUUUUUUUUUUUUUUCK!!! DAS IST �BERALL SO IN DIESER VERKACKTEN $�&%$�&%$�&�$ VON SOFTWARE!
function Get_A_Real_Cancel_Link_And_Not_A_Stupid_A_Tag_With_Button($filename, $order_id)
{
	$link = 'javascript:if(confirm(\''.CONFIRM_CANCEL_ORDER.'\')){location.href=\''.xtc_href_link($filename, 'action=cancel_order&order_id='.$order_id, 'SSL').'\';}';
	return $link;
}
?>