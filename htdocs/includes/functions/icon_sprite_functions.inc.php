<?php

$array_sprite_locations = array(
	'lang_icon_german' => array('x' => 0, 'y' => 0, 'h' => 20, 'w' => 20),
	'lang_icon_english' => array('x' => 20, 'y' => 0, 'h' => 20, 'w' => 20),
	'lang_icon_spanish' => array('x' => 40, 'y' => 0, 'h' => 20, 'w' => 20),
	'lang_icon_french' => array('x' => 60, 'y' => 0, 'h' => 20, 'w' => 20),
	'lang_icon_pt' => array('x' => 80, 'y' => 0, 'h' => 20, 'w' => 20),
	'lang_icon_netherlands' => array('x' => 100, 'y' => 0, 'h' => 20, 'w' => 20),
	'lang_icon_italian' => array('x' => 120, 'y' => 0, 'h' => 20, 'w' => 20),
	'lang_icon_finnish' => array('x' => 140, 'y' => 0, 'h' => 20, 'w' => 20),
	'lang_icon_danish' => array('x' => 160, 'y' => 0, 'h' => 20, 'w' => 20),

	'icon_home' => array('x' => 180, 'y' => 0, 'h' => 20, 'w' => 20),

	'currency_icon_gbp' => array('x' => 200, 'y' => 0, 'h' => 20, 'w' => 20),
	'currency_icon_usd' => array('x' => 220, 'y' => 0, 'h' => 20, 'w' => 20),
	'currency_icon_cad' => array('x' => 240, 'y' => 0, 'h' => 20, 'w' => 20),
	'currency_icon_aud' => array('x' => 260, 'y' => 0, 'h' => 20, 'w' => 20),
	'currency_icon_jpy' => array('x' => 280, 'y' => 0, 'h' => 20, 'w' => 20),
	'currency_icon_dkk' => array('x' => 300, 'y' => 0, 'h' => 20, 'w' => 20),
	'currency_icon_nok' => array('x' => 320, 'y' => 0, 'h' => 20, 'w' => 20),
	'currency_icon_chf' => array('x' => 340, 'y' => 0, 'h' => 20, 'w' => 20),
	'currency_icon_sek' => array('x' => 360, 'y' => 0, 'h' => 20, 'w' => 20),
	'currency_icon_eur' => array('x' => 380, 'y' => 0, 'h' => 20, 'w' => 20),

	'icon_search' => array('x' => 400, 'y' => 0, 'h' => 20, 'w' => 20),
	'icon_login' => array('x' => 420, 'y' => 0, 'h' => 20, 'w' => 20),

	'payment_footer_bank' => array('x' => 0, 'y' => 20, 'h' => 17, 'w' => 58),
	'payment_footer_paypal' => array('x' => 58, 'y' => 20, 'h' => 16, 'w' => 54),
	'payment_footer_visa' => array('x' => 112, 'y' => 20, 'h' => 19, 'w' => 68),
	'payment_footer_visa' => array('x' => 112, 'y' => 20, 'h' => 19, 'w' => 68),
	'payment_footer_sofort_ueberweisung' => array('x' => 240, 'y' => 20, 'h' => 24, 'w' => 37),
	'payment_footer_ideal' => array('x' => 277, 'y' => 20, 'h' => 25, 'w' => 27),
	'payment_footer_nordea' => array('x' => 304, 'y' => 20, 'h' => 18, 'w' => 77),
	'payment_footer_bleue' => array('x' => 381, 'y' => 20, 'h' => 23, 'w' => 47),
	'payment_footer_postepay' => array('x' => 428, 'y' => 20, 'h' => 16, 'w' => 73),
);

function create_sprite_icon($key, $type = 'img', $alt = '', $additional_html = '')
{
	global $array_sprite_locations;

	if (!$array_sprite_locations[$key])
		return false;

	$location = $array_sprite_locations[$key];

	$css = 'background-position: -'.$location['x'].'px -'.$location['y'].'px;';

	$src = '';
	if ($type == 'input')
		$src.= '<input type="image" src="images/pixel_trans.gif" alt="'.htmlentities($alt).'" class="sprite" ';
	else
		$src.= '<img src="images/pixel_trans.gif" alt="'.htmlentities($alt).'" class="sprite" ';
	$src.= 'height="'.$location['h'].'" width="'.$location['w'].'" ';
	$src.= 'style="'.htmlentities($css).'" ';
	$src.= $additional_html;
	$src.= ' />';

	return $src;
}