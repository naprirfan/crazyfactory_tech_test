<?php
/* -----------------------------------------------------------------------------------------
   $Id: header.php 1140 2009-05-12 10:16:00Z ak $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(header.php,v 1.40 2003/03/14); www.oscommerce.com
   (c) 2003	 nextcommerce (header.php,v 1.13 2003/08/17); www.nextcommerce.org

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contribution:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org
   Copyright (c) 2008, 2009, 2010 http://www.creations.de


   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

if($_GET['error'] == '404') {
	header('HTTP/1.1 404 Not Found');
	header('Status: 404 Not Found');
	header('Content-type: text/html');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />


<?php


// Der Facebook Bot kriegt Englisch
if (strpos($_SERVER['HTTP_USER_AGENT'], 'facebookexternalhit') !== false)
	$og_lang_id = 1;
else
	$og_lang_id = $_SESSION['languages_id'];

// OpenGraph Daten f�duktseiten
if (
	(basename($_SERVER['PHP_SELF']) == 'product_info_ng.php' || basename($_SERVER['PHP_SELF']) == 'product_info.php')
	&& $_GET['products_id']
)
{
	$product_data = array_pop(query('
		SELECT *
		FROM `products_description`
		WHERE `products_id` = '.sqlval($_GET['products_id']).'
		  AND `language_id` = '.sqlval($og_lang_id).'
	'));

	$product_images = get_product_image_array($_GET['products_id']);

	$og_title = strip_tags($product_data['products_name']);
	$og_image = HTTP_SERVER.DIR_WS_CATALOG.$product_images['S'];
	$og_url = HTTP_SERVER.DIR_WS_CATALOG.'product_info.php?products_id='.$_GET['products_id'];
}
	// OpenGraph f�duktliste
elseif (basename($_SERVER['PHP_SELF']) == 'product_listing_ng.php' && $_GET['cat_id'])
{
	$product_data = array_pop(query('
		SELECT *
		FROM `categories_description`
		WHERE `categories_id` = '.sqlval($_GET['cat_id']).'
		  AND `language_id` = '.sqlval($og_lang_id).'
	'));

	$og_title = 'Crazy Factory Piercing - '.$product_data['categories_name'];
}

// Fallbackzeug
if (!$og_title)
	$og_title = 'Crazy Factory Piercing';
if (!$og_image)
	$og_image = HTTP_SERVER.DIR_WS_CATALOG.'images/opengraph/'.mt_rand(1, 8).'.jpg';
if (!$og_url)
	$og_url = HTTP_SERVER.$_SERVER['SCRIPT_NAME'].($_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '');

echo '<meta property="og:title" content="'.htmlentities($og_title).'"/>'."\n";
echo '<meta property="og:type" content="product"/>'."\n";
echo '<meta property="og:url" content="'.htmlentities($og_url).'"/>'."\n";
echo '<meta property="og:image" content="'.htmlentities($og_image).'"/>'."\n";
echo '<meta property="og:site_name" content="crazy-factory.com"/>'."\n";
echo '<meta property="og:description" content="Crazy Factory Piercing - The best piercings you can buy for less!"/>'."\n";
echo '<meta property="fb:admins" content="517943391"/>'."\n"; // <- Andr矍

// MySpace (shits broken)
//echo '<meta name="title" content="'.htmlentities(strip_tags($product_data['products_name'])).'" />'."\n";
//echo '<meta name="description" content="Crazy Factory Piercing - The best piercings you can buy for less!" />'."\n";
//echo '<link rel="image_src" href="'.htmlentities(HTTP_SERVER.DIR_WS_CATALOG.$product_images['S']).'" />'."\n";
?>
<style type="text/css">
	input.sprite,
	img.sprite {
		background-image: url(templates/<?php echo CURRENT_TEMPLATE; ?>/img/sprites/1.png?<?php echo filemtime('templates/'.CURRENT_TEMPLATE.'/img/sprites/1.png'); ?>);
	}
</style>
<?php
/*
  The following copyright announcement is in compliance
  to section 2c of the GNU General Public License, and
  thus can not be removed, or can only be modified
  appropriately.

  Please leave this comment intact together with the
  following copyright announcement.

*/

/*
 * This OnlineStore is brought to you by XT-Commerce, Community made shopping
 * XTC is a free open source e-Commerce System
 * created by Mario Zanier & Guido Winger and licensed under GNU/GPL.
 * Information and contribution at http://www.xt-commerce.com
 */
?>
<meta name="generator" content="(c) by crazy factory ltd.">
<?php include(DIR_WS_MODULES.FILENAME_METATAGS); ?>
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<link rel="stylesheet" type="text/css" href="<?php echo 'templates/'.CURRENT_TEMPLATE.'/stylesheet.css'; ?>?<?php echo filemtime('templates/'.CURRENT_TEMPLATE.'/stylesheet.css'); ?>" />
<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<?php echo 'templates/'.CURRENT_TEMPLATE.'/stylesheet-ie.css'; ?>" />
<![endif]-->
<script type="text/javascript"><!--
var selected;
var submitter = null;

function submitFunction() {
    submitter = 1;
}
function popupWindow(url) {
  window.open(url,'popupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=100,height=100,screenX=150,screenY=150,top=150,left=150')
}

function popupWindowShipping(url) { window.open(url,'popupWindowShipping','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=600,height=600,screenX=150,screenY=150,top=150,left=150')
}

function selectRowEffect(object, buttonSelect) {
  if (!selected) {
    if (document.getElementById) {
      selected = document.getElementById('defaultSelected');
    } else {
      selected = document.all['defaultSelected'];
    }
  }

  if (selected) selected.className = 'moduleRow';
  object.className = 'moduleRowSelected';
  selected = object;

// one button is not an array
  if (document.getElementById('payment'[0])) {
    document.getElementById('payment'[buttonSelect]).checked=true;
  } else {
    //document.getElementById('payment'[selected]).checked=true;
  }
}

function rowOverEffect(object) {
  if (object.className == 'moduleRow') object.className = 'moduleRowOver';
}

function rowOutEffect(object) {
  if (object.className == 'moduleRowOver') object.className = 'moduleRow';
}

function popupImageWindow(url) {
  window.open(url,'popupImageWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=100,height=100,screenX=150,screenY=150,top=150,left=150');
}
//-->
</script>
<?php
require('templates/'.CURRENT_TEMPLATE.'/javascript/general.js.php');

if (strstr($PHP_SELF, FILENAME_CHECKOUT_PAYMENT)) {
 echo $payment_modules->javascript_validation();
}

$form_check_array = array(
	FILENAME_CREATE_ACCOUNT,
	FILENAME_ACCOUNT_PASSWORD,
	FILENAME_ACCOUNT_EDIT,
	FILENAME_CHECKOUT_SHIPPING_ADDRESS,
	FILENAME_CHECKOUT_PAYMENT_ADDRESS,
);

if (in_array(basename($PHP_SELF), $form_check_array) || (basename($PHP_SELF) == FILENAME_ADDRESS_BOOK_PROCESS && isset($_GET['delete']) == false))
{
	require_once(dirname(__FILE__).'/form_check.php');
	add_js_file_to_index_template_footer('form_check');
}

if (strstr($PHP_SELF, FILENAME_ADVANCED_SEARCH )) {
?>
<script type="text/javascript" src="includes/general.js"></script>
<script type="text/javascript"><!--
function check_form() {
  var error_message = unescape("<?php echo xtc_js_lang(JS_ERROR); ?>");
  var error_found = false;
  var error_field;
  var keywords = document.getElementById("advanced_search").keywords.value;
  /*var pfrom = document.getElementById("advanced_search").pfrom.value;
  var pto = document.getElementById("advanced_search").pto.value;
  var pfrom_float;
  var pto_float;*/

  if ( (keywords == '' || keywords.length < 1)
		&& $('#gauge_min_input').val() == '' && $('#gauge_max_input').val() == ''
		&& $('#diameter_min_input').val() == '' && $('#diameter_max_input').val() == ''
		&& $('#length_mm_min_input').val() == '' && $('#length_cm_max_input').val() == ''
		&& $('#length_cm_min_input').val() == '' && $('#length_cm_max_input').val() == ''
		&& $('#material').val() == -1/* && (pfrom == '' || pfrom.length < 1) && (pto == '' || pto.length < 1) */
		&& $('#winding').val() == -1)
  {
    error_message = error_message + unescape("<?php echo xtc_js_lang(JS_AT_LEAST_ONE_INPUT); ?>");
    error_field = document.getElementById("advanced_search").keywords;
    error_found = true;
  }
/*
  if (pfrom.length > 0) {
    pfrom_float = parseFloat(pfrom);
    if (isNaN(pfrom_float)) {
      error_message = error_message + unescape("<?php echo xtc_js_lang(JS_PRICE_FROM_MUST_BE_NUM); ?>");
      error_field = document.getElementById("advanced_search").pfrom;
      error_found = true;
    }
  } else {
    pfrom_float = 0;
  }

  if (pto.length > 0) {
    pto_float = parseFloat(pto);
    if (isNaN(pto_float)) {
      error_message = error_message + unescape("<?php echo xtc_js_lang(JS_PRICE_TO_MUST_BE_NUM); ?>");
      error_field = document.getElementById("advanced_search").pto;
      error_found = true;
    }
  } else {
    pto_float = 0;
  }

  if ( (pfrom.length > 0) && (pto.length > 0) ) {
    if ( (!isNaN(pfrom_float)) && (!isNaN(pto_float)) && (pto_float < pfrom_float) ) {
      error_message = error_message + unescape("<?php echo xtc_js_lang(JS_PRICE_TO_LESS_THAN_PRICE_FROM); ?>");
      error_field = document.getElementById("advanced_search").pto;
      error_found = true;
    }
  }
*/
  if (error_found == true) {
    alert(error_message);
    error_field.focus();
    return false;
  }
}

function popupWindow(url) {
  window.open(url,'popupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=450,height=280,screenX=150,screenY=150,top=150,left=150')
}
//--></script>
<?php
}

if (strstr($PHP_SELF, FILENAME_PRODUCT_REVIEWS_WRITE )) {
?>

<script type="text/javascript"><!--
function checkForm() {
  var error = 0;
  var error_message = unescape("<?php echo xtc_js_lang(JS_ERROR); ?>");

  var review = document.getElementById("product_reviews_write").review.value;

  if (review.length < <?php echo REVIEW_TEXT_MIN_LENGTH; ?>) {
    error_message = error_message + unescape("<?php echo xtc_js_lang(JS_REVIEW_TEXT); ?>");
    error = 1;
  }

  if (!((document.getElementById("product_reviews_write").rating[0].checked) || (document.getElementById("product_reviews_write").rating[1].checked) || (document.getElementById("product_reviews_write").rating[2].checked) || (document.getElementById("product_reviews_write").rating[3].checked) || (document.getElementById("product_reviews_write").rating[4].checked))) {
    error_message = error_message + unescape("<?php echo xtc_js_lang(JS_REVIEW_RATING); ?>");
    error = 1;
  }

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}
//--></script>
<?php
}
if (strstr($PHP_SELF, FILENAME_POPUP_IMAGE )) {
?>

<script type="text/javascript"><!--
var i=0;
function resize() {
  if (navigator.appName == 'Netscape') i=40;
  if (document.images[0]) window.resizeTo(document.images[0].width +30, document.images[0].height+60-i);
  self.focus();
}
//--></script>
<?php
}
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-267283-16']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<?php
if (strstr($PHP_SELF, FILENAME_POPUP_IMAGE )) {
echo '<body class="'.$_SESSION['language'].'" onload="resize();"> ';
} else {
echo '<body class="'.$_SESSION['language'].'">';
}
// econda tracking
if (TRACKING_ECONDA_ACTIVE=='true') {
?>
<script type="text/javascript">
<!--
var emos_kdnr='<?php echo TRACKING_ECONDA_ID; ?>';
//-->
</script>
<a name="emos_sid" rel="<?php echo session_id(); ?>" rev=""></a>
<a name="emos_name" title="siteid" rel="<?php echo $_SESSION['languages_id']; ?>" rev=""></a>
<?php
//	require_once(DIR_WS_INCLUDES . 'econda/econda.php');
}


if (strstr($PHP_SELF, FILENAME_CHECKOUT_SUCCESS) && GOOGLE_CONVERSION == 'true') {
require('includes/google_conversiontracking.js.php');
}


  // include needed functions
  require_once('inc/xtc_output_warning.inc.php');
  require_once('inc/xtc_image.inc.php');
  require_once('inc/xtc_parse_input_field_data.inc.php');
  require_once('inc/xtc_draw_separator.inc.php');

//  require_once('inc/xtc_draw_form.inc.php');
//  require_once('inc/xtc_draw_pull_down_menu.inc.php');

  // check if the 'install' directory exists, and warn of its existence
  if (WARN_INSTALL_EXISTENCE == 'true') {
    if (file_exists(dirname($_SERVER['SCRIPT_FILENAME']) . '/xtc_installer')) {
      xtc_output_warning(WARNING_INSTALL_DIRECTORY_EXISTS);
    }
  }

  // check if the configure.php file is writeable
  if (WARN_CONFIG_WRITEABLE == 'true') {
    if ( (file_exists(dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php')) && (is_writeable(dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php')) ) {
      xtc_output_warning(WARNING_CONFIG_FILE_WRITEABLE);
    }
  }

  // check if the session folder is writeable
  if (WARN_SESSION_DIRECTORY_NOT_WRITEABLE == 'true') {
    if (STORE_SESSIONS == '') {
      if (!is_dir(xtc_session_save_path())) {
        xtc_output_warning(WARNING_SESSION_DIRECTORY_NON_EXISTENT);
      } elseif (!is_writeable(xtc_session_save_path())) {
        xtc_output_warning(WARNING_SESSION_DIRECTORY_NOT_WRITEABLE);
      }
    }
  }

  // check session.auto_start is disabled
  if ( (function_exists('ini_get')) && (WARN_SESSION_AUTO_START == 'true') ) {
    if (ini_get('session.auto_start') == '1') {
      xtc_output_warning(WARNING_SESSION_AUTO_START);
    }
  }

  if ( (WARN_DOWNLOAD_DIRECTORY_NOT_READABLE == 'true') && (DOWNLOAD_ENABLED == 'true') ) {
    if (!is_dir(DIR_FS_DOWNLOAD)) {
      xtc_output_warning(WARNING_DOWNLOAD_DIRECTORY_NON_EXISTENT);
    }
  }


$smarty->assign('navtrail',$breadcrumb->trail(' &raquo; '));
if (isset($_SESSION['customer_id'])) {

$smarty->assign('logoff',xtc_href_link(FILENAME_LOGOFF, '', 'SSL'));
}
if ( $_SESSION['account_type']=='0') {
$smarty->assign('account',xtc_href_link(FILENAME_ACCOUNT, '', 'SSL'));
}
$smarty->assign('cart',xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
$smarty->assign('checkout',xtc_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
$smarty->assign('store_name',TITLE);
$smarty->assign('feedback_link', xtc_href_link(FILENAME_CONTENT, 'coID=7'));


  if (isset($_SESSION['error_message']) && xtc_not_null($_SESSION['error_message'])) {

$smarty->assign('error','
        <div class="headerError">'. urldecode($_SESSION['error_message']).'</div>
');
	unset($_SESSION['error_message']);

  }

  if (isset($_SESSION['info_message']) && xtc_not_null($_SESSION['info_message'])) {

$smarty->assign('error','
        <div class="headerInfo">'.urldecode($_SESSION['info_message']).'</div>
');
	unset($_SESSION['info_message']);

  }
  include(DIR_WS_INCLUDES.FILENAME_BANNER);
?>