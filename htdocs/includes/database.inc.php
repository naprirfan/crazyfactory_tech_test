<?php

require_once('configure.php'); // f�r die DB Variablen im xtc_db_connect
require_once(dirname(__FILE__).'/../inc/xtc_db_connect.inc.php');

// Da diese Datei hier eh schon gepatcht ist und wir in diesem Projekt nie wieder andere Zeichens�tze
// brauchen werden, kann das hier hin:
query('SET NAMES "utf8"');

database_include_early_init_config();

unset($DBDATA['on_query_error']);

// ================================================================================

// Wird ausgef�hrt, wenn die Datei includet wird; setzt die Defaults, bevor die Konfiguration durch
// die Anwendung ge�ndert wird
function database_include_early_init_config()
{
	static $already_inited;
	if ($already_inited++)
		return;

	global $DBDATA;

	$DBDATA['on_query_error'] = array('database_include_errorcallback_query');
}

function database_include_errorcallback_query($errorstring,$backtrace,$querystring)
{
	define ('LF',"\n");
	echo 'MySQL error in '.$backtrace[0]['file'].' ('.$backtrace[0]['line'].'):'.LF.$errorstring.LF.'Query:'.LF.$querystring;
}

function database_include_call_callbacks($hook)
{
	global $DBDATA;

	if (is_string($DBDATA[$hook]))
		$callbacks = array($DBDATA[$hook]);
	if (is_array($DBDATA[$hook]))
		$callbacks = $DBDATA[$hook];

	if ($callbacks)
	{
		$args = func_get_args();
		array_shift($args); // Namen des Hooks abshiften
		foreach ($callbacks as $callback)
			call_user_func_array($callback,$args); // Alle anderen Argumente werden weitergegeben
	}
}

function query($querystring)
{
	global $DBDATA;
	static $DATABASE_CONNECTION;

	// xtCommerce Datenbankverbindung reinholen
	global $db_link;
	if (!$db_link) // Verbindung aufbauen wenn noch nicht getan
		xtc_db_connect();
	if (!$DATABASE_CONNECTION)
		$DATABASE_CONNECTION = $db_link;

	$querystring = trim($querystring);

	database_include_call_callbacks('on_query',$querystring);
	$result = @mysql_query($querystring,$DATABASE_CONNECTION);

	if (!$result)
	{
		database_include_call_callbacks('on_query_error',mysql_error(),debug_backtrace(),$querystring);
		database_include_call_callbacks('on_error',mysql_error());
	}

	$O = false;

	if ($result)
	{
		$O = true;

		if (substr($querystring,0,6)=="SELECT" || substr($querystring,0,4)=="SHOW")
		{
			$O = array();
			if (mysql_num_rows($result) > 0)
			{
				while($line=mysql_fetch_array($result,MYSQL_ASSOC))
					$O[]=$line;
			}
			database_include_call_callbacks('on_select_result',$O);
		}

		if (substr($querystring,0,6)=="INSERT")
		{
			$O=mysql_insert_id($DATABASE_CONNECTION);
			database_include_call_callbacks('on_insert_result',$O);
		}

		if (substr($querystring,0,6)=="UPDATE")
		{
			$O = mysql_affected_rows($DATABASE_CONNECTION);
			database_include_call_callbacks('on_update_result',$O);
		}
		if (substr($querystring,0,7)=="REPLACE")
		{
			$O = mysql_affected_rows($DATABASE_CONNECTION);
			database_include_call_callbacks('on_replace_result',$O);
		}
		if (substr($querystring,0,11)=="DELETE FROM")
		{
			$O = mysql_affected_rows($DATABASE_CONNECTION);
			database_include_call_callbacks('on_delete_result',$O);
		}
	}

	return $O;
}

function query_firstrow($querystring)
{
	return array_pop(query($querystring));
}

function query_firstcell($querystring)
{
	return array_pop(query_firstrow($querystring));
}

function sqlval($value) {
	return "'".addslashes($value)."'";
}