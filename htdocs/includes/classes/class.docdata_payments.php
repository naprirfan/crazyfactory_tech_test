<?php

require_once (DIR_FS_CATALOG.'includes/functions/cancel_order.php');
class parent_docdata_payments {

	// legacy constructor
	var $version = '1.0';
	// Das Zahlungsmodul kann �ber diese Eigenschaft veranlassen, da� der letzte Teil der checkout_process.php
	// (der auch die Zahlungsmodul-Methode after_process() aufruft) erst ausgef�hrt wird, wenn die
	// checkout_process.php zum zweiten Mal aufgerufen wird (respektive $_SESSION['tmp_oID'] gesetzt ist). Wenn
	// checkout_payment.php aufgerufen wird, wird diese Variable wieder gel�scht
	var	$tmpOrders = true;
	var	$repost = false;
	var	$debug = false;
	var $debugpw = '98HgvM3';
	var $form_action_url = 'https://www.tripledeal.com/ps/com.tripledeal.paymentservice.servlets.PaymentService';
	var $tmpStatus = _PAYMENT_DOCDATA_PAYMENTS_TMP_STATUS_ID;
	var $status_pending = _PAYMENT_DOCDATA_PAYMENTS_TMP_STATUS_ID;
	var $status_waiting = _PAYMENT_DOCDATA_PAYMENTS_PENDING_STATUS_ID;
	var $status_paid = _PAYMENT_DOCDATA_PAYMENTS_PROCESSED_STATUS_ID;
	var $new_status = '';

	function parent_docdata_payments() {
		global $order;

		// Ein vollst�ndiger Zahlungsmethoden-Eintrag enth�lt die Schl�ssel:
		// - profile => Zahlungsprofil bei Docdata Payments
		// - description => Name der Zahlungsart
		// - docdata_return => ob die Zahlung sofort von Docdata Payments best�tigt ("paid") wird (Sofortzahlungsmethoden)
		// - order_approval => ob die Bestellung sofort nach Abschlu� des Zahlungsprozesses freigegeben wird
		//
		// Es gibt zwei M�glichkeiten abzuk�rzen:
		// - null => Anhand des Keys werden die Werte aufgef�llt (funktioniert z.B. mit den Kreditkarten, s.u.)
		// - String => Dito, der Name der Zahlungsart wird allerdings mit dem String �berschrieben

		}

	function _setCode($code='CC',$payment_method='ACC') {

		$this->module = $code;
		$this->method = $payment_method;

		$this->code = 'docdata_payments_'.strtolower($code);

		if (defined('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($code).'_TEXT_TITLE')) {
			// $this->title = 'DocData Payments: ';
			$this->title .= constant('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($code).'_TEXT_TITLE');
			$this->description = constant('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($code).'_TEXT_DESCRIPTION');
			$this->info = constant('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($code).'_TEXT_INFO').'<br><br>';
		}

		if (defined('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($code).'_STATUS')) {
			$this->sort_order = constant('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($code).'_SORT_ORDER');
			$this->enabled = ((constant('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($code).'_STATUS') == 'True') ? true : false);
			$this->tmpStatus = constant('_PAYMENT_DOCDATA_PAYMENTS_TMP_STATUS_ID');
		}

		if (defined('_VALID_XTC')) {
			$icons = explode(',', $this->images);
			$accepted='';
			foreach ($icons as $key => $val)
				$accepted .= xtc_image('../images/icons/docdata/'. $val) . ' ';
			if ($this->allowed!='')
				$this->title.=' ('.$this->allowed.')';

			$this->title .='<br />'.$accepted;
		}

	}


	function update_status() {
		global $order;

		if (($this->enabled == true) && ((int) constant('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($this->module).'_ZONE') > 0)) {
			$check_flag = false;
			$check_query = xtc_db_query("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . constant('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($this->module).'_ZONE') . "' and zone_country_id = '" . $order->billing['country']['id'] . "' order by zone_id");
			while ($check = xtc_db_fetch_array($check_query)) {
				if ($check['zone_id'] < 1) {
					$check_flag = true;
					break;
				}
				elseif ($check['zone_id'] == $order->billing['zone_id']) {
					$check_flag = true;
					break;
				}
			}

			if ($check_flag == false) {
				$this->enabled = false;
			}
		}

	}


	// Aufgerufen auf checkout_payments.php
	// Soll zus�tzliches Javascript zur�ckgeben, um die Eingaben des Zahlungmoduls clientseitig zu validieren
	function javascript_validation() {
		return null;
	}


	// Aufgerufen auf checkout_payments.php und checkout_confirmation.php (nur wenn dieses Zahlungsmodul gew�hlt ist)
	// Soll den Wert des Radiobuttons, den Titel und die Beschreibung zur�ckgeben, mit der das
	// Zahlungsmodul angezeigt wird. Der Wert des Radiobuttons mu� dem Namen der Zahlungsmodulklasse
//	// entsprechen, sonst wird die Auswahl nicht �bernommen
//	function selection() {
//		global $order;
//
//		if ($this->we_are_on_confirmation_page)
//			$description .= $this->method_mapping[$this->getCountryIsoCode($order->billing['country']['iso_code_2'])][$_SESSION['ddpay_payment_profile']]['description'];
//		else
//		{
//			$description .= MODULE_PAYMENT_DOCDATA_PAYMENTS_TEXT_INFO;
//			$description .= '<br><br>';
//			$description .= '<select name="payment_profile">';
//			foreach ($this->method_mapping[$this->getCountryIsoCode($order->billing['country']['iso_code_2'])] as $key => $data)
//				$description .= '<option '.($_SESSION['ddpay_payment_profile'] == $data['profile'] ? 'selected="selected"' : '').'value="'.$data['profile'].'">'.$data['description'].'</option>';
//			$description .= '</select>';
//		}
//		return array ('id' => 'docdata_payments', 'module' => 'Docdata Payments', 'description' => $description);
//	}



	// Aufgerufen auf checkout_confirmation.php (nur wenn dieses Zahlungsmodul gew�hlt ist), keine R�ckgabe
	function pre_confirmation_check() {
		// define ('MODULE_PAYMENT_DOCDATA_PAYMENT_TEXT_TITLE','Hallo!');

		$this->we_are_on_confirmation_page = true;
	}


	// Beim Anzeigen der Zahlungsweise auf der checkout_confirmation-Seite
	function confirmation() {
//		return array(
//			'title' => 'Test',
//			'fields' => array(
////				array('title' => '','field' => ''), // F�r tabellarische Ansichten
////				array('title' => '','field' => '')
//			)
//		);
	}

	// Aufgerufen auf der checkout_success-Seite (Anpassung creations)
	function success() {
		return array(
			'title' => '',
			'fields' => array(
//				array('title' => '','field' => ''), // F�r tabellarische Ansichten
//				array('title' => '','field' => '')
			)
		);
	}

	// Aufgerufen auf checkout_confirmation.php (nur wenn dieses Zahlungsmodul gew�hlt ist)
	// Soll das HTML f�r zus�tzliche Buttons zur�ckgeben
	function process_button() {
		return false;
	}

	// Aufgerufen auf checkout_process.php, aber nur, wenn das Zahlungsmodul mit tempor�ren Bestellungen (s.o.) arbeitet
	function payment_action() {
		// xtc_redirect(xtc_href_link('checkout_payment_iframe.php', '', 'SSL'));
		xtc_redirect($this->iframeAction(), '', 'SSL');

	}

	// Aufgerufen auf checkout_process.php, keine R�ckgabe
	function before_process() {
		// unset($_SESSION['tmp_oID']); // Hack, damit beim zweiten Versuch immer noch die payment_action() aufgerufen wird
	}


	// Nach dem Anlegen der Bestellung, keine R�ckgabe
	function after_process() {
/*
		global $_SESSION['tmp_oID'];
		if ($this->order_status)
			xtc_db_query("UPDATE ".TABLE_ORDERS." SET orders_status='".$this->order_status."' WHERE orders_id='".$_SESSION['tmp_oID']."'");
*/
	}

	// Parse the predefinied array to be 'module install' friendly
	// as it is used for select in the module's install() function
	function show_array($aArray) {
		$aFormatted = "array(";
		foreach ($aArray as $key => $sVal) {
			$aFormatted .= "\'$sVal\', ";
		}
		$aFormatted = substr($aFormatted, 0, strlen($aFormatted) - 2);
		return $aFormatted;
	}

	function get_error() {
		global $_GET;

		$error = array (
			'title' => constant('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($this->module).'_TEXT_ERROR'),
			'error' => stripslashes(urldecode($_GET['error']))
			);

		return $error;
	}


	// Aufgerufen im Admin

	function check() {
		if (!isset ($this->_check)) {
			$check_query = xtc_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_PAYMENT_DOCDATA_PAYMENTS_".strtoupper($this->module)."_STATUS'");
			$this->_check = xtc_db_num_rows($check_query);
		}
		return $this->_check;
	}


	// Aufgerufen im Admin, beim Klick auf "Installieren"
	// Legt die Konfigurationsschl�ssel an, die die abgeleiteten Module benutzten
	function install() {

		$this->remove();

		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, set_function, date_added) values ('MODULE_PAYMENT_DOCDATA_PAYMENTS_".strtoupper($this->module)."_STATUS', 'True',  '6', '0', 'xtc_cfg_select_option(array(\'True\', \'False\'), ', now())");
		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, date_added) values ('MODULE_PAYMENT_DOCDATA_PAYMENTS_".strtoupper($this->module)."_SORT_ORDER', '0',  '6', '4', now())");
		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, use_function, set_function, date_added) values ('MODULE_PAYMENT_DOCDATA_PAYMENTS_".strtoupper($this->module)."_ZONE', '0',  '6', '7', 'xtc_get_zone_class_title', 'xtc_cfg_pull_down_zone_classes(', now())");
		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, date_added) values ('MODULE_PAYMENT_DOCDATA_PAYMENTS_".strtoupper($this->module)."_ALLOWED', '".$this->allowed."', '6', '0', now())");


}


	// Aufgerufen im Admin, beim Klick auf "Deinstallieren"
	// L�scht die benutzten Konfigurationsschl�ssel aus der DB
	function remove() {
		xtc_db_query("delete from ".TABLE_CONFIGURATION." where configuration_key in ('".implode("', '", $this->keys())."')");
	}

	// Aufgerufen im Admin
	// Soll die Konfigurationsschl�ssel zur�ckgeben, die das Modul benutzt
	function keys() {
		return array (
			'MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($this->module).'_STATUS',
			'MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($this->module).'_SORT_ORDER',
			'MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($this->module).'_ALLOWED',
			'MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($this->module).'_ZONE'
		);
	}

	public function on_ddpay_trigger($get_tid)
	{
		$result = xtc_db_query('SELECT orders_id, customers_name, customers_email_address, payment_cluster_key, payment_class, order_billing_country, payment_profile, orders_status FROM '.TABLE_ORDERS.' WHERE orders_id = '.$this->sqlval($get_tid).'');
		$first_row = xtc_db_fetch_array($result);

		$orders_id = $first_row['orders_id'];
		$pck = $first_row['payment_cluster_key'];
		$country = $first_row['order_billing_country'];
		$profile = $first_row['payment_profile'];
		$status = $first_row['orders_status'];
		file_put_contents('ddpay_return.log','['.date('d.m.Y H:i:s').'] '.$_GET['tid'].': start check! '.$status.' '.$this->status_paid.' '.$pck.' '.$country.' '.$profile."\n",FILE_APPEND);

		if ($status == $this->status_pending && $this->is_acquired($pck,$country,$profile) && !$this->is_paid($pck))
		{
			$sql = 'UPDATE '.TABLE_ORDERS.' SET orders_status='.$this->sqlval($this->status_waiting).' WHERE orders_id='.$this->sqlval($orders_id);
			xtc_db_query($sql);
//			file_put_contents('ddpay_sql_start.log','[Update Orders '.date('d.m.Y H:i:s').' '.$this->sqlval($orders_id).'] : '.$result.''."\n",FILE_APPEND);
			$sql = "insert into ".TABLE_ORDERS_STATUS_HISTORY." (orders_id, orders_status_id, date_added, customer_notified, comments) values (".$this->sqlval($orders_id).", ".$this->sqlval($this->status_waiting).", now(), '0', '')";
			xtc_db_query($sql);
//			file_put_contents('ddpay_sql_start.log','[Update OrdersHistory '.date('d.m.Y H:i:s').' '.$this->sqlval($orders_id).'] : '.$result.''."\n",FILE_APPEND);
		}


		if ($status == 8 && $this->is_paid($pck))
		{
			// Docdata meldet doppelt.
			$result = xtc_db_query('SELECT order_id FROM cm_book_inpayment_import WHERE order_id = "'.$orders_id.'" LIMIT 1');
			$orderchecktransfer = xtc_db_fetch_array($result);
			if (!$orderchecktransfer['order_id'])
			{
				// diese Bestellung wurde zwischenzeitlich gek�ndigt. Muss manuell gepr�ft werden.
				$sql = 'INSERT INTO cm_book_inpayment_import (order_id,book_inpayment_type,from_name,from_email,controlstring,error,date_receive)
					VALUES
					("'.$orders_id.'","docdata","'.$first_row['customers_name'].'","'.$first_row['customers_email_address'].'","'.$first_row['payment_class'].'","CANCELED",NOW())
				';
				xtc_db_query($sql);
			}
		}
		//if (($status == $this->status_pending || $status == $this->status_waiting) && $this->is_paid($pck))
		elseif (!self::_status_means_at_least_paid($status) && $this->is_paid($pck))
		{
			file_put_contents('ddpay_return.log','['.date('d.m.Y H:i:s').'] '.$_GET['tid'].': Bezahlt! '."\n",FILE_APPEND);
			// Wenn der Status auf bezahlt gestellt wurde, s�mtliche anderen offenen bestellungen des Kunden auf storniert stellen.

			$sql = '
				SELECT
					customers_id,
					customers_name,
					customers_email_address,
					orders_status,
					date_purchased,
					language,
					currency,
					stock_freed,
					value
				FROM orders INNER JOIN orders_total
					ON (orders.orders_id = orders_total.orders_id AND orders_total.class = "ot_total")
				WHERE orders.orders_id = "' . xtc_db_input($orders_id) . '"
			';
			$check_status_query = xtc_db_query($sql);
			$check_status = xtc_db_fetch_array($check_status_query);

			$sql= "select orders.orders_id,  orders_status from ".TABLE_ORDERS."
						INNER JOIN orders_total ON (orders.orders_id = orders_total.orders_id AND class = 'ot_total')
					where customers_id = '".xtc_db_input($check_status['customers_id'])."'
					AND orders.orders_id < '". xtc_db_input($orders_id) ."'
					AND orders_total.value = '".$check_status['value']."'
					AND orders.currency = '".$check_status['currency']."'
					AND (orders_status = 1
						OR orders_status = 7
						OR orders_status = 9
						OR orders_status = 10
						OR orders_status = 13
						OR orders_status = 14
						)
					";

			$check_orders_query = xtc_db_query($sql);
			while ($check_orders = xtc_db_fetch_array($check_orders_query))
			{
				cancel_order($check_orders['orders_id'], 'INFO: Docdata payment on later order: auto_cancel');
			}
			xtc_set_cashpoints_for_paid_order($orders_id,substr(basename(__FILE__),0,-4));
			if ($check_status['stock_freed']) // nur checken wenn diese Bestellung Stockartikel hatte und gek�ndigt war zwischenzeitlich
				xtc_check_for_stock_items($orders_id);

			xtc_activate_buyable_coupons($orders_id);

			// Emailbenachrichtigung:
			$customer_notified = '0';
			$notify_comments = '';

			// richtige Sprachvariable f�r den umzustellenden Status holen f�r den Kunden
			$sql = "select orders_status_name from orders_status JOIN languages ON (languages.languages_id = orders_status.language_id) WHERE languages.directory = '".$check_status['language']."' AND orders_status_id = '2'";
			$res = xtc_db_query($sql);
			$order_status = xtc_db_fetch_array($res);

			// hole korrektes charset:
			$lang_query = xtc_db_query("select  language_charset from " . TABLE_LANGUAGES . " where directory = '" . $check_status['language'] . "'");
			$lang = xtc_db_fetch_array($lang_query);

			// Status umstellen: (machen wir vor Smarty und Mail da die Benachrichtigung des Kunden zur Not mal Kaputt sein darf.
			xtc_db_query('UPDATE '.TABLE_ORDERS.' SET orders_status='.$this->sqlval($this->status_paid).' WHERE orders_id='.$this->sqlval($orders_id).'');

			$sql = "insert into ".TABLE_ORDERS_STATUS_HISTORY." (orders_id, orders_status_id, date_added, customer_notified, comments) values (".$this->sqlval($orders_id).", ".$this->sqlval($this->status_paid).", now(), '1', '')";
			xtc_db_query($sql);

			// damit die Doppelte Meldung abgefangen werden kann muss hier der Zahlungseingang eingetragen werden.
			$sql = 'INSERT INTO cm_book_inpayment_import (order_id,book_inpayment_type,from_name,from_email,controlstring,error,date_receive,hidden)
				VALUES
				("'.$orders_id.'","docdata","'.$first_row['customers_name'].'","'.$first_row['customers_email_address'].'","'.$first_row['payment_class'].'","NONE",NOW(),1)
			';
			xtc_db_query($sql);

			$smarty = new Smarty;
			// assign language to template for caching
			$smarty->assign('language', $check_status['language'] ); // $_SESSION['language']
			$smarty->caching = false;

			// set dirs manual
			$smarty->template_dir = DIR_FS_CATALOG.'templates';
			$smarty->compile_dir = DIR_FS_CATALOG.'templates_c';
			$smarty->config_dir = DIR_FS_CATALOG.'lang';

			$smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');
			$smarty->assign('logo_path', HTTP_SERVER.DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/img/');

			$smarty->assign('NAME', $check_status['customers_name']);
			$smarty->assign('ORDER_NR', $orders_id);

			$smarty->assign('NOTIFY_COMMENTS', $notify_comments);
			$smarty->assign('ORDER_STATUS', $order_status['orders_status_name']);

			$html_mail = $smarty->fetch(CURRENT_TEMPLATE.'/admin/mail/'.$check_status['language'].'/change_order_mail_12.html');
			$txt_mail = $smarty->fetch(CURRENT_TEMPLATE.'/admin/mail/'.$check_status['language'].'/change_order_mail_12.txt');

			xtc_php_mail(
				EMAIL_BILLING_ADDRESS,
				EMAIL_BILLING_NAME,
				$check_status['customers_email_address'],
				$check_status['customers_name'],
				'',
				EMAIL_BILLING_REPLY_ADDRESS,
				EMAIL_BILLING_REPLY_ADDRESS_NAME,
				'',
				'',
				EMAIL_BILLING_SUBJECT,
				$html_mail,
				$txt_mail,
				NULL,
				$check_status['language'],
				'#o#m#g#!#w#t#f#!#x#D#'
			);
		}
		//if (($status == $this->status_pending || $status == $this->status_waiting) && $this->is_canceled($pck))
		if ($status != $this->status_paid  && $this->is_canceled($pck))
		{
			cancel_order($orders_id, 'INFO: docdata cancel');
		}

		if ($status == $this->status_paid)
			file_put_contents('ddpay_return.log','['.date('d.m.Y H:i:s').'] '.$_GET['tid'].': bereits bezahlt!'."\n",FILE_APPEND);


	}

	// Ob die Zahlung bei Docdata Payments fertig erfa�t ist, der Kunde also den Zahlungsproze� im VT
	// abgeschlossen hat. Das bedeutet nicht automatisch, da� sie auch bezahlt ist, das ist nur bei
	// Sofortzahlungsmethoden so
	private function is_acquired($pck,$country,$profile)
	{
		$pc_status = $this->get_status($pck);

		// Der Sauberkeit halber sollte hier eigentlich �berpr�ft werden, ob die tats�chliche Zahlungsart
		// mit der ausgew�hlten �bereinstimmt. Das ist aber etwas komplizierter, weil es mehrere Payment
		// Attempts geben kann, daher hab ichs erstmal weggelassen. Das schlimmste was bei Manipulation
		// passieren kann, ist, da� man erfolgreich per Kreditkarte zahlt, aber der Shop sagt, da� auf
		// Zahlungseingang gewartet wird - da ist der Manipulierer eben selber schuld.

		// siehe auch unten

		// Wenn der PC "paid" oder "closed_success" ist, ist der Vorgang im VT auf jeden Fall abgeschlossen...
		if ($pc_status == 'paid' || $pc_status == 'closed_success')
			return true;


		// ...und bei Nicht-Sofortzahlungsmethoden auch bei "started"
		// $order->billing['country']['iso_code_2']
		// $_SESSION['ddpay_payment_profile']
		if ($pc_status == 'started' && $this->docdata_return == 'delayed')
			return true;

		return false;
	}

	// Ob bezahlt wurde
	private function is_paid($pck)
	{
		// $_SESSION['ddpay_payment_cluster_key']
				$pc_status = $this->get_status($pck);

		// s.o.

		if ($pc_status == 'paid' || $pc_status == 'closed_success')
			return true;

		return false;
	}

		// Ob storniert wurde
	private function is_canceled($pck)
	{
		// $_SESSION['ddpay_payment_cluster_key']
		$pc_status = $this->get_status($pck);

		// s.o.

		if ($pc_status == 'closed_canceled' )
			return true;

		return false;
	}

	private function _status_means_at_least_paid($order_status)
	{
		return in_array($order_status,array(2, 3, 4, 5, 6, 11, 12));
	}

	private function get_status($payment_cluster_key)
	{
		if ($this->new_status == '')
		{
			if ($this->debug)
			{
				$url = 'https://test.tripledeal.com/ps/com.tripledeal.paymentservice.servlets.PaymentService';
				$password = $this->debugpw;
			}
			else
			{
				$url = $this->form_action_url;
				$password = _PAYMENT_DOCDATA_PAYMENTS_PWD ;
			}

			$options['postfields'] = array(
				'command' => 'status_payment_cluster',
				'merchant_name' => _PAYMENT_DOCDATA_PAYMENTS_MERCHANTID ,
				'merchant_password' => $password ,
				'payment_cluster_key' => $payment_cluster_key,
				'report_type' => 'xml_ext'
			);

			$xmlresult = $this->_curl_call($url,$options);
			file_put_contents('ddpay_return.log','['.date('d.m.Y H:i:s').'] '.$_GET['tid'].': '.$xmlresult."\n",FILE_APPEND);

			preg_match('#<payment_cluster_process>([a-z_]+)</payment_cluster_process>#',$xmlresult,$matches);

			// keine payment_cluster_id?
			if (strpos($xmlresult,'payment_cluster_id_missing'))
			{
				// Docdata meldet doppelt.
				$result = xtc_db_query('SELECT order_id FROM cm_book_inpayment_import WHERE order_id = '.$this->sqlval($_GET['tid']).' LIMIT 1');
				$orderchecktransfer = xtc_db_fetch_array($result);
				if (!$orderchecktransfer['order_id'])
				{
					$result = xtc_db_query('SELECT orders_id, customers_name, customers_email_address,  payment_class, order_billing_country FROM '.TABLE_ORDERS.' WHERE orders_id = '.$this->sqlval($_GET['tid']).'');
					$first_row = xtc_db_fetch_array($result);
					$sql = 'INSERT INTO cm_book_inpayment_import (order_id,book_inpayment_type,from_name,from_email,controlstring,error,date_receive)
						VALUES
						("'.$first_row['orders_id'].'","docdata","'.$first_row['customers_name'].'","'.$first_row['customers_email_address'].'","'.$first_row['payment_class'].'","NO_PAYMENTCLUSTERID_FOUND",NOW())
					';
					xtc_db_query($sql);
				}
			}

			$this->new_status = $matches[1];
		}

		return $this->new_status;
	}

	private function _curl_call($url,$options=false)
	{
		static $ch;

		if (!$ch)
			$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		if (isset($options['timeout']))
			curl_setopt($ch, CURLOPT_TIMEOUT, $options['timeout']);
		else
			curl_setopt($ch, CURLOPT_TIMEOUT, 10); // Default-Timeout: 5 Sekunden

		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // Redirects erlauben // nicht m�glich, wenn open_basedir gesetzt ist
		curl_setopt($ch, CURLOPT_HTTPGET, 1);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);

		if (isset($options['postfields']))
		{
			curl_setopt($ch, CURLOPT_POST, 1);


			$postvars = '';

			foreach ($options['postfields'] as $key => $value)
				$postvars .= $key.'='.urlencode($value).'&';
			$postvars = substr($postvars,0,-1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		}

		$result = curl_exec($ch);

		if ($result === false)
			throw new Exception('Error connecting to gateway',-4);

		return $result;
	}

	// So kann ich URLs in mehreren Zeilen angeben
	private function _remove_breaks_and_tabs($text)
	{
		return str_replace("\n",'',str_replace("\r",'',str_replace("\t",'',$text)));
	}

	private function sqlval($text)
	{
		return "'".addslashes($text)."'";
	}

	private function session_language()
	{
		$result = xtc_db_query('SELECT code FROM '.TABLE_LANGUAGES.' WHERE directory = '.$this->sqlval($_SESSION['language']).'');
		$first_row = xtc_db_fetch_array($result);
		return $first_row['code'];
	}

	function iframeAction()
	{
		global $order,$xtPrice, $_GET;

		$ddLanguage = strtoupper($_SESSION['language_code']);
		$ddCurrency = $_SESSION['currency'];

		if ($this->debug)
		{
			$url = 'https://test.tripledeal.com/ps/com.tripledeal.paymentservice.servlets.PaymentService';
			$password = $this->debugpw;
		}
		else
		{
			$url = $this->form_action_url;
			$password = _PAYMENT_DOCDATA_PAYMENTS_PWD ;
		}
		// es gibt nur folgende Emailsprache: available nl, en, de, fr, it and es (February 2010)

		$options['postfields'] = array(
			'command' => 'new_payment_cluster',
			'merchant_name' => _PAYMENT_DOCDATA_PAYMENTS_MERCHANTID,
			'merchant_password' => $password ,
			'merchant_transaction_id' => $_SESSION['tmp_oID'],
			'profile' => $this->ddprofile,
			'css_id' => 2,
			'client_id' => $order->customer['email_address'],
			'price' => round($order->info['total'],2),
			'cur_price' => $order->info['currency'],
			'client_email' => $order->customer['email_address'],
			'client_firstname' => $order->billing['firstname'],
			'client_lastname' => $order->billing['lastname'],
			'client_address' => $order->billing['street_address'],
			'client_zip' => $order->billing['postcode'],
			'client_city' => $order->billing['city'],
			'client_country' => $order->billing['country']['iso_code_2'],
			'client_language' =>  (!in_array($this->session_language(),array('nl','de','fr','it','es')) ? 'en' : $this->session_language()),
			'days_pay_period' => 0,
			'include_costs' => 'no'
		);

		 foreach ($options['postfields'] as $key => $value) {
          $value = strtr($value, "������������������", "aeiooouuuAEIOOOUUU");
          if ($key!='status_url') {
          	$value=urlencode(trim($value));
          }

          $data .= $key . '=' . $value . "&";
        }
		// echo $url.'?'.$data."<br><br>";	die();
		file_put_contents('ddpay_get.log','['.date('d.m.Y H:i:s').'] '.$_SESSION['tmp_oID'].': getting..'."\n",FILE_APPEND);
		$result = $this->_curl_call($url,$options);
		file_put_contents('ddpay_get.log','['.date('d.m.Y H:i:s').'] '.$_SESSION['tmp_oID'].': '.$result.''."\n",FILE_APPEND);
		preg_match('/key value="([^"]+)"/',$result,$matches);

		unset($pck);
		$pck = $matches[1];

		// keine Transaktion starten k�nnen? Zur�ck zur Paymentauswahl.
		if ($pck == '' || !isset($pck))
		{
			// das wird von der Paymentseite gemacht! xtc_db_query('UPDATE '.TABLE_ORDERS.' SET orders_status = "8" WHERE orders_id = '.$this->sqlval($_SESSION['tmp_oID']).'');
			xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PAYMENT, 'tid=&payment_error='.$this->code . constant('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($this->module).'_ERRORTEXT2'), 'NONSSL', true, false));
		}

		$_SESSION['transaction_id'] = $pck;

		$res = xtc_db_query('UPDATE '.TABLE_ORDERS.' SET payment_cluster_key = '.$this->sqlval($pck).',order_billing_country='.$this->sqlval($order->billing['country']['iso_code_2']).',payment_profile='.$this->sqlval($this->ddprofile).' WHERE orders_id = '.$this->sqlval($_SESSION['tmp_oID']).'');

		if (!$res)
			xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PAYMENT, 'tid=&payment_error='.$this->code . constant('MODULE_PAYMENT_DOCDATA_PAYMENTS_'.strtoupper($this->module).'_ERRORTEXT2'), 'NONSSL', true, false));


 		$params = array('command' => 'show_payment_cluster',
			'merchant_name'=> _PAYMENT_DOCDATA_PAYMENTS_MERCHANTID,
			'client_language' => $this->session_language(),
			'payment_cluster_key' => $pck,
			'return_url_success' => xtc_href_link(FILENAME_CHECKOUT_PROCESS ,'&tid=', 'NONSSL', true, false),
			'return_url_canceled' => xtc_href_link(FILENAME_CHECKOUT_PAYMENT ,'&tid=', 'NONSSL', true, false),
			'return_url_pending' => xtc_href_link(FILENAME_CHECKOUT_PROCESS ,'&tid=', 'NONSSL', true, false)
		);

//			'return_url_success' => xtc_href_link('docdata_callback.php', 'dd_target='. FILENAME_CHECKOUT_PROCESS .'&tid=', 'NONSSL', true, false),
//			'return_url_canceled' => xtc_href_link('docdata_callback.php', 'dd_target='. FILENAME_CHECKOUT_PAYMENT .'&tid=', 'NONSSL', true, false),
//			'return_url_pending' => xtc_href_link('docdata_callback.php', 'dd_target='. FILENAME_CHECKOUT_PROCESS .'&tid=', 'NONSSL', true, false)

		$data = '';
        foreach ($params as $key => $value) {
          $value = strtr($value, "������������������", "aeiooouuuAEIOOOUUU");
          if ($key!='status_url') {
          	$value=urlencode(trim($value));
          }
          $data .= $key . '=' . $value . "&";
        }

        	// direktweiterleitung wenn gew�nscht.
	// ideal ben�tigt noch die ideal_issuer_id.. wenn das supportet werden soll muss dies hier ebenfalls �bergeben werden.
		if ($this->skip_paymentselection)
			$data = $data.'&default_pm='.$this->ddpmodule.'&default_act=yes';

//die($url.'?'.$data);
		return $url.'?'.$data;


	}
}
