<?php


/* -----------------------------------------------------------------------------------------
   $Id: xtcPrice.php 1316 2005-10-21 15:30:58Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(currencies.php,v 1.15 2003/03/17); www.oscommerce.com
   (c) 2003         nextcommerce (currencies.php,v 1.9 2003/08/17); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------
   Modified by Gunnar Tillmann (August 2006)
   http://www.gunnart.de

   Everywhere a price is displayed you see any existing kind of discount in percent and
   in saved money in your chosen currency

   Changes in following lines:

   347-352 / 365-366 / 384-389
   ---------------------------------------------------------------------------------------*/

class xtcPrice {
	var $currencies;

	// class constructor
	function xtcPrice($currency, $cGroup) {

		$this->currencies = array ();
		$this->cStatus = array ();
		$this->actualGroup = $cGroup;
		$this->actualCurr = $currency;
		$this->TAX = array ();
		$this->SHIPPING = array();
		$this->showFrom_Attributes = true;

		// select Currencies

		$currencies_query = "SELECT *
				                                    FROM
				                                         ".TABLE_CURRENCIES;
		$currencies_query = xtDBquery($currencies_query);
		while ($currencies = xtc_db_fetch_array($currencies_query, true)) {
			$this->currencies[$currencies['code']] = array ('title' => $currencies['title'], 'symbol_left' => $currencies['symbol_left'], 'symbol_right' => $currencies['symbol_right'], 'decimal_point' => $currencies['decimal_point'], 'thousands_point' => $currencies['thousands_point'], 'decimal_places' => $currencies['decimal_places'], 'value' => $currencies['value']);
		}
		// select Customers Status data
		$customers_status_query = "SELECT *
				                                        FROM
				                                             ".TABLE_CUSTOMERS_STATUS."
				                                        WHERE
				                                             customers_status_id = '".$this->actualGroup."' AND language_id = '".$_SESSION['languages_id']."'";
		$customers_status_query = xtDBquery($customers_status_query);
		$customers_status_value = xtc_db_fetch_array($customers_status_query, true);
		$this->cStatus = array ('customers_status_id' => $this->actualGroup, 'customers_status_name' => $customers_status_value['customers_status_name'], 'customers_status_image' => $customers_status_value['customers_status_image'], 'customers_status_public' => $customers_status_value['customers_status_public'], 'customers_status_discount' => $customers_status_value['customers_status_discount'], 'customers_status_ot_discount_flag' => $customers_status_value['customers_status_ot_discount_flag'], 'customers_status_ot_discount' => $customers_status_value['customers_status_ot_discount'], 'customers_status_graduated_prices' => $customers_status_value['customers_status_graduated_prices'], 'customers_status_show_price' => $customers_status_value['customers_status_show_price'], 'customers_status_show_price_tax' => $customers_status_value['customers_status_show_price_tax'], 'customers_status_add_tax_ot' => $customers_status_value['customers_status_add_tax_ot'], 'customers_status_payment_unallowed' => $customers_status_value['customers_status_payment_unallowed'], 'customers_status_shipping_unallowed' => $customers_status_value['customers_status_shipping_unallowed'], 'customers_status_discount_attributes' => $customers_status_value['customers_status_discount_attributes'], 'customers_fsk18' => $customers_status_value['customers_fsk18'], 'customers_fsk18_display' => $customers_status_value['customers_fsk18_display']);

		// prefetch tax rates for standard zone
		$zones_query = xtDBquery("SELECT tax_class_id as class FROM ".TABLE_TAX_CLASS);
		while ($zones_data = xtc_db_fetch_array($zones_query,true)) {

			// calculate tax based on shipping or deliverey country (for downloads)
			if (isset($_SESSION['billto']) && isset($_SESSION['sendto'])) {
			$tax_address_query = xtc_db_query("select ab.entry_country_id, ab.entry_zone_id from " . TABLE_ADDRESS_BOOK . " ab left join " . TABLE_ZONES . " z on (ab.entry_zone_id = z.zone_id) where ab.customers_id = '" . $_SESSION['customer_id'] . "' and ab.address_book_id = '" . ($this->content_type == 'virtual' ? $_SESSION['billto'] : $_SESSION['sendto']) . "'");
      		$tax_address = xtc_db_fetch_array($tax_address_query);
			$this->TAX[$zones_data['class']]=xtc_get_tax_rate($zones_data['class'],$tax_address['entry_country_id'], $tax_address['entry_zone_id']);
			} else {
			$this->TAX[$zones_data['class']]=xtc_get_tax_rate($zones_data['class']);
			}


		}

	}

	// get products Price
	function xtcGetPrice($pID, $format = true, $qty, $tax_class, $pPrice, $vpeStatus = 0, $cedit_id = 0) {

			// check if group is allowed to see prices
	if ($this->cStatus['customers_status_show_price'] == '0')
			return $this->xtcShowNote($vpeStatus, $vpeStatus);

		// get Tax rate
		if ($cedit_id != 0) {
			$cinfo = xtc_oe_customer_infos($cedit_id);
			$products_tax = xtc_get_tax_rate($tax_class, $cinfo['country_id'], $cinfo['zone_id']);
		} else {
			$products_tax = $this->TAX[$tax_class];
		}

		if ($this->cStatus['customers_status_show_price_tax'] == '0')
			$products_tax = '';

		// add taxes
		if ($pPrice == 0)
			$pPrice = $this->getPprice($pID);
		$pPrice = $this->xtcAddTax($pPrice, $products_tax);

		// check specialprice
		if ($sPrice = $this->xtcCheckSpecial($pID))
			return $this->xtcFormatSpecial($pID, $this->xtcAddTax($sPrice, $products_tax), $pPrice, $format, $vpeStatus);

		// check graduated
		if ($this->cStatus['customers_status_graduated_prices'] == '1') {
			if ($sPrice = $this->xtcGetGraduatedPrice($pID, $qty))
				return $this->xtcFormatSpecialGraduated($pID, $this->xtcAddTax($sPrice, $products_tax), $pPrice, $format, $vpeStatus, $pID);
		} else {
			// check Group Price
			if ($sPrice = $this->xtcGetGroupPrice($pID, 1))
				return $this->xtcFormatSpecialGraduated($pID, $this->xtcAddTax($sPrice, $products_tax), $pPrice, $format, $vpeStatus, $pID);
		}

		// check Product Discount
		if ($discount = $this->xtcCheckDiscount($pID))
			return $this->xtcFormatSpecialDiscount($pID, $discount, $pPrice, $format, $vpeStatus);

		return $this->xtcFormat($pPrice, $format, 0, false, $vpeStatus, $pID);

	}

	function getPprice($pID) {
		$pQuery = "SELECT products_price FROM ".TABLE_PRODUCTS." WHERE products_id='".$pID."'";
		$pQuery = xtDBquery($pQuery);
		$pData = xtc_db_fetch_array($pQuery, true);
		return $pData['products_price'];


	}

	function xtcAddTax($price, $tax) {
		$price = $price + $price / 100 * $tax;
		$price = $this->xtcCalculateCurr($price);
		return round($price, $this->currencies[$this->actualCurr]['decimal_places']);
	}

	function xtcCheckDiscount($pID) {

		// check if group got discount
		if ($this->cStatus['customers_status_discount'] != '0.00') {

			$discount_query = "SELECT products_discount_allowed FROM ".TABLE_PRODUCTS." WHERE products_id = '".$pID."'";
			$discount_query = xtDBquery($discount_query);
			$dData = xtc_db_fetch_array($discount_query, true);

			$discount = $dData['products_discount_allowed'];
			if ($this->cStatus['customers_status_discount'] < $discount)
				$discount = $this->cStatus['customers_status_discount'];
			if ($discount == '0.00')
				return false;
			return $discount;

		}
		return false;
	}

	function xtcGetGraduatedPrice($pID, $qty) {
		if (GRADUATED_ASSIGN == 'true')
			if (xtc_get_qty($pID) > $qty)
				$qty = xtc_get_qty($pID);
		//if (!is_int($this->cStatus['customers_status_id']) && $this->cStatus['customers_status_id']!=0) $this->cStatus['customers_status_id'] = DEFAULT_CUSTOMERS_STATUS_ID_GUEST;
		$graduated_price_query = "SELECT max(quantity) as qty
				                                FROM ".TABLE_PERSONAL_OFFERS_BY.$this->actualGroup."
				                                WHERE products_id='".$pID."'
				                                AND quantity<='".$qty."'";
		$graduated_price_query = xtDBquery($graduated_price_query);
		$graduated_price_data = xtc_db_fetch_array($graduated_price_query, true);
		if ($graduated_price_data['qty']) {
			$graduated_price_query = "SELECT personal_offer
						                                FROM ".TABLE_PERSONAL_OFFERS_BY.$this->actualGroup."
						                                WHERE products_id='".$pID."'
						                                AND quantity='".$graduated_price_data['qty']."'";
			$graduated_price_query = xtDBquery($graduated_price_query);
			$graduated_price_data = xtc_db_fetch_array($graduated_price_query, true);

			$sPrice = $graduated_price_data['personal_offer'];
			if ($sPrice != 0.00)
				return $sPrice;
		} else {
			return;
		}

	}

	function xtcGetGroupPrice($pID, $qty) {

		$graduated_price_query = "SELECT max(quantity) as qty
				                                FROM ".TABLE_PERSONAL_OFFERS_BY.$this->actualGroup."
				                                WHERE products_id='".$pID."'
				                                AND quantity<='".$qty."'";
		$graduated_price_query = xtDBquery($graduated_price_query);
		$graduated_price_data = xtc_db_fetch_array($graduated_price_query, true);
		if ($graduated_price_data['qty']) {
			$graduated_price_query = "SELECT personal_offer
						                                FROM ".TABLE_PERSONAL_OFFERS_BY.$this->actualGroup."
						                                WHERE products_id='".$pID."'
						                                AND quantity='".$graduated_price_data['qty']."'";
			$graduated_price_query = xtDBquery($graduated_price_query);
			$graduated_price_data = xtc_db_fetch_array($graduated_price_query, true);

			$sPrice = $graduated_price_data['personal_offer'];
			if ($sPrice != 0.00)
				return $sPrice;
		} else {
			return;
		}

	}

	function xtcShowNote($vpeStatus, $vpeStatus = 0) {
		if ($vpeStatus == 1)
			return array ('formated' => NOT_ALLOWED_TO_SEE_PRICES, 'plain' => 0);
		return NOT_ALLOWED_TO_SEE_PRICES;
	}

	function xtcCheckSpecial($pID) {
		$product_query = '
			SELECT 
				s.specials_new_products_price,
				s.discount_in_percent,
				p.products_price
			FROM '.TABLE_SPECIALS.' s
			JOIN products p USING (products_id)
			WHERE p.products_status = 1
				AND p.products_id = '.xtc_db_prepare_input($pID).'
				AND s.start_date = DATE(NOW())
				AND s.expires_date = DATE(NOW())
				AND s.status = 1
		';
		$product = xtc_db_fetch_array(xtDBquery($product_query));
		if ($product['specials_new_products_price'] == 0)
			return $product['products_price'] * (100 - $product['discount_in_percent']) / 100;

		return $product['specials_new_products_price'];

	}

	function xtcCalculateCurr($price) {
		return $this->currencies[$this->actualCurr]['value'] * $price;
	}

	function calcTax($price, $tax) {
		return $price * $tax / 100;
	}

	function xtcRemoveCurr($price) {

		// check if used Curr != DEFAULT curr
		if (DEFAULT_CURRENCY != $this->actualCurr) {
			return $price * (1 / $this->currencies[$this->actualCurr]['value']);
		} else {
			return $price;
		}

	}

	function xtcRemoveTax($price, $tax) {
		$price = ($price / (($tax +100) / 100));
		return $price;
	}

	function xtcGetTax($price, $tax) {
		$tax = $price - $this->xtcRemoveTax($price, $tax);
		return $tax;
	}

	function xtcRemoveDC($price,$dc) {

		$price = $price - ($price/100*$dc);

		return $price;
	}

	function xtcGetDC($price,$dc) {

		$dc = $price/100*$dc;

		return $dc;
	}

	function checkAttributes($pID)
	{
		if (!$this->showFrom_Attributes)
			return;

		if ($pID == 0)
			return;

		$formula = get_cached_formula($pID);

		if ($formula['rules'] && $formula['especialnesses'])
			return ' <span class="price_from">'.strtolower(FROM).'</span> ';
	}

	function xtcCalculateCurrEx($price, $curr) {
		return $price * ($this->currencies[$curr]['value'] / $this->currencies[$this->actualCurr]['value']);
	}

	/*
	*
	*    Format Functions
	*
	*
	*
	*/

	function xtcFormat($price, $format, $tax_class = 0, $curr = false, $vpeStatus = 0, $pID = 0) {

		if ($curr)
			$price = $this->xtcCalculateCurr($price);

		if ($tax_class != 0) {
			$products_tax = $this->TAX[$tax_class];
			if ($this->cStatus['customers_status_show_price_tax'] == '0')
				$products_tax = '';
			$price = $this->xtcAddTax($price, $products_tax);
		}

		if ($format) {
			$Pprice = number_format($price, $this->currencies[$this->actualCurr]['decimal_places'], $this->currencies[$this->actualCurr]['decimal_point'], $this->currencies[$this->actualCurr]['thousands_point']);
			$Pprice = $this->checkAttributes($pID).$this->currencies[$this->actualCurr]['symbol_left'].$Pprice;
			if($this->currencies[$this->actualCurr]['symbol_right'])
				$Pprice .= ' '.$this->currencies[$this->actualCurr]['symbol_right'];
			if ($vpeStatus == 0) {
				return $Pprice;
			} else {
				return array ('formated' => $Pprice, 'plain' => $price);
			}
		} else {

			return round($price, $this->currencies[$this->actualCurr]['decimal_places']);

		}

	}

	function xtcFormatSpecialDiscount($pID, $discount, $pPrice, $format, $vpeStatus = 0) {
		$sPrice = $pPrice - ($pPrice / 100) * $discount;
		if ($format) {
			//$price = '<span class="productOldPrice">'.INSTEAD.$this->xtcFormat($pPrice, $format).'</span><br />'.ONLY.$this->checkAttributes($pID).$this->xtcFormat($sPrice, $format).'<br />'.YOU_SAVE.$discount.'%';
			$price = '<span class="productOldPrice"><small>'.INSTEAD.'</small><del>'.$this->xtcFormat($pPrice, $format).'</del></span><br />'.ONLY.$this->checkAttributes($pID).$this->xtcFormat($sPrice, $format).'<br /><small>'.YOU_SAVE.round(($pPrice-$sPrice) / $pPrice * 100,1).' % /'.$this->xtcFormat($pPrice-$sPrice, $format);
			// Ausgabe des g�ltigen Kundengruppen-Rabatts (sofern vorhanden)
			if ($discount != 0)
					{ $price .= '<br />'.BOX_LOGINBOX_DISCOUNT.': '.round($discount).' %'; }
				$price .= '</small>';
			if ($vpeStatus == 0) {
				return $price;
			} else {
				return array ('formated' => $price, 'plain' => $sPrice);
			}
		} else {
			return round($sPrice, $this->currencies[$this->actualCurr]['decimal_places']);
		}
	}

	function xtcFormatSpecial($pID, $sPrice, $pPrice, $format, $vpeStatus = 0) {
		if ($format) {
			//$price = '<span class="productOldPrice">'.INSTEAD.$this->xtcFormat($pPrice, $format).'</span><br />'.ONLY.$this->checkAttributes($pID).$this->xtcFormat($sPrice, $format);
			$price = '<div class="new_price"><span>'.ONLY.'</span><div id="discounted_price">'.$this->xtcFormat($sPrice, $format).'</div></div>';
			$price .= '<div class="old_price">'.INSTEAD.'<span id="normal_price">'.$this->xtcFormat($pPrice, $format).'</span></div>';
//			$price = ''.INSTEAD.''.$this->xtcFormat($pPrice, $format).'</del></span><br />'.ONLY.$this->checkAttributes($pID).$this->xtcFormat($sPrice, $format).'<br /><small>'.YOU_SAVE.round(($pPrice-$sPrice) / $pPrice * 100,1).' % /'.$this->xtcFormat($pPrice-$sPrice, $format).'</small>';
			if ($vpeStatus == 0) {
				return $price;
			} else {
				return array ('formated' => $price, 'plain' => $sPrice);
			}
		} else {
			return round($sPrice, $this->currencies[$this->actualCurr]['decimal_places']);
		}
	}

function xtcFormatSpecialGraduated($pID, $sPrice, $pPrice, $format, $vpeStatus = 0, $pID) {
	// NEU HINZUGEF�GT "Steuerklasse ermitteln"
	$tQuery = "SELECT products_tax_class_id
		FROM ".TABLE_PRODUCTS." WHERE
		products_id='".$pID."'";
	$tQuery = xtc_db_query($tQuery);
   	$tQuery = xtc_db_fetch_array($tQuery);
   	$tax_class = $tQuery[products_tax_class_id];
	// ENDE "Steuerklasse ermitteln"

	if ($pPrice == 0)
		return $this->xtcFormat($sPrice, $format, 0, false, $vpeStatus);
	if ($discount = $this->xtcCheckDiscount($pID))
		$sPrice -= $sPrice / 100 * $discount;
	if ($format) {
		// NEU HINZUGEF�GT
		$sQuery = "SELECT max(quantity) as qty
			FROM ".TABLE_PERSONAL_OFFERS_BY.$this->actualGroup."
			WHERE products_id='".$pID."'";
		$sQuery = xtDBquery($sQuery);
		$sQuery = xtc_db_fetch_array($sQuery, true);
		// NEU! Damit "UVP"-Anzeige wieder m�glich ist
		// if ( ($this->cStatus['customers_status_graduated_prices'] == '1') || ($sQuery[qty] > 1) ) {
		if ( ($this->cStatus['customers_status_graduated_prices'] == '1') && ($sQuery[qty] > 1) ) {
			$bestPrice = $this->xtcGetGraduatedPrice($pID, $sQuery[qty]);
			if ($discount)
				$bestPrice -= $bestPrice / 100 * $discount;
			$price .= FROM.$this->xtcFormat($bestPrice, $format, $tax_class)
				.' <br /><small>Einzelpreis: '
				.$this->xtcFormat($sPrice, $format)
				.'</small>';
		} else if ($sPrice != $pPrice) { // if ($sPrice != $pPrice) {
			$price = '<span class="productOldPrice">'.MSRP.' '.$this->xtcFormat($pPrice, $format).'</span><br />'.YOUR_PRICE.$this->checkAttributes($pID).$this->xtcFormat($sPrice, $format);
		} else {
			$price = FROM.$this->xtcFormat($sPrice, $format);
		}
		if ($vpeStatus == 0) {
			return $price;
		} else {
			return array ('formated' => $price, 'plain' => $sPrice);
		}
	} else {
		return round($sPrice, $this->currencies[$this->actualCurr]['decimal_places']);
	}
}

	function get_decimal_places($code) {
		return $this->currencies[$this->actualCurr]['decimal_places'];
	}

}

/*
     function xtcFormatI($price,$format,$tax_class=0,$curr=false) {

     	if ($curr=='true')
	    $price = $this->xtcCalculateCurr($price);

		if ($tax_class != 0)
		{
	    $products_tax = $this->TAX[$tax_class];
		  if ($this->cStatus['customers_status_show_price_tax'] == '0')
				$products_tax = '';
	    $price = $this->xtcAddTax($price, $products_tax);
		}
		if ($format==true) {
		    $Pprice = number_format($price, $this->currencies[$this->actualCurr]['decimal_places'], $this->currencies[$this->actualCurr]['decimal_point'], $this->currencies[$this->actualCurr]['thousands_point']);
		    $Pprice = $this->currencies[$this->actualCurr]['symbol_left'].' '.$Pprice.' '.$this->currencies[$this->actualCurr]['symbol_right'];

		    if($this->currencies[$this->actualCurr]['symbol_left'] != "") {
		    	$curr_code = $this->currencies[$this->actualCurr]['symbol_left'];
		    } else {
		    	$curr_code = $this->currencies[$this->actualCurr]['symbol_right'];
		    }
		    if(USE_PRICE_IMAGES == 'true') {
	   	    	$price =$this->xtcPriceAsImage($Pprice, $curr_code);
		    } else {
		    	$price = $Pprice;
		    }
			return $price;

		} else {
		   return $price;
		}

	}

    function xtcPriceAsImage($price, $curr_code) {
	  $imagePath = DIR_WS_TEMPLATE_FOLDER.CURRENT_TEMPLATE. '/img/digits/';

	  $price = explode($this->currencies[$this->actualCurr]['decimal_point'],trim((string)$price));
	  $prefix = $price[0];
	  $prefix = str_replace($this->currencies[$this->actualCurr]['thousands_point'],"",$prefix);
	  $suffix = $price[1];
	  $leer = strpos($suffix, ' ');
	  $suffix = substr($price[1], 0, $leer);
	  $pre='';
	  $pre.='<img border="0" src="'.$imagePath.strtolower($curr_code).'.gif" />';
	  for ($i=0;$i<strlen($prefix);$i++) {
	         $pre.='<img border="0" src="'.$imagePath.substr($prefix,$i,1).'.gif" />';
	  }
	  $pre.='<img border="0" src="'.$imagePath.',.gif" />';
	  $su='';
	  if($suffix[0] == 0 && $suffix[1] == 0) {
	  	       $su.='<img border="0" src="'.$imagePath.'-.gif" />';
	  } else {
		  for ($i=0;$i<strlen($suffix);$i++) {
		         $su.='<img border="0" src="'.$imagePath.substr($suffix,$i,1).'small.gif" />';
		  }
	  }
	  return $pre.$su;
	 }
*/
?>