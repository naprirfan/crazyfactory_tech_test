<?php
require_once(DIR_FS_DOCUMENT_ROOT.'/includes/functions/log.php');

class lists
{
	private $list;

	function lists()
	{
		$this->list = array();
		$this->get_complete_list();
	}

	function get_complete_list($list_type = 'watchlist', $customers_id = 0)
	{
		if ($customers_id == 0)
			$customers_id = $_SESSION['customer_id'];

		$list_array = $this->get_list($list_type, $customers_id);
		if (!is_array($list_array))
		{
			$this->create_list($list_type, $customers_id);
			$list_array = $this->get_list($list_type, $customers_id);
		}
		$sql = '
			SELECT
				clp.customers_list_products_id,
				clp.products_id,
				clp.datetime_added,
                clp.attributes
			FROM customers_list_products clp
			JOIN products p USING (products_id)
			WHERE clp.customers_list_id = '.$list_array['customers_list_id'].'
		';
		$list_products_query = xtc_db_query($sql);
        $products_array = array();
		while ($product = xtc_db_fetch_array($list_products_query))
		{
			$incomplete_product = false;
			$product['attributes'] = unserialize($product['attributes']);
			$product['attributes_in_db'] = $product['attributes'];
			@ksort($product['attributes']);

			$formula = get_cached_formula($product['products_id']);
			if(!check_attribute_combination($formula['rules'], $product['attributes']))
				$incomplete_product = true;

			$products_array[] = array(
				'customers_list_products_id' => $product['customers_list_products_id'],
				'products_id' => $product['products_id'],
				'date_added' => $this->parse_date($product['datetime_added']),
				'attributes' => $product['attributes'],
				'attributes_in_db' => $product['attributes_in_db'],
				'incomplete' => $incomplete_product,
			);
		}
		$this->list = array(
			'customers_list_id' => $list_array['customers_list_id'],
			'date_created' => $this->parse_date($list_array['date_created']),
			'date_changed' => $this->parse_date($list_array['date_changed']),
			'products' => $products_array,
		);
	}

	function create_list($list_type = 'watchlist', $customers_id = 0)
	{
		if ($customers_id == 0)
			$customers_id = $_SESSION['customer_id'];
		$sql = '
			INSERT INTO customers_list (
				customers_id,
				list_type,
				date_created
			) VALUES (
				'.$customers_id.',
				"'.$list_type.'",
				NOW()
			)
		';
		xtc_db_query($sql);
	}

	function get_list($list_type, $customers_id)
	{
		$sql = '
			SELECT
				customers_list_id,
				date_created,
				date_changed
			FROM customers_list
			WHERE customers_id = '.$customers_id.'
				AND list_type = "'.$list_type.'"
		';
		return xtc_db_fetch_array(xtc_db_query($sql));
	}

	function add_to_list($products_id, $attributes = array())
	{
		if (is_numeric($products_id))
		{
			$constructor = @array_pop(array_pop(query('
				SELECT `konstruktor`
				FROM `products`
				WHERE `products_id` = '.sqlval($products_id).'
			')));

			if (is_string($attributes))
			{
				$AttributesKeys = array_merge( //Namen der Felder
					array_filter(
						preg_split('/(\[|\]|\]\[)/', $constructor),
						create_function('$val', 'return !empty($val) && $val != "*";')
					)
				);
				$AttributesValues = array_merge( //Namen der Felder
					array_filter(
						preg_split('/(\[|\]|\]\[)/', $attributes),
						create_function('$val', 'return !empty($val) && $val != "*";')
					)
				);

				if(count($AttributesKeys) && count($AttributesKeys) == count($AttributesValues))
					$attributes = array_combine($AttributesKeys, $AttributesValues);
			}

			@ksort($attributes);

			$formula = get_cached_formula($products_id);
			if (!check_attribute_combination($formula['rules'], $attributes))
				return 'attribute_not_found';

			$index_in_list = $this->find_in_list($products_id, $attributes);

			if ($index_in_list === false && is_numeric($products_id))
			{
				$index_in_list = count($this->list['products']);

				$this->list['products'][$index_in_list] = array(
					'products_id' => $products_id,
					'date_added' => date('d.m.Y H:i:s'),
					'attributes' => $attributes
				);
				$sql = '
					INSERT INTO customers_list_products (
						customers_list_id,
						products_id,
						attributes,
						datetime_added
					) VALUES (
						'.$this->list['customers_list_id'].',
						'.$products_id.',
						\''.addslashes(serialize($attributes)).'\',
						NOW()
					)
				';

				xtc_db_query($sql);
				$this->list['products'][$index_in_list]['customers_list_products_id'] = xtc_db_insert_id();
				return $this->build_product_array($this->list['products'][$index_in_list]);
			}
			else
				return 'already_in_list';
		}
		else
			return 'doubled';
	}

	function parse_date($date)
	{
		$timestamp = strtotime($date);
		return date('d.m.Y', $timestamp);
	}

	function build_product_array($product_from_list)
	{
		$sql = '
			SELECT
				pd.products_name,
				pd.products_description,
				p.products_model,
				p.products_image,
				p.products_price
			FROM products p
			LEFT OUTER JOIN products_description pd USING (products_id)
			WHERE p.products_id = '.$product_from_list['products_id'].'
				AND pd.language_id = '.$_SESSION['languages_id'].'
		';
		$product_query = xtc_db_query($sql);
		$product_array = xtc_db_fetch_array($product_query);
		$image = get_product_image_array($product_from_list['products_id'], $product_from_list['attributes']);
		return array(
			'products_id' => $product_from_list['products_id'],
			'name' => $product_array['products_name'],
			'description' => $product_array['products_description'],
			'products_model' => $product_array['products_model'],
			'image' => $image['S'],
			'price' => $product_array['products_price'],
			'attributes' => $product_from_list['attributes'],
			'incomplete' => $product_from_list['incomplete'],
		);
	}

	function get_products()
	{
		if (count($this->list['products']) > 0)
		{
			foreach ($this->list['products'] as $product)
			{
				$products[] = $this->build_product_array($product);
			}
			return $products;
		}
		else
			return false;
	}

	function remove_from_list($position)
	{
		$current_product = $this->list['products'][$position];

		if (isset($current_product['products_id']))
		{
			$sql = '
				DELETE FROM customers_list_products
				WHERE customers_list_products_id = '.$current_product['customers_list_products_id'].'
			';
			xtc_db_query($sql);
			unset($this->list['products'][$position]);
		}
	}

	function find_in_list($products_id, $attributes = array())
	{
        foreach($this->list['products'] as $index => $current_product)
        {
            if($current_product['products_id'] == $products_id)
            {
                if(!$current_product['attributes'])
                    return $index;
                if($current_product['attributes'] == $attributes)
                    return $index;
            }
        }
        return false;
	}
}