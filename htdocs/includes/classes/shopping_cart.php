<?php
/* -----------------------------------------------------------------------------------------
   $Id: shopping_cart.php 1534 2006-08-20 19:39:22Z mz $
   XT-Commerce - community made shopping
   http://www.xt-commerce.com
   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(shopping_cart.php,v 1.32 2003/02/11); www.oscommerce.com
   (c) 2003	 nextcommerce (shopping_cart.php,v 1.21 2003/08/17); www.nextcommerce.org

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   Customers Status v3.x  (c) 2002-2003 Copyright Elari elari@free.fr | www.unlockgsm.com/dload-osc/ | CVS : http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/elari/?sortby=date#dirlist
   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

require_once('shopping_cart_ng.php');

// include needed functions
require_once (DIR_FS_INC.'xtc_create_random_value.inc.php');
require_once (DIR_FS_INC.'xtc_get_prid.inc.php');
require_once (DIR_FS_INC.'xtc_draw_form.inc.php');
require_once (DIR_FS_INC.'xtc_draw_input_field.inc.php');
require_once (DIR_FS_INC.'xtc_image_submit.inc.php');
require_once (DIR_FS_INC.'xtc_get_tax_description.inc.php');
require_once(DIR_FS_DOCUMENT_ROOT.'/includes/functions/cash_points_functions.php');
require_once(DIR_FS_DOCUMENT_ROOT.'/includes/database.inc.php');
require_once(DIR_FS_DOCUMENT_ROOT.'/includes/functions/price_functions.inc.php');
require_once(DIR_FS_DOCUMENT_ROOT.'/includes/functions/log.php');

// Ich denk mir jetzt keine tollen Funktionsnamen mehr aus wenn ich hier dauernd irgendwelche dämliche kacke
// hin und her schieben muss weil sie an 200 verschiedenen Stellen gemacht wird und ich sie nicht an die 201. Stelle
// kopieren will
function produktpreis_mit_formel_ausrechnen($product_data, $attributes)
{
	return array(
	    'original_price' => get_products_price($product_data['products_id']),
	    'price' => get_products_price($product_data['products_id'], $attributes)
    );
}

class shoppingCart
{
	private $contents, $total, $weight, $content_type, $tax, $total_discount;
	public $cartID;

	function __construct()
	{
		// Das wird nicht mehr gebraucht... reset() ist jetzt nurnoch zum tats�chlichen leeren des Warenkorbs da
		// wenn man das beim erstellen machen w�rde w�rde er auch die Inhalte in der DB l�schen - und das w�re... schei�e
		//$this->reset();
	}

	function check_contents()
	{
		if (!isset ($_SESSION['customer_id']))
			return false;
		$products_query = xtc_db_query("
			SELECT
				products_id,
				customers_basket_quantity
			FROM " . TABLE_CUSTOMERS_BASKET . "
			WHERE customers_id = '" . $_SESSION['customer_id'] . "'
		");
	    if (xtc_db_num_rows($products_query))
    		return true;
    	else
	    	return false;
	}

	// Zusammenfassung:
	// 1. Merged Warenk�rbe
	// 2. verschiebt deaktivierte Produkte auf den Merkzettel
	//
	// 1. wird jetzt gesondert behandelt, 2. als Wrapper umgesetzt
	function restore_contents()
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);
		$cart->move_broken_or_deactivated_products_to_watchlist();
	}

	function reset($reset_database = false)
	{
		if ($reset_database == true)
		{
			$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);
			$cart->reset();
		}
	}

	function add_cart($products_id, $qty = '1', $attributes = null, $notify = true, $group_id = 1)
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		$array_product = array(
			'products_id' => $products_id,
			'attributes' => $attributes,
			'centralised_buying_group_id' => 1
		);

		$cart->add_to_cart($array_product, $qty);

		return $cart->get_specific_entry($array_product);
	}

	function update_quantity($basket_id, $quantity = '', $attributes = '', $group_id = 1)
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		$array_product = @$cart->get_specific_entry_by_entry_id($basket_id);

		if (!$array_product)
			return false;

		// Wenn die $quantity 0 ist soll das Produkt gel�scht werden
		if ($quantity < 1)
		{
			$cart->remove_entry($array_product);
			return true;
		}

		// Attribute in String-Form (altes Format) in ein Array umwandeln
		if (is_string($attributes))
		{
			$constructor = @array_pop(array_pop(query('SELECT `konstruktor` FROM `products` WHERE `products_id` = '.$array_product['products_id'])));

			$AttributesKeys = array_merge( //Namen der Felder
				array_filter(
					preg_split('/(\[|\]|\]\[)/', $constructor),
					create_function('$val', 'return !empty($val) && $val != "*";')
				)
			);
			$AttributesValues = array_merge( //Namen der Felder
				array_filter(
					preg_split('/(\[|\]|\]\[)/', $attributes),
					create_function('$val', 'return !empty($val) && $val != "*";')
				)
			);
			if (count($AttributesKeys) && count($AttributesKeys) == count($AttributesValues))
			{
				$attributes = array_combine($AttributesKeys, $AttributesValues);
			}
		}

		@ksort($attributes);

		// Wenn Attribute oder Sammelbestellungs-ID anders sind muss das Produkt neu hinzugef�gt werden
		if ($attributes !== $array_product['attributes'] ||
			intval($array_product['centralised_buying_group_id']) != intval($group_id))
		{
			// Alte Produktwerte zwischenspeichern
			$array_old_product = $array_product;

			$array_product = array(
				'products_id' => $array_product['products_id'],
				'attributes' => $attributes,
				'centralised_buying_group_id' => $group_id
			);

			// Pr�fen ob das neue Produkt so �berhaupt funktioniert
			if (!shopping_cart_ng::check_product_array($array_product))
				return false;

			$cart->remove_entry($array_old_product);

			// Wenn das Produkt mit diesen Attributen und Sammelbestellungs-ID schon vorhanden ist
			// muss die Anzahl erh�ht werden
			if ($cart->entry_in_cart($array_product))
			{
				$cart->add_to_cart($array_product, $quantity, false);
				return true;
			}
		}

		$cart->add_to_cart($array_product, $quantity, true);

		return true;
	}

	// Notizen: Das Ding hier l�scht offenbar Artikel die eine Menge < 1 haben
	function cleanup()
	{

	}

	function count_contents()
	{ // get total number of items in cart
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		$array_cart_entries = $cart->get_all_entries();

		$total_items = 0;
		foreach ($array_cart_entries as $le)
			$total_items+= $le['quantity'];

		$total_items+= count($cart->get_buyable_coupons());

		return $total_items;
	}

	function get_quantity($basket_id)
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		$array_product = @$cart->get_specific_entry_by_entry_id($basket_id);

		return $array_product['quantity'];
	}

	function in_cart($products_id, $group_id, $attributes = null)
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		$cart_entry_id = $cart->entry_in_cart(array(
		                                           'products_id' => $products_id,
		                                           'attributes' => $attributes,
		                                           'centralised_buying_group_id' => $group_id
		                                      ));

		return ($cart_entry_id ? true : false);
	}

	function remove($basket_id)
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		$array_product = @$cart->get_specific_entry_by_entry_id($basket_id);

		$cart->remove_entry($array_product);
	}

	function remove_all()
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		$array_cart_entires = $cart->get_all_entries();

		foreach ($array_cart_entires as $le)
			$cart->remove_entry($le);
	}

	function get_product_id_list()
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		$array_cart_entires = $cart->get_all_entries();
		$product_id_list = '';

		foreach ($array_cart_entires as $le)
			$product_id_list.= $le['products_id'].', ';

		return substr($product_id_list, -2);
	}

	function calculate()
	{
	}

	function get_products()
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		$array_products = $cart->get_all_entries();
		$array_products_return = $this->attach_old_cart_values_to_shopping_cart_ng_product_arrays($array_products);

		// Coupon anh�ngen
		$array_coupon = $cart->get_coupon_data();
		if ($array_coupon)
			$array_products_return[] = array(
				'basket_id' => '',
				'id' => $array_coupon['id'],
				'name' => $array_coupon['coupon_name'],
				'coupon_type' => $array_coupon['coupon_type'],
				'coupon_percent' => $array_coupon['coupon_percent'],
				'model' => 'coupon',
				'image' => 'coupon.jpg',
				'price' => $array_coupon['amount'] * -1,
				'individualprice' => $array_coupon['amount'] * -1,
				'quantity' => 1,
				'weight' => 0,
				'shipping_time' => '',
				'final_price' => $array_coupon['amount'] * -1,
				'tax_class_id' => 0,
				'attributes' => '',
				'centralised_buying_group_id' => 0,
				'attributes' => array(),
				'coupon_code' => $array_coupon['code'],
				'discountable' => 1,
			);

		$array_products_return = $this->group_products($array_products_return);

		// Gekaufte Coupons nach dem Gruppieren anh�ngen, da sonst Gutscheine rausfliegen k�nnen
		$array_buyable_coupons = $cart->get_buyable_coupons();

		foreach ($array_buyable_coupons as $key => $le)
			$array_products_return[] = array(
				'basket_id' => '',
				'id' => $key,
				'name' => $le['title'],
				'model' => 'CF-COUPON',
				'image' => 'coupon.jpg',
				'price' => convert_price_to_currency(convert_price_from_currency($le['amount'], $le['currency_code'])),
				'price_in_default_currency' => convert_price_from_currency($le['amount'], $le['currency_code']),
				'quantity' => 1,
				'weight' => 0,
				'shipping_time' => '',
				'final_price' => convert_price_to_currency(convert_price_from_currency($le['amount'], $le['currency_code'])),
				'tax_class_id' => 0,
				'attributes' => $le,
				'centralised_buying_group_id' => 0,
				'discountable' => 1,
			);

		return $array_products_return;
	}

	function show_total()
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		return $cart->calculate_total();
	}

	function show_weight()
	{
		$this->calculate();
		return $this->weight;
	}

	function show_tax($format = true)
	{
	}

	function generate_cart_id($length = 5)
	{
		return xtc_create_random_value($length, 'digits');
	}

	function get_content_type()
	{
		$this->content_type = false;

		if ((DOWNLOAD_ENABLED == 'true') && ($this->count_contents() > 0))
		{
			reset($this->contents);
			foreach($this->contents as $basket_id => $product)
			{
				/*$products_id = $product['products_id'];
				if (isset ($this->contents[$products_id][$group_id]['attributes'])) {
					reset($this->contents[$products_id][$group_id]['attributes']);
					while (list (, $value) = each($this->contents[$products_id][$group_id]['attributes'])) {
						$virtual_check_query = xtc_db_query("
							select count(*) as total
							from ".TABLE_PRODUCTS_ATTRIBUTES." pa,
								".TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD." pad
							where pa.products_id = '".$products_id."'
								and pa.options_values_id = '".$value."'
								and pa.products_attributes_id = pad.products_attributes_id
						");
						$virtual_check = xtc_db_fetch_array($virtual_check_query);

						if ($virtual_check['total'] > 0) {
							switch ($this->content_type) {
								case 'physical' :
									$this->content_type = 'mixed';
									return $this->content_type;
									break;

								default :
									$this->content_type = 'virtual';
									break;
							}
						} else {
							switch ($this->content_type) {
								case 'virtual' :
									$this->content_type = 'mixed';
									return $this->content_type;
									break;

								default :
									$this->content_type = 'physical';
									break;
							}
						}
					}
				} else {*/
				switch ($this->content_type)
				{
					case 'virtual' :
						$this->content_type = 'mixed';
						return $this->content_type;
						break;

					default :
						$this->content_type = 'physical';
						break;
				}
				//}
			}
		} else
		{
			$this->content_type = 'physical';
		}
		return $this->content_type;
	}

	// GV Code Start
	// ------------------------ ICW CREDIT CLASS Gift Voucher Addittion-------------------------------Start
	// amend count_contents to show nil contents for shipping
	// as we don't want to quote for 'virtual' item
	// GLOBAL CONSTANTS if NO_COUNT_ZERO_WEIGHT is true then we don't count any product with a weight
	// which is less than or equal to MINIMUM_WEIGHT
	// otherwise we just don't count gift certificates

	function count_contents_virtual()
	{ // get total number of items in cart disregard gift vouchers
		$total_items = 0;
		if (is_array($this->contents))
		{
			reset($this->contents);
			foreach($this->contents as $basket_id => $product)
			{
				$products_id = $product['products_id'];
				$no_count = false;
				$gv_query = xtc_db_query("
					SELECT products_model FROM ".TABLE_PRODUCTS."
					WHERE products_id = '".$products_id."'
				");
				$gv_result = xtc_db_fetch_array($gv_query);
				if (ereg('^GIFT', $gv_result['products_model']))
					$no_count = true;
				if (NO_COUNT_ZERO_WEIGHT == 1)
				{
					$gv_query = xtc_db_query("
						SELECT products_weight FROM ".TABLE_PRODUCTS."
						WHERE products_id = '".$products_id."'
					");
					$gv_result = xtc_db_fetch_array($gv_query);
					if ($gv_result['products_weight'] <= MINIMUM_WEIGHT)
						$no_count = true;
				}
				if (!$no_count)
					$total_items += $this->get_quantity($basket_id);
			}
		}
		return $total_items;
	}
	// ------------------------ ICW CREDIT CLASS Gift Voucher Addittion-------------------------------End
	//GV Code End

	//pr�fe ob der hinzugef�gte artikel schon mit gruppe 1 vorhanden ist
	function check_group($products_id, $new_group_id = 1, $attributes = null)
	{
		$return = false;
		if ($_SESSION['customer_id'])
		{
			$sql = '
				SELECT customers_basket_id FROM '.TABLE_CUSTOMERS_BASKET.'
				WHERE customers_id = "'.$_SESSION['customer_id'].'"
					AND products_id = '.$products_id.'
					AND centralised_buying_group_id = '.$new_group_id.'
					AND attributes = \''.xtc_db_input(serialize($attributes)).'\'
			';
			$basket_id = xtc_db_fetch_array(xtc_db_query($sql));
			if ($basket_id['customers_basket_id'])
				$return = true;
		}
		else
		{
			foreach ($this->contents as $product)
			{
				if ($product['products_id'] == $products_id)
				{
					if (serialize($product['attributes']) == serialize($attributes))
					{
						$return = true;
						break;
					}
				}
			}
		}
		return $return;
	}

	function switch_centralised_buying($customer_id)
	{
		$sql = '
			UPDATE `customers`
			SET `centralised_buying_active` = IF(`centralised_buying_active` = 0, 1, 0)
			WHERE `customers_id` = '.sqlval($customer_id).'
		';
		query($sql);

		if ($_SESSION['centralised_buying_active'] == 0 || $_SESSION['centralised_buying_active'] == '')
			$_SESSION['centralised_buying_active'] = 1;
		else
			$_SESSION['centralised_buying_active'] = 0;

		$cart = shopping_cart_ng::get_instance($customer_id);
		if (count($cart->get_all_entries()) > 0 && !is_array($_SESSION['group_color_codes']))
			$this->set_group_color(1);

		xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, xtc_get_all_get_params(array('action'))));
	}

	function get_centralised_buying_status($customer_id)
	{
		$sql = '
			SELECT `centralised_buying_active`
			FROM `customers`
			WHERE `customers_id` = '.sqlval($customer_id).'
		';
		$centralised_buying_active = @array_pop(array_pop(query($sql)));

		$cart = shopping_cart_ng::get_instance($customer_id);
		if (count($cart->get_all_entries()) > 0 and !is_array($_SESSION['group_color_codes']))
			$this->set_group_color(1);

		return $centralised_buying_active;
	}

	// Diese Funktion wird nicht mehr benutzt (sagt zumindest in dateien suchen)
	function update_centralised_buying_group($basket_id, $group_id, $attributes)
	{
	}

	function set_group_color($group_id)
	{
		if (is_array($_SESSION['group_color_codes']))
		{
			$last = end($_SESSION['group_color_codes']);
			$last_group_id = end(array_keys($_SESSION['group_color_codes']));
			$plus = $group_id - $last_group_id;
			$sql = '
				SELECT MAX(centralised_buying_color_codes_id)
				FROM centralised_buying_color_codes
			';
			$max_color_id = end(xtc_db_fetch_array(xtc_db_query($sql)));
			if ($plus > $max_color_id or ($last + $plus > $max_color_id))
				$plus = intval($plus * ceil($plus / $max_color_id));
			if ($last > $max_color_id - 1)
				$_SESSION['group_color_codes'][$group_id] = $plus;
			else
				$_SESSION['group_color_codes'][$group_id] = $last + $plus;
		}
		else
			$_SESSION['group_color_codes'][$group_id] = 1;
		if ($group_id > $_SESSION['max_groups'])
			$_SESSION['max_groups'] = $group_id;
	}

	function get_group_color($group_id, $add_to_session = true)
	{
		if (!isset($_SESSION['group_color_codes'][$group_id]) and $add_to_session == true)
			$this->set_group_color($group_id);

		if ($add_to_session == true)
			$color_id = $_SESSION['group_color_codes'][$group_id];

		if ($add_to_session == false)
		{
			$sql = '
				SELECT MAX(centralised_buying_color_codes_id)
				FROM centralised_buying_color_codes
			';
			$max_color_id = end(xtc_db_fetch_array(xtc_db_query($sql)));
			$color_id = $group_id;
			if ($group_id > $max_color_id)
			{
				$color_id = $group_id % $max_color_id;
			}
			if ($color_id == 0)
				$color_id = $max_color_id;
		}
		$sql = '
			SELECT color_code FROM centralised_buying_color_codes
			WHERE centralised_buying_color_codes_id = '.sqlval($color_id).'
		';
		$query = query($sql);

		if ($group_id > $_SESSION['max_groups'] and $add_to_session == true)
			$_SESSION['max_groups'] = $group_id;

		return ($query ? $query[0]['color_code'] : '');
	}

	function group_products($products_array)
	{
		if (is_array($products_array) and count($products_array) > 0 and $_SESSION['centralised_buying_active'] == 1)
		{
			$n = 0;
			foreach ($products_array as $product)
			{
				if ($product['centralised_buying_group_id'] > 0)
				{
					$a[$product['centralised_buying_group_id']][] = $product;
					if ($n == 0)
						$n = $product['centralised_buying_group_id'];
					else
					if ($n < $product['centralised_buying_group_id'])
						$n = $product['centralised_buying_group_id'];
				}
				else
					$coupon = $product;
			}
			if ($n > $_SESSION['max_groups'])
				$_SESSION['max_groups'] = $n;
			$array = array();
			for ($i = 1; $i <= $n; $i++)
			{
				if (isset($a[$i]))
					foreach ($a[$i] as $part)
						$array[] = $part;
			}
			if (isset($coupon))
				$array[] = $coupon;
			return $array;
		}
		elseif (is_array($products_array) and count($products_array) > 0 and $_SESSION['centralised_buying_active'] == 0)
			return $products_array;
		else
			return false;
	}

	function add_group($product_id, $group_id)
	{
		if (isset($_SESSION['max_groups']) and $_SESSION['max_groups'] >= 6)
			$_SESSION['max_groups']++;
		else
			$_SESSION['max_groups'] = 7;
		$this->set_group_color($_SESSION['max_groups']);
		$_SESSION['added_group'] = $product_id.'_'.$group_id;
	}

	//
	// Cash Points
	//

	var $cash_points;

	/**
	 * Setzt die Anzahl der Cash Points im Warenkorb
	 *
	 * @param int $points Anzahl der Cash Points
	 * @return void
	 */
	function __set_max_cash_points($points)
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);
		return $cart->set_max_cash_points($points);
	}

	/**
	 * @return int Anzahl der Cash Points im Warenkorb
	 */
	function __get_max_cash_points()
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);
		return $cart->get_max_cash_points();
	}

	/**
	 * Berechnet automatisch wieviele der maximal gewünschten CashPoints für diesen Warenkorb eingesetzt werden können
	 * und rechnet zusätzlich den Wert der einsetzbaren CashPoints aus
	 *
	 * @return array count = Anzahl der möglichen CashPoints, value = Wert der CashPoints in momentaren Währung
	 */
	function __get_usable_cash_points_with_value()
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		return $cart->get_usable_cash_points_with_value();
	}

	function __der_kunde_hat_die_preiswarnungen_gesehen_und_soll_erst_bei_der_naechsten_preisaenderung_wieder_eine_meldung_kriegen()
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		foreach ($cart->get_all_entries() as $le)
			$cart->reset_last_seen_price($le);
	}

	function get_coupon()
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);
		return $cart->get_coupon();
	}

	function set_coupon($coupon)
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);
		return $cart->set_coupon($coupon);
	}

	function get_product($basketID)
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		$cart_entry = @$cart->get_specific_entry_by_entry_id($basketID); // @ weil das Ding ein E_USER_WARNING ausgibt um idiotischen
		                                                                 // Programmierern zu sagen, dass das eigentlich nich mehr drin is
		                                                                 // backwards compatibility und so

		$product_data = $this->attach_old_cart_values_to_shopping_cart_ng_product_arrays(array($basketID => $cart_entry));

		return $product_data[0];
	}

	// Was macht diese Funktion eigentlich? Das ist doch sau gef�hrlich so ohne Attribute :| Marian wirds wohl wissen
	function get_product_by_id($pID)
	{
		$cart = shopping_cart_ng::get_instance($_SESSION['customer_id']);

		foreach ($cart->get_all_entries() as $key => $le)
			if ($le['products_id'] == $pID)
			{
				$product_data = $this->attach_old_cart_values_to_shopping_cart_ng_product_arrays(array($key => $le));

				return $product_data[0];
			}

		return;
	}

	// Ich kann weder den Funktionsnamen noch den GLOBALS key mit suchen in dateien finden
	// wird wohl nich mehr benutzt
	function get_last_added_product()
	{
		trigger_error('get_last_product ist alt und tot', E_USER_ERROR);
	}

	function set_to_discard_flag($flag)
	{
		$sql = '
			UPDATE customers_basket
			SET to_discard = '.xtc_db_prepare_input($flag).'
			WHERE customers_id = '.xtc_db_prepare_input($_SESSION['customer_id']).'
		';
		xtc_db_query($sql);
	}


	/**
	 * F�gt die ganzen Keys vom alten Warenkorb ($this->contents) zu einem Array vom neuen Warenkorb hinzu
	 *
	 * @param array $array_products Array vom neuen Warenkorb
	 * @return array Bla.
	 */
	private function attach_old_cart_values_to_shopping_cart_ng_product_arrays($array_products)
	{
		global $main;

		// Sind �berhaupt Produkte da?
		if (!$array_products)
			return array();

		// Liste der IDs schustern
		$id_list = '';
		foreach ($array_products as $le)
			$id_list.= sqlval($le['products_id']).', ';
		$id_list = substr($id_list, 0, -2);

		// Produktdaten laden
		$sql = '
			SELECT
				p.products_id,
				pd.products_name,
				p.products_shippingtime,
				p.products_image,
				p.products_model,
				p.products_price,
				p.products_discount_allowed,
				p.products_weight,
				p.products_tax_class_id,
				p.color_chart,
				p.konstruktor,
				p.products_discountable
			FROM `products` as `p`
			JOIN `products_description` as `pd` USING (`products_id`)
			WHERE `p`.`products_id` IN ('.$id_list.')
				AND `pd`.`language_id` = '.sqlval($_SESSION['languages_id']).'
		';
		//echo $sql;
		$array_product_data = query($sql);

		// Mapping basteln
		$array_product_data_mapping = array();
		foreach ($array_product_data as $le)
			$array_product_data_mapping[$le['products_id']] = $le;

		// Daten zu den einzelnen Produkten hinzuf�gen
		$array_products_return = array();
		foreach ($array_products as $basket_id => $array)
		{
			$product_data = $array_product_data_mapping[$array['products_id']];

			$price = produktpreis_mit_formel_ausrechnen($product_data, $array['attributes']);

			// Attribute korrekt sortieren
			$constructor = __prepare_constructor_array($product_data['konstruktor']);
			$tmp = $array['attributes'];
			$array['attributes'] = array();
			foreach ($constructor as $key)
				$array['attributes'][$key] = $tmp[$key];

			// wtf?
			$newAttributesKeys = @array_keys($array['attributes']);

			$array_products_return[] = array(
				'basket_id' => $basket_id,
				'id' => $array['products_id'],
				'name' => $product_data['products_name'],
				'model' => $product_data['products_model'],
				'image' => $product_data['products_image'],
				'price_in_default_currency' => $price['price'],
				'price' => convert_price_to_currency($price['price']),
				'original_price' => convert_price_to_currency($price['original_price']),
				'quantity' => $array['quantity'],
				'weight' => $product_data['products_weight'],
				'shipping_time' => $main->getShippingStatusName($product_data['products_shippingtime']),
				'final_price' => $price['price'] /* + $this->attributes_price($products_id, $group_id)*/,
				'tax_class_id' => $product_data['products_tax_class_id'],
				'centralised_buying_group_id' => (int) $array['centralised_buying_group_id'],
				'attributes' => $array['attributes'],
				'formel_array' => '',
				'inputs_names' => ($newAttributesKeys ? $newAttributesKeys : false),
				'price_in_default_currency_when_last_seen' => floatval($array['price_when_last_seen']),
				'color_chart' => $product_data['color_chart'],
				'discountable' => $product_data['products_discountable'],
				'individualprice' => convert_price_to_currency($price['price']),
				'prepared_constructor_array' => $constructor
			);
		}

		return $array_products_return;
	}
}