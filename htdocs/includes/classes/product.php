<?php

require_once(DIR_FS_DOCUMENT_ROOT.'/includes/functions/get_product_image_array.php');

/* -----------------------------------------------------------------------------------------
   $Id: product.php 1316 2005-10-21 15:30:58Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2005 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(Coding Standards); www.oscommerce.com

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

class product {

	/**
	 *
	 * Constructor
	 *
	 */
	function product($pID = 0) {
		$this->pID = $pID;
		$this->useStandardImage=false;
		$this->standardImage='noimage_m.gif';
		if ($pID = 0) {
			$this->isProduct = false;
			return;
		}
		// query for Product
		$group_check = "";
		if (GROUP_CHECK == 'true') {
			$group_check = " and p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
		}

		$fsk_lock = "";
		if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
			$fsk_lock = ' and p.products_fsk18!=1';
		}

		$product_query = "select * FROM ".TABLE_PRODUCTS." p,
	                                      ".TABLE_PRODUCTS_DESCRIPTION." pd
	                                      where p.products_status = '1'
	                                      and p.products_id = '".$this->pID."'
	                                      and pd.products_id = p.products_id
	                                      ".$group_check.$fsk_lock."
	                                      and pd.language_id = '".(int) $_SESSION['languages_id']."'";

		$product_query = xtDBquery($product_query);

		if (!xtc_db_num_rows($product_query, true)) {
			$this->isProduct = false;
		} else {
			$this->isProduct = true;
			$this->data = xtc_db_fetch_array($product_query, true);
		}

	}

	/**
	 *
	 *  Query for attributes count
	 *
	 */

	function getAttributesCount() {

		$products_attributes_query = xtDBquery("select count(*) as total from ".TABLE_PRODUCTS_OPTIONS." popt, ".TABLE_PRODUCTS_ATTRIBUTES." patrib where patrib.products_id='".$this->pID."' and patrib.options_id = popt.products_options_id and popt.language_id = '".(int) $_SESSION['languages_id']."'");
		$products_attributes = xtc_db_fetch_array($products_attributes_query, true);
		return $products_attributes['total'];

	}

	/**
	 *
	 * Query for reviews count
	 *
	 */

	function getReviewsCount() {
		$reviews_query = xtDBquery("select count(*) as total from ".TABLE_REVIEWS." r, ".TABLE_REVIEWS_DESCRIPTION." rd where r.products_id = '".$this->pID."' and r.reviews_id = rd.reviews_id and rd.languages_id = '".$_SESSION['languages_id']."' and rd.reviews_text !=''");
		$reviews = xtc_db_fetch_array($reviews_query, true);
		return (int)$reviews['total'];
	}

	/**
	 *
	 * Query for average review
	 *
	 */

	function getReviewsAverage() {
		$reviews_query = xtDBquery("select AVG(reviews_rating) as average from ".TABLE_REVIEWS." where products_id = '".$this->pID."'");
		$reviews = xtc_db_fetch_array($reviews_query, true);
		return $reviews['average'];
	}

	/**
	 *
	 * select reviews
	 *
	 */

	function getReviews() {

		$data_reviews = array ();
		$reviews_query = xtDBquery('
			SELECT
				r.reviews_rating,
				r.reviews_id,
				r.customers_name,
				r.date_added,
				r.last_modified,
				r.reviews_read,
				rd.reviews_text
			FROM '.TABLE_REVIEWS.' r
			JOIN '.TABLE_REVIEWS_DESCRIPTION.' rd USING (reviews_id)
			WHERE r.products_id = "'.$this->pID.'"
				AND rd.languages_id = "'.$_SESSION['languages_id'].'"
			ORDER BY reviews_id DESC
		');
		if (xtc_db_num_rows($reviews_query, true)) {
			$row = 0;
			$data_reviews = array ();
			while ($reviews = xtc_db_fetch_array($reviews_query, true)) {
				$row ++;
				$data_reviews[] = array (
					'AUTHOR' => $reviews['customers_name'],
					'DATE' => xtc_date_short($reviews['date_added']),
					'RATING' => $reviews['reviews_rating'],
					'TEXT' => $reviews['reviews_text']
				);
				if ($row == PRODUCT_REVIEWS_VIEW)
					break;
			}
		}
		return $data_reviews;

	}

	/**
	 *
	 * return model if set, else return name
	 *
	 */

	function getBreadcrumbModel() {

		if ($this->data['products_model'] != "")
			return $this->data['products_model'];
		return $this->data['products_name'];

	}

	/**
	 *
	 * get also purchased products related to current
	 *
	 */

	function getAlsoPurchased() {
		global $xtPrice;

		$module_content = array ();

		$fsk_lock = "";
		if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
			$fsk_lock = 'AND p.products_fsk18!=1';
		}
		$group_check = "";
		if (GROUP_CHECK == 'true') {
			$group_check = "AND p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
		}

		$orders_query = "
			SELECT
				p.products_fsk18,
				p.products_id,
				p.products_price,
				p.products_tax_class_id,
				p.products_image,
				pd.products_name,
				p.products_vpe,
				p.products_vpe_status,
				p.products_vpe_value,
				pd.products_short_description,
				p.products_model
			FROM ".TABLE_ORDERS_PRODUCTS." opa,
				".TABLE_ORDERS_PRODUCTS." opb,
				".TABLE_ORDERS." o,
				".TABLE_PRODUCTS." p,
				".TABLE_PRODUCTS_DESCRIPTION." pd
			WHERE opa.products_id = '".$this->pID."'
				AND opa.orders_id = opb.orders_id
				AND opb.products_id != '".$this->pID."'
				AND opb.products_id = p.products_id
				AND opb.orders_id = o.orders_id
				AND p.products_status = '1'
				AND pd.language_id = '".(int) $_SESSION['languages_id']."'
				AND opb.products_id = pd.products_id
				".$group_check."
				".$fsk_lock."
			GROUP BY p.products_id
			ORDER BY o.date_purchased DESC
			LIMIT ".MAX_DISPLAY_ALSO_PURCHASED."
		";

		$orders_query = '
			SELECT
				`products`.`products_fsk18`,
				`products`.`products_id`,
				`products`.`products_price`,
				`products`.`products_tax_class_id`,
				`products`.`products_image`,
				`products_description`.`products_name`,
				`products`.`products_vpe`,
				`products`.`products_vpe_status`,
				`products`.`products_vpe_value`,
				`products_description`.`products_short_description`,
				`products`.`products_model`
			FROM `products`
			JOIN `products_description` USING (`products_id`)
			JOIN `cm_also_purchased_cache` ON (`cm_also_purchased_cache`.`also_purchased_products_id` = `products`.`products_id`)
			WHERE `products`.`products_status` = 1
			  AND `products_description`.`language_id` = '.sqlval($_SESSION['languages_id']).'
			  AND `cm_also_purchased_cache`.`products_id` = '.sqlval($this->pID).'
			ORDER BY `cm_also_purchased_cache`.`date_purchased` DESC
			LIMIT '.MAX_DISPLAY_ALSO_PURCHASED.'
		';

		$orders_query = xtDBquery($orders_query);
		while ($orders = xtc_db_fetch_array($orders_query, true))
			$module_content[] = $this->buildDataArray($orders);

		return $module_content;

	}

	/**
	 *
	 *
	 *  Get Cross sells
	 *
	 *
	 */
	function getCrossSells() {
		global $xtPrice;

		$cs_groups = "SELECT products_xsell_grp_name_id FROM ".TABLE_PRODUCTS_XSELL." WHERE products_id = '".$this->pID."' GROUP BY products_xsell_grp_name_id";
		$cs_groups = xtDBquery($cs_groups);
		$cross_sell_data = array ();
		if (xtc_db_num_rows($cs_groups, true)>0) {
		while ($cross_sells = xtc_db_fetch_array($cs_groups, true)) {

			$fsk_lock = '';
			if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
				$fsk_lock = ' and p.products_fsk18!=1';
			}
			$group_check = "";
			if (GROUP_CHECK == 'true') {
				$group_check = " and p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
			}

				$cross_query = "select p.products_fsk18,
																														 p.products_tax_class_id,
																								                                                 p.products_id,
																								                                                 p.products_image,
																								                                                 pd.products_name,
																														 						pd.products_short_description,
																								                                                 p.products_fsk18,p.products_price,p.products_vpe,
						                           																									p.products_vpe_status,
						                           																									p.products_vpe_value,
																								                                                 xp.sort_order from ".TABLE_PRODUCTS_XSELL." xp, ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd
																								                                            where xp.products_id = '".$this->pID."' and xp.xsell_id = p.products_id ".$fsk_lock.$group_check."
																								                                            and p.products_id = pd.products_id and xp.products_xsell_grp_name_id='".$cross_sells['products_xsell_grp_name_id']."'
																								                                            and pd.language_id = '".$_SESSION['languages_id']."'
																								                                            and p.products_status = '1'
																								                                            order by xp.sort_order asc";

			$cross_query = xtDBquery($cross_query);
			if (xtc_db_num_rows($cross_query, true) > 0)
				$cross_sell_data[$cross_sells['products_xsell_grp_name_id']] = array ('GROUP' => xtc_get_cross_sell_name($cross_sells['products_xsell_grp_name_id']), 'PRODUCTS' => array ());

			while ($xsell = xtc_db_fetch_array($cross_query, true)) {

				$cross_sell_data[$cross_sells['products_xsell_grp_name_id']]['PRODUCTS'][] = $this->buildDataArray($xsell);
			}

		}
		return $cross_sell_data;
		}
	}


	/**
	 *
	 * get reverse cross sells
	 *
	 */

	 function getReverseCrossSells() {
	 			global $xtPrice;


			$fsk_lock = '';
			if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
				$fsk_lock = ' and p.products_fsk18!=1';
			}
			$group_check = "";
			if (GROUP_CHECK == 'true') {
				$group_check = " and p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
			}

			$cross_query = xtDBquery("select p.products_fsk18,
																						 p.products_tax_class_id,
																                                                 p.products_id,
																                                                 p.products_image,
																                                                 pd.products_name,
																						 						pd.products_short_description,
																                                                 p.products_fsk18,p.products_price,p.products_vpe,
						                           																p.products_vpe_status,
						                           																p.products_vpe_value,
																                                                 xp.sort_order from ".TABLE_PRODUCTS_XSELL." xp, ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd
																                                            where xp.xsell_id = '".$this->pID."' and xp.products_id = p.products_id ".$fsk_lock.$group_check."
																                                            and p.products_id = pd.products_id
																                                            and pd.language_id = '".$_SESSION['languages_id']."'
																                                            and p.products_status = '1'
																                                            order by xp.sort_order asc");


			while ($xsell = xtc_db_fetch_array($cross_query, true)) {

				$cross_sell_data[] = $this->buildDataArray($xsell);
			}


		return $cross_sell_data;



	 }


function getGraduated() {
		global $xtPrice;

		$staffel_query = xtDBquery("SELECT
				                                     quantity,
				                                     personal_offer
				                                     FROM
				                                     ".TABLE_PERSONAL_OFFERS_BY.(int) $_SESSION['customers_status']['customers_status_id']."
				                                     WHERE
				                                     products_id = '".$this->pID."'
				                                     ORDER BY quantity ASC");
		$discount = $xtPrice->xtcCheckDiscount($this->pID); //EINGEF�GT!
		$staffel = array ();
		while ($staffel_values = xtc_db_fetch_array($staffel_query, true)) {
			$staffel[] = array ('stk' => $staffel_values['quantity'], 'price' => $staffel_values['personal_offer']);
		}
		$staffel_data = array ();
		for ($i = 0, $n = sizeof($staffel); $i < $n; $i ++) {
			if ($staffel[$i]['stk'] == 1) {
				$quantity = $staffel[$i]['stk'];
				if ($staffel[$i +1]['stk'] != '')
					$quantity = $staffel[$i]['stk'].'-'. ($staffel[$i +1]['stk'] - 1);
			} else {
				$quantity = ' > '.$staffel[$i]['stk'];
				if ($staffel[$i +1]['stk'] != '')
					$quantity = $staffel[$i]['stk'].'-'. ($staffel[$i +1]['stk'] - 1);
			}
			$vpe = '';
			if ($product_info['products_vpe_status'] == 1 && $product_info['products_vpe_value'] != 0.0 && $staffel[$i]['price'] > 0) {
				$vpe = $staffel[$i]['price'] - $staffel[$i]['price'] / 100 * $discount;
				$vpe = $vpe * (1 / $product_info['products_vpe_value']);
				$vpe = $xtPrice->xtcFormat($vpe, true, $product_info['products_tax_class_id']).TXT_PER.xtc_get_vpe_name($product_info['products_vpe']);
			}
			$staffel_data[$i] = array ('QUANTITY' => $quantity, 'VPE' => $vpe, 'PRICE' => $xtPrice->xtcFormat($staffel[$i]['price'] - $staffel[$i]['price'] / 100 * $discount, true, $this->data['products_tax_class_id']));
		}

		return $staffel_data;

	}
	/**
	 *
	 * valid flag
	 *
	 */

	function isProduct() {
		return $this->isProduct;
	}

	// beta
	function getBuyNowButton($id, $name) {
		global $PHP_SELF;
		if(basename($PHP_SELF) != 'commerce_seo_url.php')
		return '<a href="'.xtc_href_link(basename($PHP_SELF), 'action=buy_now&BUYproducts_id='.$id.'&'.xtc_get_all_get_params(array ('action')), 'NONSSL').'">'.xtc_image_button('button_buy_now.png', TEXT_BUY.$name.TEXT_NOW).'</a>';
		else
		return '<a href="'.xtc_href_link($_REQUEST['linkurl'], 'action=buy_now&BUYproducts_id='.$id.'&'.xtc_get_all_get_params(array ('action')), 'NONSSL').'">'.xtc_image_button('button_buy_now.png', TEXT_BUY.$name.TEXT_NOW).'</a>';

	}

	function getVPEtext($product, $price) {
		global $xtPrice;

		require_once (DIR_FS_INC.'xtc_get_vpe_name.inc.php');

		if (!is_array($product))
			$product = $this->data;

		if ($product['products_vpe_status'] == 1 && $product['products_vpe_value'] != 0.0 && $price > 0) {
			return $xtPrice->xtcFormat($price * (1 / $product['products_vpe_value']), true).TXT_PER.xtc_get_vpe_name($product['products_vpe']);
		}

		return;

	}

	function buildDataArray(&$array,$image = 's') {
		global $xtPrice,$main;

		$tax_rate = $xtPrice->TAX[$array['products_tax_class_id']];

		$products_price = get_products_price($array['products_id']);

		if ($_SESSION['customers_status']['customers_status_show_price'] != '0') {
			if ($_SESSION['customers_status']['customers_fsk18'] == '1')
			{
				if ($array['products_fsk18'] == '0')
					$buy_now = $this->getBuyNowButton($array['products_id'], $array['products_name']);
			}
			else
				$buy_now = $this->getBuyNowButton($array['products_id'], $array['products_name']);
		}

		$shipping_status_name = $main->getShippingStatusName($array['products_shippingtime']);
		$shipping_status_image = $main->getShippingStatusImage($array['products_shippingtime']);

		return array (
			'PRODUCTS_NAME' => xtc_parse_input_field_data($array['products_name'], array('"' => '&quot;')),
			'COUNT'=>$array['ID'],
			'PRODUCTS_ID'=>$array['products_id'],
			'PRODUCTS_VPE' => convert_price_to_currency($array['products_price']),
			'PRODUCTS_IMAGE' => xtc_href_link($this->productImage($array['products_id'], $image)),
			'PRODUCTS_LINK' => xtc_href_link(FILENAME_PRODUCT_INFO, xtc_product_link($array['products_id'], $array['products_name'])),
			'PRODUCTS_PRICE' => convert_and_format_price($products_price, $array['products_id']),
			'PRODUCTS_TAX_INFO' => $main->getTaxInfo($tax_rate),
			'PRODUCTS_SHIPPING_LINK' => $main->getShippingLink(),
			'PRODUCTS_BUTTON_BUY_NOW' => $buy_now,
			'PRODUCTS_SHIPPING_NAME'=>$shipping_status_name,
			'PRODUCTS_SHIPPING_IMAGE'=>$shipping_status_image,
			'PRODUCTS_DESCRIPTION' => $array['products_description'],
			'PRODUCTS_EXPIRES' => $array['expires_date'],
			'PRODUCTS_CATEGORY_URL'=>$array['cat_url'],
			'PRODUCTS_SHORT_DESCRIPTION' => $array['products_short_description'],
			'PRODUCTS_FSK18' => $array['products_fsk18'],
			'PRODUCTS_MODEL' => $array['products_model'],
		);
	}


	function productImage($id, $type)
	{
		// Bilder des Produkts laden
		$array_images = get_product_image_array($id);

	    // TODO: Hier sollte man mal prüfen ob $type korrekt ist
	    // Passendes Bild zum Typ zurückgeben
		if ($type == 'info')
			return $array_images['M'];
	    elseif ($type == 'thumbnail')
		    return $array_images['TH'];
	    elseif ($type == 'popup')
		    return $array_images['L'];
	    else
		    return $array_images['S'];
	}

	function getAllreadyBought()
	{
		if ($_SESSION['customer_id'])
		{
			$sql = '
				SELECT COUNT(op.products_id) count
				FROM orders_products op
				JOIN orders o USING (orders_id)
				WHERE o.customers_id = '.xtc_db_prepare_input($_SESSION['customer_id']).'
					AND op.products_id = '.xtc_db_prepare_input($this->pID).'
					AND o.orders_status = 3
			';
			$query = xtc_db_query($sql);
			$a = xtc_db_fetch_array($query);
			if ($a['count'])
				return true;
			else
				return false;
		}
		else
			return false;
	}

	function getReviewWritten()
	{
		if ($_SESSION['customer_id'])
		{
			$sql = '
				SELECT COUNT(reviews_id) count
				FROM reviews
				WHERE products_id = '.xtc_db_prepare_input($this->pID).'
					AND customers_id = '.xtc_db_prepare_input($_SESSION['customer_id']).'
			';
			$query = xtc_db_query($sql);
			$a = xtc_db_fetch_array($query);
			if ($a['count'])
				return 1;
			else
				return -1;
		}
		else
			return 0;
	}
}

class product_preview {

	/**
	 *
	 * Constructor
	 *
	 */
	function product_preview($pID = 0) {
		$this->pID = $pID;
		$this->useStandardImage=false;
		$this->standardImage='noimage_m.gif';
		if ($pID = 0) {
			$this->isProduct = false;
			return;
		}
		// query for Product
		$group_check = "";
		if (GROUP_CHECK == 'true') {
			$group_check = " and p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
		}

		$fsk_lock = "";
		if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
			$fsk_lock = ' and p.products_fsk18!=1';
		}

		$product_query = "select * FROM ".TABLE_PRODUCTS." p,
	                                      ".TABLE_PRODUCTS_DESCRIPTION." pd
	                                      where p.products_status = '0'
	                                      AND p.products_new = '1'
	                                      and p.products_id = '".$this->pID."'
	                                      and pd.products_id = p.products_id
	                                      ".$group_check.$fsk_lock."
	                                      and pd.language_id = '".(int) $_SESSION['languages_id']."'";

		$product_query = xtDBquery($product_query);

		if (!xtc_db_num_rows($product_query, true)) {
			$this->isProduct = false;
		} else {
			$this->isProduct = true;
			$this->data = xtc_db_fetch_array($product_query, true);
		}

	}

	/**
	 *
	 *  Query for attributes count
	 *
	 */

	function getAttributesCount() {

		$products_attributes_query = xtDBquery("select count(*) as total from ".TABLE_PRODUCTS_OPTIONS." popt, ".TABLE_PRODUCTS_ATTRIBUTES." patrib where patrib.products_id='".$this->pID."' and patrib.options_id = popt.products_options_id and popt.language_id = '".(int) $_SESSION['languages_id']."'");
		$products_attributes = xtc_db_fetch_array($products_attributes_query, true);
		return $products_attributes['total'];

	}

	/**
	 *
	 * Query for reviews count
	 *
	 */

	function getReviewsCount() {
		$reviews_query = xtDBquery("select count(*) as total from ".TABLE_REVIEWS." r, ".TABLE_REVIEWS_DESCRIPTION." rd where r.products_id = '".$this->pID."' and r.reviews_id = rd.reviews_id and rd.languages_id = '".$_SESSION['languages_id']."' and rd.reviews_text !=''");
		$reviews = xtc_db_fetch_array($reviews_query, true);
		return (int)$reviews['total'];
	}

	/**
	 *
	 * Query for average review
	 *
	 */

	function getReviewsAverage() {
		$reviews_query = xtDBquery("select AVG(reviews_rating) as average from ".TABLE_REVIEWS." where products_id = '".$this->pID."'");
		$reviews = xtc_db_fetch_array($reviews_query, true);
		return $reviews['average'];
	}

	/**
	 *
	 * select reviews
	 *
	 */

	function getReviews() {

		$data_reviews = array ();
		$reviews_query = xtDBquery('
			SELECT
				r.reviews_rating,
				r.reviews_id,
				r.customers_name,
				r.date_added,
				r.last_modified,
				r.reviews_read,
				rd.reviews_text
			FROM '.TABLE_REVIEWS.' r
			JOIN '.TABLE_REVIEWS_DESCRIPTION.' rd USING (reviews_id)
			WHERE r.products_id = "'.$this->pID.'"
				AND rd.languages_id = "'.$_SESSION['languages_id'].'"
			ORDER BY reviews_id DESC
		');
		if (xtc_db_num_rows($reviews_query, true)) {
			$row = 0;
			$data_reviews = array ();
			while ($reviews = xtc_db_fetch_array($reviews_query, true)) {
				$row ++;
				$data_reviews[] = array (
					'AUTHOR' => $reviews['customers_name'],
					'DATE' => xtc_date_short($reviews['date_added']),
					'RATING' => $reviews['reviews_rating'],
					'TEXT' => $reviews['reviews_text']
				);
				if ($row == PRODUCT_REVIEWS_VIEW)
					break;
			}
		}
		return $data_reviews;

	}

	/**
	 *
	 * return model if set, else return name
	 *
	 */

	function getBreadcrumbModel() {

		if ($this->data['products_model'] != "")
			return $this->data['products_model'];
		return $this->data['products_name'];

	}

	/**
	 *
	 * get also purchased products related to current
	 *
	 */

	function getAlsoPurchased() {
		global $xtPrice;

		$module_content = array ();

		$fsk_lock = "";
		if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
			$fsk_lock = 'AND p.products_fsk18!=1';
		}
		$group_check = "";
		if (GROUP_CHECK == 'true') {
			$group_check = "AND p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
		}

		$orders_query = "
			SELECT
				p.products_fsk18,
				p.products_id,
				p.products_price,
				p.products_tax_class_id,
				p.products_image,
				pd.products_name,
				p.products_vpe,
				p.products_vpe_status,
				p.products_vpe_value,
				pd.products_short_description,
				p.products_model
			FROM ".TABLE_ORDERS_PRODUCTS." opa,
				".TABLE_ORDERS_PRODUCTS." opb,
				".TABLE_ORDERS." o,
				".TABLE_PRODUCTS." p,
				".TABLE_PRODUCTS_DESCRIPTION." pd
			WHERE opa.products_id = '".$this->pID."'
				AND opa.orders_id = opb.orders_id
				AND opb.products_id != '".$this->pID."'
				AND opb.products_id = p.products_id
				AND opb.orders_id = o.orders_id
				AND p.products_status = '1'
				AND pd.language_id = '".(int) $_SESSION['languages_id']."'
				AND opb.products_id = pd.products_id
				".$group_check."
				".$fsk_lock."
			GROUP BY p.products_id
			ORDER BY o.date_purchased DESC
			LIMIT ".MAX_DISPLAY_ALSO_PURCHASED."
		";

		$orders_query = '
			SELECT
				`products`.`products_fsk18`,
				`products`.`products_id`,
				`products`.`products_price`,
				`products`.`products_tax_class_id`,
				`products`.`products_image`,
				`products_description`.`products_name`,
				`products`.`products_vpe`,
				`products`.`products_vpe_status`,
				`products`.`products_vpe_value`,
				`products_description`.`products_short_description`,
				`products`.`products_model`
			FROM `products`
			JOIN `products_description` USING (`products_id`)
			JOIN `cm_also_purchased_cache` ON (`cm_also_purchased_cache`.`also_purchased_products_id` = `products`.`products_id`)
			WHERE `products`.`products_status` = 1
			  AND `products_description`.`language_id` = '.sqlval($_SESSION['languages_id']).'
			  AND `cm_also_purchased_cache`.`products_id` = '.sqlval($this->pID).'
			ORDER BY `cm_also_purchased_cache`.`date_purchased` DESC
			LIMIT '.MAX_DISPLAY_ALSO_PURCHASED.'
		';

		$orders_query = xtDBquery($orders_query);
		while ($orders = xtc_db_fetch_array($orders_query, true))
			$module_content[] = $this->buildDataArray($orders);

		return $module_content;

	}

	/**
	 *
	 *
	 *  Get Cross sells
	 *
	 *
	 */
	function getCrossSells() {
		global $xtPrice;

		$cs_groups = "SELECT products_xsell_grp_name_id FROM ".TABLE_PRODUCTS_XSELL." WHERE products_id = '".$this->pID."' GROUP BY products_xsell_grp_name_id";
		$cs_groups = xtDBquery($cs_groups);
		$cross_sell_data = array ();
		if (xtc_db_num_rows($cs_groups, true)>0) {
		while ($cross_sells = xtc_db_fetch_array($cs_groups, true)) {

			$fsk_lock = '';
			if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
				$fsk_lock = ' and p.products_fsk18!=1';
			}
			$group_check = "";
			if (GROUP_CHECK == 'true') {
				$group_check = " and p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
			}

				$cross_query = "select p.products_fsk18,
																														 p.products_tax_class_id,
																								                                                 p.products_id,
																								                                                 p.products_image,
																								                                                 pd.products_name,
																														 						pd.products_short_description,
																								                                                 p.products_fsk18,p.products_price,p.products_vpe,
						                           																									p.products_vpe_status,
						                           																									p.products_vpe_value,
																								                                                 xp.sort_order from ".TABLE_PRODUCTS_XSELL." xp, ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd
																								                                            where xp.products_id = '".$this->pID."' and xp.xsell_id = p.products_id ".$fsk_lock.$group_check."
																								                                            and p.products_id = pd.products_id and xp.products_xsell_grp_name_id='".$cross_sells['products_xsell_grp_name_id']."'
																								                                            and pd.language_id = '".$_SESSION['languages_id']."'
																								                                            and p.products_status = '1'
																								                                            order by xp.sort_order asc";

			$cross_query = xtDBquery($cross_query);
			if (xtc_db_num_rows($cross_query, true) > 0)
				$cross_sell_data[$cross_sells['products_xsell_grp_name_id']] = array ('GROUP' => xtc_get_cross_sell_name($cross_sells['products_xsell_grp_name_id']), 'PRODUCTS' => array ());

			while ($xsell = xtc_db_fetch_array($cross_query, true)) {

				$cross_sell_data[$cross_sells['products_xsell_grp_name_id']]['PRODUCTS'][] = $this->buildDataArray($xsell);
			}

		}
		return $cross_sell_data;
		}
	}


	/**
	 *
	 * get reverse cross sells
	 *
	 */

	 function getReverseCrossSells() {
	 			global $xtPrice;


			$fsk_lock = '';
			if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
				$fsk_lock = ' and p.products_fsk18!=1';
			}
			$group_check = "";
			if (GROUP_CHECK == 'true') {
				$group_check = " and p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
			}

			$cross_query = xtDBquery("select p.products_fsk18,
																						 p.products_tax_class_id,
																                                                 p.products_id,
																                                                 p.products_image,
																                                                 pd.products_name,
																						 						pd.products_short_description,
																                                                 p.products_fsk18,p.products_price,p.products_vpe,
						                           																p.products_vpe_status,
						                           																p.products_vpe_value,
																                                                 xp.sort_order from ".TABLE_PRODUCTS_XSELL." xp, ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd
																                                            where xp.xsell_id = '".$this->pID."' and xp.products_id = p.products_id ".$fsk_lock.$group_check."
																                                            and p.products_id = pd.products_id
																                                            and pd.language_id = '".$_SESSION['languages_id']."'
																                                            and p.products_status = '1'
																                                            order by xp.sort_order asc");


			while ($xsell = xtc_db_fetch_array($cross_query, true)) {

				$cross_sell_data[] = $this->buildDataArray($xsell);
			}


		return $cross_sell_data;



	 }


function getGraduated() {
		global $xtPrice;

		$staffel_query = xtDBquery("SELECT
				                                     quantity,
				                                     personal_offer
				                                     FROM
				                                     ".TABLE_PERSONAL_OFFERS_BY.(int) $_SESSION['customers_status']['customers_status_id']."
				                                     WHERE
				                                     products_id = '".$this->pID."'
				                                     ORDER BY quantity ASC");
		$discount = $xtPrice->xtcCheckDiscount($this->pID); //EINGEF�GT!
		$staffel = array ();
		while ($staffel_values = xtc_db_fetch_array($staffel_query, true)) {
			$staffel[] = array ('stk' => $staffel_values['quantity'], 'price' => $staffel_values['personal_offer']);
		}
		$staffel_data = array ();
		for ($i = 0, $n = sizeof($staffel); $i < $n; $i ++) {
			if ($staffel[$i]['stk'] == 1) {
				$quantity = $staffel[$i]['stk'];
				if ($staffel[$i +1]['stk'] != '')
					$quantity = $staffel[$i]['stk'].'-'. ($staffel[$i +1]['stk'] - 1);
			} else {
				$quantity = ' > '.$staffel[$i]['stk'];
				if ($staffel[$i +1]['stk'] != '')
					$quantity = $staffel[$i]['stk'].'-'. ($staffel[$i +1]['stk'] - 1);
			}
			$vpe = '';
			if ($product_info['products_vpe_status'] == 1 && $product_info['products_vpe_value'] != 0.0 && $staffel[$i]['price'] > 0) {
				$vpe = $staffel[$i]['price'] - $staffel[$i]['price'] / 100 * $discount;
				$vpe = $vpe * (1 / $product_info['products_vpe_value']);
				$vpe = $xtPrice->xtcFormat($vpe, true, $product_info['products_tax_class_id']).TXT_PER.xtc_get_vpe_name($product_info['products_vpe']);
			}
			$staffel_data[$i] = array ('QUANTITY' => $quantity, 'VPE' => $vpe, 'PRICE' => $xtPrice->xtcFormat($staffel[$i]['price'] - $staffel[$i]['price'] / 100 * $discount, true, $this->data['products_tax_class_id']));
		}

		return $staffel_data;

	}
	/**
	 *
	 * valid flag
	 *
	 */

	function isProduct() {
		return $this->isProduct;
	}

	// beta
	function getBuyNowButton($id, $name) {
		global $PHP_SELF;
		if(basename($PHP_SELF) != 'commerce_seo_url.php')
		return '<a href="'.xtc_href_link(basename($PHP_SELF), 'action=buy_now&BUYproducts_id='.$id.'&'.xtc_get_all_get_params(array ('action')), 'NONSSL').'">'.xtc_image_button('button_buy_now.png', TEXT_BUY.$name.TEXT_NOW).'</a>';
		else
		return '<a href="'.xtc_href_link($_REQUEST['linkurl'], 'action=buy_now&BUYproducts_id='.$id.'&'.xtc_get_all_get_params(array ('action')), 'NONSSL').'">'.xtc_image_button('button_buy_now.png', TEXT_BUY.$name.TEXT_NOW).'</a>';

	}

	function getVPEtext($product, $price) {
		global $xtPrice;

		require_once (DIR_FS_INC.'xtc_get_vpe_name.inc.php');

		if (!is_array($product))
			$product = $this->data;

		if ($product['products_vpe_status'] == 1 && $product['products_vpe_value'] != 0.0 && $price > 0) {
			return $xtPrice->xtcFormat($price * (1 / $product['products_vpe_value']), true).TXT_PER.xtc_get_vpe_name($product['products_vpe']);
		}

		return;

	}

	function buildDataArray(&$array,$image = 's') {
		global $xtPrice,$main;

		$tax_rate = $xtPrice->TAX[$array['products_tax_class_id']];

		$products_price = get_products_price($array['products_id']);

		if ($_SESSION['customers_status']['customers_status_show_price'] != '0') {
			if ($_SESSION['customers_status']['customers_fsk18'] == '1')
			{
				if ($array['products_fsk18'] == '0')
					$buy_now = $this->getBuyNowButton($array['products_id'], $array['products_name']);
			}
			else
				$buy_now = $this->getBuyNowButton($array['products_id'], $array['products_name']);
		}

		$shipping_status_name = $main->getShippingStatusName($array['products_shippingtime']);
		$shipping_status_image = $main->getShippingStatusImage($array['products_shippingtime']);

		return array (
			'PRODUCTS_NAME' => xtc_parse_input_field_data($array['products_name'], array('"' => '&quot;')),
			'COUNT'=>$array['ID'],
			'PRODUCTS_ID'=>$array['products_id'],
			'PRODUCTS_VPE' => convert_price_to_currency($array['products_price']),
			'PRODUCTS_IMAGE' => xtc_href_link($this->productImage($array['products_id'], $image)),
			'PRODUCTS_LINK' => xtc_href_link(FILENAME_PRODUCT_INFO, xtc_product_link($array['products_id'], $array['products_name'])),
			'PRODUCTS_PRICE' => convert_and_format_price($products_price, $array['products_id']),
			'PRODUCTS_TAX_INFO' => $main->getTaxInfo($tax_rate),
			'PRODUCTS_SHIPPING_LINK' => $main->getShippingLink(),
			'PRODUCTS_BUTTON_BUY_NOW' => $buy_now,
			'PRODUCTS_SHIPPING_NAME'=>$shipping_status_name,
			'PRODUCTS_SHIPPING_IMAGE'=>$shipping_status_image,
			'PRODUCTS_DESCRIPTION' => $array['products_description'],
			'PRODUCTS_EXPIRES' => $array['expires_date'],
			'PRODUCTS_CATEGORY_URL'=>$array['cat_url'],
			'PRODUCTS_SHORT_DESCRIPTION' => $array['products_short_description'],
			'PRODUCTS_FSK18' => $array['products_fsk18'],
			'PRODUCTS_MODEL' => $array['products_model'],
		);
	}


	function productImage($id, $type)
	{
		// Bilder des Produkts laden
		$array_images = get_product_image_array($id);

	    // TODO: Hier sollte man mal prüfen ob $type korrekt ist
	    // Passendes Bild zum Typ zurückgeben
		if ($type == 'info')
			return $array_images['M'];
	    elseif ($type == 'thumbnail')
		    return $array_images['TH'];
	    elseif ($type == 'popup')
		    return $array_images['L'];
	    else
		    return $array_images['S'];
	}

	function getAllreadyBought()
	{
		if ($_SESSION['customer_id'])
		{
			$sql = '
				SELECT COUNT(op.products_id) count
				FROM orders_products op
				JOIN orders o USING (orders_id)
				WHERE o.customers_id = '.xtc_db_prepare_input($_SESSION['customer_id']).'
					AND op.products_id = '.xtc_db_prepare_input($this->pID).' 
					AND o.orders_status = 3
			';
			$query = xtc_db_query($sql);
			$a = xtc_db_fetch_array($query);
			if ($a['count'])
				return true;
			else
				return false;
		}
		else
			return false;
	}

	function getReviewWritten()
	{
		if ($_SESSION['customer_id'])
		{
			$sql = '
				SELECT COUNT(reviews_id) count
				FROM reviews
				WHERE products_id = '.xtc_db_prepare_input($this->pID).'
					AND customers_id = '.xtc_db_prepare_input($_SESSION['customer_id']).'
			';
			$query = xtc_db_query($sql);
			$a = xtc_db_fetch_array($query);
			if ($a['count'])
				return 1;
			else
				return -1;
		}
		else
			return 0;
	}
}
?>