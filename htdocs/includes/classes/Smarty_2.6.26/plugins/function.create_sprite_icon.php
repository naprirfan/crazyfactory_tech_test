<?php

require_once(dirname(__FILE__).'/../../../functions/icon_sprite_functions.inc.php');

function smarty_function_create_sprite_icon($params, &$smarty)
{
	return create_sprite_icon($params['key'], $params['type'], $params['alt'], $params['additional_html']);
}