<?php

function smarty_function_ssl_active($params, &$smarty)
{
	if (!ENABLE_SSL)
		return false;

	global $request_type;
	return ($request_type == 'NONSSL' ? false : true);
}