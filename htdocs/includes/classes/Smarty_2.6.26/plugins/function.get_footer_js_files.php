<?php

/**
 * @param array $params
 * @param Smarty $smarty
 * @return array
 */
function smarty_function_get_footer_js_files($params, &$smarty)
{
	if ($params['assign'])
		$smarty->assign($params['assign'], array_keys($GLOBALS['footer_js_files']));
	else
		return array_keys($GLOBALS['footer_js_files']);
}