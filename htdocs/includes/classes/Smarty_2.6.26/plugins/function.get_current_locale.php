<?php

function smarty_function_get_current_locale($params, &$smarty)
{
	$sql = '
		SELECT `locale`
		FROM `languages`
		WHERE `languages_id` = '.sqlval($_SESSION['languages_id']).'
	';
	$locale = @array_pop(array_pop(query($sql)));

	if (!$locale)
	{
		$sql = '
			SELECT `locale`
			FROM `languages`
			WHERE `code` = "en"
		';
		$locale = @array_pop(array_pop(query($sql)));
	}

	return $locale;
}