<?php

function smarty_function_build_href_link($params, &$smarty)
{
	static $http_path = NULL;
	static $https_path = NULL;
	global $request_type;

	if (!$http_path)
	{
		$http_path = preg_replace('#([^:])//+#', '${1}/', HTTP_SERVER.'/'.DIR_WS_CATALOG.'/');
		$https_path = preg_replace('#([^:])//+#', '${1}/', HTTPS_SERVER.'/'.DIR_WS_CATALOG.'/');
	}

	$ssl_link = $https_path.$params['link'];
	$nonssl_link = $http_path.$params['link'];

	if (!ENABLE_SSL)
		return $nonssl_link;

	if (!isset($params['ssl']))
		return ($request_type == 'NONSSL' ? $nonssl_link : $ssl_link);

	if ($params['ssl'])
		return $ssl_link;
	else
		return $nonssl_link;
}