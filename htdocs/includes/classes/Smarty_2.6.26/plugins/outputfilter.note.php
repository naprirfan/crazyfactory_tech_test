<?php
/* -----------------------------------------------------------------------------------------
   $Id: outputfilter.note.php 779 2005-02-19 17:19:28Z novalis $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/



function smarty_outputfilter_note($tpl_output, &$smarty) {
    /*
    The following copyright announcement is in compliance
    to section 2c of the GNU General Public License, and
    thus can not be removed, or can only be modified
    appropriately.*/
$cop='
    <p align="center" class="copyright">
      Copyright  &copy; 2009 <a href="http://www.commerce-seo.de">commerce:SEO</a><br />
      Commerce:SEO provides no warranty and is redistributable under the <a href="http://www.fsf.org/licensing/licenses/gpl.txt" target="_blank">GNU General Public License</a>
    </p>
';

function NoEntities($Input) {
	$TransTable1 = get_html_translation_table (HTML_ENTITIES);
	foreach($TransTable1 as $ASCII => $Entity) {
		$TransTable2[$ASCII] = '&#'.ord($ASCII).';';
	}
	$TransTable1 = array_flip ($TransTable1);
	$TransTable2 = array_flip ($TransTable2);
	return strtr (strtr ($Input, $TransTable1), $TransTable2);
}
function AmpReplace($Treffer) {
	return $Treffer[1].htmlentities(NoEntities($Treffer[2]), ENT_COMPAT, $_SESSION['language_charset']).$Treffer[3];
}
$tpl_output = preg_replace_callback("/(<a[^>]*href=\"|<form[^>]*action=\")(.*)(\"[^<]*>)/Usi","AmpReplace",$tpl_output);
$tpl_output = preg_replace_callback("/(<a[^>]*href='|<form[^>]*action=')(.*)('[^<]*>)/Usi","AmpReplace",$tpl_output);

return $tpl_output;
return $tpl_output.$cop;
}
?>
