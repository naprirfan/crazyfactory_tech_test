<?php

require_once (dirname(__FILE__).'/../database.inc.php');
require_once (dirname(__FILE__).'/../functions/price_functions.inc.php');

/**
 * Superduper neuer Warenkorb! Eine Instanz wird �ber {@link shopping_cart_ng::get_instance()} geholt.
 */
class shopping_cart_ng {
	private $cart_entries;
	private $customer_id;
	private $use_database;

	/**
	 * @param int|null $customer_id
	 */
	private function __construct($customer_id = NULL)
	{
		// Grunds�tzlich ist der Warenkorb in der Session
		$this->use_database = false;
		$this->customer_id = NULL;
		$this->cart_entries = array(0 => null);
		$this->cart_options = array();

		// Wenn der Warenkorb f�r einen bestimmten Benutzer gedacht ist liegt er in der Datenbank
		// dies wird in den Funktionen �ber $this->use_database == true erkannt
		if ($customer_id)
		{
			$this->customer_id = $customer_id;
			$this->use_database = true;
		}
	}

	/**
	 * F�gt ein genormtes Produkt-Array in die Warenkorbeintr�ge hinzu bzw. addiert die Anzahl auf einen bestehenden Eintrag.
	 *
	 * @param array $array_product Genormtes Produkt-Array
	 * @param int $quantity Gew�nschte Anzahl
	 * @param bool $replace_quantity Wenn true wird die Anzahl von bestehenden Eintr�ge ERSETZT
	 * @return bool|int false im Fehlerfall, ansonsten die Datenbank-ID bzw. den lokalen Array-Key des Eintrags (sollte nicht verwendet werden, das ist nur aus Kompatiblit�tsgr�nden so!)
	 */
	public function add_to_cart($array_product, $quantity, $replace_quantity = false)
	{
		// Produktarray pr�fen, bei Fehlern direkt abbrechen
		if (!shopping_cart_ng::check_product_array($array_product))
			return false;

		if ($quantity < 1)
			return false;

		if ($this->use_database)
		{
			// Zu speichernde Daten vorbereiten
			@ksort($array_product['attributes']);
			$array_db_values = array(
				'`customers_id`' => sqlval($this->customer_id),
				'`products_id`' => sqlval($array_product['products_id']),
				'`quantity`' => intval($quantity),
				'`attributes`' => sqlval(json_encode($array_product['attributes'])),
				'`centralised_buying_group_id`' => sqlval(intval($array_product['centralised_buying_group_id'])),
				'`price_when_last_seen`' => get_products_price($array_product['products_id'], $array_product['attributes'])
			);

			// Produkt ist bereits im Warenkorb und muss aktualisiert werden
			$cart_entry_id = $this->entry_in_cart($array_product);
			if ($cart_entry_id)
			{
				// Die Menge soll nicht ersetzt werden, also muss die aktuelle Menge hinzugerechnet werden
				if (!$replace_quantity)
				{
					$product_in_cart = $this->get_specific_entry($array_product);
					//var_dump($product_in_cart);
					$array_db_values['`quantity`']+= $product_in_cart['quantity'];
				}

				$sql = '
					UPDATE `cm_shopping_cart_ng_entries`
					SET '.__prepare_sql_set_values($array_db_values).'
					WHERE '.__db_build_product_where_clause($this->customer_id, $array_product).'
				';
				//echo $sql;
				query($sql);

				// Aus Kompatiblit�tsgr�nden geben wir die ID aus der Datenbank zur�ck
				return $cart_entry_id;
			}
			// Produkt ist noch nicht im Warenkorb und muss eingef�gt werden
			else
			{
				$sql = '
					INSERT INTO `cm_shopping_cart_ng_entries`
					SET '.__prepare_sql_set_values($array_db_values).'
				';
				$cart_entry_id = query($sql);

				// Aus Kompatiblit�tsgr�nden geben wir die ID aus der Datenbank zur�ck
				return $cart_entry_id;
			}
		}
		else
		{
			if ($this->entry_in_cart($array_product))
			{
				// Produkt im Array suchen und Quantity anpassen
				foreach ($this->cart_entries as $key => &$le)
					if (shopping_cart_ng::compare_product_arrays($array_product, $le))
					{
						if ($replace_quantity)
							$le['quantity'] = intval($quantity);
						else
							$le['quantity']+= $quantity;

						// Aus Kompatiblit�tsgr�nden geben wir den Key vom lokalen Array zur�ck
						return $key;
					}
			}
			else
			{
				$this->cart_entries[] = $array_product + array(
					'quantity' => intval($quantity),
					'price_when_last_seen' => get_products_price($array_product['products_id'], $array_product['attributes'])
				);

				// Aus Kompatiblit�tsgr�nden geben wir den Key vom lokalen Array zur�ck
				return count($this->cart_entries)-1;
			}
		}

		return true;
	}

	/**
	 * Entfernt einen Eintrag aus dem Warenkorb
	 *
	 * @param array $array_product Genormtes Produkt-Array
	 * @return bool false wenn Parameter nicht wohlgeformt sind bzw. das Produkt garnicht im Warenkorb liegt
	 */
	public function remove_entry($array_product)
	{
		// Produktarray pr�fen, bei Fehlern direkt abbrechen
		if (!shopping_cart_ng::check_product_array($array_product, false))
			return false;

		if (!$this->entry_in_cart($array_product))
			return false;

		if ($this->use_database)
		{
			$sql = '
				DELETE FROM `cm_shopping_cart_ng_entries`
				WHERE '.__db_build_product_where_clause($this->customer_id, $array_product).'
			';
			query($sql);
		}
		else
		{
			// Produkt im Array suchen und entfernen
			foreach ($this->cart_entries as $key => $le)
				if (shopping_cart_ng::compare_product_arrays($array_product, $le))
				{
					unset($this->cart_entries[$key]);
					break;
				}
		}

		return true;
	}

	/**
	 * Anzahl der Eintr�ge im Warenkorb (1 Produkt mit 2 verschiedenen Attributen = 2 Eintr�ge)
	 *
	 * @return int Is klar, oder?
	 */
	public function get_entry_count()
	{
		if ($this->use_database)
		{
			$sql = '
				SELECT COUNT(*)
				FROM `cm_shopping_cart_ng_entries`
				WHERE `customers_id` = '.sqlval($this->customer_id).'
			';

			return intval(array_pop(array_pop(query($sql)))) + count($this->get_buyable_coupons());
		}
		else
		{
			$array_return = $this->cart_entries;
			unset($array_return[0]); // 0 -> dummy damit die Eintr�ge mit 1 beginnen
			return count($array_return) + count($this->get_buyable_coupons());
		}
	}

	/**
	 * Gibt ein Array mit allen Eintr�gen im Warenkorb zur�ck.
	 * Format:
	 * array(
	 *   int products_id,
	 *   array attributes,
	 *   quantity,
	 *   centralised_buying_group_id,
	 *   price_when_last_seen (Preis in Standardw�hrung des Shops!)
	 * )
	 *
	 * @return array Array mit ganz vielen Produkten. Oder leer.
	 */
	public function get_all_entries()
	{
		if ($this->use_database)
		{
			$sql = '
				SELECT *
				FROM `cm_shopping_cart_ng_entries`
				WHERE `customers_id` = '.sqlval($this->customer_id).'
				ORDER BY `cart_entry_id` ASC
			';
			$array_products = query($sql);

			$array_return = array();
			foreach ($array_products as $le)
				$array_return[$le['cart_entry_id']] = array(
					'products_id' => $le['products_id'],
					'attributes' => json_decode($le['attributes'], true),
					'quantity' => intval($le['quantity']),
					'centralised_buying_group_id' => intval($le['centralised_buying_group_id']),
					'price_when_last_seen' => floatval($le['price_when_last_seen'])
				);

			return $array_return;
		}
		else
		{
			$array_return = $this->cart_entries;
			unset($array_return[0]); // 0 -> dummy damit die Eintr�ge mit 1 beginnen
			return $array_return;
		}
	}

	/**
	 * Sucht ein bestimmtes Produkt anhand eines genormten Produkt-Array aus den Warenkorb-Eintr�gen heraus.
	 * Format:
	 * array(
	 *   int products_id,
	 *   array attributes,
	 *   quantity,
	 *   centralised_buying_group_id,
	 *   price_when_last_seen (Preis in Standardw�hrung des Shops!)
	 * )
	 *
	 * @param array $array_product Genormtes Produkt-Array nach dem gesucht werden soll
	 * @return array|bool Produkt-Array, wenn nicht gefunden false
	 */
	public function get_specific_entry($array_product)
	{
		// Produktarray pr�fen, bei Fehlern direkt abbrechen
		if (!shopping_cart_ng::check_product_array($array_product))
			return false;

		if (!$this->entry_in_cart($array_product))
			return false;

		if ($this->use_database)
		{
			$sql = '
				SELECT *
				FROM `cm_shopping_cart_ng_entries`
				WHERE '.__db_build_product_where_clause($this->customer_id, $array_product).'
			';
			$db_entry_data = array_pop(query($sql));

			return array(
				'products_id' => $db_entry_data['products_id'],
				'attributes' => json_decode($db_entry_data['attributes'], true),
				'quantity' => intval($db_entry_data['quantity']),
				'centralised_buying_group_id' => intval($db_entry_data['centralised_buying_group_id']),
				'price_when_last_seen' => floatval($db_entry_data['price_when_last_seen'])
			);
		}
		else
		{
			// Produkt im Array suchen und zur�ckgeben
			foreach ($this->cart_entries as $le)
				if (shopping_cart_ng::compare_product_arrays($array_product, $le))
					return $le;
		}
	}

	/**
	 * Siehe get_specific_entry. DEPRECATED!
	 *
	 * @param int $entry_id ID in der Datenbank bzw. Key im Session-Array. Simuliert die alte basket_id.
	 * @return array|bool Siehe get_specific_entry
	 * @deprecated
	 */
	public function get_specific_entry_by_entry_id($entry_id)
	{
		// Echt jetzt...
		trigger_error('get_specific_entry_by_entry_id is only available for backwards compatibility and should NEVER be used in new code!', E_USER_WARNING);

		if ($this->use_database)
		{
			$sql = '
				SELECT *
				FROM `cm_shopping_cart_ng_entries`
				WHERE `customers_id` = '.sqlval($this->customer_id).' AND `cart_entry_id` = '.sqlval($entry_id).'
			';
			$db_entry_data = array_pop(query($sql));

			return array(
				'products_id' => $db_entry_data['products_id'],
				'attributes' => json_decode($db_entry_data['attributes'], true),
				'quantity' => intval($db_entry_data['quantity']),
				'centralised_buying_group_id' => intval($db_entry_data['centralised_buying_group_id']),
				'price_when_last_seen' => floatval($db_entry_data['price_when_last_seen'])
			);
		}
		else
		{
			if (!$entry_id)
				return false;

			return $this->cart_entries[$entry_id];
		}
	}

	/**
	 * Pr�ft ob ein Produkt mit bestimmten Attributen in einer bestimmten Gruppe im Warenkorb vorhanden ist
	 *
	 * @param array $array_product Genormtes Produkt-Array
	 * @return bool true = drin, false = nicht drin
	 */
	public function entry_in_cart($array_product)
	{
		// Produktarray pr�fen, bei Fehlern direkt abbrechen
		if (!shopping_cart_ng::check_product_array($array_product, false))
			return false;

		if ($this->use_database)
		{
			$sql = '
				SELECT `cart_entry_id`
				FROM `cm_shopping_cart_ng_entries`
				WHERE '.__db_build_product_where_clause($this->customer_id, $array_product).'
			';
			//echo $sql;
			$cart_entry_id = @array_pop(array_pop(query($sql)));

			return ($cart_entry_id ? $cart_entry_id : false);
		}
		else
		{
			foreach ($this->cart_entries as $key => $le)
				if (shopping_cart_ng::compare_product_arrays($array_product, $le))
					return $key;

			return false;
		}
	}

	/**
	 * F�gt alle Eintr�ge und andere Einstellungen (Cash Points, Coupon) dieses Warenkorbs in einen anderen ein.
	 * F�r gew�hnlich sollte danach {@link reset()} aufgerufen werden.
	 *
	 * @param shopping_cart_ng $new_cart Warenkorb in den der ganze Kram gestopft werden soll
	 * @return bool false im Fehlerfall
	 */
	public function merge_with_cart(&$new_cart)
	{
		// Wir k�nnen nur in andere Warenk�rbe mergen
		if (@get_class($new_cart) != 'shopping_cart_ng')
		{
			trigger_error('Parameter 1 of merge_with_cart MUST be an instance of shopping_cart_ng!', E_USER_ERROR);
			return false;
		}

		$new_cart->set_coupon($this->get_coupon());
		$new_cart->set_max_cash_points($this->get_max_cash_points());
		$array_articles = $this->get_all_entries();

		foreach ($array_articles as $le)
			$new_cart->add_to_cart($le, $le['quantity']);

		// gekaufte Gutscheine hinzuf�gen
		foreach ($this->get_buyable_coupons() as $coupon)
			$new_cart->add_buyable_coupon ($coupon['title'], $coupon['text'], $coupon['amount'], $coupon['currency_code'], $coupon['style']);

		return true;
	}

	/**
	 * Erstellt bzw. gibt eine bestehende eine Instanz eines Warenkorbs zur�ck.
	 *
	 * @static
	 * @param int|null $customer_id Wenn angegeben wird der Warenkorb f�r einen bestimmten Kunden aus der Datenbank benutzt. Wenn NULL liegt er in $_SESSION['cart_ng'].
	 * @return shopping_cart_ng
	 */
	static function get_instance($customer_id = NULL)
	{
		if ($customer_id)
		{
			static $instance = NULL;

			if (is_null($instance[$customer_id]))
				$instance[$customer_id] = new shopping_cart_ng($customer_id);

			return $instance[$customer_id];
		}
		else
		{
			if (is_null($_SESSION['cart_ng']))
				$_SESSION['cart_ng'] = new shopping_cart_ng();

			return $_SESSION['cart_ng'];
		}
	}

	/**
	 * Pr�ft ob ein genormtes Produkt-Array korrekt formatiert ist.
	 *
	 * @static
	 * @param array $array_product Das zu pr�fende Produkt-Array.
	 * @param bool $check_attributes Wenn false werden die Attribute NICHT gepr�ft. Wenn nicht angegeben true.
	 * @return bool true wenn korrekt formatiert, false wenn nicht. Simpel, oder?
	 */
	static function check_product_array($array_product, $check_attributes = true)
	{
		if (!$array_product['products_id'])
			return false;

		if (!is_numeric($array_product['centralised_buying_group_id']))
			return false;

		if (!is_array($array_product['attributes']))
			return false;

		if ($check_attributes && !shopping_cart_ng::check_product_attributes($array_product))
			return false;

		return true;
	}

	/**
	 * Vergleicht 2 genormte Produkt-Arrays. Vergleicht werden Produkt-ID, Attribute und centralised_buying_group_id
	 *
	 * @static
	 * @param array $array_product_1 Genormtes Produkt-Array
	 * @param array $array_product_2 Genormtes Produkt-Array
	 * @return bool true wenn die Produkte gleich sind, false wenn nicht
	 */
	static function compare_product_arrays($array_product_1, $array_product_2)
	{
		if (
			$array_product_1['products_id'] == $array_product_2['products_id'] &&
			$array_product_1['attributes'] === $array_product_2['attributes'] &&
			intval($array_product_1['centralised_buying_group_id']) == intval($array_product_2['centralised_buying_group_id'])
		)
			return true;

		return false;
	}

	/**
	 * L�scht alle Eintr�ge und Konfigurationen dieses Warenkorbs.
	 *
	 * @return void
	 */
	public function reset()
	{
		if ($this->use_database)
		{
			$sql = '
				DELETE FROM `cm_shopping_cart_ng_entries`
				WHERE `customers_id` = '.sqlval($this->customer_id).'
			';
			query($sql);

			$sql = '
				DELETE FROM `cm_shopping_cart_ng_options`
				WHERE `customers_id` = '.sqlval($this->customer_id).'
			';
			query($sql);
		}
		else
		{
			$this->cart_entries = array(0 => null);
			$this->cart_options = array();
		}
	}

	/**
	 * Interne Funktion um eine Option des Warenkorbs zu laden.
	 *
	 * @param string $option Name der Option
	 * @param null|mixed $default Optionaler default-Wert wenn diese Option nicht gesetzt ist
	 * @return bool|mixed Wert der Option, false wenn keine Option angegeben wurde
	 */
	private function __get_option($option, $default = NULL)
	{
		if (!$option)
			return false;

		if ($this->use_database)
		{
			$sql = '
				SELECT `value`
				FROM `cm_shopping_cart_ng_options`
				WHERE `customers_id` = '.sqlval($this->customer_id).'
					AND `key` = '.sqlval($option).'
			';
			$query = @array_pop(query($sql));

			if ($query)
				return unserialize($query['value']);
			else
				return $default;
		}
		else
		{
			return ($this->cart_options[$option] !== NULL ? $this->cart_options[$option] : $default);
		}
	}

	/**
	 * Interne Funktion um eine Option des Warenkorbs zu setzen.
	 *
	 * @param string $option Name der Option
	 * @param mixed $value Zu setzender Wert (wird serialisiert in der DB gespeichert)
	 * @return bool false wenn keine Option angegeben wurde
	 */
	private function __set_option($option, $value)
	{
		if (!$option)
			return false;

		if ($this->use_database)
		{
			$sql = '
				INSERT INTO `cm_shopping_cart_ng_options`
				SET `customers_id` = '.sqlval($this->customer_id).',
						`key` = '.sqlval($option).',
						`value` = '.sqlval(serialize($value)).'
				ON DUPLICATE KEY UPDATE `value` = VALUES(`value`)
			';
			query($sql);
		}
		else
		{
			$this->cart_options[$option] = $value;
		}

		return true;
	}

	/**
	 * Berechnet den kompletten Wert des Warenkorbs inkl. Coupon, exkl. Cash-Points
	 *
	 * @return float Wert in der gew�hlten W�hrung des Benutzers
	 */
	public function calculate_total()
	{
		$array_products = $this->get_all_entries();
		$total = 0.0;

		foreach ($array_products as $le)
		{
			$products_price = convert_price_to_currency(get_products_price($le['products_id'], $le['attributes']));
			$total+= round_price($products_price) * $le['quantity'];
		}

		//zu kaufende Gutscheine hinzurechnen
		$buyable_coupons = $this->get_buyable_coupons();

		foreach ($buyable_coupons as $buyable_coupon)
		{
			$value_in_shop_currency = convert_price_from_currency($buyable_coupon['amount'], $buyable_coupon['currency_code']);

			$coupon_price = convert_price_to_currency($value_in_shop_currency);
			$total += round_price($coupon_price);
		}

		// Gutschein abziehen
		$array_coupon = $this->get_coupon_data();
		if ($array_coupon)
			$total-= $array_coupon['amount'];

		return $total;
	}

	/**
	 * Setzt die maximale Anzahl der Cash Points im Warenkorb die der Benutzer einsetzen m�chte
	 * {@link get_usable_cash_points_with_value()} berechnet wieviele CashPoints tats�chlich eingesetzt werden k�nnen
	 *
	 * @param int $points Anzahl der Cash Points
	 * @return void
	 */
	public function set_max_cash_points($points)
	{
		$this->__set_option('cash_points', $points);
	}

	/**
	 * Gibt die maximale Anzahl der Cash Points im Warenkorb die der Benutzer einsetzen m�chte zur�ck
	 * {@link get_usable_cash_points_with_value()} berechnet wieviele CashPoints tats�chlich eingesetzt werden k�nnen
	 *
	 * @return int Anzahl der Cash Points im Warenkorb
	 */
	public function get_max_cash_points()
	{
		return $this->__get_option('cash_points');
	}

	/**
	 * Berechnet automatisch wieviele der maximal gew�nschten CashPoints f�r diesen Warenkorb eingesetzt werden k�nnen
	 * und rechnet zus�tzlich den Wert der einsetzbaren CashPoints aus
	 *
	 * @return array count = Anzahl der m�glichen CashPoints, value = Wert der CashPoints in momentaren W�hrung
	 */
	public function get_usable_cash_points_with_value()
	{
		$array_return = array();

		// CashPoints des Users laden
		$customers_cash_points = get_user_cash_points();

		// Anzahl der CashPoints laden die der User einsetzen m�chte
		$max_cash_points = $this->get_max_cash_points();

		if (!$max_cash_points)
			return $array_return;

		// Es k�nnen niemals mehr CashPoints eingesetzt werden als der User hat
		if ($customers_cash_points < $max_cash_points)
			$max_cash_points = $customers_cash_points;

		// Wert der CashPoints ausrechnen
		$max_cash_points_value = convert_cash_points_to_actual_money($max_cash_points);

		// Wert des Warenkorbs mit Shipping und Rabatt ausrechnen
		$shoppingcart_total = $this->calculate_total();
		$shoppingcart_total += calculate_shipping_costs($shoppingcart_total);
		$graduated_discount = calculate_graduated_discount();
		$shoppingcart_total -= $graduated_discount['calculated_discount'];

		// Wenn der Wert des Warenkorbs ins Minus gehen w�rde wenn wir die CashPoints abziehen muss ausgerechnet werden
		// wieviele CashPoints wir abziehen k�nnen BEVOR er ins Minus geht und den �brigen Betrag als Kulanz extra abziehen
		if ($shoppingcart_total > 0)
		{
			if ($max_cash_points_value > $shoppingcart_total )
			{
				// Wert eines CashPoints
				$cashpoint_value = convert_price_to_currency(CASH_POINTS_TO_MONEY_MODIFIER);

				// Ausrechnen wieviele CashPoints maximal in diesen Warenkorb k�nnen

				$max_cash_points = ceil($shoppingcart_total / $cashpoint_value);
				// Anzahl der CashPoints zur�ckgeben und gesamten Warenkorbwert als Kulanz berechnen
				$array_return = array(
					'cash_points' => $max_cash_points,
					'value' => $shoppingcart_total
				);
			}
				// Wenn die CashPoints den Wert des Warenkorbs nicht �bersteigen k�nnen alle eingesetzt werden
			else
			{
				$array_return = array(
					'cash_points' => $max_cash_points,
					'value' => $max_cash_points_value
				);
			}
		}
		else
		{
			$array_return = array(
					'cash_points' => 0,
					'value' => 0
				);
		}
		return $array_return;
	}

	/**
	 * Speichert den Coupon f�r diesen Warenkorb. Das komische Format kommt noch vom alten Warenkorb.
	 *
	 * @param array $coupon array('products_id' => coupon_id)
	 * @return void
	 */
	public function set_coupon($coupon)
	{
		$this->__set_option('coupon', $coupon);
	}

	/**
	 * Gibt den momentanen Coupon dieses Warenkorbs zur�ck. Das komische Format kommt noch vom alten Warenkorb.
	 *
	 * @return array|void array('products_id' => coupon_id)
	 */
	public function get_coupon()
	{
		return $this->__get_option('coupon');
	}

	/**
	 * Gibt ID, Code und Wert vom Coupon dieses Warenkorbs zur�ck. Wert wird in die aktuelle W�hrung des Kunden umgerechnet und gerundet.
	 *
	 * @return array|bool false wenn kein Coupon gesetzt bzw. nicht in der DB gefunden, sonst array(id, code, amount)
	 */
	public function get_coupon_data()
	{
		// Das wurde mal sau fies in den alten Warenkorb reingehackt, deswegen ist das alles so komisch...
		$coupon = $this->__get_option('coupon');

		$coupon_id = $coupon['products_id'];

		if (!$coupon_id)
			return false;

		$sql = '
			SELECT `coupon_amount`,
					`coupon_code`,
					`coupon_type`,
					UNIX_TIMESTAMP(`coupon_expire_date`) + (3600*24) as expires,
					UNIX_TIMESTAMP() as curr_db_time,
					`cd`.`coupon_name`
			FROM `coupons`
			LEFT JOIN `coupons_description` cd ON (`coupons`.`coupon_id` = `cd`.`coupon_id` AND `cd`.`language_id` = '.sqlval($_SESSION['languages_id']).')
			WHERE `coupons`.`coupon_id` = '.sqlval($coupon_id).'
		';

		$coupon_data = @array_pop(query($sql));

		if ($coupon_data)
		{
			if ($coupon_data['expires'] > $coupon_data['curr_db_time'])
			{
				return array(
					'coupon_type' => $coupon_data['coupon_type'],
					'coupon_percent' => ($coupon_data['coupon_type'] == 'P' ? round($coupon_data['coupon_amount']) : 0),
					'coupon_name' => $coupon_data['coupon_name'],
					'amount' => ($coupon_data['coupon_type'] != 'P' ? convert_and_round_price($coupon_data['coupon_amount']) : convert_and_round_price(0)),
					'original_price' => $coupon_data['coupon_amount'],
					'code' => $coupon_data['coupon_code'],
					'id' => $coupon_id
				);
			}
			else
			{
				// gutschein ist abgelaufen.. l�schen.
				$this->set_coupon(NULL);
				return false;
			}
		}
		else
			return false;
	}

	/**
	 * Setzt den zuletzt gesehenen Preis eines Produkts auf den aktuellen Preis.
	 *
	 * @param array $array_product Genormtes Produkt-Array
	 * @return bool false wenn das Produkt nicht im Warenkorb liegt
	 */
	public function reset_last_seen_price($array_product)
	{
		// Produktarray pr�fen, bei Fehlern direkt abbrechen
		if (!shopping_cart_ng::check_product_array($array_product))
			return false;

		if (!$this->entry_in_cart($array_product))
			return false;

		// Preis ermitteln
		$price = get_products_price($array_product['products_id'], $array_product['attributes']);

		if ($this->use_database)
		{
			$sql = '
				UPDATE `cm_shopping_cart_ng_entries`
				SET `price_when_last_seen` = '.sqlval($price).'
				WHERE '.__db_build_product_where_clause($this->customer_id, $array_product).'
			';
			query($sql);
		}
		else
		{
			$this->cart_entries[$this->entry_in_cart($array_product)]['price_when_last_seen'] = $price;
		}

		return true;
	}

	/**
	 * Pr�ft ob die Attribute in einem genormten Produkt-Array korrekt sind.
	 *
	 * @static
	 * @param array $array_product Genormtes Produkt-Array
	 * @return bool false wenn fehlerhafte Parameter �bergeben, Attribute fehlen oder gew�hlte Attribute deaktiviert sind
	 */
	static function check_product_attributes($array_product)
	{
		if (!$array_product['products_id'])
			return false;

		$formula = get_cached_formula($array_product['products_id']);
		return check_attribute_combination($formula['rules'], $array_product['attributes']);
	}

	/**
	 * Pr�ft ob in den Eintr�gen Produkt-Arrays mit fehlerhaften oder deaktivierten Attributen sind und schiebt diese auf die Merkliste des Kunden.
	 * Speichert zus�tzlich Produktdaten in $_SESSION['deactivated_products'] f�r die JavaScript Meldungen.
	 *
	 * @return array|bool false im Fehlerfalle, ansonsten ein Array mit den verschobenen Eintr�gen.
	 */
	public function move_broken_or_deactivated_products_to_watchlist()
	{
		// Das ganze hier funktioniert nur wenn man angemeldet ist
		if (!$this->use_database)
			return false;

		$array_products = $this->get_all_entries();

		// Keine Produkte im Warenkorb
		if (!$array_products)
			return false;

		$array_broken_products = array();
		foreach ($array_products as $key => $le)
		{
			if (!shopping_cart_ng::check_product_attributes($le))
				$array_broken_products[$key] = $le;

			$sql = '
				SELECT `products_status`
				FROM `products`
				WHERE `products_id` = '.sqlval($le['products_id']).'
			';
			$products_status = @array_pop(array_pop(query($sql)));
			if (!$products_status)
				$array_broken_products[$key] = $le;
		}

		if ($array_broken_products)
		{
			$sql = '
				SELECT `customers_list_id`
				FROM `customers_list`
				WHERE `customers_id` = '.sqlval($_SESSION['customer_id']).'
					AND `list_type` = "watchlist"
			';
			$d = xtc_db_fetch_array(xtc_db_query($sql));
			$customers_list_id = $d['customers_list_id'];

			// Wird von der general.js.php behandelt
			$ajax_data = array();

			// Debug-Zeug speichern
			if (SHOPPING_CART_NG_DEBUG === true)
			{
				query('
					INSERT INTO `cm_shopping_cart_ng_merklisten_debug`
					SET `customers_id` = '.sqlval($this->customer_id).',
							`cart_entries_before` = '.sqlval(json_encode($array_products)).',
							`broken_products` = '.sqlval(json_encode($array_broken_products)).',
							`product_data` = "deprecated",
							`product_data_prepared` = "deprecated",
							`datetime_added` = NOW()
				');
			}

			// ID Liste basteln
			$id_list = '';
			foreach ($array_broken_products as $le)
				$id_list.= sqlval($le['products_id']).', ';
			$id_list = substr($id_list, 0, -2);

			// Produktnamen f�rs Ajax holen
			$sql = '
				SELECT `products_id`, `products_name`
				FROM `products_description`
				WHERE `products_id` IN ('.$id_list.')
					AND `language_id` = '.sqlval($_SESSION['languages_id']).'
			';
			//echo $sql;
			$array_product_data = query($sql);

			// Mapping bauen
			$array_product_names = array();
			foreach ($array_product_data as $le)
				$array_product_names[$le['products_id']] = $le['products_name'];

			foreach ($array_broken_products as $key => $le)
			{
				if (!$_SESSION['watchlist']->find_in_list($le['products_id'], $le['attributes']))
				{
					query('
						INSERT INTO `customers_list_products`
						SET customers_list_id = '.sqlval($customers_list_id).',
								products_id = '.sqlval($le['products_id']).',
								datetime_added = NOW(),
								attributes = '.sqlval(serialize($le['attributes'])).'
					');
				}
				$this->remove_entry($le);

				// Daten f�r die Ajax-Meldung speichern
				$ajax_data[] = array(
					'products_id' => $le['products_id'],
					'products_name' => preg_replace('#\<.*?\>#', ' ', $array_product_names[$le['products_id']]),
					'customers_basket_id' => $key,
					'attributes' => $le['attributes']
				);
			}

			// Wird von der general.js.php behandelt
			$_SESSION['deactivated_products'] = json_encode($ajax_data);
		}

		return $array_broken_products;
	}

	/**
	 * F�gt dem Warenkorb einen kaufbaren Gutschein hinzu
	 *
	 * @param String $title Titel des Gutscheins
	 * @param String $text Text des Gutscheins
	 * @param float $amount Gutscheinbetrag in der gew�hlten W�hrung (siehe $currency_code)
	 * @param int $currency_code W�hrung
	 * @param String $style gew�hltes Motiv
	 */
	public function add_buyable_coupon($title, $text, $amount, $currency_code, $style)
	{
		$coupon_array = $this->__get_option('buyable_coupons', array());
		$coupon_array[] = array(
			'title' => $title,
			'text' => $text,
			'amount' => $amount,
			'currency_code' => $currency_code,
			'style' => $style,
		);
		$this->__set_option('buyable_coupons', $coupon_array);
	}

	/**
	 * Entfernt einen kaufbaren Gutschein aus dem Warenkorb
	 *
	 * @param int $coupon_position die Position des Gutscheins in der Gutscheinliste
	 */
	public function remove_buyable_coupon($coupon_position)
	{
		$coupon_array = $this->__get_option('buyable_coupons');
		unset($coupon_array[$coupon_position]);
		$this->__set_option('buyable_coupons', $coupon_array);
	}

	/**
	 * liefert ein Array mit allen zu kaufenden Gutscheinen
	 * @return array
	 */
	public function get_buyable_coupons()
	{
		$buyable_coupons = $this->__get_option('buyable_coupons');

		if (!$buyable_coupons)
			$buyable_coupons = array();

		return $buyable_coupons;
	}
}





################################################################################
### Hilfsfunktionen

/**
 * Baut einen SQL String im Format "key = value, key = value, ..." zusammen. Erwartet vorbereitete Schl�ssel und Werte (sqlval, etc.).
 *
 * @param array $array
 * @return string
 */
function __prepare_sql_set_values($array)
{
	$sql = '';

	foreach ($array as $key => $value)
		$sql.= $key.' = '.$value.', ';

	// Letztes Komma wegmachen
	$sql = substr($sql, 0, -2);

	return $sql;
}

/**
 * Hilfsfunktion die aus einem genormten Produkt-Array einen korrekten WHERE-String zusammenbaut
 *
 * @param int $customers_id Kunden-ID
 * @param array $array_product Genormtes Produkt-Array
 * @return string String der an ein WHERE angeh�ngt werden kann.
 */
function __db_build_product_where_clause($customers_id, $array_product)
{
	@ksort($array_product['attributes']);
	$array_fields = array(
		'`customers_id`' => sqlval($customers_id),
		'`products_id`' => sqlval($array_product['products_id']),
		'`attributes`' => sqlval(json_encode($array_product['attributes'])),
		'`centralised_buying_group_id`' => sqlval($array_product['centralised_buying_group_id'])
	);

	$sql = '';
	foreach ($array_fields as $key => $value)
		$sql.= $key.' = '.$value.' AND ';

	// Letztes AND wegmachen
	$sql = substr($sql, 0, -5);

	return $sql;
}

/**
 * Baut aus einem Konstruktor-String das Konstruktor-Array zusammen
 *
 * @param string $constructor
 * @return array
 */
function __prepare_constructor_array($constructor)
{
	return array_merge(
		array_filter(
			preg_split('/(\[|\]|\]\[)/', $constructor),
			create_function('$val', 'return !empty($val) && $val != "*";')
		)
	);
}

/**
 * Baut aus einem Attribute-Array (gauge = 2, length = 2, etc.) ein nach Konstruktor sortiertes Attribute-Array zusammen.
 * @param array $array_attributes Attribute-Array (gauge = 2, length = 2, etc.)
 * @param string|array $constructor Konstruktor des Produkts. Wenn string wird {@link __prepare_constructor_array()} aufgerufen.
 * @return array|bool false wenn ein Attribut fehlt
 */
function __construct_attribute_array($array_attributes, $constructor)
{
	if (is_string($constructor))
		$constructor = __prepare_constructor_array($constructor);

	$array_constructed_attributes = array();
	foreach ($constructor as $current_attribute)
	{
		// Hier fehlt ein Attribut
		if (!$array_attributes[$current_attribute])
			return false;

		$array_constructed_attributes[$current_attribute] = $array_attributes[$current_attribute];
	}

	return $array_constructed_attributes;
}