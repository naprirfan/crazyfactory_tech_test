<?php
require_once(dirname(__FILE__).'/../includes/database.inc.php');

// die Fehleingaben, die bei Mailadressen verhindert werden sollen, holen
$sql = '
	SELECT check_string FROM mailaddress_check
	WHERE active = 1
';
$data = query($sql);

$mail_regex = array();
foreach ($data as $row)
{
	$row['check_string'] = str_replace('.', '\.', $row['check_string']);
	$mail_regex[] = preg_replace('/(?<!\.)\*/i', '.*?', $row['check_string']);
}

// Sprachvariablen zur �bergabe ans Javascript
$language = array(
	'text_mail_check' => TEXT_MAIL_CHECK,
	'text_mail_www_error' => TEXT_MAIL_WWW_ERROR,
	'text_mail_error' => TEXT_MAIL_ERROR,
	'text_your_mail_address' => TEXT_YOUR_MAIL_ADDRESS,
	'js_error_submitted' => JS_ERROR_SUBMITTED,
	'text_really_your_country' => TEXT_REALLY_YOUR_COUNTRY,
	'entry_first_name_error' => ENTRY_FIRST_NAME_ERROR,
	'entry_last_name_error' => ENTRY_LAST_NAME_ERROR,
	'entry_email_address_error' => ENTRY_EMAIL_ADDRESS_ERROR,
	'entry_street_address_error' => ENTRY_STREET_ADDRESS_ERROR,
	'entry_postcode_error' => ENTRY_POST_CODE_ERROR,
	'entry_city_error' => ENTRY_CITY_ERROR,
	'entry_country_error' => ENTRY_COUNTRY_ERROR,
	'entry_password_error' => ENTRY_PASSWORD_ERROR,
	'entry_password_error_not_matching' => ENTRY_PASSWORD_ERROR_NOT_MATCHING,
	'entry_password_new_error' => ENTRY_PASSWORD_NEW_ERROR,
	'entry_password_new_error_not_matching' => ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING,
);

// Mindestl�nge der Felder
$field_length_array = array(
	'firstname' => ENTRY_FIRST_NAME_MIN_LENGTH,
	'lastname' => ENTRY_LAST_NAME_MIN_LENGTH,
	'email_address' => ENTRY_EMAIL_ADDRESS_MIN_LENGTH,
	'street_address' => ENTRY_STREET_ADDRESS_MIN_LENGTH,
	'postcode' => ENTRY_POSTCODE_MIN_LENGTH,
	'city' => ENTRY_CITY_MIN_LENGTH,
	'password' => ENTRY_PASSWORD_MIN_LENGTH,
);

// L�nder welche ein confirm ausl�sen, bei dem der Benutzer best�tigen musste, dass es sein Land ist
$sql = 'SELECT countries_id FROM countries WHERE check_country = 1';
$country_list = query($sql);

// alles ans Javascript �bergeben
echo '<script type="text/javascript">'."\n";
echo 'var form_check_current_template = \''.CURRENT_TEMPLATE.'\';';
echo 'var form_check_mail_regex = JSON.parse(\''.addslashes(json_encode($mail_regex)).'\');'."\n";
echo 'var form_check_language = JSON.parse(\''.addslashes(json_encode($language)).'\');'."\n";
echo 'var form_check_field_length = JSON.parse(\''.addslashes(json_encode($field_length_array)).'\');'."\n";
echo 'var form_check_countries = JSON.parse(\''.addslashes(json_encode($country_list)).'\');'."\n";
echo '</script>'."\n";