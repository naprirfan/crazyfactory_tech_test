<?php
/* -----------------------------------------------------------------------------------------
   $Id: form_check.js.php 1296 2005-10-08 17:52:26Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(form_check.js.php,v 1.9 2003/05/19); www.oscommerce.com
   (c) 2003	 nextcommerce (form_check.js.php,v 1.3 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/


?>
<script type="text/javascript"><!--
var form = "";
var submitted = false;
var error = false;
var error_message = "";

function check_input(field_name, field_size, message) {
  if (document.forms[form].elements[field_name] && (document.forms[form].elements[field_name].type != "hidden")) {
    var field_value = document.forms[form].elements[field_name].value;

    if (field_value == '' || field_value.length < field_size) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}

function check_radio(field_name, message) {
  var isChecked = false;

  if (document.forms[form].elements[field_name] && (document.forms[form].elements[field_name].type != "hidden")) {
    var radio = document.forms[form].elements[field_name];

    for (var i=0; i<radio.length; i++) {
      if (radio[i].checked == true) {
        isChecked = true;
        break;
      }
    }

    if (isChecked == false) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}

function check_select(field_name, field_default, message) {
  if (document.forms[form].elements[field_name] && (document.forms[form].elements[field_name].type != "hidden")) {
    var field_value = document.forms[form].elements[field_name].value;

    if (field_value == field_default) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}

function check_password(field_name_1, field_name_2, field_size, message_1, message_2) {
  if (document.forms[form].elements[field_name_1] && (document.forms[form].elements[field_name_1].type != "hidden")) {
    var password = document.forms[form].elements[field_name_1].value;
    var confirmation = document.forms[form].elements[field_name_2].value;

    if (password == '' || password.length < field_size) {
      error_message = error_message + "* " + message_1 + "\n";
      error = true;
    } else if (password != confirmation) {
      error_message = error_message + "* " + message_2 + "\n";
      error = true;
    }
  }
}

function check_password_new(field_name_1, field_name_2, field_name_3, field_size, message_1, message_2, message_3) {
  if (document.forms[form].elements[field_name_1] && (document.forms[form].elements[field_name_1].type != "hidden")) {
    var password_current = document.forms[form].elements[field_name_1].value;
    var password_new = document.forms[form].elements[field_name_2].value;
    var password_confirmation = document.forms[form].elements[field_name_3].value;

    if (password_current == '' || password_current.length < field_size) {
      error_message = error_message + "* " + message_1 + "\n";
      error = true;
    } else if (password_new == '' || password_new.length < field_size) {
      error_message = error_message + "* " + message_2 + "\n";
      error = true;
    } else if (password_new != password_confirmation) {
      error_message = error_message + "* " + message_3 + "\n";
      error = true;
    }
  }
}

function check_country()
{
	var shipping_address = $('#shipping_billing_switch').val();
	var country_id_billing = $('#country_list_billing').val();
	var country_id_shipping = $('#country_list_shipping').val();
	var billing_confirm = shipping_confirm = true;
	var country_list = new Array(<?php
$sql = 'SELECT countries_id FROM '.TABLE_COUNTRIES.' WHERE check_country = 1';
$query = xtc_db_query($sql);
$count = xtc_db_num_rows($query);
if ($count > 1)
{
	while ($row = xtc_db_fetch_array($query))
	{
		echo $row['countries_id'];
		if ($count > 1)
			echo ', ';
		$count--;
	}
}
elseif($count)
{
	$row = xtc_db_fetch_array($query);
	echo $row['countries_id'];
	echo ', 1';
}
?>);
<?php
if ($count == 1)
{
?>
	country_list.pop();
<?php
}
?>
	for (var i = 0; i < country_list.length; i++)
	{
		if (country_id_billing == country_list[i])
		{
			billing_confirm = confirm('<?php echo TEXT_REALLY_YOUR_COUNTRY; ?>');
			break;
		}
	}
	if (shipping_address != 'on' && billing_confirm == false)
	{
		for (var i = 0; i < country_list.length; i++)
		{
			if (country_id_shipping == country_list[i])
			{
				shipping_confirm = confirm('<?php echo TEXT_REALLY_YOUR_COUNTRY; ?>');
				break;
			}
		}
	}
	if (billing_confirm == false || shipping_confirm == false)
		return true;
	else
		return false;
}

function check_mail_format()
{
    if(arguments[0])
    {
       var mail_address = arguments[0];
    }
    else
    {
      var mail_address = document.forms[form].elements['email_address'].value;
    }
     
	var known_mistakes = new Array(<?php
$sql = '
	SELECT check_string FROM mailaddress_check
	WHERE active = 1
';
$query = xtc_db_query($sql);
$count = xtc_db_num_rows($query);
while ($row = xtc_db_fetch_array($query))
{
	echo '/'.preg_replace('/(?<!\.)\*/i', '.*?', $row['check_string']).'/i';
	if ($count > 1)
		echo ', ';
	$count--;
}
?>);
	var www_error = www_error2 = check_error = -1;
	//pr�fen auf www. vor dem Namen und der TLD
	www_error = mail_address.search(/^www\./i);
	www_error2 = mail_address.search(/\@www\./i);
	//pr�fen auf bekannte tippfehler
	for (var i = 0; i < known_mistakes.length; i++)
	{
		if (mail_address.search(known_mistakes[i]) != -1)
		{
			check_error = 1;
			break;
		}
	}
	var error_text = '<?php echo TEXT_MAIL_CHECK; ?>' + "\n";
	if (www_error >= 0 || www_error2 >= 0)
		error_text = error_text + '<?php echo TEXT_MAIL_WWW_ERROR; ?>' + "\n";
	if (check_error == 1)
		error_text = error_text + '<?php echo TEXT_MAIL_ERROR; ?>';
	error_text = error_text + "\n\n" + '<?php echo TEXT_YOUR_MAIL_ADDRESS; ?>' + mail_address;

	var confirm_result = true;
	if (check_error == 1 || www_error >= 0 || www_error2 >= 0)
		confirm_result = confirm(error_text);

	if (confirm_result == false)
		return true;
	else
		return false;
}

function check_form(form_name) {
  if (submitted == true) {
    alert(unescape("<?php echo xtc_js_lang(JS_ERROR_SUBMITTED); ?>"));
    return false;
  }

  var shipping_address = $('#shipping_billing_switch:checked').val();

  error = false;
  form = form_name;
  error_message = unescape("<?php echo xtc_js_lang(JS_ERROR); ?>");

<?php if (ACCOUNT_GENDER == 'true' and false) echo '  check_radio("gender", "' . ENTRY_GENDER_ERROR . '");' . "\n"; ?>
  check_input("firstname", <?php echo ENTRY_FIRST_NAME_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_FIRST_NAME_ERROR); ?>");
  if (shipping_address != 'on')
  	check_input("firstname_shipping", <?php echo ENTRY_FIRST_NAME_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_FIRST_NAME_ERROR); ?>");
  check_input("lastname", <?php echo ENTRY_LAST_NAME_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_LAST_NAME_ERROR); ?>");
  if (shipping_address != 'on')
  	check_input("lastname_shipping", <?php echo ENTRY_LAST_NAME_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_LAST_NAME_ERROR); ?>");

<?php if (ACCOUNT_DOB == 'true' and false) echo '  check_input("dob", ' . ENTRY_DOB_MIN_LENGTH . ', "' . xtc_js_lang(ENTRY_DATE_OF_BIRTH_ERROR) . '");' . "\n"; ?>

  check_input("email_address", <?php echo ENTRY_EMAIL_ADDRESS_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_EMAIL_ADDRESS_ERROR); ?>");
  check_input("email_address_confirm", <?php echo ENTRY_EMAIL_ADDRESS_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_EMAIL_ADDRESS_ERROR); ?>");

  var error_mail_format = check_mail_format();

  check_input("street_address", <?php echo ENTRY_STREET_ADDRESS_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_STREET_ADDRESS_ERROR); ?>");
  if (shipping_address != 'on')
  	check_input("street_address_shipping", <?php echo ENTRY_STREET_ADDRESS_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_STREET_ADDRESS_ERROR); ?>");
  check_input("postcode", <?php echo ENTRY_POSTCODE_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_POST_CODE_ERROR); ?>");
  if (shipping_address != 'on')
  	check_input("postcode_shipping", <?php echo ENTRY_POSTCODE_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_POST_CODE_ERROR); ?>");
  check_input("city", <?php echo ENTRY_CITY_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_CITY_ERROR); ?>");
  if (shipping_address != 'on')
  	check_input("city_shipping", <?php echo ENTRY_CITY_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_CITY_ERROR); ?>");

<?php if (ACCOUNT_STATE == 'true' and false) echo '  check_input("state", ' . ENTRY_STATE_MIN_LENGTH . ', "' . xtc_js_lang(ENTRY_STATE_ERROR) . '");' . "\n"; ?>

  check_select("country_list_billing", "", "<?php echo xtc_js_lang(ENTRY_COUNTRY_ERROR); ?>");
  if (shipping_address != 'on')
  	check_select("country_list_shipping", "", "<?php echo xtc_js_lang(ENTRY_COUNTRY_ERROR); ?>");

<?php if (false) { ?>  check_input("telephone", <?php echo ENTRY_TELEPHONE_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_TELEPHONE_NUMBER_ERROR); ?>");<?php } ?>

  if (document.forms[form].elements["password"].value || document.forms[form].elements["confirmation"].value)
  {
  	check_password("password", "confirmation", <?php echo ENTRY_PASSWORD_MIN_LENGTH; ?>, "<?php echo xtc_js_lang(ENTRY_PASSWORD_ERROR); ?>", "<?php echo xtc_js_lang(ENTRY_PASSWORD_ERROR_NOT_MATCHING); ?>");
  	check_password_new("password_current", "password_new", "password_confirmation", <?php echo xtc_js_lang(ENTRY_PASSWORD_MIN_LENGTH); ?>, "<?php echo xtc_js_lang(ENTRY_PASSWORD_ERROR); ?>", "<?php echo xtc_js_lang(ENTRY_PASSWORD_NEW_ERROR); ?>", "<?php echo xtc_js_lang(ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING); ?>");
  }

  var error_country = check_country();

  if (error == true && (error_mail_format == true || error_country == true))
  {
    return false;
  }
  else if (error == false && error_mail_format == false && error_country == false)
  {
    submitted = true;
    return true;
  }
  else if (error == false && (error_mail_format == true || error_country == true))
	  return false;
  else
	  return true;
}
//--></script>