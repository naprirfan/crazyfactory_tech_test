<div class="pageHeading">Alle Actionshots</div>
	<table width="100%" cellpadding="2" class="actionshottable">
<?php
// CONCAT_WS(".",DAY(a.upload_time),MONTH(a.upload_time),YEAR(a.upload_time)) AS uploadtime,
$sql = 'SELECT
	a.*,
	p.products_model,
	DATE_FORMAT(a.upload_time, "%d.%m.%Y") AS uploadtime,
	pd.products_name,
	customers_firstname,customers_lastname,customers_email_address
	FROM cm_actionshots a
	JOIN products p USING(products_id)
	JOIN products_description pd USING(products_id)
	JOIN customers c USING(customers_id)
	WHERE a.active=1 and deleted=0
	AND pd.language_id='.$_SESSION['languages_id'].'
	';
$res = xtc_db_query($sql);

while($image = xtc_db_fetch_array($res))
{
	actionshotrow($image);
}
?>
</table>