<?php
/* --------------------------------------------------------------
   $Id: column_left.php 2100 2008-08-19 19:46:00 Kai f�r xtc-load.de $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(column_left.php,v 1.15 2002/01/11); www.oscommerce.com
   (c) 2003	 nextcommerce (column_left.php,v 1.25 2003/08/19); www.nextcommerce.org

   Released under the GNU General Public License
   --------------------------------------------------------------*/
	if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['customers'] == '1' || $admin_access['customers_status'] == '1'))
	  echo ('<div class="menue" style="margin-top:0px">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>'.BOX_HEADING_CUSTOMERS.'</b></div>');
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['customers'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CUSTOMERS, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CUSTOMERS . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['customers_status'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CUSTOMERS_STATUS, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CUSTOMERS_STATUS . '</a></div>';

  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['rma'] == '1' || $admin_access['rma_refund'] == '1' || $admin_access['rma_packets'] == '1'))
  	echo ('<div class="menue" style="margin-top:0px">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>RMA</b></div>');
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['rma'] == '1'))	echo '<div class="menueitem_bg"><a href="' . xtc_href_link('rma.php', '', 'NONSSL') . '" class="menuBoxContentLink">'.xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'R&uuml;cksendungen</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['rma_packets'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link('rma_packets.php', '', 'SSL') . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'RMA-Pakete' . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['rma_refund'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link('rma_refund.php', '', 'SSL') . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'R&uuml;ckerstattungen' . '</a></div>';

  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['orders'] == '1'))
		echo ('<div class="menue" style="margin-top:0px">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>Orders</b></div>');
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['orders'] == '1')) echo '<div class="menueitem_bg" style="margin-top: 10px;"><a class="menueitem" href="' . xtc_href_link(FILENAME_ORDERS, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_ORDERS . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['orders_export_in_euro'] == '1')) echo '<div class="menueitem_bg"><a href="' . xtc_href_link('orders_export_in_euro.php', '', ADMINSSL) . '" class="menuBoxContentLink">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'CSV Euro-Export  </a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['orders_export'] == '1')) echo '<div class="menueitem_bg"><a href="' . xtc_href_link('orders_export.php', '', ADMINSSL) . '" class="menuBoxContentLink">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'CSV Export Bestellungen </a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['orders_mass_switch'] == '1' && $admin_access['paypal_conflict_check'] == '1')) echo '<div class="menueitem_bg"><a href="' . xtc_href_link('orders_mass_switch.php', '', ADMINSSL) . '" class="menuBoxContentLink">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Bestellstatus &Auml;nderungen</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['orders_allow_export'] == '1')) echo '<div class="menueitem_bg"><a href="' . xtc_href_link('orders_allow_export.php', '', ADMINSSL) . '" class="menuBoxContentLink">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'<nobr>Manuelle Exportfreigabe</nobr></a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['orders_allow_export_manuell'] == '1')) echo '<div class="menueitem_bg"><a href="' . xtc_href_link('orders_allow_export_manuell.php', '', ADMINSSL) . '" class="menuBoxContentLinkRed">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'<nobr>Manuell exportieren</a></nobr></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['collect_new_orders'] == '1')) echo '<div class="menueitem_bg"><a href="' . xtc_href_link('collect_new_orders.php', '', ADMINSSL) . '" class="menuBoxContentLink">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Bestellpool abholen</a></div>';
//  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['orders_invoice_print'] == '1')) echo '<div class="menueitem_bg"><a href="' . xtc_href_link('orders_invoice_print.php', '', ADMINSSL) . '" class="menuBoxContentLink">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Rechnungen drucken</a></div>';

	echo ('<div class="menue" style="margin-top:0px">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>Payments</b></div>');
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['paypal_conflict_check'] == '1'))
  {
    // paypal Sonderf�lle z�hlen
  $sql= 'SELECT count(*) as anzahl from cm_book_inpayment_import
						WHERE (order_id = 0 OR error != "NONE")
						AND book_inpayment_type = "paypal"
						AND hidden = 0
						AND book_inpayment_status != "Pending"
						AND book_inpayment_status != "Reversed"
						AND book_inpayment_status != "Refunded"
						order by book_inpayment_import_id DESC
					';
	  $res = xtc_db_query ($sql);
	  $red = '';
	  $paypal_count = xtc_db_fetch_array($res);
		if ($paypal_count['anzahl'] > 0)
			$red = ' style="color:#ff0000;"';
  	echo '<div class="menueitem_bg" style="margin-top: 10px;"><a href="' . xtc_href_link('paypal_conflict_check.php', '', ADMINSSL) . '" class="';
  	echo 'menuBoxContentLink';
  	echo '"'.$red.'>'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Paypal Sonderf&auml;lle';

  	if ($paypal_count['anzahl'] > 0)
  		echo ' ('.$paypal_count['anzahl'].')';

  	echo '</a></div>';
  }
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['docdata_conflict_check'] == '1'))
  {
  	    // Docdata Sonderf�lle z�hlen
  $sql= 'SELECT count(*) as anzahl from cm_book_inpayment_import
						WHERE
						hidden = 0
						AND book_inpayment_type = "docdata"
						order by book_inpayment_import_id DESC
					';
	  $res = xtc_db_query ($sql);
	  $red ='';
	  $docdata_count = xtc_db_fetch_array($res);
	  if ($docdata_count['anzahl'] > 0)
	 				$red = ' style="color:#ff0000;"';
  	echo '<div class="menueitem_bg"><a href="' . xtc_href_link('docdata_conflict_check.php', '', ADMINSSL) . '" class="';
  	echo 'menuBoxContentLink';
  	echo '"'.$red.'>'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Docdata Sonderf&auml;lle';

  	if ($docdata_count['anzahl'] > 0)
  		echo ' ('.$docdata_count['anzahl'].')';

  	echo '</a></div>';
	}

  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['transfer_check'] == '1') && $admin_access['paypal_conflict_check'] == '1')
  {
  	    // �berweisungs Sonderf�lle z�hlen
  $sql= 'SELECT count(*) as anzahl from cm_book_inpayment_import
						WHERE (order_id = 0
						OR error != "NONE")
						AND hidden = 0
						AND unclear = 0
						AND book_inpayment_type = "transfer"
						order by book_inpayment_import_id DESC
					';
	  $res = xtc_db_query ($sql);
	  $red ='';
	  $transfer_count = xtc_db_fetch_array($res);
	  if ($transfer_count['anzahl'] > 0)
	 				$red = ' style="color:#ff0000;"';
  	echo '<div class="menueitem_bg"><a href="' . xtc_href_link('transfer_check.php', '', ADMINSSL) . '" class="';
  	echo 'menuBoxContentLink';
  	echo '"'.$red.'>'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'&Uuml;berweisungen';

  	if ($transfer_count['anzahl'] > 0)
  		echo ' ('.$transfer_count['anzahl'].')';

  	echo '</a></div>';
	}
	if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['transfer_check'] == '1') )
	{
  	echo '<div class="menueitem_bg"><a href="' . xtc_href_link('transfer_check.php?show_unclear=on', '', ADMINSSL) . '" class="';
  	echo 'menuBoxContentLink';
  	echo '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'unklare &Uuml;berweisungen';

  	echo '</a></div>';
	}



  if (($_SESSION['customers_status']['customers_status_id'] == '0') &&
  				($admin_access['categories'] == '1' ||
  				 $admin_access['specials'] == '1' ||
  				 $admin_access['csv_backend'] == '1' ||
  				 $admin_access['new_products_list'] == '1' ||
  				 $admin_access['products_expected'] == '1')
  				 )
	{
	echo ('<div class="menue">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>'.BOX_HEADING_PRODUCTS.'</b></div>');
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['categories'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CATEGORIES, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CATEGORIES . '</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['categories'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link('disable_list.php', '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Deaktivierte Varianten</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['categories'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link('stockproducts_list.php', '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Stockartikel</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['specials'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_SPECIALS, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_SPECIALS . '</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['new_products_list'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link('new_products_list.php', '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Neue Produkte</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['csv_backend'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link('csv_backend.php') . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_IMPORT . '</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['products_expected'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_PRODUCTS_EXPECTED, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_PRODUCTS_EXPECTED . '</a></div>';
	}

  if ($_SESSION[customer_id]==1) {
	  echo ('<div class="menue">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>'.BOX_HEADING_STATISTICS.'</b></div>');

	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['stats_paymentmethods'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link('stats_paymentmethods.php', '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Bezahlmethoden Statistik</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['stats_products_viewed'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_STATS_PRODUCTS_VIEWED, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_PRODUCTS_VIEWED . '</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['stats_products_purchased'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_STATS_PRODUCTS_PURCHASED, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_PRODUCTS_PURCHASED . '</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['stats_customers'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_STATS_CUSTOMERS, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_STATS_CUSTOMERS . '</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['stats_campaigns'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CAMPAIGNS_REPORT, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"'). BOX_CAMPAIGNS_REPORT . '</a></div>';
	// customproducts report
	// Boxlink f?old Cart
	//  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['stats_unsold_carts'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_STATS_UNSOLD_CARTS, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"') . BOX_STATS_UNSOLD_CARTS . '</a></div>';
	//  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['stats_search_keywords'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_STATS_SEARCH_KEYWORDS, '', ADMINSSL) . '">'.xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_SEARCH_KEYWORDS.'</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['stats_sales_report'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_SALES_REPORT, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_SALES_REPORT . '</a></div>';

	}
		if (($_SESSION['customers_status']['customers_status_id'] == '0') &&
		($admin_access['content_manager'] == '1' ||
		 $admin_access['email_manager'] == '1' ||
		 $admin_access['email_known_typos'] == '1' ||
		 $admin_access['check_missing_images'] == '1' ||
		 $admin_access['customers'] == '1' ||
		 $admin_access['actionshots'] == '1')
		 )
	{
	  echo ('<div class="menue">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>'.BOX_HEADING_TOOLS.'</b></div>');

	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['actionshots'] == '1'))
	  {
			$sql= 'SELECT count(a.actionshot_id) as anzahl from cm_actionshots a
							INNER JOIN cm_reported_actionshots ra ON (a.actionshot_id = ra.actionshot_id AND ra.deleted=0)
						WHERE active=0 ';
	  	$res = xtc_db_query ($sql);
	  	$actionshots= xtc_db_fetch_array($res);
	  	$red = '';
			if ($actionshots['anzahl'] > 0)
				$red = ' style="color:#ff0000;"';
	  	echo '<div class="menueitem_bg"><a href="' . xtc_href_link(FILENAME_ACTION_SHOTS, '', ADMINSSL)  . '" class="menuBoxContentLink"';
	  	echo '"'.$red.'>'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'User Action Shots';

	  	if ($actionshots['anzahl'] > 0)
	  		echo ' ('.$actionshots['anzahl'].')';

	  	echo '</a></div>';
	  }

	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['content_manager'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONTENT_MANAGER) . '">'.xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONTENT . '</a></div>';
	//  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['backup'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_BACKUP) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_BACKUP . '</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['email_manager'] == '1')) echo '<div class="menueitem_bg"><a href="' . xtc_href_link('email_manager.php') . '" class="menuBoxContentLink">'.xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Email Manager</a></div>';
	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['email_known_typos'] == '1'))
	  {
		  echo '<div class="menueitem_bg"><a href="' . xtc_href_link('email_known_typos.php', '', ADMINSSL) . '" class="menuBoxContentLink">'.xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_KNOWN_EMAIL_TYPOS.'</a></div>';
	  }


	  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['check_missing_images'] == '1'))
	  {
		  $sql= '
		  SELECT count(*) as anzahl
		  FROM (
			SELECT missing_images_id, image, product_id FROM cm_missing_images
			UNION SELECT missing_colorcharts_id, image, product_id FROM cm_missing_colorcharts
		  ) AS m
		  ';
		  $res = xtc_db_query ($sql);
		  $missing_images= xtc_db_fetch_array($res);
		  $red = '';
			if ($missing_images['anzahl'] > 0)
				$red = ' style="color:#ff0000;"';
		  echo '<div class="menueitem_bg"><a href="' . xtc_href_link('check_missing_images.php', '', ADMINSSL) . '" class="menuBoxContentLink"';
		 	echo '"'.$red.'>'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Fehlende Produktbilder';

	  	if ($missing_images['anzahl'] > 0)
	  		echo ' ('.$missing_images['anzahl'].')';

	  	echo '</a></div>';
	  }
	  // ########### adminrecht?erung auf shop_translation!
	    if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['email_manager'] == '1')) echo '<a href="'.xtc_href_link('LanguageParser/index.php') .'" target="_blank" class="menuBoxContentLink">'.xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'TRANSLATOR</font></a><br>';
	//  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['banner_manager'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_BANNER_MANAGER) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_BANNER_MANAGER . '</a></div>';
	//  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['blacklist'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_BLACKLIST, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_TOOLS_BLACKLIST . '</a></div>';
	//  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['module_newsletter'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_MODULE_NEWSLETTER) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_MODULE_NEWSLETTER . '</a></div>';
	//  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['server_info'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_SERVER_INFO) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_SERVER_INFO . '</a></div>';
	//  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['whos_online'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_WHOS_ONLINE) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_WHOS_ONLINE . '</a></div>';
}
if (ACTIVATE_GIFT_SYSTEM=='true') {
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['coupon_admin'] == '1'))
	  echo ('<div class="menue">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>'.BOX_HEADING_GV_ADMIN.'</b></div>');
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['coupon_admin'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_COUPON_ADMIN, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_COUPON_ADMIN . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['gv_queue'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_GV_QUEUE, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_GV_ADMIN_QUEUE . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['gv_mail'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_GV_MAIL, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_GV_ADMIN_MAIL . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['gv_sent'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_GV_SENT, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_GV_ADMIN_SENT . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['coupon_themes'] == '1')) echo '<div class="menueitem_bg"><a href="' . xtc_href_link('coupon_themes.php', '', ADMINSSL)  . '" class="menuBoxContentLink">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').'Coupon Themes</a></div>';

}

  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1'))
	  echo ('<div class="menue">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>'.BOX_HEADING_CONFIGURATION.'</b></div>');
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=1', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_1 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=2', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_2 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=3', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_3 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=8', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_8 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['orders_status'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_ORDERS_STATUS, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_ORDERS_STATUS . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=4', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_4 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=11', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_11 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['cross_sell_groups'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_XSELL_GROUPS, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"') . BOX_ORDERS_XSELL_GROUP . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=13', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_13 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=12', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_12 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=14', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_14 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['campaigns'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CAMPAIGNS, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"') . BOX_CAMPAIGNS . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=5', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_5 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=9', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_9 . '</a></div>';
  if (ACTIVATE_SHIPPING_STATUS=='true') {
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['shipping_status'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_SHIPPING_STATUS, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_SHIPPING_STATUS . '</a></div>';
  }
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=10', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_10 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=16', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_16 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=15', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_15 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=22', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_22 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=18', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_18 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=7', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_7 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['products_vpe'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_PRODUCTS_VPE, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_PRODUCTS_VPE . '</a></div>';
//  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=60', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"') . BOX_DOWN_FOR_MAINTENANCE . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=19', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_19 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=17', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_17 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=365', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_365 . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=366', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CONFIGURATION_366 . '</a></div>';

  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['modules'] == '1'))
	  echo ('<div class="menue">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>'.BOX_HEADING_MODULES.'</b></div>');
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['modules'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_MODULES, 'set=payment', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_PAYMENT . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['modules'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_MODULES, 'set=shipping', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_SHIPPING . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['modules'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_MODULES, 'set=ordertotal', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_ORDER_TOTAL . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['module_export'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_MODULE_EXPORT) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_MODULE_EXPORT . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['customers_status'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CUSTOMERS_STATUS, '', 'SSL') . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CUSTOMERS_STATUS . '</a></div>';

if (($_SESSION['customers_status']['customers_status_id'] == '0') &&
	($admin_access['languages'] == '1' ||
	 $admin_access['countries'] == '1' ||
	 $admin_access['currencies'] == '1' ||
	 $admin_access['zones'] == '1' ||
	 $admin_access['geo_zones'] == '1' ||
	 $admin_access['tax_classes'] == '1' ||
	 $admin_access['tax_rates'] == '1'
	 ))
{
  echo ('<div class="menue">'.xtc_image(DIR_WS_IMAGES . 'header.gif', '', '', '', 'class="imgtext"').'&nbsp;<b>'.BOX_HEADING_ZONE.'</b></div>');
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['languages'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_LANGUAGES, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_LANGUAGES . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['countries'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_COUNTRIES, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_COUNTRIES . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['currencies'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_CURRENCIES, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_CURRENCIES. '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['zones'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_ZONES, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_ZONES . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['geo_zones'] == '1')) echo '<a class="menueitem" href="' . xtc_href_link(FILENAME_GEO_ZONES, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_GEO_ZONES . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['tax_classes'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_TAX_CLASSES, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_TAX_CLASSES . '</a></div>';
  if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['tax_rates'] == '1')) echo '<div class="menueitem_bg"><a class="menueitem" href="' . xtc_href_link(FILENAME_TAX_RATES, '', ADMINSSL) . '">'. xtc_image(DIR_WS_IMAGES . 'bullet.gif', '', '', '', 'class="imgmiddle"').BOX_TAX_RATES . '</a></div>';
}

?>
