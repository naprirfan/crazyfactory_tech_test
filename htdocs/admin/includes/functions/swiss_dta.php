<?php

define('SWISS_DTA_SEGMENT_END', "\r\n");

define('DTA_TYPE_NUMBER', 1);
define('DTA_TYPE_AMOUNT', 3);
define('DTA_TYPE_ASCII', 2);

$GLOBALS['swiss_dta']['sequence_iterator'] = 1;

function create_swiss_dta_ta836($array_transfers, $config)
{
	$GLOBALS['swiss_dta']['sequence_iterator'] = 1;
	$dta = '';
	$total = 0.0;

	foreach ($array_transfers as $le)
	{
		// Segment 1
		$dta.= '01';
		$dta.= create_swiss_dta_header($config);
		$dta.= swiss_dta_field($config['dta_initiator_identification'].''/*Referenznummer*/, 16); // TODO Andr� sagt die Referenznummer muss nich
		$dta.= swiss_dta_field($config['account_number'], 24);
		$dta.= date('ymd').$le['currency_code'].swiss_dta_field($le['amount'], 15, DTA_TYPE_AMOUNT);
		$dta.= swiss_dta_field('', 11); // Laut Doku "Reserve"
		$dta.= SWISS_DTA_SEGMENT_END;

		// Segment 2
		$dta.= '02';
		$dta.= swiss_dta_field('', 12); // Devisenkurse mit der Bank
		$dta.= swiss_dta_field($config['initiator_address_1'], 35);
		$dta.= swiss_dta_field($config['initiator_address_2'], 35);
		$dta.= swiss_dta_field($config['initiator_address_3'], 35);
		$dta.= swiss_dta_field('', 9); // "Reserve"
		$dta.= SWISS_DTA_SEGMENT_END;

		// Segment 3
		$dta.= '03';
		$dta.= 'A';
		$dta.= swiss_dta_field($le['bic'], 35).swiss_dta_field('', 35);
		$dta.= swiss_dta_field($le['iban'], 34);
		$dta.= swiss_dta_field('', 21); // "Reserve"
		$dta.= SWISS_DTA_SEGMENT_END;

		// Segment 4
		$dta.= '04';
		$dta.= swiss_dta_field($le['name'], 35).swiss_dta_field($le['address_1'], 35).swiss_dta_field($le['address_2'], 35);
		$dta.= swiss_dta_field('', 21); // "Reserve"
		$dta.= SWISS_DTA_SEGMENT_END;

		// Segment 5
		$dta.= '05';
		$dta.= 'U';
		$dta.= swiss_dta_field($le['transfer_reason_1'], 35).
			swiss_dta_field($le['transfer_reason_2'], 35).
			swiss_dta_field($le['transfer_reason_3'], 35);
		$dta.= '0'; // Alle Spesen zu Lasten Auftraggeber
		$dta.= swiss_dta_field('', 19); // "Reserve"
		$dta.= SWISS_DTA_SEGMENT_END;

		$GLOBALS['swiss_dta']['sequence_iterator']++;
		$total+= $le['amount'];
	}

	// TA 890 Totalrecord
	$dta.= '01';
	$dta.= create_swiss_dta_header($config, '890');
	$dta.= swiss_dta_field($total, 16, DTA_TYPE_AMOUNT); // Laut Doku hat das hier maximal 3 Nachkommastellen. 2 sollten hoffentlich auch gehen.
	$dta.= swiss_dta_field('', 59);

	return $dta;
}

function create_swiss_dta_header($config, $transaction_type = '836')
{
	$dta_header = '';

	$dta_header.= swiss_dta_field(0, 6, DTA_TYPE_NUMBER); // Buchungstag (nur bei TA 826 und TA 827)
	$dta_header.= swiss_dta_field('', 12); // nur TA 827
	$dta_header.= swiss_dta_field(0, 5, DTA_TYPE_NUMBER); // Bankintern
	$dta_header.= date('ymd'); // Datum der DTA Datei
	if ($transaction_type == '890')
		$dta_header.= swiss_dta_field('', 7);
	else
		$dta_header.= swiss_dta_field($config['clearing_no'], 7);
	$dta_header.= swiss_dta_field($config['dta_initiator_identification'], 5);
	$dta_header.= swiss_dta_field($GLOBALS['swiss_dta']['sequence_iterator'], 5, DTA_TYPE_NUMBER);
	$dta_header.= $transaction_type;
	$dta_header.= '0';
	$dta_header.= '0'; // Bankintern

	return $dta_header;
}

function swiss_dta_field($content, $length, $type = DTA_TYPE_ASCII)
{
	$ascii_replacements = array(
		'�' => 'ae', '�' => 'Ae', '�' => '�', '�' => 'Oe', '�' => 'ue', '�' => 'Ue'
	);

	if ($type == DTA_TYPE_AMOUNT)
		$content = number_format($content, 2, ',', '');

	if ($type == DTA_TYPE_NUMBER)
		$content = intval($content);

	// Bei ASCII-Feldern sind nur die Zeichen a-Z und 0-9 erlaubt (und bestimmte Satzzeichen)
	if ($type == DTA_TYPE_ASCII)
	{
		$content = str_replace(array_keys($ascii_replacements), $ascii_replacements, $content);
		$content = preg_replace('#[^A-Za-z0-9 ?\'+,-./:()]#', '.', $content);
	}

	$content = substr($content, 0, $length);

	if ($type == DTA_TYPE_NUMBER)
		return sprintf('%0'.$length.'s', $content);
	else
		return sprintf('%-'.$length.'s', $content);
}