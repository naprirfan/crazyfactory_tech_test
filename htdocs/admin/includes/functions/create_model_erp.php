<?php
function create_model_erp($product_model, $konstruktor, $attributes)
{
	$model_erp = $product_model; // mit der modelnummer fängt es an.
	if (!is_array($attributes))
		$attributes = unserialize($attributes);

	if ($konstruktor)
	{
		foreach ($attributes as $name => $value)
			$konstruktor = str_replace('['.$name.']', preg_replace('/(\.|\/|,)/', '', $value), $konstruktor);
		$model_erp .= $konstruktor;
	}

	return $model_erp;
}