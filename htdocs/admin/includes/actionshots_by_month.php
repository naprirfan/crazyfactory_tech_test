<div class="pageHeading">Actionshots des Monats <?php echo $_GET['month']; ?></div>
	<table width="100%" cellpadding="2" class="actionshottable">
<?php
$month = $_GET['month'];

$sql = 'SELECT
	a.*,
	DATE_FORMAT(a.upload_time, "%d.%m.%Y") AS uploadtime,
	pd.products_name,
	p.products_model,
	p.products_image,
	customers_firstname,customers_lastname,customers_email_address
	FROM cm_actionshots a
	JOIN products p USING(products_id)
	JOIN products_description pd USING(products_id)
	JOIN customers c USING(customers_id)
	WHERE a.active=1 and deleted=0
	AND pd.language_id='.$_SESSION['languages_id'].'
	AND CONCAT_WS("/",MONTH(a.upload_time),YEAR(a.upload_time)) = "'.$month.'"
	';
$res = xtc_db_query($sql);

while($image = xtc_db_fetch_array($res))
{
	actionshotrow($image);
}
?>
</table>