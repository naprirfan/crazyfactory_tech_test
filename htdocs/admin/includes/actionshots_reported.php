<div class="pageHeading"><?php echo HEADING_TITLE_ACTIONSHOTS_REPORTED ?></div>
	<table width="100%" cellspacing="3" class="actionshottable">
<?php
$sql = 'SELECT
	a.*,
	DATE_FORMAT(a.upload_time, "%d.%m.%Y") AS uploadtime,
	p.products_model, p.products_picture,pd.products_name,
	customers_firstname,customers_lastname,customers_email_address
	FROM 
	INNER JOIN cm_reported_actionshots ra ON (ra.actionshot_id = a.actionshot_id AND ra.deleted=0)
	JOIN products p USING(products_id)
	LEFT JOIN products_description pd USING(products_id)
	JOIN customers c USING(customers_id)
	WHERE a.reported=1 and deleted=0
	AND pd.language_id='.$_SESSION['languages_id'].'';
$res = xtc_db_query($sql);
while($image = xtc_db_fetch_array($res))
{
	actionshotrow($image, true);
}
?>

</table>