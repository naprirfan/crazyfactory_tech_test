<?php 
/* This will return the mapping of excel file field and products table field
* KEY = EXCEL field, VALUE = TABLE FIELD
*/
return array(
	'products_price'                      => 'products_price',
	'products_discount_allowed'           => 'products_discount_allowed',
	'new_product'                         => 'products_new',
	'products_status'                     => 'products_status',
	'products_tax_class_id'               => 'products_tax_class_id',
	'products_stock_item'                 => 'products_stock_item',
	'image_forced_attributes'             => 'image_forced_attributes',
	'products_name'                       => 'products_model',
	'konstruktor'                         => 'konstruktor',
	'f_cid'                               => 'f_cid',
	'special_price'                       => 'special_price',
	'filter_style'                        => 'filter_style',
	'filter_for_which_material_thickness' => 'filter_for_which_material_thickness',
	'filter_material'                     => 'filter_material',
	'filter_thread_type'                  => 'filter_thread_type',
	'filter_material_thickness'           => 'filter_material_thickness',
	'filter_product_type'                 => 'filter_product_type',
	'measurement_info'                    => 'measurement_info',
	'product_info1'                       => 'product_info1',
	'product_info2'                       => 'product_info2',
);
