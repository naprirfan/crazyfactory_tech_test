<?php
/*
*	New excel to MySQL importer class
*	@author = Muhammad Irfan
*	
*/

class ExcelToMysqlImporter {

	private $counter = array('prod_new' => 0, 'cat_new' => 0, 'prod_upd' => 0, 'cat_upd' => 0);
	private $error_log;
	private $product_headings;
	private $product_records;
	private $product_dictionary;

	public function __construct($filename)
	{
		require(DIR_WS_CLASSES . 'excel_reader2.php');

		if(is_file(DIR_FS_CATALOG.'import/'.$filename))
          $excel = new Spreadsheet_Excel_Reader(DIR_FS_CATALOG.'import/'.$filename, false, 'UTF-8');
        else
          die('<b>ERROR:</b> File not found');

      	$this->product_headings = $excel->sheets[0]['cells'][1];
      	
      	//build products record
        for($i = 2; $i <= $excel->sheets[0]['numRows']; $i++)
        { 
            foreach($this->product_headings as $number => $heading)
                $this->product_records[$i-2][$heading] = $excel->sheets[0]['cells'][$i][$number];
        }

        //get products dictionary, a mapping between excel field and products table field
        $this->product_dictionary = require_once('product_dictionary.php');

	}

	public function import()
	{	
		//calculate elapsed time
		$starttime = microtime(true);

		$this->modifyProducts();

		$this->modifyCategories();

		$this->modifyProductDescription();

		$endtime = microtime(true);
		$timediff = $endtime - $starttime;

		return array($this->counter, $this->error_log, $timediff);
	}

	/*
	*	- There should be an array to set up the mapping between the columns in Excel and the columns in DB table "products"
	*	- If a product is already in the database (compare the column products_model) it should be updated
	*/
	private function modifyProducts()
	{
        //build query
        $query = "insert into products (products_price, products_discount_allowed, products_new, products_status, products_tax_class_id, products_stock_item, image_forced_attributes, products_model, konstruktor, f_cid, special_price, filter_style, filter_for_which_material_thickness, filter_material, filter_thread_type, filter_material_thickness, filter_product_type, measurement_info, product_info1, product_info2) VALUES ";
		foreach ($this->product_records as $key => $record) {
			$query .= '(';
			foreach ($this->product_dictionary as $excel_key => $value) {
				$query .= " '". addslashes($record[$excel_key]) . "',";
			}
			//remove trailing comma
			$query = rtrim($query, ",");
			//add closing parentheses
			$query .= '),';
		}
		$query = rtrim($query, ",");

		//build last clause
		$query .= ' ON DUPLICATE KEY UPDATE ';
		foreach ($this->product_dictionary as $excel_key => $value) {
			$query .= ' '. $value .' = VALUES('. $value .'),';
		}
		$query = rtrim($query, ",");
		$query .= ';';

		//execute query
		if (query($query)) {
			$this->counter['prod_new'] = count($this->product_records); 
		}
	}

	/*	
	* - There is an Excel column "categorie", 
	*   this value should be compared to a new column "name" (yet to be created) 
	*   in the table "categories" and an entry in products_to_categories should be made accordingly

	* To test this feature :
	* 1. truncate products_to_categories
	* 2. update any record in categories table, set name to : Nose Piercings
	* 3. After import done, we should have 55 new records in products_to_categories table
	*/
	private function modifyCategories()
	{	
		foreach ($this->product_records as $key => $record) {
			
			$categories = query("SELECT categories_id from categories where name = '". $record['categorie']. "';");
			$products = query("SELECT products_id from products where products_model = '". $record['products_name'] ."';");

			//assume there's only one record returned in select-category clause
			if ($categories && 
				query("insert into products_to_categories (products_id, categories_id) values (". $products[0]['products_id'] .", ". $categories[0]['categories_id'] .");")
			) {
				$this->counter['cat_new']++;
			}
		}
	}

	/*
	*	- The values from products_description_de, ..._en, etc. 
	*	should be imported into the table products_description
	*/
	private function modifyProductDescription()
	{
		$result = array();
		foreach ($this->product_records as $idx => $record) {
			foreach ($record as $key => $value) {
				if (strpos($key, "products_description_") !== false OR strpos($key, "products_short_description_") !== false) {
					$result[substr($key, -2)][$key] = $value;
				}
			}
		}

		//build query
		$query = 'Insert into products_description (products_description, products_short_description) VALUES';
		foreach ($result as $key => $value) {
			$query .= ' ("'. addslashes($result[$key]['products_description_'.$key]). '", "'. addslashes($result[$key]['products_description_'.$key]).'"),';
		}
		$query = rtrim($query, ",");

		query($query);
	}


}