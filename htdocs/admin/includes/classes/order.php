<?php
/* --------------------------------------------------------------
   $Id: order.php 1037 2005-07-17 15:25:32Z gwinger $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(order.php,v 1.6 2003/02/06); www.oscommerce.com
   (c) 2003	 nextcommerce (order.php,v 1.12 2003/08/18); www.nextcommerce.org

   Released under the GNU General Public License
   --------------------------------------------------------------
   Third Party contribution:

   Customers Status v3.x  (c) 2002-2003 Copyright Elari elari@free.fr | www.unlockgsm.com/dload-osc/ | CVS : http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/elari/?sortby=date#dirlist

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   --------------------------------------------------------------*/
defined( '_VALID_XTC' ) or die( 'Direct Access to this location is not allowed.' );
class order
{
    var $info, $totals, $products, $customer, $delivery;

    function order($order_id) {
      $this->info = array();
      $this->totals = array();
      $this->products = array();
      $this->customer = array();
      $this->delivery = array();

      $this->query($order_id);
    }

    function query($order_id) {
		$order_query = xtc_db_query("
			SELECT
				orders.*,
				languages.languages_id,
				r.orders_id AS rma_orders_id
			FROM orders
			INNER JOIN languages ON (languages.directory = orders.language)
			LEFT JOIN cm_rma r USING (rma_id)
			WHERE orders.orders_id = '" . xtc_db_input($order_id) . "'
		");

		$order = xtc_db_fetch_array($order_query);

		$totals_query = xtc_db_query("select title, text from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . xtc_db_input($order_id) . "' order by sort_order");
		while ($totals = xtc_db_fetch_array($totals_query)) {
		$this->totals[] = array('title' => $totals['title'],
								'text' => $totals['text']);
		}

		$sql = '
			SELECT o.orders_id
			FROM orders o
			JOIN cm_rma r USING (rma_id)
			WHERE r.orders_id = '.xtc_db_input($order_id).'
		';
		$res = xtc_db_query($sql);

		while ($rmas = xtc_db_fetch_array($res)) {
			$this->rma[] = $rmas['orders_id'];
		}

		$this->info = array(
			'currency' => $order['currency'],
			'currency_value' => $order['currency_value'],
			'payment_method' => $order['payment_method'],
			'payment_class' => $order['payment_class'],
			'shipping_class' => $order['shipping_class'],
			'status' => $order['customers_status'],
			'status_name' => $order['customers_status_name'],
			'status_image' => $order['customers_status_image'],
			'status_discount' => $order['customers_status_discount'],
			'cc_type' => $order['cc_type'],
			'cc_owner' => $order['cc_owner'],
			'cc_number' => $order['cc_number'],
			'cc_expires' => $order['cc_expires'],
			'cc_cvv' => $order['cc_cvv'],
			'comments' => $order['comments'],
			'language' => $order['language'],
			'languages_id' => $order['languages_id'],
			'date_purchased' => $order['date_purchased'],
			'orders_status' => $order['orders_status'],
			'ibn_billnr'    => $order['ibn_billnr'],      // pdfrechnung
			'ibn_billdate'  => $order['ibn_billdate'],    // pdfrechnung
			'ibn_pdfnotifydate'  => $order['ibn_pdfnotifydate'],
			'extra_products' => $order['extra_products'],
			'rma_orders_id' => $order['rma_orders_id'],
			'stock_freed' => $order['stock_freed'],
			'exported_to_erp' => $order['exported_to_erp'],
			'force_export_to_erp' => $order['force_export_to_erp'],
			'prevent_export_to_erp' => $order['prevent_export_to_erp'],
			'last_modified' => $order['last_modified']
		);

		$this->customer = array(
			'name' => $order['customers_name'],
			'company' => $order['customers_company'],
			'csID' => $order['customers_id'],
			'vat_id' => $order['customers_vat_id'],
			'shop_id' => $order['shop_id'],
			'ID' => $order['customers_id'],
			'cIP' => $order['customers_ip'],
			'street_address' => $order['customers_street_address'],
			'suburb' => $order['customers_suburb'],
			'city' => $order['customers_city'],
			'postcode' => $order['customers_postcode'],
			'state' => $order['customers_state'],
			'country' => $order['customers_country'],
			'format_id' => $order['customers_address_format_id'],
			'telephone' => $order['customers_telephone'],
			'email_address' => $order['customers_email_address']
		);

		$this->delivery = array(
			'name' => $order['delivery_name'],
			'company' => $order['delivery_company'],
			'street_address' => $order['delivery_street_address'],
			'suburb' => $order['delivery_suburb'],
			'city' => $order['delivery_city'],
			'postcode' => $order['delivery_postcode'],
			'state' => $order['delivery_state'],
			'country' => $order['delivery_country'],
			'format_id' => $order['delivery_address_format_id']
		);

		$this->billing = array(
			'name' => $order['billing_name'],
			'company' => $order['billing_company'],
			'street_address' => $order['billing_street_address'],
			'suburb' => $order['billing_suburb'],
			'city' => $order['billing_city'],
			'postcode' => $order['billing_postcode'],
			'state' => $order['billing_state'],
			'country' => $order['billing_country'],
			'billing_country_iso_code_2' => $order['billing_country_iso_code_2'],
			'format_id' => $order['billing_address_format_id']
		);

      $index = 0;
      $orders_products_query = xtc_db_query("select
                                                 orders_products_id,products_id, products_name, products_model,products_model_erp, products_price, products_tax, products_quantity, final_price,allow_tax, products_discount_made, attributes
                                             from
                                                 " . TABLE_ORDERS_PRODUCTS . "
                                             where
                                                 orders_id ='" . xtc_db_input($order_id) . "' order by products_model, products_model_erp");

      while ($orders_products = xtc_db_fetch_array($orders_products_query)) {
        $this->products[$index] = array('qty' => $orders_products['products_quantity'],
                                        'name' => $orders_products['products_name'],
                                        'id' => $orders_products['products_id'],
                                        'opid' => $orders_products['orders_products_id'],
                                        'model' => $orders_products['products_model'],
                                        'model_erp' => $orders_products['products_model_erp'],
                                        'tax' => $orders_products['products_tax'],
                                        'price' => $orders_products['products_price'],
                                        'discount' => $orders_products['products_discount_made'],
                                        'final_price' => $orders_products['final_price'],
										'allow_tax' => $orders_products['allow_tax'],
										'attributes' => unserialize($orders_products['attributes'])
			);


        $index++;
      }
    }
  }
?>