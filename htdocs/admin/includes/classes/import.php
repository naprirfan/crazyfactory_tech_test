<?php
//include('dump.php'); //debug
set_time_limit(0);
/* --------------------------------------------------------------
   $Id: import.php 1319 2005-10-23 10:35:15Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce

   Released under the GNU General Public License
   --------------------------------------------------------------
*/
defined('_VALID_XTC') or die('Direct Access to this location is not allowed.');
error_reporting(E_ALL & ~E_NOTICE);

require_once(dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'product_attributes.php');
require_once(dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'products.inc.php');

class xtcImport
{

    function xtcImport($filename)
    {
        $this->seperator = CSV_SEPERATOR;
        $this->TextSign = CSV_TEXTSIGN;
        if (CSV_SEPERATOR == '')
            $this->seperator = "\t";
        if (CSV_SEPERATOR == '\t')
            $this->seperator = "\t";
        $this->filename = $filename;
        $this->ImportDir = DIR_FS_CATALOG.'import/';
        $this->catDepth = 6;
        $this->languages = $this->get_lang();
        $this->counter = array ('prod_new' => 0, 'cat_new' => 0, 'prod_upd' => 0, 'cat_upd' => 0);
        $this->mfn = $this->get_mfn();
        $this->errorlog = array ();
        $this->time_start = time();
        $this->debug = false;
        $this->CatTree = array ('ID' => 0);
        // precaching categories in array ?
        $this->CatCache = true;
        $this->FileSheme = array ();
        $this->Groups = xtc_get_customers_statuses();

    }

    /**
     *   generating file layout
     *   @param array $mapping standard fields
     *   @return array
     */
    function generate_map()
    {

        // lets define a standard fieldmapping array, with importable fields
        $file_layout = array ('p_model' => '', // products_model
                'p_stock' => '', // products_quantity
                'p_tpl' => '', // products_template
                'p_sorting' => '', // products_sorting
                'p_manufacturer' => '', // manufacturer
                'p_fsk18' => '', // FSK18
                'p_priceNoTax' => '', // Nettoprice
                'p_tax' => '', // taxrate in percent
                'p_status' => '', // products status
                'p_weight' => '', // products weight
                'p_disc' => '', // products discount
                'p_opttpl' => '', // options template
                'p_image' => '', // product image
                'p_colorchart' => '', // product image
                'p_info1' => '', // product image
								'p_info2' => '', // product image
                'p_vpe' => '', // products VPE
                'p_vpe_status' => '', // products VPE Status
                'p_vpe_value' => '', // products VPE value
                'p_shipping' => '' // product shipping_time
        );

        // Group Prices
        for ($i = 0; $i < count($this->Groups) - 1; $i ++)
        {
            $file_layout = array_merge($file_layout, array ('p_priceNoTax.'.$this->Groups[$i +1]['id'] => ''));
        }

        // Group Permissions
        for ($i = 0; $i < count($this->Groups) - 1; $i ++)
        {
            $file_layout = array_merge($file_layout, array ('p_groupAcc.'.$this->Groups[$i +1]['id'] => ''));
        }

        // product images
        for ($i = 1; $i < MO_PICS + 1; $i ++)
        {
            $file_layout = array_merge($file_layout, array ('p_image.'.$i => ''));
        }

        // add lang fields
        for ($i = 0; $i < sizeof($this->languages); $i ++)
        {
            $file_layout = array_merge($file_layout, array ('p_name.'.$this->languages[$i]['code'] => '',
            																								'p_desc.'.$this->languages[$i]['code'] => '',
            																								'p_shortdesc.'.$this->languages[$i]['code'] => '',
            																								'p_meta_title.'.$this->languages[$i]['code'] => '',
            																								'p_meta_desc.'.$this->languages[$i]['code'] => '',
            																								'p_meta_key.'.$this->languages[$i]['code'] => '',
            																								'p_keywords.'.$this->languages[$i]['code'] => '',
            																								'p_url.'.$this->languages[$i]['code'] => ''));
        }
        // add categorie fields
        for ($i = 0; $i < $this->catDepth; $i ++)
            $file_layout = array_merge($file_layout, array ('p_cat.'.$i => ''));

        $file_layout = array_merge($file_layout, array( //'formel' => '', ich habe keine Ahnung, warum dieser Kack hier steht... dieses Script muss dringend weg!!!
						        																	 'products_catalog_page' => '',
        																							 'formel_array' => '', // ebenso
        																							 'konstruktor' => '',
        																							 'f_material' => '',
        																							 'f_cid' => '',
        																							 'f_art' => ''));

        return $file_layout;

    }

    /**
     *   generating mapping layout for importfile
     *   @param array $mapping standard fields
     *   @return array
     */
    function map_file($mapping)
    {
        if (!file_exists($this->ImportDir.$this->filename))
        {
            // error
            return 'error';
        } else
        {
            // file is ok, creating mapping
            $inhalt = array ();
            $inhalt = file($this->ImportDir.$this->filename);
            // get first line into array
            $content = explode($this->seperator, rtrim($inhalt[0]));

            foreach ($mapping as $key => $value)
            {
                // try to find our field in fieldlayout
                foreach ($content as $key_c => $value_c)
                    if ($key == trim($this->RemoveTextNotes($content[$key_c])))
                    {
                        $mapping[$key] = trim($this->RemoveTextNotes($key_c));
                        $this->FileSheme[$key] = 'Y';
                    }

            }
            return $mapping;
        }
    }

    /**
     *   Get installed languages
     *   @return array
     */
    function get_lang()
    {

        $languages_query = xtc_db_query("select languages_id, name, code, image, directory from ".TABLE_LANGUAGES." order by sort_order");
        while ($languages = xtc_db_fetch_array($languages_query))
        {
            $languages_array[] = array ('id' => $languages['languages_id'], 'name' => $languages['name'], 'code' => $languages['code']);
        }

        return $languages_array;
    }

    function import($mapping)
    {
        // open file
        $inhalt = file($this->ImportDir.$this->filename);
        $lines = count($inhalt);

        // walk through file data, and ignore first line
        for ($i = 1; $i < $lines; $i ++)
        {
            $line_content = '';

            // get line content
            $line_fetch = $this->get_line_content($i, $inhalt, $lines);
            $line_content = explode($this->seperator, $line_fetch['data']);
            $i += $line_fetch['skip'];

            // ok, now crossmap data into array
            $line_data = $this->generate_map();
            foreach ($mapping as $key => $value)
                $line_data[$key] = $this->RemoveTextNotes($line_content[$value]);
            if ($this->debug)
            {
                echo '<pre>';
                print_r($line_data);
                echo '</pre>';

            }

            if ($line_data['p_model'] != '')
            {
                if ($line_data['p_cat.0'] != '' || $this->FileSheme['p_cat.0'] != 'Y')
                {
                    if ($this->FileSheme['p_cat.0'] != 'Y')
                    {
                        if ($this->checkModel($line_data['p_model']))
                        {
                            $this->insertProduct($line_data, 'update');
                        } else
                        {
                            $this->insertProduct($line_data,'insert');
                        }
                    } else
                    {
                        if ($this->checkModel($line_data['p_model']))
                        {
                            $this->insertProduct($line_data, 'update',true);
                        } else
                        {
                            $this->insertProduct($line_data,'insert',true);
                        }
                    }
                } else
                {
                    $this->errorLog[] = '<b>ERROR:</b> no Category, line: '.$i.' dataset: '.$line_data['p_cat.0'];
                }
            } else
            {
                $this->errorLog[] = '<b>ERROR:</b> no Modelnumber, line: '.$i.' dataset: '.$line_data['p_model'];
            }
        }
        return array ($this->counter, $this->errorLog, $this->calcElapsedTime($this->time_start));
    }

    /**
     *   Check if a product exists in database, query for model number
     *   @param string $model products modelnumber
     *   @return boolean
     */
    function checkModel($model)
    {
        $model_query = xtc_db_query("SELECT products_id FROM ".TABLE_PRODUCTS." WHERE products_model='".addslashes($model)."'");
        if (!xtc_db_num_rows($model_query))
            return false;
        return true;
    }

    /**
     *   Check if a image exists
     *   @param string $model products modelnumber
     *   @return boolean
     */
    function checkImage($imgID,$pID)
    {
        $img_query = xtc_db_query("SELECT image_id FROM ".TABLE_PRODUCTS_IMAGES." WHERE products_id='".$pID."' and image_nr='".$imgID."'");
        if (!xtc_db_num_rows($img_query))
            return false;
        return true;
    }

    /**
     *   removing textnotes from a dataset
     *   @param String $data data
     *   @return String cleaned data
     */
    function RemoveTextNotes($data)
    {
        $data = trim($data);
        if (substr($data, -1) == $this->TextSign)
            $data = substr($data, 1, strlen($data) - 2);

        return $data;

    }

    /**
     *   Get/create manufacturers ID for a given Name
     *   @param String $manufacturer Manufacturers name
     *   @return int manufacturers ID
     */
    function getMAN($manufacturer)
    {
        if ($manufacturer == '')
            return;
        if (isset ($this->mfn[$manufacturer]['id']))
            return $this->mfn[$manufacturer]['id'];
        $man_query = xtc_db_query("SELECT manufacturers_id FROM ".TABLE_MANUFACTURERS." WHERE manufacturers_name = '".$manufacturer."'");
        if (!xtc_db_num_rows($man_query))
        {
            $manufacturers_array = array ('manufacturers_name' => $manufacturer);
            xtc_db_perform(TABLE_MANUFACTURERS, $manufacturers_array);
            $this->mfn[$manufacturer]['id'] = mysql_insert_id();
        } else
        {
            $man_data = xtc_db_fetch_array($man_query);
            $this->mfn[$manufacturer]['id'] = $man_data['manufacturers_id'];

        }
        return $this->mfn[$manufacturer]['id'];
    }
    /**
     *   Insert a new product into Database
     *   @param array $dataArray Linedata
     *   @param string $mode insert or update flag
     */
    function insertProduct($dataArray, $mode = 'insert',$touchCat = false)
    {
        $products_array = array ('products_model' => $dataArray['p_model']);
        if ($this->FileSheme['p_stock'] == 'Y')
            $products_array['products_quantity'] = $dataArray['p_stock'];
        if ($this->FileSheme['p_priceNoTax'] == 'Y')
            $products_array['products_price'] = str_replace(',','.',$dataArray['p_priceNoTax']);
        if ($this->FileSheme['p_weight'] == 'Y')
            $products_array['products_weight'] = $dataArray['p_weight'];
        if ($this->FileSheme['p_status'] == 'Y')
            $products_array['manually_deactivated'] = $dataArray['p_status'] ? 0 : 1; // Bedeutung invertiert!
        if ($this->FileSheme['p_image'] == 'Y')
            $products_array['products_image'] = $dataArray['p_image'];
        if ($this->FileSheme['p_colorchart'] == 'Y')
            $products_array['color_chart'] = $dataArray['p_colorchart'];
        if ($this->FileSheme['p_info1'] == 'Y')
            $products_array['product_info1'] = $dataArray['p_info1'];
        if ($this->FileSheme['p_info2'] == 'Y')
            $products_array['product_info2'] = $dataArray['p_info2'];
        if ($this->FileSheme['p_disc'] == 'Y')
            $products_array['products_discount_allowed'] = $dataArray['p_disc'];
        if ($this->FileSheme['p_tax'] == 'Y')
            $products_array['products_tax_class_id'] = 1 ; // immer Steuerfähig. $dataArray['p_tax'];
        if ($this->FileSheme['p_opttpl'] == 'Y')
            $products_array['options_template'] = $dataArray['p_opttpl'];
        if ($this->FileSheme['p_manufacturer'] == 'Y')
            $products_array['manufacturers_id'] = $this->getMAN(trim($dataArray['p_manufacturer']));
        if ($this->FileSheme['p_fsk18'] == 'Y')
            $products_array['products_fsk18'] = $dataArray['p_fsk18'];
        if ($this->FileSheme['p_tpl'] == 'Y')
            $products_array['product_template'] = $dataArray['p_tpl'];
        if ($this->FileSheme['p_vpe'] == 'Y')
            $products_array['products_vpe'] = $dataArray['p_vpe'];
        if ($this->FileSheme['p_vpe_status'] == 'Y')
            $products_array['products_vpe_status'] = $dataArray['p_vpe_status'];
        if ($this->FileSheme['p_vpe_value'] == 'Y')
            $products_array['products_vpe_value'] = $dataArray['p_vpe_value'];
        if ($this->FileSheme['p_shipping'] == 'Y')
            $products_array['products_shippingtime'] = $dataArray['p_shipping'];
        if ($this->FileSheme['p_sorting'] == 'Y')
            $products_array['products_sort'] = $dataArray['p_sorting'];
//        if ($this->FileSheme['formel'] == 'Y')
//            $products_array['formel'] = $dataArray['formel'];
        if ($this->FileSheme['formula_main'] == 'Y')
            $products_array['formula_main'] = $dataArray['formula_main'];
        if ($this->FileSheme['formula_needs_processing'] == 'Y')
            $products_array['formula_needs_processing'] = $dataArray['formula_needs_processing'];
        if ($this->FileSheme['formel_array'] == 'Y')
            $products_array['formel_array'] = $dataArray['formel_array'];
        if ($this->FileSheme['konstruktor'] == 'Y')
            $products_array['konstruktor'] = $dataArray['konstruktor'];
        if ($this->FileSheme['f_cid'] == 'Y')
            $products_array['f_cid'] = $dataArray['f_cid'];
        if ($this->FileSheme['products_catalog_page'] == 'Y')
            $products_array['products_catalog_page'] = $dataArray['products_catalog_page'];
        if ($this->FileSheme['has_price_changes'] == 'Y')
            $products_array['has_price_changes'] = $dataArray['has_price_changes'];
        if ($this->FileSheme['filter_style'] == 'Y')
            $products_array['filter_style'] = $dataArray['filter_style'];
        if ($this->FileSheme['filter_for_which_material_thickness'] == 'Y')
            $products_array['filter_for_which_material_thickness'] = $dataArray['filter_for_which_material_thickness'];
        if ($this->FileSheme['filter_material'] == 'Y')
            $products_array['filter_material'] = $dataArray['filter_material'];
        if ($this->FileSheme['filter_thread_type'] == 'Y')
            $products_array['filter_thread_type'] = $dataArray['filter_thread_type'];
        if ($this->FileSheme['filter_material_thickness'] == 'Y')
            $products_array['filter_material_thickness'] = $dataArray['filter_material_thickness'];
        if ($this->FileSheme['filter_product_type'] == 'Y')
            $products_array['filter_product_type'] = $dataArray['filter_product_type'];
        if ($this->FileSheme['measurement_info'] == 'Y')
            $products_array['measurement_info'] = $dataArray['measurement_info'];
        if ($this->FileSheme['products_new'] == 'Y')
            $products_array['products_new'] = $dataArray['products_new'];
        if ($this->FileSheme['special_price'] == 'Y')
            $products_array['special_price'] = $dataArray['special_price'];
        if ($this->FileSheme['image_forced_attributes'] == 'Y')
            $products_array['image_forced_attributes'] = $dataArray['image_forced_attributes'];
        if ($this->FileSheme['products_discountable'] == 'Y')
            $products_array['products_discountable'] = $dataArray['products_discountable'];
        if ($this->FileSheme['products_stock_item'] == 'Y')
            $products_array['products_stock_item'] = $dataArray['products_stock_item'];


        if ($mode == 'insert')
        {
            $this->counter['prod_new']++;
            xtc_db_perform(TABLE_PRODUCTS, $products_array);
            $products_id = mysql_insert_id();
        } else
        {
            $this->counter['prod_upd']++;

            xtc_db_perform(TABLE_PRODUCTS, $products_array, 'update', 'products_model = \''.addslashes($dataArray['p_model']).'\'');
            $prod_query = xtc_db_query("SELECT products_id FROM ".TABLE_PRODUCTS." WHERE products_model='".addslashes($dataArray['p_model'])."'");
            $prod_data = xtc_db_fetch_array($prod_query);
            $products_id = $prod_data['products_id'];
        }

		// Stock setzen:
		if ($products_array['products_stock_item'])
		{
			$sql = 'INSERT INTO products_stock (`products_id`,`stock`) VALUES ('.(int)$products_id.','.(int)$dataArray['products_stock_add_quantity'].')
					ON DUPLICATE KEY UPDATE `stock` = `stock`+'.(int)$dataArray['products_stock_add_quantity'];
			xtc_db_query($sql);
		}

        // Insert Group Permissions.
        for ($i = 0; $i < count($this->Groups) - 1; $i ++)
        {
            // seperate string ::
            if (isset ($dataArray['p_groupAcc.'.$this->Groups[$i +1]['id']]))
            {
                $insert_array = array ('group_permission_'.$this->Groups[$i +1]['id'] => $dataArray['p_groupAcc.'.$this->Groups[$i +1]['id']]);
                xtc_db_perform(TABLE_PRODUCTS, $insert_array, 'update', 'products_id = \''.$products_id.'\'');
            }
        }

        // insert images
        $i = 0;
        while (++$i && isset($dataArray['p_image.'.$i]) && $dataArray['p_image.'.$i]!="")
        {
            // check if entry exists
            if ($this->checkImage($i,$products_id))
            {
                $insert_array = array ('image_name' => $dataArray['p_image.'.$i]);
                xtc_db_perform(TABLE_PRODUCTS_IMAGES, $insert_array, 'update', 'products_id = \''.$products_id.'\' and image_nr=\''.$i.'\'');
            } else
            {
                $insert_array = array ('image_name' => $dataArray['p_image.'.$i],'image_nr'=>$i,'products_id'=>$products_id);
                xtc_db_perform(TABLE_PRODUCTS_IMAGES, $insert_array);
            }
        }

        // if ($touchCat) $this->insertCategory($dataArray, $mode, $products_id);

        if ($touchCat) // Eigentlich überflüssig... wir wollen schließlich immer die Pussy anfassen...
        {
        	xtc_db_query('DELETE FROM '.TABLE_PRODUCTS_TO_CATEGORIES.' WHERE products_id="'.addslashes($products_id).'"');
        	xtc_db_query('INSERT INTO '.TABLE_PRODUCTS_TO_CATEGORIES.' SET products_id="'.addslashes($products_id).'", categories_id="'.addslashes($dataArray['CAT_ID']).'"');
        }

        for ($i_insert = 0; $i_insert < sizeof($this->languages); $i_insert ++)
        {
            $prod_desc_array = array ('products_id' => $products_id, 'language_id' => $this->languages[$i_insert]['id']);
						// cfimport sieht ein wenig anders aus für den produktnamen ist hier die description verwendet worden
       //     if ($this->FileSheme['p_name.'.$this->languages[$i_insert]['code']] == 'Y')
                $prod_desc_array = array_merge($prod_desc_array, array ('products_name' => (isset($dataArray['p_name.'.$this->languages[$i_insert]['code']])?$dataArray['p_name.'.$this->languages[$i_insert]['code']] : $dataArray['p_name.en'] )));
       //     if ($this->FileSheme['p_shortdesc.'.$this->languages[$i_insert]['code']] == 'Y')
                $prod_desc_array = array_merge($prod_desc_array, array ('products_description' => (isset($dataArray['p_desc.'.$this->languages[$i_insert]['code']])?$dataArray['p_desc.'.$this->languages[$i_insert]['code']] : $dataArray['p_desc.en'] )));
// nicht gempappt                $prod_desc_array = array_merge($prod_desc_array, array ('products_short_description' => addslashes( (isset($dataArray['p_shortdesc.'.$this->languages[$i_insert]['code']])?$dataArray['p_shortdesc.'.$this->languages[$i_insert]['code']] : $dataArray['p_shortdesc.en'] ))));
//            if ($this->FileSheme['p_meta_title.'.$this->languages[$i_insert]['code']] == 'Y')
                $prod_desc_array = array_merge($prod_desc_array, array ('products_meta_title' => (isset($dataArray['p_meta_title.'.$this->languages[$i_insert]['code']])?$dataArray['p_meta_title.'.$this->languages[$i_insert]['code']] : $dataArray['p_meta_title.en'] )));
//            if ($this->FileSheme['p_meta_desc.'.$this->languages[$i_insert]['code']] == 'Y')
                $prod_desc_array = array_merge($prod_desc_array, array ('products_meta_description' => (isset($dataArray['p_meta_desc.'.$this->languages[$i_insert]['code']])?$dataArray['p_meta_desc.'.$this->languages[$i_insert]['code']] : $dataArray['p_meta_desc.en'] )));
//            if ($this->FileSheme['p_meta_key.'.$this->languages[$i_insert]['code']] == 'Y')
                $prod_desc_array = array_merge($prod_desc_array, array ('products_meta_keywords' => (isset($dataArray['p_meta_key.'.$this->languages[$i_insert]['code']])?$dataArray['p_meta_key.'.$this->languages[$i_insert]['code']] : $dataArray['p_meta_key.en'] )));
//            if ($this->FileSheme['p_keywords.'.$this->languages[$i_insert]['code']] == 'Y')
                $prod_desc_array = array_merge($prod_desc_array, array ('products_keywords' => (isset($dataArray['p_keywords.'.$this->languages[$i_insert]['code']])?$dataArray['p_keywords.'.$this->languages[$i_insert]['code']] : $dataArray['p_keywords.en'] )));
//            if ($this->FileSheme['p_url.'.$this->languages[$i_insert]['code']] == 'Y')
                $prod_desc_array = array_merge($prod_desc_array, array ('products_url' => (isset($dataArray['p_url.'.$this->languages[$i_insert]['code']])?$dataArray['p_url.'.$this->languages[$i_insert]['code']] : $dataArray['p_url.en'] )));

            if ($mode == 'insert')
            {
                xtc_db_perform(TABLE_PRODUCTS_DESCRIPTION, $prod_desc_array);
            } else
            {
                xtc_db_perform(TABLE_PRODUCTS_DESCRIPTION, $prod_desc_array, 'update', 'products_id = \''.$products_id.'\' and language_id=\''.$this->languages[$i_insert]['id'].'\'');
            }
        }
    }

    /**
     *   Match and insert Categories
     *   @param array $dataArray data array
     *   @param string $mode insert mode
     *   @param int $pID  products ID
     */
    function insertCategory(& $dataArray, $mode = 'insert', $pID)
    {
        if ($this->debug)
        {
            echo '<pre>';
            print_r($this->CatTree);
            echo '</pre>';
        }
        $cat = array ();
        $catTree = '';
        for ($i = 0; $i < $this->catDepth; $i ++)
            if (trim($dataArray['p_cat.'.$i]) != '')
            {
                $cat[$i] = trim($dataArray['p_cat.'.$i]);
                $catTree .= '[\''.addslashes($cat[$i]).'\']';
            }
        $code = '$ID=$this->CatTree'.$catTree.'[\'ID\'];';
        if ($this->debug)
            echo $code;
        eval ($code);

        if ((is_int($dataArray['p_cat_id.0']) || $dataArray['p_cat_id.0'] == '0') && $pID != 0)
        {

            $this->insertPtoCconnection($pID, $ID);
        }
        else
        {

            $catTree = '';
            $parTree = '';
            $curr_ID = 0;
            for ($i = 0; $i < count($cat); $i ++)
            {

                $catTree .= '[\''.addslashes($cat[$i]).'\']';

                $code = '$ID=$this->CatTree'.$catTree.'[\'ID\'];';
                eval ($code);
                if (is_int($dataArray['p_cat_id.'.$i]) || $dataArray['p_cat_id.'.$i] == '0')
                {
                    $cat_id = $dataArray['p_cat_id.'.$i];
                } else
                {

                    $parent = $dataArray['p_cat_parent_id.'.$i];
                    // check if categorie exists
                    $cat_query = xtc_db_query("SELECT c.categories_id FROM ".TABLE_CATEGORIES." c
										WHERE  c.categories_id = '".$dataArray['p_cat_id.'.$i]."'");

                    if (!xtc_db_num_rows($cat_query))
                    { // insert category

                        $categorie_data = array ('categories_id' => $dataArray['p_cat_id.'.$i],
                        												'categories_image' => $dataArray['p_cat_image.'.$i],
                        												'parent_id' => $parent,
                        												'categories_template' => 'category_listing.html',
                        												'categories_status' => $dataArray['p_cat_status.'.$i],
                        												'listing_template' => 'product_listing_v1.html',
                        												'sort_order' => $dataArray['p_cat_sort_order.'.$i],
                        												'date_added' => 'now()',
                        												'last_modified' => 'now()');

                        xtc_db_perform(TABLE_CATEGORIES, $categorie_data);
                        $cat_id = $dataArray['p_cat_id.'.$i];
                        $this->counter['cat_new']++;
                        $code = '$this->CatTree'.$parTree.'[\''.addslashes($cat[$i]).'\'][\'ID\']='.$cat_id.';';
                        eval ($code);
                        $parent = $cat_id;
                        for ($i_insert = 0; $i_insert < sizeof($this->languages); $i_insert ++)
                        {
                            $categorie_data = array ('language_id' => $this->languages[$i_insert]['id'], 'categories_id' => $cat_id, 'categories_name' => $cat[$i]);
                            xtc_db_perform(TABLE_CATEGORIES_DESCRIPTION, $categorie_data);

                        }
                    } elseif ($pID == 0)
                    {
                    	 // updaten
                    }
                    else
                    {
                        $this->counter['cat_touched']++;
                        $cData = xtc_db_fetch_array($cat_query);
                        $cat_id = $cData['categories_id'];
                        $code = '$this->CatTree'.$parTree.'[\''.addslashes($cat[$i]).'\'][\'ID\']='.$cat_id.';';
                        eval ($code);
                    }

                }
                $parTree = $catTree;
            }
            if ($pID != 0)
	            $this->insertPtoCconnection($pID, $cat_id);


        }

    }

    /**
     *   Insert products to categories connection
     *   @param int $pID products ID
     *   @param int $cID categories ID
     */
    function insertPtoCconnection($pID, $cID)
    {
        $prod2cat_query = xtc_db_query("SELECT *
								FROM ".TABLE_PRODUCTS_TO_CATEGORIES."
								WHERE
								categories_id='".$cID."'
								and products_id='".$pID."'");

        if (!xtc_db_num_rows($prod2cat_query))
        {
            $insert_data = array ('products_id' => $pID, 'categories_id' => $cID);

            xtc_db_perform(TABLE_PRODUCTS_TO_CATEGORIES, $insert_data);
        }
    }

    /**
     *   Parse Inputfile until next line
     *   @param int $line taxrate in percent
     *   @param string $file_content taxrate in percent
     *   @param int $max_lines taxrate in percent
     *   @return array
     */
    function get_line_content($line, $file_content, $max_lines)
    {
        // get first line
        $line_data = array ();
        $line_data['data'] = $file_content[$line];
        $lc = 1;
        // check if next line got ; in first 50 chars
        while (!strstr(substr($file_content[$line + $lc], 0, 6), 'XTSOL') && $line + $lc <= $max_lines)
        {
            $line_data['data'] .= $file_content[$line + $lc];
            $lc ++;
        }
        $line_data['skip'] = $lc -1;
        return $line_data;
    }

    /**
     *   Calculate Elapsed time from 2 given Timestamps
     *   @param int $time old timestamp
     *   @return String elapsed time
     */
    function calcElapsedTime($time)
    {

        // calculate elapsed time (in seconds!)
        $diff = time() - $time;
        $daysDiff = 0;
        $hrsDiff = 0;
        $minsDiff = 0;
        $secsDiff = 0;

        $sec_in_a_day = 60 * 60 * 24;
        while ($diff >= $sec_in_a_day)
        {
            $daysDiff ++;
            $diff -= $sec_in_a_day;
        }
        $sec_in_an_hour = 60 * 60;
        while ($diff >= $sec_in_an_hour)
        {
            $hrsDiff ++;
            $diff -= $sec_in_an_hour;
        }
        $sec_in_a_min = 60;
        while ($diff >= $sec_in_a_min)
        {
            $minsDiff ++;
            $diff -= $sec_in_a_min;
        }
        $secsDiff = $diff;

        return ('(elapsed time '.$hrsDiff.'h '.$minsDiff.'m '.$secsDiff.'s)');

    }

    /**
     *   Get manufacturers
     *   @return array
     */
    function get_mfn()
    {
        $mfn_query = xtc_db_query("select manufacturers_id, manufacturers_name from ".TABLE_MANUFACTURERS);
        while ($mfn = xtc_db_fetch_array($mfn_query))
        {
            $mfn_array[$mfn['manufacturers_name']] = array ('id' => $mfn['manufacturers_id']);
        }
        return $mfn_array;
    }

}

//IMPORT from Excel Spreadsheet
class xtcImportFromXls extends xtcImport
{
    function xtcImportFromXls($filename)
    {
        require(DIR_WS_CLASSES . 'excel_reader2.php');

        $this->filename = $filename;
        $this->ImportDir = DIR_FS_CATALOG.'import/';
        $this->catDepth = 6;
        $this->languages = $this->get_lang();
        $this->counter = array ('prod_new' => 0, 'cat_new' => 0, 'prod_upd' => 0, 'cat_upd' => 0);
        $this->mfn = $this->get_mfn();
        $this->errorlog = array ();
        $this->time_start = time();
        $this->debug = false;
        $this->CatTree = array ('ID' => 0);
        // precaching categories in array ?
        $this->CatCache = true;
        $this->FileSheme = array ();
        $this->Groups = xtc_get_customers_statuses();

        if(is_file($this->ImportDir.$this->filename))
          $this->excel = new Spreadsheet_Excel_Reader($this->ImportDir.$this->filename, false, 'UTF-8');
        else
          die('<b>ERROR:</b> File not found');
    }

    function array_from_elements($moeglichkeiten)
    { //Baut eine Matrix aus dem zweidimensionalen Array $moeglichkeiten
        $order = array();
        $order = array_flip(array_shift($moeglichkeiten));

        foreach($order as &$order_point)
        {
            $order_point = array();
            if(count($moeglichkeiten))
                $order_point = $this->array_from_elements($moeglichkeiten);

            else
                $order_point = array(0 => true);
        }
        return $order;
    }
    function array_extend($a, $b)
    { //"Extend" recursively array $a with array $b values (no deletion in $a, just added and updated values) (Amund, php.net/array_merge)
        foreach($b as $k=>$v)
        {
            if( is_array($v) )
            {
                if( !isset($a[$k]) )
                {
                    $a[$k] = $v;
                } else
                {
                    $a[$k] = $this->array_extend($a[$k], $v);
                }
            } else
            {
                $a[$k] = $v;
            }
        }
        return $a;
    }
    function array_diff_assoc_recursive($array1, $array2)
    {
        foreach($array1 as $key => $value)
        {
            if(is_array($value))
            {
                if(!isset($array2[$key]))
                {
                    $difference[$key] = $value;
                }
                elseif(!is_array($array2[$key]))
                {
                    $difference[$key] = $value;
                }
                else
                {
                    $new_diff = $this->array_diff_assoc_recursive($value, $array2[$key]);
                    if($new_diff != FALSE)
                    {
                        $difference[$key] = $new_diff;
                    }
                }
            }
            elseif(!isset($array2[$key]) || $array2[$key] != $value)
            {
                $difference[$key] = $value;
            }
        }
        return !isset($difference) ? 0 : $difference;
    }


  /*
    // alle Kategorien importieren.
    function import_categories()
    {

    	//Kategorien auslesen
        $categories = array();
        for ($curr_xls_row = 2; $curr_xls_row <= $this->excel->sheets[2]['numRows']; $curr_xls_row++)
        {

            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["categories_id"] = $this->excel->sheets[2]['cells'][$curr_xls_row][1];
            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["categrories_image"] = $this->excel->sheets[2]['cells'][$curr_xls_row][2];
            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["parent_id"] = $this->excel->sheets[2]['cells'][$curr_xls_row][3];
            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["categories_status"] = $this->excel->sheets[2]['cells'][$curr_xls_row][4];
            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["sort_order"] = $this->excel->sheets[2]['cells'][$curr_xls_row][5];
            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["name"] = $this->excel->sheets[2]['cells'][$curr_xls_row][6];

        }

 			for($j=0; $i <= $this->excel->sheets[2]['numRows']; $i++)
            { //Und einsetzen
                if($categories[$products[$i]['categories_id_'.($j+1)]]['name'])
                {
                    $finished_products[$i]['p_cat.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['name'];
                    $finished_products[$i]['p_cat_id.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['categories_id'];
                    $finished_products[$i]['p_cat_image.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['categories_image'];
                    $finished_products[$i]['p_cat_parent_id.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['parent_id'];
                    $finished_products[$i]['p_cat_status.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['categories_status'];
                    $finished_products[$i]['p_cat_sort_order.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['sort_order'];
                    $this->FileSheme['p_cat.'.$j] = 'Y';
                }
            }

    }
*/
    function import()
    {

        //STEP 1. MAPPING ARRAY XLS TO TABLEFIELD
        //Die können direkt aus der Datei übernommen werden
        //This can be transferred directly from the file
        $elements_from_file = array(
                "XTSOL",
				"products_catalog_page",
                "p_stock",
                "p_sorting",
                "p_shipping",
                "p_tpl",
                "p_manufacturer",
                "p_fsk18",
                "p_priceNoTax",
                "p_priceNoTax.1",
                "p_priceNoTax.2",
                "p_priceNoTax.3",
                "p_tax",
                "p_status",
                "p_weight",
                "p_disc",
                "p_opttpl",
                "p_vpe",
                "p_vpe_status",
                "p_vpe_value",
                "p_image",
                "p_colorchart",
                "p_info1",
                "p_info2",
                "p_image.1",
                "p_image.2",
                "p_image.3",
                "p_image.4",
                "p_image.5",
                "p_image.6",
                "p_image.7",
                "p_image.8",
                "p_image.9",
                "p_image.10",
                "f_cid",
                'filter_style',
                'filter_for_which_material_thickness',
                'filter_material',
                'filter_thread_type',
                'filter_material_thickness',
                'filter_product_type',
                'measurement_info',
                'konstruktor',
                'products_new',
                'special_price',
                'image_forced_attributes',
                'products_discountable',
                'products_stock_item',
                'products_stock_add_quantity'
            );

		        //Was aus $elements_from_file unter einem anderen Namen in der Quelldatei steht
                //What made ​​$elements_from_file under a different name in the source file is

		        // In diesem Script = In this script
		        //     |
		        //     |        In der Excel-Datei = In excel
		        //     |                 |
		        //     v                 v

		        $this->zuordnungen = array(
	            'p_model' => 'products_name',
	            'p_sorting' => 'products_sort',
	            'p_priceNoTax' => 'products_price',
	            'p_status' => 'products_status',
	            'p_image' => 'products_image_bg',
	            'p_colorchart' => 'f_cc',
	            'p_info1' => 'product_info1',
	            'p_info2' => 'product_info2',
	            'p_image.1' => 'additional_image_1',
	            'p_image.2' => 'additional_image_2',
	            'p_image.3' => 'additional_image_3',
	            'p_image.4' => 'additional_image_4',
	            'p_image.5' => 'additional_image_5',
	            'p_image.6' => 'additional_image_6',
	            'p_image.7' => 'additional_image_7',
	            'p_image.8' => 'additional_image_8',
	            'p_image.9' => 'additional_image_9',
	            'p_image.10' => 'additional_image_10',
	            'p_name.' => 'products_description_',
	            "p_desc." => 'products_short_description_',
	            "p_meta_title." => 'products_meta_title_',
	            "p_keywords." => 'products_meta_keywords_',
	            'products_new' => 'new_product',
	            'products_discountable' => 'products_discount_allowed'
		        );

        //Die müssen für jede Sprache einzeln gesetzt werden
        //need to be set individually foreach language
        $language_dependent = array(
                "p_name.",
                "p_desc.",
                "p_shortdesc.",
                "p_meta_title.",
                "p_meta_desc.",
                "p_meta_key.",
                "p_keywords.",
                "p_url.",
            );

/*
				// Es ist hübsch, hier so eine Liste zu haben, sie wird aber nirgendwo verwendet

        //Und die erst noch generiert
        $generated_elements = array("p_model",
                "p_cat.0",
                "p_cat.1",
                "p_cat.2",
                "p_cat.3",
                "p_cat.4",
                "p_cat.5",
                "formel",
                "formel_array",
                "konstruktor",
				"has_price_changes",
            );

        //Und nochmal alle zusammen
        $csv_headers = array_merge($elements_from_file,$language_dependent,$generated_elements); // Wird auch nicht verwendet
*/

        //Alle möglichen Attribute
        $attribute = array(
                'gauge',
                'diameter',
                'length',
                'length_cm',
                'size',
                'ball',
                'attachment',
                'pincolor',
                'stonecolor',
                'color',
                'picture',
            );

        //Kategorien auslesen

        // Auskommentiert - es gibt keine Kategorie-Tabelle mehr
        /*
        $categories = array();
        for ($curr_xls_row = 2; $curr_xls_row <= $this->excel->sheets[2]['numRows']; $curr_xls_row++)
        {

            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["categories_id"] = $this->excel->sheets[2]['cells'][$curr_xls_row][1];
            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["categrories_image"] = $this->excel->sheets[2]['cells'][$curr_xls_row][2];
            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["parent_id"] = $this->excel->sheets[2]['cells'][$curr_xls_row][3];
            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["categories_status"] = $this->excel->sheets[2]['cells'][$curr_xls_row][4];
            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["sort_order"] = $this->excel->sheets[2]['cells'][$curr_xls_row][5];
            $categories[$this->excel->sheets[2]['cells'][$curr_xls_row][1]]["name"] = $this->excel->sheets[2]['cells'][$curr_xls_row][6];

        }
        */

        //STEP 2. MAPPING CATEGORIES TABLE
				$categories = array();
				$query = xtc_db_query('SELECT categories_id, import_key FROM '.TABLE_CATEGORIES.'');
        while($result = xtc_db_fetch_array($query))
        	$cat_key_to_id_lut[$result['import_key']] = $result['categories_id'];  //cat_key_to_id_lut = some kind of dictionary

        //STEP 3. GET ALL PRODUCT
        //Produkte
        $product_headings = $this->excel->sheets[0]['cells'][1]; //Feldnamen

        //$curr_xls_row = current xls row
        for($curr_xls_row = 2; $curr_xls_row <= $this->excel->sheets[0]['numRows']; $curr_xls_row++)
        { //Alle Produkte zusammenbauen -> Assemble all products
            foreach($product_headings as $number => $heading)
                $products[$curr_xls_row-2][$heading] = $this->excel->sheets[0]['cells'][$curr_xls_row][$number];
        }


        //STEP 4. MAPPING PRODUCT TO FILESHEME
        for($i=0;$i<count($products);$i++)
        {
            if(!$products[$i]['products_name']) break;

            //4A. MAPPING FILESHEME WITH ARRAY $ELEMENTS_FROM_FILE
            $finished_products[$i]['XTSOL'] = 'XTSOL'; //Das muss aus irgendeinem Grund da rein -> THERE'S A REASON BEHIND THIS
            foreach($elements_from_file as $element)
            { //Erster Schritt: Alle unveränderten Elemente übernehmen, ggf. mit neuem Namen
                if($this->zuordnungen[$element] && $products[$i][$this->zuordnungen[$element]])
                {
                    $finished_products[$i][$element] = $products[$i][$this->zuordnungen[$element]];
                    $this->FileSheme[$element] = 'Y';
                     //echo $element.' wurde gesetzt (Zuordnung via '.$this->zuordnungen[$element].').<br />';
                }
                elseif(!$this->zuordnungen[$element] && $products[$i][$element])
                {
                    $finished_products[$i][$element] = $products[$i][$element];
                    $this->FileSheme[$element] = 'Y';
                    //echo $element.' wurde gesetzt (direkt).<br />';
                }
                else
                {
                    $finished_products[$i][$element] = '';
                    $this->FileSheme[$element] = 'Y'; // wir wollen das alle Felder geupdatet werden
                }
            }

            //4B. MAPPING FILESHEME WITH LANGUAGE FROM TABLE
            $query = xtc_db_query('SELECT code FROM '.TABLE_LANGUAGES.'');
            while($result = xtc_db_fetch_array($query)) {
                $code = $result['code'];

                foreach($language_dependent as $language)
                { //Zweiter Schritt: Sprachabhängiges Zeug übernehmen, wenn nichts da ist nimmt man das Englische
                    //language -dependent stuff take over if there is nothing, THEN you take the English
                    if($this->zuordnungen[$language])
                        $current_field = $this->zuordnungen[$language].$code;
                    else
                        $current_field = $language.$code;

                    if($products[$i][$current_field])
                        $finished_products[$i][$language.$code] = $products[$i][$current_field];

                    else
                        $finished_products[$i][$language.$code] = $products[$i][$this->zuordnungen[$language.'en']];

                    $this->FileSheme[$language] = 'Y';
                }
            }

            $finished_products[$i]['p_model'] = str_replace(array("/",",","."), "", $products[$i][$this->zuordnungen['p_model']]);

/*
            for($j=0;$j<3; $j++)
            { //Und einsetzen

                if($categories[$products[$i]['categories_id_'.($j+1)]]['name'])
                {



                    $finished_products[$i]['p_cat.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['name'];
                    $finished_products[$i]['p_cat_id.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['categories_id'];
                    $finished_products[$i]['p_cat_image.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['categories_image'];
                    $finished_products[$i]['p_cat_parent_id.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['parent_id'];
                    $finished_products[$i]['p_cat_status.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['categories_status'];
                    $finished_products[$i]['p_cat_sort_order.'.$j] = $categories[$products[$i]['categories_id_'.($j+1)]]['sort_order'];
                    $this->FileSheme['p_cat.'.$j] = 'Y';
                }
            }
*/

						// Kategorie-ID in die insertProduct() rüberschaffen
						$finished_products[$i]['CAT_ID'] = $cat_key_to_id_lut[$products[$i]['categorie']];

            if($products[$i]['v_formel']) //Es gibt eine Formel, wir können sie gerade so nehmen
                $finished_products[$i]['formula_main'] = str_replace('v_', '', $products[$i]['v_formel']) . str_replace('v_', '', $products[$i]['v_formel2']) . str_replace('v_', '', $products[$i]['v_formel3']);

            // $this->FileSheme['formel'] = 'Y';
            $this->FileSheme['formula_main'] = 'Y';
            $finished_products[$i]['formula_main'] = str_replace(' ','',$finished_products[$i]['formula_main']);
            $this->FileSheme['formula_needs_processing'] = 'Y';
            $finished_products[$i]['formula_needs_processing'] = 1;
            $finished_products[$i]['export_to_erp'] = 1;



            $this->attributes = array();

						// echo '('.$finished_products[$i]['p_model'].'):';

            // $result = formula_syntax_check(); // Gibts noch nicht
            $result = true;

            if (!$result)
            	$this->errorLog[] = '<b>Fehler:</b> Ungueltige Formel / Konstruktor passt nicht, Zeile '.$i.', Produkt: '.$finished_products[$i]['p_model'];

            $finished_products[$i]['attributes_for_search'] = $this->attributes;

            $this->FileSheme['p_image'] = 'Y';
            $this->FileSheme['p_colorchart'] = 'Y';
            $this->FileSheme['p_info1'] = 'Y';
            $this->FileSheme['p_info2'] = 'Y';


            for($j=1;$j<=10;$j++) {
                if($finished_products[$i]['p_image.'.$j])
                    $this->FileSheme['p_image.'.$j] = 'Y';
            }

        }

        //Und ab damit zum Speichern

		$import_pause_fuer_mysql_cluster_check = time();

        for ($i = 0; $i < count($finished_products); $i ++)
        {

			// Alle 5 Sekunden 2 Sekunden warten damit der MySQL Cluster Check laufen kann
			if ((time() - $import_pause_fuer_mysql_cluster_check) >= 5)
			{
				sleep(2);
				$import_pause_fuer_mysql_cluster_check = time();
			}

            if ($this->debug)
            {
                echo '<pre>';
                print_r($finished_products[$i]);
                echo '</pre>';

            }


            if ($finished_products[$i]['p_model'] != '')
            {
                if ($finished_products[$i]['CAT_ID'] != '')
                {
                    if ($this->checkModel($finished_products[$i]['p_model']))
                        $this->insertProduct($finished_products[$i], 'update',true);
					else
                        $this->insertProduct($finished_products[$i],'insert',true);
                }
				else
                    $this->errorLog[] = '<b>ERROR:</b> no or unknown Category, line: '.$i.' dataset: '.$finished_products[$i]['p_model'].' Mapping:'.$products[$i]['categorie'];
            }
			else
                $this->errorLog[] = '<b>ERROR:</b> no Model number, line: '.$i.' dataset: '.$finished_products[$i]['p_model'];
        }

	      // Zu guter Letzt bauen wir noch die Zwischentabellen für die Filter neu
				// xtc_db_query('CALL prepare_filters();'); // ...was aber nicht geht wegen MySQL-Extension

				update_products_status();

        return array ($this->counter, $this->errorLog, $this->calcElapsedTime($this->time_start));


    }


}

// EXPORT

class xtcExport
{

    function xtcExport($filename)
    {
        $this->catDepth = 6;
        $this->languages = $this->get_lang();
        $this->filename = $filename;
        $this->CAT = array ();
        $this->PARENT = array ();
        $this->counter = array ('prod_exp' => 0);
        $this->time_start = time();
        $this->man = $this->getManufacturers();
        $this->TextSign = CSV_TEXTSIGN;
        $this->seperator = CSV_SEPERATOR;
        if (CSV_SEPERATOR == '')
            $this->seperator = "\t";
        if (CSV_SEPERATOR == '\t')
            $this->seperator = "\t";
        $this->Groups = xtc_get_customers_statuses();
    }

    /**
     *   Get installed languages
     *   @return array
     */
    function get_lang()
    {

        $languages_query = xtc_db_query("select languages_id, name, code, image, directory from ".TABLE_LANGUAGES);
        while ($languages = xtc_db_fetch_array($languages_query))
        {
            $languages_array[] = array ('id' => $languages['languages_id'], 'name' => $languages['name'], 'code' => $languages['code']);
        }

        return $languages_array;
    }

    function exportProdFile()
    {

        $fp = fopen(DIR_FS_DOCUMENT_ROOT.'export/'.$this->filename, "w+");
        $heading = $this->TextSign.'XTSOL'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'products_catalog_page'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_model'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_stock'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_sorting'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_shipping'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_tpl'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_manufacturer'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_fsk18'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_priceNoTax'.$this->TextSign.$this->seperator;
        for ($i = 0; $i < count($this->Groups) - 1; $i ++)
        {
            $heading .= $this->TextSign.'p_priceNoTax.'.$this->Groups[$i +1]['id'].$this->TextSign.$this->seperator;
        }
        if (GROUP_CHECK == 'true')
        {
            for ($i = 0; $i < count($this->Groups) - 1; $i ++)
            {
                $heading .= $this->TextSign.'p_groupAcc.'.$this->Groups[$i +1]['id'].$this->TextSign.$this->seperator;
            }
        }
        $heading .= $this->TextSign.'p_tax'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_status'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_weight'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_disc'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_opttpl'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_vpe'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_vpe_status'.$this->TextSign.$this->seperator;
        $heading .= $this->TextSign.'p_vpe_value'.$this->TextSign.$this->seperator;
        // product images

        for ($i = 1; $i < MO_PICS + 1; $i ++)
        {
            $heading .= $this->TextSign.'p_image.'.$i.$this->TextSign.$this->seperator;
            ;
        }

        $heading .= $this->TextSign.'p_image'.$this->TextSign;

        // add lang fields
        for ($i = 0; $i < sizeof($this->languages); $i ++)
        {
            $heading .= $this->seperator.$this->TextSign;
            $heading .= 'p_name.'.$this->languages[$i]['code'].$this->TextSign.$this->seperator;
            $heading .= $this->TextSign.'p_desc.'.$this->languages[$i]['code'].$this->TextSign.$this->seperator;
            $heading .= $this->TextSign.'p_shortdesc.'.$this->languages[$i]['code'].$this->TextSign.$this->seperator;
            $heading .= $this->TextSign.'p_meta_title.'.$this->languages[$i]['code'].$this->TextSign.$this->seperator;
            $heading .= $this->TextSign.'p_meta_desc.'.$this->languages[$i]['code'].$this->TextSign.$this->seperator;
            $heading .= $this->TextSign.'p_meta_key.'.$this->languages[$i]['code'].$this->TextSign.$this->seperator;
            $heading .= $this->TextSign.'p_keywords.'.$this->languages[$i]['code'].$this->TextSign.$this->seperator;
            $heading .= $this->TextSign.'p_url.'.$this->languages[$i]['code'].$this->TextSign;

        }
        // add categorie fields
        for ($i = 0; $i < $this->catDepth; $i ++)
            $heading .= $this->seperator.$this->TextSign.'p_cat.'.$i.$this->TextSign;

        // $heading .= $this->seperator.$this->TextSign.'formel'.$this->TextSign;
        $heading .= $this->seperator.$this->TextSign.'formel_array'.$this->TextSign;
        $heading .= $this->seperator.$this->TextSign.'konstruktor'.$this->TextSign;
        $heading .= $this->seperator.$this->TextSign.'f_material'.$this->TextSign;
        $heading .= $this->seperator.$this->TextSign.'f_cid'.$this->TextSign;
        $heading .= $this->seperator.$this->TextSign.'f_art'.$this->TextSign;

        $heading .= "\n";

        fputs($fp, $heading);
        // content
        $export_query = xtc_db_query("SELECT
										                             *
										                         FROM
										                             ".TABLE_PRODUCTS);

        while ($export_data = xtc_db_fetch_array($export_query))
        {

            $this->counter['prod_exp']++;
            $line = $this->TextSign.'XTSOL'.$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_catalog_page'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_model'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_quantity'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_sort'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_shippingtime'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['product_template'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$this->man[$export_data['manufacturers_id']].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_fsk18'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_price'].$this->TextSign.$this->seperator;
            // group prices  Qantity:Price::Quantity:Price
            for ($i = 0; $i < count($this->Groups) - 1; $i ++)
            {
                $price_query = "SELECT * FROM ".TABLE_PERSONAL_OFFERS_BY.$this->Groups[$i +1]['id']." WHERE products_id = '".$export_data['products_id']."'ORDER BY quantity";
                $price_query = xtc_db_query($price_query);
                $groupPrice = '';
                while ($price_data = xtc_db_fetch_array($price_query))
                {
                    if ($price_data['personal_offer'] > 0)
                    {
                        $groupPrice .= $price_data['quantity'].':'.$price_data['personal_offer'].'::';
                    }
                }
                $groupPrice .= ':';
                $groupPrice = str_replace(':::', '', $groupPrice);
                if ($groupPrice == ':')
                    $groupPrice = "";
                $line .= $this->TextSign.$groupPrice.$this->TextSign.$this->seperator;

            }

            // group permissions
            if (GROUP_CHECK == 'true')
            {
                for ($i = 0; $i < count($this->Groups) - 1; $i ++)
                {
                    $line .= $this->TextSign.$lang_data['group_permission_'.$this->Groups[$i +1]['id']].$this->TextSign.$this->seperator;
                }
            }

            $line .= $this->TextSign.$export_data['products_tax_class_id'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_status'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_weight'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_discount_allowed'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['options_template'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_vpe'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_vpe_status'].$this->TextSign.$this->seperator;
            $line .= $this->TextSign.$export_data['products_vpe_value'].$this->TextSign.$this->seperator;

            if (MO_PICS > 0)
            {
                $mo_query = "SELECT * FROM ".TABLE_PRODUCTS_IMAGES." WHERE products_id='".$export_data['products_id']."'";
                $mo_query = xtc_db_query($mo_query);
                $img = array ();
                while ($mo_data = xtc_db_fetch_array($mo_query))
                {
                    $img[$mo_data['image_nr']] = $mo_data['image_name'];
                }

            }

            // product images
            for ($i = 1; $i < MO_PICS + 1; $i ++)
            {
                if (isset ($img[$i]))
                {
                    $line .= $this->TextSign.$img[$i].$this->TextSign.$this->seperator;
                } else
                {
                    $line .= $this->TextSign."".$this->TextSign.$this->seperator;
                }
            }

            $line .= $this->TextSign.$export_data['products_image'].$this->TextSign.$this->seperator;

            for ($i = 0; $i < sizeof($this->languages); $i ++)
            {
                $lang_query = xtc_db_query("SELECT * FROM ".TABLE_PRODUCTS_DESCRIPTION." WHERE language_id='".$this->languages[$i]['id']."' and products_id='".$export_data['products_id']."'");
                $lang_data = xtc_db_fetch_array($lang_query);
                $lang_data['products_description'] = str_replace("\n", "", $lang_data['products_description']);
                $lang_data['products_short_description'] = str_replace("\n", "", $lang_data['products_short_description']);
                $lang_data['products_description'] = str_replace("\r", "", $lang_data['products_description']);
                $lang_data['products_short_description'] = str_replace("\r", "", $lang_data['products_short_description']);
                $lang_data['products_description'] = str_replace(chr(13), "", $lang_data['products_description']);
                $lang_data['products_short_description'] = str_replace(chr(13), "", $lang_data['products_short_description']);
                $line .= $this->TextSign.stripslashes($lang_data['products_name']).$this->TextSign.$this->seperator;
                $line .= $this->TextSign.stripslashes($lang_data['products_description']).$this->TextSign.$this->seperator;
                $line .= $this->TextSign.stripslashes($lang_data['products_short_description']).$this->TextSign.$this->seperator;
                $line .= $this->TextSign.stripslashes($lang_data['products_meta_title']).$this->TextSign.$this->seperator;
                $line .= $this->TextSign.stripslashes($lang_data['products_meta_description']).$this->TextSign.$this->seperator;
                $line .= $this->TextSign.stripslashes($lang_data['products_meta_keywords']).$this->TextSign.$this->seperator;
                $line .= $this->TextSign.stripslashes($lang_data['products_keywords']).$this->TextSign.$this->seperator;
                $line .= $this->TextSign.$lang_data['products_url'].$this->TextSign.$this->seperator;

            }
            $cat_query = xtc_db_query("SELECT categories_id FROM ".TABLE_PRODUCTS_TO_CATEGORIES." WHERE products_id='".$export_data['products_id']."'");
            $cat_data = xtc_db_fetch_array($cat_query);

            $line .= $this->buildCAT($cat_data['categories_id']);
            $line .= $this->TextSign;

            //$line .= $this->seperator.$this->TextSign.$export_data['formel'].$this->TextSign;
            $line .= $this->seperator.$this->TextSign.$export_data['formel_array'].$this->TextSign;
            $line .= $this->seperator.$this->TextSign.$export_data['konstruktor'].$this->TextSign;
            $line .= $this->seperator.$this->TextSign.$export_data['f_material'].$this->TextSign;
            $line .= $this->seperator.$this->TextSign.$export_data['f_cid'].$this->TextSign;
            $line .= $this->seperator.$this->TextSign.$export_data['f_art'].$this->TextSign;

            $line .= "\n";
            fputs($fp, $line);
        }

        fclose($fp);
        /*
		if (COMPRESS_EXPORT=='true') {
			$backup_file = DIR_FS_DOCUMENT_ROOT.'export/' . $this->filename;
			exec(LOCAL_EXE_ZIP . ' -j ' . $backup_file . '.zip ' . $backup_file);
		   unlink($backup_file);
		}
        */
        return array (0 => $this->counter, 1 => '', 2 => $this->calcElapsedTime($this->time_start));
    }

    /**
     *   Calculate Elapsed time from 2 given Timestamps
     *   @param int $time old timestamp
     *   @return String elapsed time
     */
    function calcElapsedTime($time)
    {

        $diff = time() - $time;
        $daysDiff = 0;
        $hrsDiff = 0;
        $minsDiff = 0;
        $secsDiff = 0;

        $sec_in_a_day = 60 * 60 * 24;
        while ($diff >= $sec_in_a_day)
        {
            $daysDiff ++;
            $diff -= $sec_in_a_day;
        }
        $sec_in_an_hour = 60 * 60;
        while ($diff >= $sec_in_an_hour)
        {
            $hrsDiff ++;
            $diff -= $sec_in_an_hour;
        }
        $sec_in_a_min = 60;
        while ($diff >= $sec_in_a_min)
        {
            $minsDiff ++;
            $diff -= $sec_in_a_min;
        }
        $secsDiff = $diff;

        return ('(elapsed time '.$hrsDiff.'h '.$minsDiff.'m '.$secsDiff.'s)');

    }

    function buildCAT($catID)
    {

        if (isset ($this->CAT[$catID]))
        {
            return $this->CAT[$catID];
        } else
        {
            $cat = array ();
            $tmpID = $catID;

            while ($this->getParent($catID) != 0 || $catID != 0)
            {
                $cat_select = xtc_db_query("SELECT categories_name FROM ".TABLE_CATEGORIES_DESCRIPTION." WHERE categories_id='".$catID."' and language_id='".$this->languages[0]['id']."'");
                $cat_data = xtc_db_fetch_array($cat_select);
                $catID = $this->getParent($catID);
                $cat[] = $cat_data['categories_name'];

            }
            $catFiller = '';
            for ($i = $this->catDepth - count($cat); $i > 0; $i --)
            {
                $catFiller .= $this->TextSign.$this->TextSign.$this->seperator;
            }
            $catFiller .= $this->TextSign;
            $catStr = '';
            for ($i = count($cat); $i > 0; $i --)
            {
                $catStr .= $this->TextSign.$cat[$i -1].$this->TextSign.$this->seperator;
            }
            $this->CAT[$tmpID] = $catStr.$catFiller;
            return $this->CAT[$tmpID];
        }
    }

    /**
     *   Get the tax_class_id to a given %rate
     *   @return array
     */
    function getTaxRates() // must be optimazed (pre caching array)

    {
        $tax = array ();
        $tax_query = xtc_db_query("Select
										                                      tr.tax_class_id,
										                                      tr.tax_rate,
										                                      ztz.geo_zone_id
										                                      FROM
										                                      ".TABLE_TAX_RATES." tr,
										                                      ".TABLE_ZONES_TO_GEO_ZONES." ztz
										                                      WHERE
										                                      ztz.zone_country_id='".STORE_COUNTRY."'
										                                      and tr.tax_zone_id=ztz.geo_zone_id
										                                      ");
        while ($tax_data = xtc_db_fetch_array($tax_query))
        {

            $tax[$tax_data['tax_class_id']] = $tax_data['tax_rate'];

        }
        return $tax;
    }

    /**
     *   Prefetch Manufactrers
     *   @return array
     */
    function getManufacturers()
    {
        $man = array ();
        $man_query = xtc_db_query("SELECT
										                                manufacturers_name,manufacturers_id
										                                FROM
										                                ".TABLE_MANUFACTURERS);
        while ($man_data = xtc_db_fetch_array($man_query))
        {
            $man[$man_data['manufacturers_id']] = $man_data['manufacturers_name'];
        }
        return $man;
    }

    /**
     *   Return Parent ID for a given categories id
     *   @return int
     */
    function getParent($catID)
    {
        if (isset ($this->PARENT[$catID]))
        {
            return $this->PARENT[$catID];
        } else
        {
            $parent_query = xtc_db_query("SELECT parent_id FROM ".TABLE_CATEGORIES." WHERE categories_id='".$catID."'");
            $parent_data = xtc_db_fetch_array($parent_query);
            $this->PARENT[$catID] = $parent_data['parent_id'];
            return $parent_data['parent_id'];
        }
    }

}
