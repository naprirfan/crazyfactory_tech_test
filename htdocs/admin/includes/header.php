<?php
/* --------------------------------------------------------------
   $Id: header.php 1025 2005-07-14 11:57:54Z gwinger $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(header.php,v 1.19 2002/04/13); www.oscommerce.com
   (c) 2003	 nextcommerce (header.php,v 1.17 2003/08/24); www.nextcommerce.org

   Released under the GNU General Public License
   --------------------------------------------------------------*/

  if ($messageStack->size > 0) {
    echo $messageStack->output();
  }
  
  $admin_access_query = xtc_db_query("select * from " . TABLE_ADMIN_ACCESS . " where customers_id = '" . $_SESSION['customer_id'] . "'");
  $admin_access = xtc_db_fetch_array($admin_access_query);
?>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<?php
echo   '<tr>';
?>
  	<td >&nbsp;</td>
    <td ><?php echo xtc_image(DIR_WS_IMAGES . 'logo.gif', 'Crazy Factory Admin'); ?></td>
    <td valign="bottom" align="left" >
    <?php echo '<a href="start.php"  class="headerLink">'. xtc_image(DIR_WS_IMAGES . 'top_index.gif', '', '', '').'</a>'; ?>
    <?php echo xtc_image(DIR_WS_IMAGES . 'img_spacer.gif', '', '', ''); ?>
    <?php echo '<a href="../index.php" class="headerLink">'. xtc_image(DIR_WS_IMAGES . 'top_shop.gif', '', '', '').'</a>'; ?>
    <?php echo xtc_image(DIR_WS_IMAGES . 'img_spacer.gif', '', '', ''); ?>
    <?php echo '<a href="' . xtc_href_link(FILENAME_LOGOUT, '', ADMINSSL) . '" class="headerLink">'. xtc_image(DIR_WS_IMAGES . 'top_logout.gif', '', '', '').'</a>'; ?>


    </td>
<td class="smallText" align="right">
     <?php 
          if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['categories'] == '1'))
          {
          	echo xtc_draw_form('search', 'categories.php', '', 'get','',ADMINSSL ); 
	          echo 'Artikelcode: ' . xtc_draw_input_field('search','', 'size="18"').xtc_draw_hidden_field(xtc_session_name(), xtc_session_id());
	          echo '</form>&nbsp;<br/>';
          }
          if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['customers'] == '1'))
          {
          	echo xtc_draw_form('search', FILENAME_CUSTOMERS, '', 'get','',ADMINSSL); 
          	echo 'Kundensuche: ' . xtc_draw_input_field('search','', 'size="18"').xtc_draw_hidden_field(xtc_session_name(), xtc_session_id()); 
          	echo '</form>&nbsp;<br/>';
          }
          if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['orders'] == '1'))
          {
          	echo xtc_draw_form('orders', FILENAME_ORDERS, '', 'get','', ADMINSSL);
           	echo 'Bestellnummer: ' . xtc_draw_input_field('oID', '', 'size="18"') . xtc_draw_hidden_field('action', 'edit').xtc_draw_hidden_field(xtc_session_name(), xtc_session_id());
           	echo '</form>&nbsp;';
          }
     ?>
	</td>
  </tr>
</table>