<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html {$smarty.const.HTML_PARAMS}>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.session.language_charset}">
		<title>{$smarty.const.TITLE}</title>
		<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="templates/css/reset-min-2.8.2r1.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/stylesheet.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery-ui-1.8.6.custom.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery.fancybox-1.3.4.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery.ui.stars.min.css"/>
		<script type="text/javascript" src="templates/js/jquery-1.4.4.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery-ui-1.8.6.custom.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="templates/js/jquery.ui.stars.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery.qtip-1.0.0-rc3.min.js"></script>
		<script type="text/javascript" src="templates/js/coupon_themes.js"></script>
		<script type="text/javascript" src="includes/general.js"></script>
		<script type="text/javascript" src="includes/javascript/categories.js"></script>
		{if isset($add_theme_error)}
		<script type="text/javascript">
			alert('Couldn\'t add theme');
		</script>
		{/if}
	</head>
	<body style="margin: 0; background-color: #FFFFFF">

		<div id="spiffycalendar" class="text"></div>
		<!-- header //-->
		{php} require(DIR_WS_INCLUDES . 'header.php'); {/php}
		<!-- header_eof //-->

		<!-- body //-->
		<table style="border:none; width:100%;" cellspacing="2" cellpadding="2">
			<tr>
				<td class="columnLeft2" width="{$smarty.const.BOX_WIDTH}" valign="top">
					<table style="border: none; width: {$smarty.const.BOX_WIDTH};" cellspacing="1" cellpadding="1"
						   class="columnLeft">
						<!-- left_navigation //-->
						{php} require(DIR_WS_INCLUDES . 'column_left.php'); {/php}
						<!-- left_navigation_eof //-->
					</table>
				</td>
				<!-- body_text //-->
				<td class="boxCenter" width="100%" valign="top" style="padding: 20px;">
					<div id="ajax_blocker" class="ajax_blocker"></div>
					<div id="coupon_themes">
						<ul>
							<li><a href="#tabs-0">coupon themes</a></li>
							<li><a href="#tabs-1">theme categories</a></li>
						</ul>
						<input type="hidden" id="loading_text" value="Lade..." />
						<div id="tabs-0">
							<button id="add_theme">add theme</button>
							<form id="theme_selection" action="">
								<input type="radio" name="mode" value="all" checked="checked" />
								<label>all</label>
								<input type="radio" name="mode" value="active" />
								<label>active</label>
								<input type="radio" name="mode" value="inactive" />
								<label>inactive</label>
								<input type="submit" value="show"  />
							</form>
							<div id="themes_list"></div>
						</div>
						<div id="tabs-1">
							<button id="add_category">add category</button>
							<div id="categories_list"></div>
						</div>
					</div>
				</td>

			</tr>
		</table>

		<!-- footer //-->
		{php} require(DIR_WS_INCLUDES . 'footer.php'); {/php}
		<!-- footer_eof //-->
	</body>
</html>