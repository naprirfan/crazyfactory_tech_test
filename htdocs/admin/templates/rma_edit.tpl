<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html {$smarty.const.HTML_PARAMS}>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.session.language_charset}">
		<title>{$smarty.const.TITLE}</title>
		<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="templates/css/reset-min-2.8.2r1.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/stylesheet.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery-ui-1.8.6.custom.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery.fancybox-1.3.4.css"/>
		<style type="text/css" media="print">{literal}
			body table
			{
				display: none;
			}

			.print
			{
				display: block !important;
				font-size: 12px;
			}

			.print table
			{
				display: block;
				font-size: 12px;
			}

			.print .edit_info
			{
				margin: 20px 0 20px;
			}

			.print table.product_list
			{
			}

			.print table.product_list tr,
			.print table.product_list td
			{
				border: 1px solid #000000;
				border-collapse: collapse;
			}

			.print table.product_list td
			{
				padding: 0 5px;
			}

			.print table.product_list table.parts
			{
				margin-left: 10px;
			}

			.print table.product_list table.parts td
			{
				border-bottom: 1px solid #000000;
			}
		</style>{/literal}
		<script type="text/javascript" src="templates/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="templates/js/rma.js"></script>
		<script type="text/javascript" src="includes/general.js"></script>
		<script type="text/javascript" src="includes/javascript/categories.js"></script>
	</head>
	<body style="margin: 0; background-color: #FFFFFF">
		<!-- header //-->
		{php} require(DIR_WS_INCLUDES . 'header.php'); {/php}
		<!-- header_eof //-->
		<table style="border:none; width:100%;" cellspacing="2" cellpadding="2">
			<tr>
				<td class="columnLeft2" width="{$smarty.const.BOX_WIDTH}" valign="top">
					<table style="border: none; width: {$smarty.const.BOX_WIDTH};" cellspacing="1" cellpadding="1" class="columnLeft">
						<!-- left_navigation //-->
						{php} require(DIR_WS_INCLUDES . 'column_left.php'); {/php}
						<!-- left_navigation_eof //-->
					</table>
				</td>
				<td class="boxCenter rma" width="100%" valign="top" style="padding: 20px;">
					<div class="heading">
						{$smarty.const.RMA_HEADING_EDIT} {$rma.rma_id}
					</div>
					<div class="rma_info">
						<table class="info">
							<tr>
								<td>{$smarty.const.CUSTOMERS_ID}:</td>
								<td>{$rma.customers_id}</td>
							</tr>
							<tr>
								<td>{$smarty.const.CUSTOMERS_NAME}:</td>
								<td>{$rma.customers_firstname} {$rma.customers_lastname}</td>
							</tr>
							<tr>
								<td>{$smarty.const.ORDERS_ID}:</td>
								<td>{$rma.orders_id}</td>
							</tr>
							<tr>
								<td>{$smarty.const.DATE_CREATED}:</td>
								<td>{$rma.date_created}</td>
							</tr>
							<tr>
								<td>{$smarty.const.DATE_EDITED}:</td>
								<td>{$rma.date_edited}</td>
							</tr>
							<tr>
								<td>{$smarty.const.EDITED_BY}:</td>
								<td>{$rma.edited_by_name}</td>
							</tr>
							<tr>
								<td>{$smarty.const.CURRENT_STATUS}:</td>
								<td>{$rma.rma_status}</td>
							</tr>
							{if $rma.refund_type == 'bank_details'}
							<tr>
								<td colspan="2">{$smarty.const.REFUND_BANK}</td>
							</tr>
							<tr>
								<td>{$smarty.const.ACCOUNT_HOLDER}:</td>
								<td>{$rma.account_holder}</td>
							</tr>
							<tr>
								<td>{$smarty.const.ACCOUNT_NUMBER}:</td>
								<td>{$rma.account_number}</td>
							</tr>
							<tr>
								<td>{$smarty.const.BANK_CODE}:</td>
								<td>{$rma.bank_code}</td>
							</tr>
							<tr>
								<td>{$smarty.const.BANK_NAME}:</td>
								<td>{$rma.bank_name}</td>
							</tr>
							{elseif $rma.refund_type == 'paypal'}
							<tr>
								<td colspan="2">{$smarty.const.REFUND_PAYPAL}</td>
							</tr>
							<tr>
								<td>{$smarty.const.PAYPAL_EMAIL}:</td>
								<td>{$payment_email}</td>
							</tr>
							{elseif $rma.refund_type == 'moneybookers'}
							<tr>
								<td colspan="2">{$smarty.const.REFUND_MONEYBOOKERS}</td>
							</tr>
							<tr>
								<td>{$smarty.const.MONEYBOOKERS_EMAIL}:</td>
								<td>{$payment_email}</td>
							</tr>
							{/if}
						</table>
					</div>
					<form method="post" action="rma.php?rma_id={$smarty.get.rma_id}">
						<div class="rma_comment">
							{$smarty.const.RMA_COMMENT}:<br>
							<textarea name="rma_comment" rows="11" cols="60">{$rma.comment}</textarea><br>
							<input type="submit" value="{$smarty.const.SUBMIT_RMA_COMMENT}" />
						</div>
					</form>
					<div class="clear"></div>
					{if $other_rma}
					<div class="other_rma">
						{$smarty.const.OTHER_RMA}
						<select name="rma_id" id="other_rma_list">
							{html_options options=$other_rma selected='default'}
						</select>
					</div>
					{/if}
					<div class="clear"></div>
					<form method="post" id="rma_edit_form" action="rma.php?rma_id={$smarty.get.rma_id}">
						<div class="innerHeading" style="margin-top: 10px;">
							<span>{$smarty.const.ARTICLE}</span>
							<span class="details">{$smarty.const.DETAILS}</span>
							<span class="qty">{$smarty.const.QUANTITY} &nbsp;<i>{$smarty.const.QUANTITY_OF}</i></span>
						</div>
						{foreach from=$rma_items item=product}
						<div class="product">
							<div id="product_thumb_image"><img src="{$product.image}" /></div>
							<div class="article">
								<strong>{$product.name[0]}</strong>
								{$product.name[1]}<br /><br />
								{$smarty.const.MODEL} <span id="model">{$product.model}</span>
							</div>
							<div class="attributes">
								<table width="160" style="text-align:left;">
									{foreach from=$product.attributes item=attribute key=attr_key}
									<tr>
										<td width="130"><strong>{$attribute_names.$attr_key}:</strong></td>
										<td>{$attribute}</td>
									</tr>
									{/foreach}
								</table>
								{if $product.info_image || $product.pin_image}
								<div class="info_images">
									{if $product.info_image}
									<img src="{$product.info_image}" />
									{/if}
									{if $product.pin_image}
									<img src="{$product.pin_image}" />
									{/if}
								</div>
								{/if}
							</div>
							<div class="qty" style="margin-left:20px;">{$product.selected_qty} &nbsp;&nbsp;<i>({$product.qty})</i></div>
							<div class="buttons">
								{if $product.deleted}
								{$smarty.const.PRODUCT_DELETED}<br />
								{/if}
								{assign var=name value=$product.rma_products_id}
								{html_radios name=status[$name] options=$product.status_array selected=$product.selected_status separator="<br />"}
								<br />
								{if $product.error_amount}
								<div class="error">
									{$product.error_amount}
								</div>
								{/if}
							</div>
							<div class="clear"></div>
							<div class="info">
								<span class="quantity">
									{$smarty.const.SELECTED_QUANTITY} <input type="text" name="selected_quantity[{$product.rma_products_id}]" size="5" value="{$product.selected_qty}" /><br />
								</span>
								{$smarty.const.SELECTED_CAUSE} {if $product.cause == 'switcheroo'}<b> <font color='red'>{/if}{$product.selected_cause}{if $product.cause == 'switcheroo'} ({$smarty.const.MIX_BAG})</font></b>{/if}<br />
								{$smarty.const.RMA_TYPE}<br>
								<input type="radio" class="rma_type" name="selected_rma_type[{$product.rma_products_id}]" id="replacement{$product.rma_products_id}" value="replacement"{if $product.selected_rma_type_raw == 'replacement'} checked="checked"{/if}><label for="replacement{$product.rma_products_id}">{$smarty.const.TYPE_REPLACEMENT}</label><br />
								<input type="radio" class="rma_type" name="selected_rma_type[{$product.rma_products_id}]" id="refund{$product.rma_products_id}" value="refund"{if $product.selected_rma_type_raw == 'refund'} checked="checked"{/if}><label for="refund{$product.rma_products_id}">{$smarty.const.TYPE_REFUND}</label><br />
								<input type="radio" class="rma_type" name="selected_rma_type[{$product.rma_products_id}]" id="refund_credit_voucher{$product.rma_products_id}" value="refund_credit_voucher"{if $product.selected_rma_type_raw == 'refund_credit_voucher'} checked="checked"{/if}><label for="refund_credit_voucher{$product.rma_products_id}">{$smarty.const.TYPE_REFUND_CREDIT_VOUCHER}</label>
								<br />
								<br />
								<input type="checkbox" name="in_bag[{$product.rma_products_id}]" value="on"{if $product.cause == 'unwanted' || $product.cause == 'unauthorised' || $product.cause == 'quality' || $product.cause == 'bad_description' || ($product.in_bag == 1 && $product.confirmed_by)} checked="checked"{/if} /> {$smarty.const.PRODUCT_IN_PACKET}<br>
							</div>

						</div>
						{/foreach}
						{if !$rma.refund_type}
						<div id="rma_edit_payment_data" style="display: none;">
							<input type="hidden" name="refund" value="{$payed_with}" />
							<input type="hidden" id="has_refund" value="{$rma.refund_type}" />
							{if $payed_with == 'moneybookers'}
							<div class="moneybookers product">
								<p>{$smarty.const.PAYMENT_MONEYBOOKERS}</p>
								{$smarty.const.MONEYBOOKERS_EMAIL}: <input type="text" name="moneybookers_email" value="{$payment_email}" />
							</div>
							{elseif $payed_with == 'paypal'}
							<div class="paypal product">
								<p>{$smarty.const.PAYMENT_PAYPAL}</p>
								{$smarty.const.PAYPAL_EMAIL}: <span>{$payment_email}</span>
								<input type="hidden" name="paypal_email" value="{$payment_email}" />
							</div>
							{else}
							<div class="bank_details product">
								<p>{$smarty.const.PAYMENT_BANK_DETAILS}</p>
								<table>
									<tr>
										<td>{$smarty.const.ACCOUNT_HOLDER}:</td>
										<td><input type="text" name="account_holder" value="" /></td>
									</tr>
									<tr>
										<td>{$smarty.const.ACCOUNT_NUMBER}:</td>
										<td><input type="text" name="account_number" value="" /></td>
									</tr>
									<tr>
										<td>{$smarty.const.BANK_CODE}:</td>
										<td><input type="text" name="bank_code" value="" /></td>
									</tr>
									<tr>
										<td>{$smarty.const.BANK_NAME}:</td>
										<td><input type="text" name="bank_name" value="" /></td>
									</tr>
								</table>
							</div>
							{/if}
						</div>
						{/if}
						<div class="button_area">
							<input type="submit" name="back" value="{$smarty.const.BACK}" />
							{if $rma.status_id != 3 && !$rma.refund_done && !$rma.count_replacements}
							<input type="submit" name="save" value="{$smarty.const.SAVE_CHANGES}" />
							{else}
							{$smarty.const.RMA_FINISHED}
							{/if}
						</div>
						<div class="clear"></div>
					</form>
					{if $rma.status_id != 3 && (!$rma.refund_done || !$rma.orders_id)}
					<form method="post" action="rma.php?rma_id={$smarty.get.rma_id}">
						<a id="itemlist_toggle" href="javascript:" onclick="$('.itemlist').toggle();">{$smarty.const.ADD_ITEM}</a><br />
						{if !$rma.refund_type}
						<input type="hidden" id="has_refund" value="{$rma.refund_type}" />
						{/if}
						<br />
						{if $has_errors}
						<script type="text/javascript">{literal}
							$(function() {
								$('#itemlist_toggle').click();
								var data = JSON.parse('{/literal}{$data}{literal}');
								var errors = JSON.parse('{/literal}{$errors}{literal}');
								console.log(errors);
								for (var key in data.item)
								{
									var itemrow = $('#itemrow' + key);
									itemrow.children('.checkbox').children('[name^=item]').click();
									if (!itemrow.children('.checkbox').children('[name^=item]').attr('checked'))
										itemrow.children('.checkbox').children('[name^=item]').attr('checked', 'checked');

									itemrow.children('.info').children('input[name^=selected_quantity]').val(data.selected_quantity[key]);
									if (typeof errors.selected_quantity != 'undefined' && errors.selected_quantity[key])
										itemrow.children('.info').children('input[name^=selected_quantity]').before('<div class="error">' + errors.selected_quantity[key] + '</div>');

									itemrow.children('.info').children('select.rma_cause').val(data.selected_cause[key]);
									if (typeof errors.selected_cause != 'undefined' && errors.selected_cause[key])
										itemrow.children('.info').children('select.rma_cause').before('<div class="error">' + errors.selected_cause[key] + '</div>');
									itemrow.children('.info').children('input.rma_type#' + data.selected_rma_type[key] + key).click();
									if (!itemrow.children('.info').children('input.rma_type#' + data.selected_rma_type[key] + key).attr('checked'))
										itemrow.children('.info').children('input.rma_type#' + data.selected_rma_type[key] + key).attr('checked', 'checked');
									if (typeof errors.selected_rma_type != 'undefined' && errors.selected_rma_type[key])
										itemrow.children('.info').children('input.rma_type#' + data.selected_rma_type[key] + key).before('<div class="error">' + errors.selected_rma_type[key] + '</div>');
								}

								if (typeof errors.payment != 'undefined' && errors.payment.type)
									$('#payment_data').children('.' + errors.payment.type).prepend('<div class="error">' + errors.payment.text + '</div>');
							});
						</script>{/literal}
						{/if}
						<div class="itemlist">
							{foreach from=$order_products item=product key=row}
							<div class="product" id="itemrow{$row}">
								<div class="checkbox">
									<input type="checkbox" name="item[{$row}]" onclick="$(this).parent().parent().children('.info').toggle();" />
									<input type="hidden" name="orders_products_id[{$row}]" value="{$product.orders_products_id}" />
								</div>
								<div id="product_thumb_image"><img src="{$product.image}" /></div>
								<div class="article">
									<strong>{$product.name[0]}</strong>
									{$product.name[1]}<br /><br />
									{$smarty.const.MODEL} <span id="model">{$product.products_model}</span>
								</div>
								<div class="attributes">
									<table width="160" style="text-align:left;">
										{foreach from=$product.attributes item=attribute key=attr_key}
										<tr>
											<td width="130"><strong>{$attribute_names.$attr_key}:</strong></td>
											<td>{$attribute}</td>
										</tr>
										{/foreach}
									</table>
									{if $product.info_image}
									<div class="info_images">
										<img src="{$product.info_image}" />
									</div>
									{/if}
								</div>
								<div class="qty">{$product.products_quantity}</div>
								<div class="buttons"></div>
								<div class="clear"></div>
								<div class="info" style="display: none;">
									{if $product.products_quantity > 1}
									{$smarty.const.QUANTITY}: <input type="text" name="selected_quantity[{$row}]" /><br />
									{elseif $product.products_quantity == 1}
									<input type="hidden" name="selected_quantity[{$row}]" value="1" />
									{/if}
									{$smarty.const.SELECTED_CAUSE}
									<select class="rma_cause" name="selected_cause[{$row}]">
										<option value="0">{$smarty.const.CAUSE_DEFAULT}</option>
										<option value="no_cause">{$smarty.const.CAUSE_NO_CAUSE}</option>
										<option value="quality">{$smarty.const.CAUSE_QUALITY}</option>
										<option value="damaged_by_carrier">{$smarty.const.CAUSE_DAMAGED_BY_CARRIER}</option>
										<option value="switcheroo">{$smarty.const.CAUSE_SWITCHEROO}</option>
										<option value="unwanted">{$smarty.const.CAUSE_UNWANTED}</option>
										<option value="unauthorised">{$smarty.const.CAUSE_UNAUTHORISED}</option>
										<option value="bad_description">{$smarty.const.CAUSE_BAD_DESCRIPTION}</option>
										<option value="item_missing">{$smarty.const.CAUSE_ITEM_MISSING}</option>
									</select><br />
									{$smarty.const.RMA_TYPE}<br />
									<input type="radio" class="rma_type" name="selected_rma_type[{$row}]" id="replacement{$row}" value="replacement" checked="checked"><label for="replacement{$row}">{$smarty.const.TYPE_REPLACEMENT}</label><br />
									<input type="radio" class="rma_type" name="selected_rma_type[{$row}]" id="refund{$row}" value="refund"><label for="refund{$row}">{$smarty.const.TYPE_REFUND}</label><br />
									<input type="radio" class="rma_type" name="selected_rma_type[{$row}]" id="refund_credit_voucher{$row}" value="refund_credit_voucher"><label for="refund_credit_voucher{$row}">{$smarty.const.TYPE_REFUND_CREDIT_VOUCHER}</label>
								</div>
							</div>
							{/foreach}
							{if !$rma.refund_type}
							<div id="payment_data" style="display: none;">
								<input type="hidden" name="refund" value="{$payed_with}" />
								{if $payed_with == 'moneybookers'}
								<div class="moneybookers product">
									<p>{$smarty.const.PAYMENT_MONEYBOOKERS}</p>
									{$smarty.const.MONEYBOOKERS_EMAIL}: <input type="text" name="moneybookers_email" value="{$payment_email}" />
								</div>
								{elseif $payed_with == 'paypal'}
								<div class="paypal product">
									<p>{$smarty.const.PAYMENT_PAYPAL}</p>
									{$smarty.const.PAYPAL_EMAIL}: <span>{$payment_email}</span>
									<input type="hidden" name="paypal_email" value="{$payment_email}" />
								</div>
								{else}
								<div class="bank_details product">
									<p>{$smarty.const.PAYMENT_BANK_DETAILS}</p>
									<table>
										<tr>
											<td>{$smarty.const.ACCOUNT_HOLDER}:</td>
											<td><input type="text" name="account_holder" value="" /></td>
										</tr>
										<tr>
											<td>{$smarty.const.ACCOUNT_NUMBER}:</td>
											<td><input type="text" name="account_number" value="" /></td>
										</tr>
										<tr>
											<td>{$smarty.const.BANK_CODE}:</td>
											<td><input type="text" name="bank_code" value="" /></td>
										</tr>
										<tr>
											<td>{$smarty.const.BANK_NAME}:</td>
											<td><input type="text" name="bank_name" value="" /></td>
										</tr>
									</table>
								</div>
								{/if}
							</div>
							{/if}
							<div class="button_area">
								<input type="submit" value="{$smarty.const.BUTTON_ADD}" />
								{if $rma.refund_type}
									<input type="hidden" name="refund" value="{$payed_with}" />
									{if $payed_with == 'moneybookers'}
									<input type="hidden" name="moneybookers_email" value="{$payment_email}" />
									{elseif $payed_with == 'paypal'}
									<input type="hidden" name="paypal_email" value="{$payment_email}" />
									{else}
									<input type="hidden" name="account_holder" value="{$rma.account_holder}" />
									<input type="hidden" name="account_number" value="{$rma.account_number}" />
									<input type="hidden" name="bank_code" value="{$rma.bank_code}" />
									<input type="hidden" name="bank_name" value="{$rma.bank_name}" />
									{/if}
								{/if}
							</div>
						</div>
					</form>
					{/if}
				</td>
			</tr>
		</table>
		<!-- footer //-->
		{php} require(DIR_WS_INCLUDES . 'footer.php'); {/php}
		<!-- footer_eof //-->
		{if $rma.status_id == 3}
		<div class="rma print" style="display: none;">
			<div class="heading">
				RMA Return {$rma.rma_id}
			</div>
			<div class="edit_info">
				{$smarty.const.DATE_CREATED}: {$rma.date_created}<br />
				{$smarty.const.DATE_EDITED}: {$rma.date_edited}<br />
				{$smarty.const.EDITED_BY}: {$rma.edited_by_name}<br />
				Packet ID: {$rma.rma_packet_id}
			</div>
			<table class="product_list">
				{foreach from=$rma_items item=product}
					{if $product.in_packet}
					<tr>
						<td style="width: 620px;">
							<strong>{$product.model_erp}
							{if $product.pieces}
							<table class="parts">
								<tr>
									<td style="width: 250px;">Item Code</td>
									<td style="width: 250px;">Stock-ID</td>
									<td>Amount</td>
								</tr>
								{foreach from=$product.pieces item=piece name=pieces}
								<tr>
									<td style="width: 250px;"><nobr>{$piece.code}</nobr></td>
									<td style="width: 250px;"><nobr>{$piece.bin}</nobr></td>
									<td>{$piece.amount}</td>
								</tr>
								{/foreach}
							</table>
							{/if}
						</td>
						<td style="vertical-align: top;">
							{$product.selected_qty}
						</td>
					</tr>
					{/if}
				{/foreach}
			</table>
		</div>
		{/if}
	</body>
</html>