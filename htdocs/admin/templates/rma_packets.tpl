<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html {$smarty.const.HTML_PARAMS}>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.session.language_charset}">
		<title>{$smarty.const.TITLE}</title>
		<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="templates/css/reset-min-2.8.2r1.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/stylesheet.css"/>
		<script type="text/javascript" src="templates/js/jquery-1.7.1.min.js"></script>
	</head>
	<body style="margin: 0; background-color: #FFFFFF">
		<!-- header //-->
		{php} require(DIR_WS_INCLUDES . 'header.php'); {/php}
		<!-- header_eof //-->
		<table style="border:none; width:100%;" cellspacing="2" cellpadding="2">
			<tr>
				<td class="columnLeft2" width="{$smarty.const.BOX_WIDTH}" valign="top">
					<table style="border: none; width: {$smarty.const.BOX_WIDTH};" cellspacing="1" cellpadding="1" class="columnLeft">
						<!-- left_navigation //-->
						{php} require(DIR_WS_INCLUDES . 'column_left.php'); {/php}
						<!-- left_navigation_eof //-->
					</table>
				</td>
				<td class="boxCenter rma" width="100%" valign="top" style="padding: 20px;">
					{if $nonempty_bag != 0}
					<script type="text/javascript">{literal}
						$(function() {
							$('.rma').children('.content').children('form').submit(function(e) {
								if (!window.confirm('{/literal}{$smarty.const.NONEMPTY_BAG}{literal}'))
									e.preventDefault();
							});
						});
					</script>{/literal}
					{/if}
					<div class="heading">
						{$smarty.const.RMA_HEADING_PACKET}
					</div>
					<div class="content">
						<br />
						{$smarty.const.RMA_IN_PACKET}{$rma_in_packet}<br />
						<br />
						<form method="post" action="rma_packets.php">
							<input type="submit" class="button" value="{$smarty.const.FINISH_PACKET}" />
							<input type="hidden" name="packet_id" value="{$open_packet}" />
						</form>
						<br /><br />
						<hr>
						<table class="packet_list" width="100%">
							<tr>
								<th>{$smarty.const.PACKET_ID}</th>
								<th>{$smarty.const.CREATED_BY}</th>
								<th>{$smarty.const.PACKET_AMOUNT}</th>
								<th>{$smarty.const.CREATED_DATETIME}</th>
								<th>{$smarty.const.FINISHED_DATETIME}</th>
							</tr>
							{foreach from=$packet_list item=packet}
							<tr>
								<td>{$packet.rma_packet_id}</td>
								<td>{$packet.customers_firstname} {$packet.customers_lastname}</td>
								<td>{$packet.amount}</td>
								<td>{$packet.create_datetime}</td>
								<td>{$packet.finished_datetime}</td>
							</tr>
							{/foreach}
						</table>
					</div>
				</td>
			</tr>
		</table>
		<!-- footer //-->
		{php} require(DIR_WS_INCLUDES . 'footer.php'); {/php}
		<!-- footer_eof //-->
	</body>
</html>