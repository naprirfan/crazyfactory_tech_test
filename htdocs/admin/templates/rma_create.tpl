<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html {$smarty.const.HTML_PARAMS}>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.session.language_charset}">
		<title>{$smarty.const.TITLE}</title>
		<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="templates/css/reset-min-2.8.2r1.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/stylesheet.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery-ui-1.8.6.custom.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery.fancybox-1.3.4.css"/>
		{if $has_errors}
			<script type="text/javascript">
				var errors = JSON.parse('{$error}');
				var customer_id = {$smarty.post.customer_id};
				var order_id = {$smarty.post.order_id};
				var rma_items = JSON.parse('{$rma_items}');
				var causes = JSON.parse('{$causes}');
				var rma_types = JSON.parse('{$rma_types}');
				var comments = JSON.parse('{$comments}');
				var quantities = JSON.parse('{$returned_quantities}');
			</script>
		{/if}
		<script type="text/javascript" src="templates/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="templates/js/rma_create.js"></script>
		<script type="text/javascript" src="includes/general.js"></script>
		<script type="text/javascript" src="includes/javascript/categories.js"></script>
	</head>
	<body style="margin: 0; background-color: #FFFFFF">
		<!-- header //-->
		{php} require(DIR_WS_INCLUDES . 'header.php'); {/php}
		<!-- header_eof //-->
		<table style="border:none; width:100%;" cellspacing="2" cellpadding="2">
			<tr>
				<td class="columnLeft2" width="{$smarty.const.BOX_WIDTH}" valign="top">
					<table style="border: none; width: {$smarty.const.BOX_WIDTH};" cellspacing="1" cellpadding="1" class="columnLeft">
						<!-- left_navigation //-->
						{php} require(DIR_WS_INCLUDES . 'column_left.php'); {/php}
						<!-- left_navigation_eof //-->
					</table>
				</td>
				<td class="boxCenter rma" width="100%" valign="top" style="padding: 20px;">
					<form method="post" action="rma_create.php">
						<div class="heading">
							{$smarty.const.RMA_CREATE_HEADING}
						</div>
						<div class="rma_create_lists">
							{if $rma_id}
							<a href="rma.php?rma_id={$rma_id}">{$smarty.const.TO_RMA}</a>
							{/if}
							<div class="row order_number">
								<div class="left">
									{$smarty.const.ORDER_NUMBER}
								</div>
								<div class="right">
									<input type="text" name="order_number" id="order_number_field" />
								</div>
								<div class="clear"></div>
							</div>
							<div class="row customer_list">
								<div class="left">
									{$smarty.const.CHOSE_CUSTOMER}
								</div>
								<div class="right">
									<input type="text" name="customer_id" id="customer_list" />
<!--									<select id="customer_list" name="customer_id">
										{foreach from=$customers_list item=customer}
										<option value="{$customer.customers_id}">{$customer.customers_firstname} {$customer.customers_lastname}</option>
										{/foreach}
									</select>-->
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="innerHeading" style="display: none; margin-top: 10px;">
							<span>{$smarty.const.ARTICLE}</span>
							<span class="details">{$smarty.const.DETAILS}</span>
							<span class="qty">{$smarty.const.QUANTITY}</span>
						</div>
						<div class="order_products" style="display: none;"></div>
						<div id="payment_data" style="display: none;">
							<input type="hidden" name="refund" value="" />
							<div class="moneybookers product">
								<p>{$smarty.const.PAYMENT_MONEYBOOKERS}</p>
								{$smarty.const.MONEYBOOKERS_EMAIL}: <input type="text" name="moneybookers_email" value="{$moneybookers_email}" />
							</div>
							<div class="paypal product">
								<p>{$smarty.const.PAYMENT_PAYPAL}</p>
								{$smarty.const.PAYPAL_EMAIL}: <span></span>
								<input type="hidden" name="paypal_email" value="{$paypal_email}" />
							</div>
							<div class="bank_details product">
								<p>{$smarty.const.PAYMENT_BANK_DETAILS}</p>
								<table>
									<tr>
										<td>{$smarty.const.ACCOUNT_HOLDER}:</td>
										<td><input type="text" name="account_holder" value="{$accountHolder}" /></td>
									</tr>
									<tr>
										<td>{$smarty.const.ACCOUNT_NUMBER}:</td>
										<td><input type="text" name="account_number" value="{$accountNumber}" /></td>
									</tr>
									<tr>
										<td>{$smarty.const.BANK_CODE}:</td>
										<td><input type="text" name="bank_code" value="{$bankCode}" /></td>
									</tr>
									<tr>
										<td>{$smarty.const.BANK_NAME}:</td>
										<td><input type="text" name="bank_code" value="{$bankName}" /></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="button_area" style="display: none;">
							<input class="button" type="submit" value="{$smarty.const.RMA_CREATE_HEADING}" />
						</div>
						<div class="clear"></div>
					</form>
					<div>
						<a href="rma.php">{$smarty.const.BACK}</a>
					</div>
					<div class="product" id="product_dummy" style="display: none;">
						<div class="rma_checkbox">
							<input type="checkbox" name="rma_item[]" value="" />
						</div>
						<div id="product_thumb_image"></div>
						<div class="article">
							<strong></strong><br /><br />
							{$smarty.const.MODEL} <span id="model"></span>
						</div>
						<div class="attributes">
							<table width="160" style="text-align:left;">
								<tr class="dummy">
									<td width="130"><b></b></td>
									<td></td>
								</tr>
							</table>
							<div class="info_images"></div>
						</div>
						<div class="qty"></div>
						<div class="price"></div>
						<div class="clear"></div>
						<div class="info" style="display: none;">
							<span class="quantity" style="display: none;">
								<strong>{$smarty.const.QUANTITY}:</strong> <input type="text" name="returned_quantity[]" value="1" size="5" /><br />
							</span>
							<strong>{$smarty.const.CAUSE}</strong>
							<select class="rma_cause" name="cause[]">
								<option value="0">{$smarty.const.CAUSE_DEFAULT}</option>
								<option value="no_cause">{$smarty.const.CAUSE_NO_CAUSE}</option>
								<option value="quality">{$smarty.const.CAUSE_QUALITY}</option>
								<option value="damaged_by_carrier">{$smarty.const.CAUSE_DAMAGED_BY_CARRIER}</option>
								<option value="switcheroo">{$smarty.const.CAUSE_SWITCHEROO}</option>
								<option value="unwanted">{$smarty.const.CAUSE_UNWANTED}</option>
								<option value="unauthorised">{$smarty.const.CAUSE_UNAUTHORISED}</option>
								<option value="bad_description">{$smarty.const.CAUSE_BAD_DESCRIPTION}</option>
								<option value="item_missing">{$smarty.const.CAUSE_ITEM_MISSING}</option>
							</select><br />
							<strong>{$smarty.const.RMA_TYPE}</strong><br />
							<input type="radio" class="rma_type" name="rma_type" id="replacement" value="replacement" checked="checked"><label for="replacement">{$smarty.const.TYPE_REPLACEMENT}</label><br />
							<input type="radio" class="rma_type" name="rma_type" id="refund" value="refund"><label for="refund">{$smarty.const.TYPE_REFUND}</label><br />
							<input type="radio" class="rma_type" name="rma_type" id="refund_credit_voucher" value="refund_credit_voucher"><label for="refund_credit_voucher">{$smarty.const.TYPE_REFUND_CREDIT_VOUCHER}</label>
							<input type="hidden" name="combinedAttributes" id="combinedAttributes" />
							<input type="hidden" name="products" id="products_id" />
						</div>
					</div>
				</td>
			</tr>
		</table>
		<!-- footer //-->
		{php} require(DIR_WS_INCLUDES . 'footer.php'); {/php}
		<!-- footer_eof //-->
	</body>
</html>