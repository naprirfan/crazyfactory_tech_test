<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html {$smarty.const.HTML_PARAMS}>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.session.language_charset}">
		<title>{$smarty.const.TITLE}</title>
		<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="templates/css/reset-min-2.8.2r1.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/stylesheet.css"/>
	</head>
	<body style="margin: 0; background-color: #FFFFFF">
		<!-- header //-->
		{php} require(DIR_WS_INCLUDES . 'header.php'); {/php}
		<!-- header_eof //-->
		<table style="border:none; width:100%;" cellspacing="2" cellpadding="2">
			<tr>
				<td class="columnLeft2" width="{$smarty.const.BOX_WIDTH}" valign="top">
					<table style="border: none; width: {$smarty.const.BOX_WIDTH};" cellspacing="1" cellpadding="1" class="columnLeft">
						<!-- left_navigation //-->
						{php} require(DIR_WS_INCLUDES . 'column_left.php'); {/php}
						<!-- left_navigation_eof //-->
					</table>
				</td>
				<td class="boxCenter rma" width="100%" valign="top" style="padding: 20px;">
					<div class="heading">
						{$smarty.const.RMA_HEADING} <a class="button" href="rma_refund.php?show=">{$smarty.const.REFUND_ARCHIVE_LINK_BACK}</a>
					</div>
					<hr>
					<div class="refund_list">
						<table width="100%">
							<tr>
								<th>{$smarty.const.RMA_TABLE_HEADING_DATE}</th>
								<th>{$smarty.const.RMA_TABLE_HEADING_USER_CREATED}</th>
								<th>{$smarty.const.RMA_TABLE_HEADING_SUM}</th>
								<th>{$smarty.const.RMA_TABLE_HEADING_AMOUNT}</th>
								<th></th>
							</tr>
							{foreach from=$refunds item=refund}
							<tr>
								<td>{$refund.datetime_created}</td>
								<td>{$refund.firstname} {$refund.lastname}</td>
								<td>{$refund.refund_sum}</td>
								<td>{$refund.refund_count}</td>
								<td>
									<a href="rma_refund.php?show=archive&amp;reexport={$refund.rma_refund_id}">{$smarty.const.DTA_REEXPORT}</a>
								</td>
							</tr>
							{/foreach}
						</table>
					</div>
				</td>
			</tr>
		</table>
		<!-- footer //-->
		{php} require(DIR_WS_INCLUDES . 'footer.php'); {/php}
		<!-- footer_eof //-->
	</body>
</html>