<div>
	<input type="hidden" id="actionshot_id" />
	<label>
		<div>The customer will receive an email with a denial reason.<br />Please choose the appropiate reason below:</div>
		<select size="1">
			{foreach from=$denial_reasons item=le key=le_key}
				<option value="{$le_key|htmlentities}">{$le|htmlentities}</option>
			{/foreach}
		</select>
	</label>
</div>