<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html {$smarty.const.HTML_PARAMS}>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.session.language_charset}">
		<title>{$smarty.const.TITLE}</title>
		<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="templates/css/reset-min-2.8.2r1.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/stylesheet.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery-ui-1.8.6.custom.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery.fancybox-1.3.4.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery.ui.stars.min.css"/>
		<script type="text/javascript" src="templates/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="templates/js/jquery.ui.stars.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery.qtip-1.0.0-rc3.min.js"></script>
		<script type="text/javascript" src="templates/js/actionshots.js"></script>
		<script type="text/javascript" src="includes/general.js"></script>
		<script type="text/javascript" src="includes/javascript/categories.js"></script>
	</head>
	<body style="margin: 0; background-color: #FFFFFF">

		<div id="spiffycalendar" class="text"></div>
		<!-- header //-->
		{php} require(DIR_WS_INCLUDES . 'header.php'); {/php}
		<!-- header_eof //-->

		<!-- body //-->
		<table style="border:none; width:100%;" cellspacing="2" cellpadding="2">
			<tr>
				<td class="columnLeft2" width="{$smarty.const.BOX_WIDTH}" valign="top">
					<table style="border: none; width: {$smarty.const.BOX_WIDTH};" cellspacing="1" cellpadding="1"
						   class="columnLeft">
						<!-- left_navigation //-->
						{php} require(DIR_WS_INCLUDES . 'column_left.php'); {/php}
						<!-- left_navigation_eof //-->
					</table>
				</td>
				<!-- body_text //-->
				<td class="boxCenter" width="100%" valign="top" style="padding: 20px;">
					{* hÃÂÃÂ¤sslicher xtC admin ENDE *}



					<div id="ajax_blocker" class="ajax_blocker"></div>

					{if $jpegtran_error || $convert_error}
					<div style="margin-bottom: 5px;">
						<span>jpegtran version:</span>
						{if $jpegtran_error}
							<span style="color: red; font-weight: bold;">{$jpegtran_error}</span>
						{else}
							<span>{$jpegtran_version}</span>
						{/if}
					</div>
					<div style="margin-bottom: 20px;">
						<span>imagemagick version:</span>
						{if $convert_error}
							<span style="color: red; font-weight: bold;">{$convert_error}</span>
						{else}
							<span>{$convert_version}</span>
						{/if}
					</div>
					{/if}

					{if !$jpegtran_error && !$convert_error}
						<div id="actionshots">
							<ul>
								<li><a href="#tabs-0">Neue ActionShots (<span></span>)</a></li>
								<li><a href="#tabs-1">Gemeldete ActionShots (<span></span>)</a></li>
								<li><a href="#tabs-2">Alle ActionShots (<span></span>)</a></li>
								{foreach name=specials_tabs" from=$special_categories item=category key=tab_name}
								<li><a title="{$category}" href="#tabs-{$smarty.foreach.specials_tabs.iteration+2}">{$tab_name} (<span></span>)</a></li>
								{/foreach}
							</ul>
							<input type="hidden" id="loading_text" value="Lade..." />
							<div id="tabs-0"></div>
							<div id="tabs-1"></div>
							<div id="tabs-2"></div>
							{foreach name=specials_contents from=$special_categories item=special_category key=name}
							<div id="tabs-{$smarty.foreach.specials_contents.iteration+2}"></div>
							{/foreach}
						</div>
					{/if}


					{* hÃÂÃÂ¤sslicher xtC admin ANFANG *}

					<!-- close tables from above modules //-->
				</td>
				<!-- body_text_eof //-->
			</tr>
		</table>
		<!-- body_eof //-->

		<!-- footer //-->
		{php} require(DIR_WS_INCLUDES . 'footer.php'); {/php}
		<!-- footer_eof //-->
	</body>
</html>