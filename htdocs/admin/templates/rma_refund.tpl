<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html {$smarty.const.HTML_PARAMS}>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.session.language_charset}">
		<title>{$smarty.const.TITLE}</title>
		<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="templates/css/reset-min-2.8.2r1.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/stylesheet.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery-ui-1.8.6.custom.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery.fancybox-1.3.4.css"/>
		<script type="text/javascript" src="templates/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="templates/js/rma_refund.js"></script>
		<script type="text/javascript" src="includes/general.js"></script>
		<script type="text/javascript" src="includes/javascript/categories.js"></script>
	</head>
	<body style="margin: 0; background-color: #FFFFFF">
		<!-- header //-->
		{php} require(DIR_WS_INCLUDES . 'header.php'); {/php}
		<!-- header_eof //-->
		<table style="border:none; width:100%;" cellspacing="2" cellpadding="2">
			<tr>
				<td class="columnLeft2" width="{$smarty.const.BOX_WIDTH}" valign="top">
					<table style="border: none; width: {$smarty.const.BOX_WIDTH};" cellspacing="1" cellpadding="1"
						   class="columnLeft">
						<!-- left_navigation //-->
						{php} require(DIR_WS_INCLUDES . 'column_left.php'); {/php}
						<!-- left_navigation_eof //-->
					</table>
				</td>
				<td class="boxCenter rma" width="100%" valign="top" style="padding: 20px;">
					<div class="heading">
						{$smarty.const.RMA_HEADING}
					</div>
					<a class="button" href="rma_refund.php?show=archive">{$smarty.const.REFUND_ARCHIVE}</a>
					<!-- wtf? no function?
					<div class="search">
						<form method="post" action="rma_refund.php">
							{$smarty.const.RMA_SEARCH}:<br />
							<input type="text" name="rma_id" id="rma_search" />
							<input class="button" type="submit" name="search" value="{$smarty.const.SEARCH}" />
						</form>
					</div>
					-->
					<br />
					<div class="refund">
					<table width="500px">
					<tr><td>
						<form method="post" action="rma_refund.php">
						{$smarty.const.TEXT_SELECT_REFUND_TYPE}<br />
							{html_options name='refund_type' options=$refund_type selected=$selected_refund}
						</form>
						</td>
						<td>
						{$smarty.const.TEXT_OPEN_PAYMENT_LINK}<br />
						<a class="button" href="http://www.paypal.com" target="_blank">{$smarty.const.TEXT_LINK_PAYPAL}</a>
						<a class="button" href="http://www.moneybookers.com" target="_blank">{$smarty.const.TEXT_LINK_MONEYBOOKERS}</a><br />
						</td>
						</tr>
						</table>
					</div>
					<hr>
					{if $rma_list}
					<div class="list">
						<script type="text/javascript">
							var selected_refund = '{$selected_refund}';
						</script>
						{if $selected_refund == 'bank' || $smarty.post.refund_type == 'moneybookers'}
						<form method="post" action="rma_refund.php">
						{/if}
							{if $selected_refund == 'bank'}
							<div class="heading">
								<div class="transfered">&nbsp;</div>
								<div class="rma_id" style="width: 85px;">{$smarty.const.RMA_ID}</div>
								<div class="order_id" style="width: 85px;">{$smarty.const.ORDER_ID}</div>
								<div class="order_id" style="width: 85px;">{$smarty.const.CUSTOMER_ID}</div>
								<div class="account_holder">{$smarty.const.ACCOUNT_HOLDER}</div>
								<div class="account_number">{$smarty.const.ACCOUNT_NUMBER}</div>
								<div class="bank_code">{$smarty.const.BANK_CODE}</div>
								<div class="bank_name">{$smarty.const.BANK_NAME}</div>
								<div class="amount">{$smarty.const.REFUND_AMOUNT}</div>
								<div class="subject">{$smarty.const.SUBJECT}</div>
								<div class="clear"></div>
							</div>
							{foreach from=$rma_list item=rma}
							<div class="rma_entry" style="background-color:{cycle values="#eeeeee,#F7F7F7"};">
								<div class="transfered">
									<input type="checkbox" name="rma_refund[]" value="{$rma.rma_id}{foreach from=$rma.rma_ids item='rma_id'},{$rma_id}{/foreach}" />
								</div>
								<div class="rma_id" style="width: 85px;">
									<a href="rma.php?rma_id={$rma.rma_id}" target="_blank">{$rma.rma_id}</a>
									{foreach from=$rma.rma_ids item='rma_id'}, <a href="rma.php?rma_id={$rma_id}" target="_blank">{$rma_id}</a>{/foreach}
								</div>
								<div class="orders_id" style="width: 85px;">
									<a href="orders.php?oID={$rma.orders_id}&action=edit" target="_blank">{$rma.orders_id}</a>
									{foreach from=$rma.order_ids item='order_id'}, <a href="orders.php?oID={$order_id}&amp;action=edit" target="_blank">{$order_id}</a>{/foreach}
								</div>
								<div class="customers_id" style="width: 85px;"><a href="customers.php?cID={$rma.customers_id}&action=edit" target="_blank">{$rma.customers_id}</a></div>
								<div class="account_holder"><input type="text" value="{$rma.account_holder}" readonly="readonly" /></div>
								<div class="account_number"><input type="text" value="{$rma.account_number}" readonly="readonly" /></div>
								<div class="bank_code"><input type="text" value="{$rma.bank_code}" readonly="readonly" /></div>
								<div class="bank_name"><input type="text" value="{$rma.bank_name}" readonly="readonly" /></div>
								<div class="amount"><input type="text" style="text-align: right;" value="{$rma.sum_formated}" readonly="readonly" /></div>
								<div class="subject"><input type="text" value="{$rma.subject}" readonly="readonly" /></div>
								<div class="clear"></div>
							</div>
							{/foreach}
							{elseif $selected_refund == 'paypal' }
							<div class="heading">
								<div class="rma_id" style="width: 85px; margin-left: 23px;">{$smarty.const.RMA_ID}</div>
								<div class="order_id" style="width: 85px;">{$smarty.const.ORDER_ID}</div>
								<div class="email" style="width: 150px;">{$smarty.const.EMAIL}</div>
								<div class="amount">{$smarty.const.REFUND_AMOUNT}</div>
								<div class="clear"></div>
							</div>
							{foreach from=$rma_list item=rma}
							<div class="rma_entry" style="height:18px;background-color:{cycle values="#eeeeee,#F7F7F7"};">
								<form method="post" action="rma_refund.php">
								<div class="rma_id" style="width: 85px;">
									<input type="hidden" name="direct_paypal_refund" value="{$rma.rma_id}" />
									<a href="rma.php?rma_id={$rma.rma_id}" target="_blank">{$rma.rma_id}</a>
								</div>
								<div class="orders_id" style="width: 85px;"><a href="orders.php?oID={$rma.orders_id}&action=edit" target="_blank">{$rma.orders_id}</a></div>
								<div class="email" style="width: 150px;"><input type="text" style="width: 150px;" value="{$rma.payment_email}" readonly="readonly" /></div>
								<div class="amount"><input type="text" name="amount" style="text-align: right;" value="{$rma.sum}" readonly="readonly" /></div>
								<div class="currency"><input type="hidden" name="currency" style="text-align: right;" value="{$rma.currency}"  />{$rma.currency}</div>
								<div ><input type="submit" value="go API!" /></div>
								</form>
							</div>
							{/foreach}
							{else}
							<div class="heading">
								<div class="rma_id" style="width: 85px; margin-left: 23px;">{$smarty.const.RMA_ID}</div>
								<div class="order_id" style="width: 85px;">{$smarty.const.ORDER_ID}</div>
								<div class="email" style="width: 150px;">{$smarty.const.EMAIL}</div>
								<div class="amount">{$smarty.const.REFUND_AMOUNT}</div>
								<div class="clear"></div>
							</div>
							{foreach from=$rma_list item=rma}
							<div class="rma_entry">
								<div class="checkbox"><input type="checkbox" name="rma_refund" value="{$rma.rma_id}" /></div>
								<div class="rma_id" style="width: 85px;"><a href="rma.php?rma_id={$rma.rma_id}" target="_blank">{$rma.rma_id}</a></div>
								<div class="orders_id" style="width: 85px;"><a href="orders.php?oID={$rma.orders_id}&action=edit" target="_blank">{$rma.orders_id}</a></div>
								<div class="email" style="width: 150px;"><input type="text" style="width: 150px;" value="{$rma.payment_email}" readonly="readonly" /></div>
								<div class="amount"><input type="text" style="text-align: right;" value="{$rma.sum}" readonly="readonly" /></div>
								<div class="currency">{$rma.currency}</div>
								<div class="clear"></div>
							</div>
							{/foreach}
							{/if}
							<div>
								{if $smarty.post.refund_type == 'bank' || $smarty.post.refund_type == ''}
									<input type="submit" id="dta_export" name="dta_export" value="{$smarty.const.DTA_EXPORT}" />
								{/if}
								<input type="hidden" name="refund_type" value="{if $smarty.post.refund_type}{$smarty.post.refund_type}{else}bank{/if}" />
								{if $smarty.post.refund_type != 'paypal'}
									<input type="submit" id="save" value="{$smarty.const.SET_TRANSFERED}" />
								{/if}
							</div>
						{if $selected_refund == 'bank' || $smarty.post.refund_type == 'moneybookers'}
						</form>
						{/if}
					</div>
					{else}
					<div class="nothing_to_do">
						{$nothing_to_do}
					</div>
					{/if}
				</td>
			</tr>
		</table>
		<!-- footer //-->
		{php} require(DIR_WS_INCLUDES . 'footer.php'); {/php}
		<!-- footer_eof //-->
	</body>
</html>