<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html {$smarty.const.HTML_PARAMS}>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.session.language_charset}">
		<title>{$smarty.const.TITLE}</title>
		<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="templates/css/reset-min-2.8.2r1.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/stylesheet.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery-ui-1.8.6.custom.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery.fancybox-1.3.4.css"/>
	</head>
	<body style="margin: 0; background-color: #FFFFFF">
		<!-- header //-->
		{php} require(DIR_WS_INCLUDES . 'header.php'); {/php}
		<!-- header_eof //-->
		<table style="border:none; width:100%;" cellspacing="2" cellpadding="2">
			<tr>
				<td class="columnLeft2" width="{$smarty.const.BOX_WIDTH}" valign="top">
					<table style="border: none; width: {$smarty.const.BOX_WIDTH};" cellspacing="1" cellpadding="1" class="columnLeft">
						<!-- left_navigation //-->
						{php} require(DIR_WS_INCLUDES . 'column_left.php'); {/php}
						<!-- left_navigation_eof //-->
					</table>
				</td>
				<td class="boxCenter rma" width="100%" valign="top" style="padding: 20px;">
					<div class="heading">
						{$smarty.const.RMA_HEADING}
					</div>
					<div class="rma_locations">
						<form method="post" action="rma.php">
							{$smarty.const.RMA_LOCATIONS}
							<select name="location">
								<option value="">-</option>
								{foreach from=$barcode_printers item='printer' key='location'}
									<option value="{$location}">{$printer.name}</option>
								{/foreach}
							</select>
							<input type="submit" value="{$smarty.const.SET_LOCATION}" />
						</form>
					</div>
				</td>
			</tr>
		</table>
		<!-- footer //-->
		{php} require(DIR_WS_INCLUDES . 'footer.php'); {/php}
		<!-- footer_eof //-->
	</body>
</html>