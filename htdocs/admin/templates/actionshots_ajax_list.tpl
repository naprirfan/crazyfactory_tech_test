{if $show_search}
<div class="search">
	<input id="search_input" type="text" value="{$search_keyword}">
	<label>
		<input type="radio" name="search_type" value="products_id" {if $search_type == 'products_id'}checked="checked"{/if} /><span> Product ID</span>
	</label>
	<label>
		<input type="radio" name="search_type" value="actionshot_id" {if $search_type == 'actionshot_id'}checked="checked"{/if} /><span> Actionshot ID</span>
	</label>
	<label>
		<input type="radio" name="search_type" value="customers_id" {if $search_type == 'customers_id'}checked="checked"{/if} /><span> Customer ID</span>
	</label>
	<input type="button" value="Search..." onclick="search_filter()" />
</div>
{/if}

{if isset($page_count) && $page_count != 0}

<div class="pagination">
	Page:
{section name=pagination start=1 loop=$page_count+1 step=1}
	{if in_array($smarty.section.pagination.index, array(1, $current_page - 1, $current_page, $current_page + 1, $page_count))}
		<span {if $smarty.section.pagination.index == $current_page}class="current"{/if} onclick="load_all($(this).text())">{$smarty.section.pagination.index}</span>
	{elseif in_array($smarty.section.pagination.index, array(2, $current_page + 2))}
		<span>...</span>
	{/if}
{/section}
</div>

{/if}

{assign var="last_products_id" value=false}
{assign var="last_customers_id" value=false}
{foreach from=$array_actionshots item=le}

{if $last_products_id != $le.products_id || $last_customers_id != $le.customers_id}
	{if $highlight_class == 'highlighted'}
		{assign var="highlight_class" value=""}
	{else}
		{assign var="highlight_class" value="highlighted"}
	{/if}
{/if}

{assign var="now" value=$smarty.now}

{* Bild zum preloaden *}
<img src="actionshots.php?actionshot_id={$le.actionshot_id|urlencode}&amp;{$now}&amp;fancybox_workaround=.jpg" style="position: absolute; left: -1000px; top: -1000px;" alt="preload" />

<div class="actionshot {$highlight_class}" id="actionshot_{$le.actionshot_id}">
	<input type="hidden" class="actionshot_id" value="{$le.actionshot_id|htmlentities}" />
	<div class="image">
		<div class="rotate_left" onclick="rotate('left', {$le.actionshot_id|htmlentities});"></div>
		<div class="rotate_right" onclick="rotate('right', {$le.actionshot_id|htmlentities});"></div>
		{strip}
			<table>
				<tr>
					<td style="vertical-align: middle; text-align: center;">
						<a href="actionshots.php?actionshot_id={$le.actionshot_id|urlencode}&amp;{$now}&amp;fancybox_workaround=.jpg">
							<img src="actionshots.php?actionshot_id={$le.actionshot_id|urlencode}&amp;thumb=true&amp;{$now}" alt="" />
						</a>
					</td>
				</tr>
			</table>
		{/strip}
	</div>
	<div class="description">
		<div style="float: left; position: relative;">
			<label>
				Description:<br />
				<textarea class="description">{$le.description}</textarea>
			</label>
			<div style="height: 5px; overflow: hidden;"></div>
				{if $le.special == 'default'}
					<label for="prod_id">Products ID:</label>
					<input type="text" id="prod_id" class="products_id" onchange="set_dirty({$le.actionshot_id|htmlentities});" value="{$le.products_id}" />
				{/if}
				{if $le.further_actionshots_for_this_product}
					<div class="additional_actionshots">
					<a href="#" style="color: red;">{$le.further_actionshots_for_this_product|@count} {if $le.special == 'valentine_2011' || $le.special == 'valentine_2012'}additional valentine greeting(s){else}additional active actionshot(s){/if}</a>
						<div style="display: none;">
							{foreach from=$le.further_actionshots_for_this_product item=le2}
							<img src="actionshots.php?actionshot_id={$le2|urlencode}&amp;thumb=true&amp;{$now}" alt="" />
							{/foreach}
						</div>
					</div>
				{/if}
		</div>
		<div style="float: left; margin-left: 10px;">
			<div>Customer: {$le.customers_firstname} {$le.customers_lastname} ({$le.customers_id})</div>
			<div>Uploaded: {$le.upload_time}</div>
			{if $le.special == 'valentine_2011' || $le.special == 'valentine_2012'}
			<div>Wait for Valentine: {if $le.wait_for_valentines_day}yes{else}no{/if}</div>
			<div>Emp&auml;nger: {$le.recipients}</div>
			{/if}
			<div {if $le.dimensions_too_small}style="color: red;"{/if}>Dimensions: {$le.dimensions}</div>
			{if $le.special == 'default'}
			<div style="overflow: hidden;">
				<span style="float: left; margin-right: 5px;">Rating:</span>
				<span class="stars_wrapper">
					{section name=rating start=1 loop=11 step=1}
						<input type="radio" name="rating" value="{$smarty.section.rating.index}" {if $le.rating == $smarty.section.rating.index}checked="checked"{/if} />
					{/section}
				</span>
			</div>
			{/if}
			<div style="margin-top: 5px;">
				<label>
					<input type="checkbox" name="active" {if $le.active}checked="checked"{/if} />
					active
				</label>
				{if $show_search}
				<br/>
				<label>
					<input type="checkbox" name="deleted" {if $le.deleted}checked="checked"{/if} />
					deleted
				</label>
				{/if}
			</div>
			{if $le.confirmed_by_name}
				<div>Confirmed by: {$le.confirmed_by_name}</div>
			{/if}
			<br />
			<div class="save_button" onclick="update({$le.actionshot_id|htmlentities})">Click here to save!</div>
		</div>
		<div style=" margin-left: 10px;">
			<span class="delete_button" onclick="remove_actionshot({$le.actionshot_id|htmlentities});">Click here to <i>DELETE</i>!</span>
		</div>
	</div>
	{if $le.special == 'default'}
	<div class="product">
		<div>{$le.products_name}</div>
		<img src="{$le.products_image}" height="75" width="75" />
	</div>
	{/if}




</div>
{assign var="last_products_id" value=$le.products_id}
{assign var="last_customers_id" value=$le.customers_id}
{/foreach}