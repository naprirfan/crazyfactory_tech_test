{if $mode == 'themes'}
	{foreach from=$themes item=theme}
	<div class="list_entry">
		<div>ID {$theme.id|escape:'html'}<br /><img src="../images/voucher/themes/{$theme.id}.jpg" alt="" /></div>
		<table>
			<tr>
				<td class="selection_column">
					<form class="select_category" action="">
						<input type="hidden" name="theme_id" value="{$theme.id|escape:'html'}" />
						<select>
							{foreach from=$categories item=category}
							<option value="{$category.id|escape:'html'}" {if $category.id == $theme.category_id}selected="selected"{/if}>{$category.name|escape:'html'}</option>
							{/foreach}
						</select>
						<input type="submit" value="save" />
					</form>
				</td>
				<td class="uses_column">uses: {$theme.uses|escape:'html'}</td>
				<td class="status_column">
					{if $theme.status == 'active'}
					<span class="active_theme">active</span>
					{elseif $theme.status == 'inactive'}
					<span class="inactive_theme">inactive</span>
					{else}
					<span class="pending_theme">pending</span>
					{/if}
				</td>
				<td>
					<button {if $theme.status == 'pending'}class="inactive"{else}onclick="toggle_theme({$theme.id|escape:'html'}, {if $theme.status == 'active'}false{else}true{/if})"{/if}>
						{if $theme.status == 'active'}deactivate{else}activate{/if}
					</button>
				</td>
			</tr>
		</table>
	</div>
	{/foreach}
{elseif $mode == 'categories'}
	{foreach from=$categories item=category}
	<div class="list_entry">
		<table>
			<tr>
				<td class="category_column">{$category.name|escape:'html'}</div>
				<td class="edit_column">
					<button onclick="show_category_edit_dialog({$category.id|escape:'html'})">edit</button>
				</td>
				<td class="status_column">
					{if $category.active}
					<span class="active_theme">active</span>
					{else}
					<span class="inactive_theme">inactive</span>
					{/if}
				</td>
				<td>
					<button onclick="toggle_category({$category.id|escape:'html'}, {if $category.active}false{else}true{/if})">
						{if $category.active}deactivate{else}activate{/if}
					</button>
				</td>
			</tr>
		</table>
	</div>
	{/foreach}
{/if}