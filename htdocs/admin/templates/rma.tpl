<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html {$smarty.const.HTML_PARAMS}>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.session.language_charset}">
		<title>{$smarty.const.TITLE}</title>
		<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="templates/css/reset-min-2.8.2r1.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/stylesheet.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery-ui-1.8.6.custom.css"/>
		<link rel="stylesheet" type="text/css" href="templates/css/jquery.fancybox-1.3.4.css"/>
		<script type="text/javascript" src="templates/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script type="text/javascript" src="templates/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="templates/js/rma.js"></script>
		<script type="text/javascript" src="includes/general.js"></script>
		<script type="text/javascript" src="includes/javascript/categories.js"></script>
	</head>
	<body style="margin: 0; background-color: #FFFFFF">
		<!-- header //-->
		{php} require(DIR_WS_INCLUDES . 'header.php'); {/php}
		<!-- header_eof //-->
		<table style="border:none; width:100%;" cellspacing="2" cellpadding="2">
			<tr>
				<td class="columnLeft2" width="{$smarty.const.BOX_WIDTH}" valign="top">
					<table style="border: none; width: {$smarty.const.BOX_WIDTH};" cellspacing="1" cellpadding="1" class="columnLeft">
						<!-- left_navigation //-->
						{php} require(DIR_WS_INCLUDES . 'column_left.php'); {/php}
						<!-- left_navigation_eof //-->
					</table>
				</td>
				<td class="boxCenter rma" width="100%" valign="top" style="padding: 20px;">
					<div id="rma_location" style="float: right; margin-top: -15px; margin-right: -10px;">
						{$smarty.const.RMA_LOCATION} {$barcode_printers[$smarty.cookies.rma_location].name}
						(<a href="rma.php?change=location">{$smarty.const.CHANGE}</a>)
					</div>
					<div class="heading">
						{$smarty.const.RMA_HEADING}
					</div>
					<div class="rma_list">
						<div class="create_rma">
							<a href="rma_create.php">{$smarty.const.CREATE_RMA}</a>
						</div>
						<div class="search">
							<form method="post" action="rma.php">
								{$smarty.const.RMA_SEARCH}: ({$smarty.const.INFO_SEARCH})<br />
								<input type="text" name="rma_id" id="rma_search" />
								<input type="submit" name="search" value="{$smarty.const.SEARCH}" />
							</form>
						</div>
						{if $nothing_found}
						<div class="error" style="width: 400px;">
							{$nothing_found}
						</div>
						{/if}
						{if $list}
						<hr>
						<div class="list">
							<div class="heading">
								<div class="rma_id">{$smarty.const.RMA_ID}</div>
								<div class="customer">{$smarty.const.CUSTOMERS_NAME}</div>
								<div class="status">{$smarty.const.CURRENT_STATUS}</div>
								<div class="date_created">{$smarty.const.DATE_CREATED}</div>
								<div class="date_edited">{$smarty.const.DATE_EDITED}</div>
								<div class="edited_by">{$smarty.const.DATE_EDITED}</div>
								<div class="clear"></div>
							</div>
							{foreach from=$list item=rma}
							<div class="rma_entry">
								<div class="rma_id">
									<a href="{$rma.link}">{$rma.rma_id}</a>
								</div>
								<div class="customer">{$rma.customers_firstname} {$rma.customers_lastname}</div>
								<div class="status">{$rma.rma_status}</div>
								<div class="date_created">{$rma.date_created}</div>
								<div class="date_edited">{if $rma.date_edited}{$rma.date_edited}{else}-{/if}</div>
								<div class="edited_by">{$rma.edited_by_name}</div>
								<div class="clear"></div>
							</div>
							{/foreach}
						</div>
						{/if}
					</div>
					<div class="rma_bag_item_list">
						<div class="heading">{$smarty.const.RMA_BAG} {$current_bag.bag_id}</div>
						<div class="list">
							<div class="heading">
								<div class="rma_id">{$smarty.const.RMA_ID}</div>
								<div class="rma_item_count">{$smarty.const.ITEM_COUNT}</div>
								<div class="clear"></div>
							</div>
							{foreach from=$current_bag.data item=bag_items}
							<div class="rma_bag_item_entry">
								<div class="rma_id">{$bag_items.rma_id}</div>
								<div class="rma_item_count">{$bag_items.item_count}</div>
								<div class="clear"></div>
							</div>
							{foreachelse}
								{$smarty.const.EMPTY_BAG}
							{/foreach}
						</div>
						<form method="post" action="rma.php">
							<input type="submit" value="{$smarty.const.FINISH_BAG}" />
							<input type="hidden" name="bag_id" value="{$current_bag.bag_id}" />
						</form>
						<div class="clear"></div>
						<hr>
						<div class="heading">{$smarty.const.LAST_BAGS}</div>
						<div class="bag_list">
							<div class="heading">
								<div class="bag_id">{$smarty.const.RMA_BAG}</div>
								<div class="packet_id">{$smarty.const.PACKET_ID}</div>
								<div class="finished_datetime">{$smarty.const.FINISHED_DATETIME}</div>
								<div class="reprint"></div>
								<div class="clear"></div>
							</div>
							{foreach from=$last_bags item=bag}
							<div class="rma_bag_entry">
								<div class="bag_id">{$bag.rma_bag_id}</div>
								<div class="packet_id">{$bag.rma_packet_id}</div>
								<div class="finished_datetime">{$bag.finished_datetime}</div>
								{if $bag.finished_datetime != '-' && $barcode_printers[$smarty.cookies.rma_location].url}
								<div class="reprint">
									<a href="{$barcode_printers[$smarty.cookies.rma_location].url}?bag_id={$bag.rma_bag_id}">{$smarty.const.PRINT_BARCODE}</a>
								</div>
								{/if}
								<div class="clear"></div>
							</div>
							{/foreach}
						</div>
					</div>
					<div class="clear"></div>
				</td>
			</tr>
		</table>
		<!-- footer //-->
		{php} require(DIR_WS_INCLUDES . 'footer.php'); {/php}
		<!-- footer_eof //-->
	</body>
</html>