$(function() {
	$('#rma_search').focus();
	$('.rma').find('input[type=radio]').click(function() {
		if ($(this).val() == 'incomplete')
			$(this).parent().parent().children('input[type=text]').show();
		else
			$(this).parent().parent().children('input[type=text]').hide();
	});
	$('.rma').find('input[type=radio]:checked').click();
	$('#other_rma_list').change(function() {
		if ($(this).val())
		{
			$.getJSON('ajax_check_rma_editing.php', {rma_id: $(this).val()}, function(response) {
				if (response.edited)
				{
					if (confirm(response.text))
						window.location.href = response.link;
				}
				else
					window.location.href = response.link;
			});
		}
	});
	$('#rma_search').parent().submit(function(e) {
		if ($(this).children('#rma_search').val())
		{
			e.preventDefault();
			$.getJSON('ajax_check_rma_editing.php', {rma_id: $(this).children('#rma_search').val()}, function(response) {
				if (response.edited)
				{
					if (confirm(response.text))
						window.location.href = response.link;
				}
				else
					window.location.href = response.link;
			});
		}
	});
	$('.list').children('.rma_entry').children('.rma_id').children().click(function(e) {
		e.preventDefault();
		$.getJSON('ajax_check_rma_editing.php', {rma_id: $(this).text()}, function(response) {
			if (response.edited)
			{
				if (confirm(response.text))
					window.location.href = response.link;
			}
			else
				window.location.href = response.link;
		});
	});
	var buttons = $('#rma_edit_form').children('.product').children('.buttons');
	buttons.children('label').children('input').hide();
	buttons.children('label').children('input').before('<img src="../templates/cf2010/img/rma_unselected.png" />');
	buttons.children('label').children('input[value="missing"]').attr('src', '../templates/cf2010/img/rma_selected.png');
	buttons.children('label').click(function() {
		$(this).children('input').attr('checked', 'checked');
		buttons.children('label').children('input').change();
	});
	buttons.children('label').children('input').change(function() {
		if ($(this).is(':checked'))
			$(this).prev().attr('src', '../templates/cf2010/img/rma_selected.png');
		else
			$(this).prev().attr('src', '../templates/cf2010/img/rma_unselected.png');
	});
	buttons.children('label').children('input').change();
	$('.itemlist').children('.product').each(function() {
		$(this).children('.info').children('.rma_type').each(function() {
			$(this).click(function() {
				if ($(this).attr('value') == 'refund')
				{
					$('#payment_data').show();
					$('#has_refund').val(1);
				}

				check_payments();
			});
		});
	});
	$('#rma_edit_form').children('.product').each(function() {
		$(this).children('.info').children('.rma_type').each(function() {
			$(this).click(function() {
				if ($(this).attr('value') == 'refund')
				{
					$('#rma_edit_payment_data').show();
					$('#has_refund').val(1);
				}

				var refund = 0;
				$('#rma_edit_form').children('.product').each(function() {
					if ($(this).children('.info').children('.rma_type:checked').val() == 'refund')
						refund++;
				});

				if (!refund)
				{
					$('#rma_edit_payment_data').hide();
					$('#has_refund').val(0);
				}
			});
		});
	});
	$('#rma_edit_form').find('input[type=text]').keypress(function(e) {
		if (e.which == 13)
		{
			e.preventDefault();
			$('#rma_edit_form').find('.button_area').children('input[name=save]').click();
		}
	});
});

function check_payments()
{
	var checkboxes = $('.itemlist').find('input[type=checkbox]');
	var unchecked, checked, refund = 0;

	for (var i = 0; i < checkboxes.length; i++)
	{
		var checkbox = $(checkboxes[i]);
		if (checkbox.attr('checked'))
		{
			checked++;

			if (checkbox.parents('.product').children('.info').children('.rma_type:checked').val() == 'refund')
				refund++;
		}
		else
			unchecked++;
	}

	if (i + 1 == unchecked || !refund)
	{
		$('#payment_data').hide();
		$('#has_refund').val('');
	}
}