$(function() {
	$('.refund').find('form').children('select').change(function() {
		$(this).parent().submit();
	});
	$('.list').find('input[type=text]').focus(function() {
		$(this).select();
	});
	$('.list').children('form').submit(function(e) {
		if (!$('.list').children('form').data('sent'))
			e.preventDefault();

		if ($('.list').children('form').data('sent') == 'dta')
			window.setTimeout('location.reload()', 1000);
	});
	$('#dta_export').click(function() {
		var checkboxes = $('.list').find('input[type=checkbox]:checked');
		if (checkboxes.length)
		{
			$('.list').children('form').data('sent', 'dta');
		}
	});
	$('#save').click(function() {
		var checkboxes = $('.list').find('input[type=checkbox]:checked');
		if (checkboxes.length)
		{
			$('.list').children('form').data('sent', 'save');
		}
	});

	if (selected_refund != 'bank')
	{
		$('.checkbox').children('input').click(function() {
			var parent = $(this).parent().parent();
			$.get('ajax_rma_mark_refund.php', {'rma_id': parent.children('.rma_id').text()}, function(response) {
				if (response == 'done')
				{
					parent.children('.checkbox').children().attr('disabled', 'disabled');
					parent.addClass('refund_done');
				}
			});
		});
	}
});