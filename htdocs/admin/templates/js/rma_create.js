$(function() {
	$('#customer_list').change(function() {
		$('.order_products').hide();
		$('.innerHeading').hide();
		$('.button_area').hide();
		$('.order_products').children().remove();

		if ($(this).val() != '0' && (typeof $(this).data('old_value') == 'undefined' || $(this).data('old_value') != $(this).val()))
		{
			$(this).data('old_value', $(this).val());
			$.getJSON('ajax_get_orders.php', {'customer_id': $(this).val()}, function(response) {
				if (!$('.orders_list').length)
					$('.rma_create_lists').append('<div class="row orders_list"></div>');
				var html = '';
				html += '<div class="left">' + response.chose_order_text + '</div>';
				html += '<div class="right"><select id="orders_list" name="order_id">';

				for (var key in response.data)
				{
					var row = response.data[key];
					if (row.orders_id == 0)
						html += '<option value="0">-</option>';
					else
					{
						var order_text = response.order_text.replace(/%order_id%/, row.orders_id);
						order_text = order_text.replace(/%date%/, row.date_purchased);
						html += '<option value="' + row.orders_id + '">' + order_text + '</option>';
					}
				}

				html += '</select></div>';
				html += '<div class="clear"></div>';
				$('.orders_list').html(html);

				$('#orders_list').change(function() {
					if ($(this).val() != '0')
					{
						$.getJSON('../ajax_get_order_products.php', {'order_id': $(this).val(), 'remove_old' : true}, function(response) {
							$('.rma').find('.order_products').children().remove();
							$('.rma').find('.innerHeading').show();
							$('.rma').find('.order_products').show();
							$('.rma').find('.button_area').show();

							if (response.payed_with == 'paypal')
							{
								$('#payment_data').children('.paypal').children('span').text(response.from_email);
								$('#payment_data').children('.paypal').children('input').val(response.from_email);
								$('#payment_data').children('.moneybookers').hide();
								$('#payment_data').children('.paypal').show();
								$('#payment_data').children('.bank_details').hide();
								$('#payment_data').children('input[name=refund]').val('paypal');
							}
							else if (response.payed_with.substr(0, 12) == 'moneybookers')
							{
								$('#payment_data').children('.moneybookers').show();
								$('#payment_data').children('.paypal').hide();
								$('#payment_data').children('.bank_details').hide();
								$('#payment_data').children('input[name=refund]').val('moneybookers');
							}
							else
							{
								$('#payment_data').children('.moneybookers').hide();
								$('#payment_data').children('.paypal').hide();
								$('#payment_data').children('.bank_details').show();
								$('#payment_data').children('input[name=refund]').val('bank_details');
							}

							image_path = response.image_path;
							price_once = $('#product_dummy').children('.info').children('.product_edit').children('#info').children('div').children('.select_dummy').children('.price_once').text();
							window.currency = response.currency;

							for (var i = 0; i < response.products.length; i++)
							{
								var product = response.products[i];
								var clone = $('#product_dummy').clone(true);
								clone.appendTo('.order_products');
								clone.removeAttr('id');
								clone.children('.rma_checkbox').children().attr({
									'value': product.orders_products_id,
									'name': 'rma_item[' + i + ']'
								});
								clone.children('.info').children('.quantity').children('input').attr('name', 'returned_quantity[' + i + ']');
								var cause_select = clone.children('.info').children('select.rma_cause');
								var type_select = clone.children('.info').children('input.rma_type');
								cause_select.attr('name', 'cause[' + i + ']');
								type_select.each(function() {
									$(this).attr({
										'name': 'rma_type[' + i + ']',
										'id': $(this).attr('id') + i
									});
									$(this).next().attr('for', $(this).attr('id'));

									$(this).click(function() {
										if ($(this).attr('value') == 'refund')
											$('#payment_data').show();
										check_payments();
									});
								});
								clone.children('.info').children('textarea[name=comment]').attr('name', 'comment[' + i + ']');

								cause_select.change(function() {
									if (!$(this).parent().children('.cause_amount').children('.no_cause_selected').data('has_errors'))
									{
										var cause_amount = $(this).parent().children('.cause_amount');
										if (cause_amount.children('.no_cause_selected').length)
										{
											if (cause_amount.children().length > 1)
												cause_amount.children('.no_cause_selected').remove();
											else
												cause_amount.remove();
										}
									}
									else
										$(this).parent().children('.cause_amount').children('.no_cause_selected').removeData('has_errors')
									if (!$(this).data('changed_before'))
									{
										$(this).data('changed_before', 1);
										$(this).children(':first').attr('disabled', 'disabled');
									}
								});
								clone.children('.info').children('.product_edit').attr('id', 'product_edit' + i);
								clone.children('.info').find('#products_id').val(product.products_id);
								clone.children('.info').find('#products_id').attr({
									'id': 'products_id' + i,
									'name': 'products_id[' + i + ']'
								});
								clone.children('.info').find('#combinedAttributes').attr({
									'id': 'combinedAttributes' + i,
									'name': 'combinedAttributes[' + i + ']'
								});

								clone.children('.rma_checkbox').children().change(function() {
									var parent = $(this).parent().parent();

									if ($(this).prop('checked'))
									{
										if (parent.children('.qty').data('quantity') > 1)
											parent.children('.info').children('span.quantity').show();
										parent.children('.info').show();
										if (parent.children('.info').children('select[name*=rma_type]').val() == 'refund' && $('#payment_data').css('display') == 'none')
											$('#payment_data').show();
										if (!$('.rma').data('old_height') && $('.rma').children('form').height() > $('.rma').height())
										{
											$('.rma').data('old_height', $('.rma').height());
											$('.rma').height('auto');
										}
									}
									else
									{
										parent.children('.info').hide();
										check_payments();
									}
								});

								clone.children('#product_thumb_image').append('<a href="' + product.link + '"></a>');
								clone.children('#product_thumb_image').children().append('<img src="' + image_path + product.image.src + '" />');
								clone.children('.article').children('strong').html(product.name[0]);
								clone.children('.article').children('strong').after(product.name[1]);
								clone.children('.article').children('#model').replaceWith(product.model);
//								clone.children('.article').children('#single_price').replaceWith(product.single_price);

								for (var x = 0; x < product.attributes.length; x++)
								{
									var attribute = product.attributes[x];
									var tclone = clone.children('.attributes').children('table').find('tr.dummy').clone();
									tclone.appendTo(clone.children('.attributes').children('table').children());
									tclone.removeAttr('class');
									tclone.children('td:first').children().text(attribute.name);
									tclone.children('td:last').text(attribute.value);
								}

								clone.children('.attributes').children('table').find('tr.dummy').remove();

								if (product.image.type == 'main' && product.image.info)
								{
									clone.children('.attributes').children('.info_images').append('<img src="../' + product.image.info + '" />');
									if (product.image.pin)
										clone.children('.attributes').children('.info_images').append('<img src="../' + product.image.pin + '" />');
								}
								else
									clone.children('.attributes').children('.info_images').remove();

								clone.children('.qty').text(product.quantity);
								clone.children('.qty').data('quantity', product.quantity);
								clone.children('.info').children('.quantity').children('input').keyup(function() {
									if (!$(this).parents('.product').children('.info').children('.cause_amount').children('.wrong_amount').data('has_errors'))
									{
										var cause_amount = $(this).parents('.product').children('.info').children('.cause_amount');
										if (cause_amount.children('.wrong_amount').length)
										{
											if (cause_amount.children().length > 1)
												cause_amount.children('.wrong_amount').remove();
											else
												cause_amount.remove();
										}
									}
									else
										$(this).parents('.product').children('.info').children('.cause_amount').children('.wrong_amount').removeData('has_errors')
								});
								clone.children('.price').html(product.price);

								clone.show();

								// Wenn ein Fehler aufgetreten ist, werden hier die Produkte wieder ausgew�hlt
								if (typeof rma_items != 'undefined')
								{
									for (var key in rma_items)
									{
										var item = rma_items[key];

										if (item == product.orders_products_id)
										{
											clone.children('.rma_checkbox').children().prop('checked', 'checked');
											clone.children('.rma_checkbox').children().change();

											clone.children('.info').children('.quantity').children('input').val(quantities[key]);

											if (causes[key])
												clone.children('.info').children('select.rma_cause').val(causes[key]);

											if (rma_types[key])
											{
												clone.children('.info').children('input.rma_type').each(function() {
													if ($(this).attr('value') == rma_types[key])
													{
														$(this).prop('checked', 'checked');
														$(this).click();
													}
												});
											}
										}
									}
								}
							}

							if ($('.rma').children('form').height() > $('.rma').height())
							{
								$('.rma').data('old_height', $('.rma').height());
								$('.rma').height('auto');
							}
						});
					}
					else
					{
						$('.rma').find('.innerHeading').hide();
						$('.rma').find('.order_products').hide();
						$('.rma').find('.button_area').hide();
						$('.rma').find('.order_products').children().remove();
					}
				});

				// Die Bestellung ausw�hlen, falls beim Ausw�hlen was vergessen wurde.
				if (typeof order_id != 'undefined' || $('#order_number_field').val())
				{
					if (typeof order_id == 'undefined')
						var order_id = $('#order_number_field').val();

					$('#orders_list').val(order_id);
					$('#orders_list').change();
				}
			});
		}
	});

	$('#customer_list').keypress(function(e) {
		if (e.which == 13)
		{
			e.preventDefault();
			$(this).change();
		}
	});

	// Im Fehlerfall den Kunden ausw�hlen.
	if (typeof customer_id != 'undefined')
	{
		$('#customer_list').val(customer_id);
		$('#customer_list').change();
	}

	$('#order_number_field').change(function() {
		if (typeof $(this).data('old_value') == 'undefined' || $(this).data('old_value') != $(this).val())
		{
			$(this).data('old_value', $(this).val());
			$.getJSON('ajax_get_order_customer.php', {'order_id': $(this).val()}, function(response) {
				$('#customer_list').val(response.customer_id);
				$('#customer_list').change();
				$(this).blur();
			});
		}
	});
	// Das hier verhindert, dass bei einer vorherigen Eingabe das Formular zum R�cksendung erstellen abgeschickt wird.
	$('#order_number_field').keypress(function(e) {
		if (e.which == 13)
		{
			e.preventDefault();
			$(this).change();
		}
	});
});

//pr�ft ob noch irgendwelche artikel zum zur�cksenden markiert sind und ob diese ne r�ckerstattung bekommen sollen
function check_payments()
{
	var checkboxes = $('.rma').find('input[type=checkbox]');
	var unchecked, checked, refund = 0;
	for (var i = 0; i < checkboxes.length; i++)
	{
		var checkbox = $(checkboxes[i]);
		if (checkbox.attr('checked'))
		{
			checked++;
			if (checkbox.parents('.product').children('.info').children('.rma_type:checked').val() == 'refund')
				refund++;
		}
		else
			unchecked++;
	}

	if (i + 1 == unchecked || !refund)
		$('#payment_data').hide();
}