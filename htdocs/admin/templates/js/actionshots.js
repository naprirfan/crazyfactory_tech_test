$(function() {
	if ($("#actionshots").length)
	{
		// prüfen, ob customer_id in URL übergeben wurde; wenn ja, dann nach Actionshots dieses Kunden suchen
		if (location.search.match(/(?:\?|&)customer_id=(\d+)/i))
		{
			search_mode = 'customers_id';
			search_keyword = RegExp.$1;
		}

		$("#actionshots").tabs({
			show: function(event, ui)
			{

				$("#actionshots > div").html($("#loading_text").val());

				var selected = ui.index;
				var params;

				if (selected == 0)									// Tab 0: neue Shots
					params = {'mode' : 'new'};
				else if (selected == 1)								// Tab 1: gemeldete Shots
					params = {'mode' : 'reported'};
				else if (selected == 2)								// Tab 3: alle Shots (am Anfang leer)
					params = {
						'mode' : 'empty',
						'search_mode' : search_mode,
						'search_keyword' : search_keyword
					};
				else												// Tab 5: Spezial-Shots (Valentin, Hot or Not u. ä.)
					params = {
						'mode' : 'special',
						'category' : $.trim(ui.tab.title)
					};

				load(selected, params);

			},
			selected: search_keyword == '' ? 0 : 2						// wenn schon ein Suchstring gesetzt wurde (Suche nach Kunden-ID), dann direkt zu Actionshots springen
		});
	}
});

function load(tab_index, params)
{
	var tab = "#tabs-" + tab_index;
	$(tab).html($("loading_text").val());

	var post_data = {
		'ajax' : true,
		'method' : 'list',
		'params' : JSON.stringify(params)
	};

	$.post("actionshots.php", post_data, function(response) {
		update_tab(tab, response);
	});
}

function load_all(page)
{
	page_num = parseInt(page);

	params = {
		'mode' : 'all',
		'page' : page_num,
		'search_mode' : search_mode,
		'search_keyword' : search_keyword
	};

	load(2, params);
}

var search_mode = 'products_id';
var search_keyword = '';

function search_filter()
{
	search_mode = $("div.search input[name=search_type]:checked").val();
	search_keyword = $("#search_input").val();

	load_all(1);	// alle Einträge der ersten Seite laden
}

function update_tab(container, html)
{
	container = $(container);

	container.hide();
	container.html(html);

	update_counters();

	$("div.image a", container).fancybox();
	//$("div.description div.additional_actionshots > div > a", container).fancybox();

	$.each($("div.actionshot", container), function(a, b) {

		$("div.description span.stars_wrapper", $(b)).stars({
			callback: function(ui, type, value) {
				var actionshot_id = $(b).children("input.actionshot_id").val();
				$("#actionshot_"+actionshot_id).find("input[name=active]").attr("checked", "checked");
				set_dirty(actionshot_id);
			}
		});

		$.each($("div.description div.additional_actionshots a"), function(index, element) {
			if ($(element).data("hasTip"))
				return;
			else
				$(element).data("hasTip", true);

			$(element).click(function(e) {e.preventDefault();});

			$(element).qtip({
				content: $(element).next(),
				position: {
					corner: {
						target: "bottomLeft",
						tooltip: "topLeft"
					}
				},
				style: {
					tip: "topMiddle",
					width: 275
				},
				show: {
					delay: 0
				}
			});
		});

		$("div.description input[name=active]", $(b)).change(function() {
			set_dirty($(b).children("input.actionshot_id").val());
		});

		$("div.description input[name=deleted]", $(b)).change(function() {
			set_dirty($(b).children("input.actionshot_id").val());
		});

		var tmp_func = function(index, element) {
			var actionshot_id = $(b).children("input.actionshot_id").val();

			var timer;

			var set_dirty_function = function() {
				set_dirty(actionshot_id);
			};
			var timer_callback = function() {
				clearTimeout(timer);

				timer = setTimeout(set_dirty_function, 200);
			};

			$(element).keypress(timer_callback);
			$(element).keydown(timer_callback);
			$(element).keyup(timer_callback);
		};

		$.each($("div.description textarea.description", $(b)), tmp_func);
		$.each($("div.description input.products_id", $(b)), tmp_func);
		$.each($("div.description input.products_id", $(b)), tmp_func);
	});


	container.show();
}

function set_dirty(actionshot_id, toggle)
{
	if (typeof toggle == 'undefined' || toggle)
		$("#actionshot_"+actionshot_id).find("div.save_button").show();
	else
		$("#actionshot_"+actionshot_id).find("div.save_button").hide();
}

function rotate(direction, actionshot_id)
{
	var blocker = $("#ajax_blocker").clone().attr("id", "");
	var image_div = $("#actionshot_"+actionshot_id).children("div.image");
	image_div.css("position", "relative");
	blocker.css("height", image_div.outerHeight());
	blocker.css("width", image_div.outerWidth());
	blocker.appendTo(image_div);

	$.post("actionshots.php", {
		"ajax" : "true",
		"method" : "rotate",
		"params" : JSON.stringify({
			"direction" : direction,
			"actionshot_id" : actionshot_id
		})
	}, function() {
		$("#actionshot_"+actionshot_id+" > div.image img").attr("src",
			"actionshots.php?actionshot_id="+actionshot_id+
			"&thumb=true"+
		    "&"+new Date().getTime()+
			"&fancybox_workaround=.jpg"
		);
		$("#actionshot_"+actionshot_id+" > div.image a").attr("href",
			"actionshots.php?actionshot_id="+actionshot_id+
		    "&"+new Date().getTime()+
			"&fancybox_workaround=.jpg"
		);
		blocker.remove();
	});
}

function update(actionshot_id)
{
	var actionshot_container = $("#actionshot_"+actionshot_id);
	var description_container = $("div.description", actionshot_container);

	var blocker = $("#ajax_blocker").clone().attr("id", "");
	description_container.css("position", "relative");
	blocker.css("height", description_container.outerHeight());
	blocker.css("width", description_container.outerWidth());
	blocker.appendTo(description_container);

	var post_data = {
		"ajax" : "true",
		"method" : "update",
		"params" : JSON.stringify({
			"actionshot_id" : actionshot_id,
			"description" : $("div.description textarea.description", actionshot_container).val(),
			"products_id" : $("div.description input.products_id", actionshot_container).val(),
			"rating" : $("div.description input[name=rating]", actionshot_container).val(),
			"active" : $("div.description input[name=active]", actionshot_container).is(":checked")
		})
	};

	$.post("actionshots.php", post_data, function(response) {
		set_dirty(actionshot_id, false);
		blocker.remove();

		var products_data_container = $("div.product", actionshot_container);
		products_data_container.children("div").html(response.products_name);
		products_data_container.children("img").attr("src", response.products_image);
	});
}

function remove_actionshot(actionshot_id)
{
	var post_data = {
		'ajax' : true,
		'method' : 'delete_dialog'
	};

	$.post("actionshots.php", post_data, function(response) {
		$(response).dialog({
			'resizable' : false,
			'title' : 'Delete ActionShot',
			'width' : 400,
			'modal' : true,
			'buttons' : {
				'Delete' : function() {
					$(this).remove();

					var actionshot_container = $("#actionshot_"+actionshot_id);
					var blocker = $("#ajax_blocker").clone().attr("id", "");
					actionshot_container.css("position", "relative");
					actionshot_container.css("overflow", "hidden");
					blocker.css("height", actionshot_container.outerHeight());
					blocker.css("width", actionshot_container.outerWidth());
					blocker.appendTo(actionshot_container);

					var post_data = {
						"ajax" : "true",
						"method" : "delete",
						"params" : JSON.stringify({
							"actionshot_id" : actionshot_id,
							"reason" : $("select > option:selected", this).val()
						})
					};

					$.post("actionshots.php", post_data, function(response) {
						actionshot_container.remove();
						update_counters();
					});
				},
				'Cancel' : function() {$(this).remove();}
			}
		});
	});
}

function update_counters()
{

	$.post("actionshots.php", {"ajax":"true","method":"counter"}, function(response) {
		$("#actionshots > ul > li:nth-child(1) > a > span").text(response["new"]);
		$("#actionshots > ul > li:nth-child(2) > a > span").text(response["reported"]);
		$("#actionshots > ul > li:nth-child(3) > a > span").text(response["all"]);
		for (var special_category in response["special_categories"])
			$('#actionshots > ul > li > a[title="' + special_category + '"] > span').text(response['special_categories'][special_category]);
		$("#actionshots > ul > li:nth-child(5) > a > span").text(response["coolpics"]);
	});
}