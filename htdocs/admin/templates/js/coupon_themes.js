// die aktuelle Filter-Auswahl speichern, um nach dem Aktivieren/Deaktivieren Bildern o. ä. einen Reload des aktuellen Inhalts machen zu können
var theme_mode = 'all';

$(function () {

	/*
		Die Liste der Kategorien braucht nicht bei jedem Tab-Wechsel neu geladen zu werden, weil
		eventuelle Änderungen sowieso zu einem Reload führen.

		Bei der Motivliste *muss* es dagegen einen Reload geben, weil sonst neu hinzugefügte Kategorien nicht
		berücksichtigt werden.
	*/
	var categories_loaded = false;

	$('#coupon_themes').tabs({

		show : function (event, ui) {
			var selection = ui.index;
			if (selection == 0)
				load_themes(theme_mode);
			else if (selection == 1 && !categories_loaded) {
				load_categories();
				categories_loaded = true;
			}
		}

	});

	$('#theme_selection').submit(function (event) {

		// Formular nicht wirklich abschicken
		event.preventDefault();

		var mode = $(this).children('input[name="mode"]:checked').val();
		theme_mode = mode;
		load_themes(mode);

	});

	$('#add_theme').click(function () {

		$.get('coupon_themes.php', {action : 'get_theme_dialog'}, function (response) {

			$(response).dialog({
				close : function () {
					$(this).remove()
				},
				buttons : {
					add : function () {
						if ($(this).find('form input[name="theme"]').val() == '')
							alert('No file selected.');
						else
							$(this).children('form').submit();
					}
				}
			});

		});

	});

	$('#add_category').click(function () {

		$.get('coupon_themes.php', {action : 'get_category_dialog'}, function (response) {

			$(response).dialog({
				close : function () {
					$(this).remove()
				},
				buttons : {
					add : function () {
						var category_names = {}
						var incomplete = false;
						$(this).find('form input[type="text"]').each(function (i, input) {
							var value = $.trim($(input).val());
							if (value == '')
								incomplete = true;
							else
								category_names[$(input).attr('name')] = value;
						});
						if (incomplete)
							alert('missing names');
						else if (add_category(category_names))
							$(this).dialog('close');
					}
				}
			});

		});

	});

});



/* Funktionen */

function load_themes(theme_mode) {
	$.get('coupon_themes.php', {action : 'get_themes', mode : theme_mode}, function (response) {
		$('#themes_list').html(response);
		$('#themes_list div.list_entry form').submit(function (event) {
			event.preventDefault();
			var theme_id = $(this).children('input[name="theme_id"]').val();
			var category_id = $(this).find('select option:selected').val();
			set_theme_category(theme_id, category_id);
		});
	});
}

function load_categories() {
	$.get('coupon_themes.php', {action : 'get_categories'}, function (response) {
		$('#categories_list').html(response);
	});
}

function toggle_theme(theme_id, active) {
	set_to = active ? 1 : 0;
	$.get('coupon_themes.php', {action : 'toggle_theme', id : theme_id, active : set_to}, function (response) {
		// Wenn der Datenbank-Update funktioniert hat, dann Bereich neu laden, sonst Fehlerdialog anzeigen
		if (response == 'ok')
			load_themes(theme_mode);
		else
			alert('Couldn\'t ' + (active ? 'activate' : 'deactivate'));
	});
}

function toggle_category(category_id, active) {
	set_to = active ? 1 : 0;
	$.get('coupon_themes.php', {action : 'toggle_category', id : category_id, active : set_to}, function (response) {
		// Wenn der Datenbank-Update funktioniert hat, dann Bereich neu laden, sonst Fehlerdialog anzeigen
		if (response == 'ok')
			load_categories();
		else
			alert('Couldn\'t ' + (active ? 'activate' : 'deactivate'));
	});
}

function add_category(names) {
	var success = false;
	var names = JSON.stringify(names);
	$.ajax({
		url: 'coupon_themes.php',
		dataType: 'json',
		data: {action : 'check_category_names', names : names},
		async : false,
		success: function (existing_names) {
			if (existing_names.length == 0)
				success = true;
			if (existing_names.length == 1)
				alert('The category name ' + existing_names[0] + ' already exists');
			else if (existing_names.length > 1)
				alert('The following category names already exist:\n' + existing_names.join('\n'));
		}
	});
	if (success)
		$.get('coupon_themes.php', {action : 'add_category', names : names}, function (response) {
			if (response == 'ok')
				load_categories();
			else
				alert('Couldn\'t add category');
		});
	return success;
}

function edit_category(category_id, names) {
	var parameters = {
		action : 'edit_category',
		id : category_id,
		names : JSON.stringify(names)
	};
	$.get('coupon_themes.php', parameters, function (response) {
		if (response == 'ok')
			load_categories();
		else
			alert('Couldn\'t edit category');
	});
}

function set_theme_category(theme_id, category_id) {
	var parameters = {
		action : 'set_theme_category',
		theme_id : theme_id,
		category_id : category_id
	};
	$.get('coupon_themes.php', parameters, function (response) {
		if (response != 'ok')
			alert('Couldn\'t set category');
	});
}

function show_category_edit_dialog(category_id) {

	$.get('coupon_themes.php', {action : 'get_category_edit_dialog', id : category_id}, function (response) {

		$(response).dialog({
			close : function () {
				$(this).remove()
			},
			buttons : {
				submit : function () {

					var category_names = {};
					var incomplete = false;
					$(this).find('form input[type="text"]').each(function (i, input) {
						var value = $.trim($(input).val());
						if (value == '')
							incomplete = true;
						else
							category_names[$(input).attr('name')] = value;
					});
					if (incomplete)
						alert('missing names');
					else {
						edit_category(category_id, category_names);
						$(this).dialog('close');
					}

				}
			}
		});

	});

}