{if $mode == 'themes'}
<div class="dialog">
	<form action="coupon_themes.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="action" value="add_theme" />
		<label>theme picture (846×567)</label>
		<input type="file" name="theme" /><br />
		<label>optional: small theme picture (332×223)</label>
		<input type="file" name="theme_small" /><br />
		<select name="category_id">
			{foreach from=$categories item=category}
			<option value="{$category.id|escape:'html'}">{$category.name|escape:'html'}</option>
			{/foreach}
		</select>
	</form>
</div>
{elseif $mode == 'categories'}
<div class="dialog">
	<form action="">
		{foreach from=$languages item=lang}
		<label>{$lang.name|escape:'html'}</label><br />
		<input name="{$lang.id|escape:'html'}" type="text" {if isset($lang.category_name)}value="{$lang.category_name|escape:'html'}"{/if} /><br />
		{/foreach}
	</form>
</div>
{/if}