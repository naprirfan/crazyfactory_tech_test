<?php

function xtc_collect_posts()
{
	global $REMOTE_ADDR, $xtPrice;

	if (!$REMOTE_ADDR)
		$REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];

	if ($_POST['gv_redeem_code'])
	{
		$sql = '
      		SELECT
      			`coupon_id`,
      			`coupon_amount`,
      			`coupon_type`,
      			`coupon_minimum_order`,
      			`uses_per_coupon`,
      			`uses_per_user`,
      			`restrict_to_products`,
      			`restrict_to_categories`
      		FROM `'.TABLE_COUPONS.'`
      		WHERE `coupon_code` = '.sqlval($_POST['gv_redeem_code']).'
      			AND `coupon_active` = "Y"
      	';
		$gv_query = xtc_db_query($sql);
		$gv_result = xtc_db_fetch_array($gv_query);

		if (xtc_db_num_rows($gv_query) != 0)
		{
			$redeem_query = xtc_db_query('
				SELECT *
				FROM `'.TABLE_COUPON_REDEEM_TRACK.'`
				WHERE `coupon_id` = '.sqlval($gv_result['coupon_id']).'
			');

			if ((xtc_db_num_rows($redeem_query) != 0) && ($gv_result['coupon_type'] == 'G'))
			{
				$_SESSION['error_message'] = urlencode(ERROR_NO_INVALID_REDEEM_GV);
				xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
			}
		}
		else
		{
			$_SESSION['error_message'] = urlencode(ERROR_NO_INVALID_REDEEM_GV);
			xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
		}

		// GIFT CODE G START
		if ($gv_result['coupon_type'] == 'G')
		{
			$gv_amount = $gv_result['coupon_amount'];
			// Things to set
			// ip address of claimant
			// customer id of claimant
			// date
			// redemption flag
			// now update customer account with gv_amount
			$gv_amount_query = xtc_db_query('
				SELECT `amount`
				FROM `'.TABLE_COUPON_GV_CUSTOMER.'`
				WHERE `customer_id` = '.sqlval($_SESSION['customer_id']).'
			');
			$customer_gv = false;
			$total_gv_amount = $gv_amount;

			if ($gv_amount_result = xtc_db_fetch_array($gv_amount_query))
			{
				$total_gv_amount = $gv_amount_result['amount'] + $gv_amount;
				$customer_gv = true;
			}

			$gv_update = xtc_db_query('
				UPDATE `'.TABLE_COUPONS.'`
				SET `coupon_active` = "N"
				WHERE `coupon_id` = '.sqlval($gv_result['coupon_id']).'
			');
			$gv_redeem = xtc_db_query('
				INSERT INTO `'.TABLE_COUPON_REDEEM_TRACK.'`
					(`coupon_id`, `customer_id`, `redeem_date`, `redeem_ip`)
				VALUES
					('.sqlval($gv_result['coupon_id']).', '.sqlval($_SESSION['customer_id']).', NOW(), '.sqlval($REMOTE_ADDR).')
			');

			if ($customer_gv)
			{
				// already has gv_amount so update
				$gv_update = xtc_db_query('
					UPDATE `'.TABLE_COUPON_GV_CUSTOMER.'`
					SET `amount` = '.sqlval($total_gv_amount).'
					WHERE `customer_id` = '.sqlval($_SESSION['customer_id']).'
				');
			}
			else
			{
				// no gv_amount so insert
				$gv_insert = xtc_db_query('
					INSERT INTO `'.TABLE_COUPON_GV_CUSTOMER.'`
						(`customer_id`, `amount`)
					VALUES
						('.sqlval($_SESSION['customer_id']).', '.sqlval($total_gv_amount).')
				');
			}

			$_SESSION['info_message'] = urlencode(REDEEMED_AMOUNT.$xtPrice->xtcFormat($gv_amount, true, 0, true));
			xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
		}
		else
		{
			if (xtc_db_num_rows($gv_query) == 0)
			{
				$_SESSION['info_message'] = urlencode(ERROR_NO_INVALID_REDEEM_COUPON);
				xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
			}

			$date_query = xtc_db_query('
				SELECT `coupon_start_date`
				FROM `'.TABLE_COUPONS.'`
				WHERE `coupon_start_date` <= NOW()
				  AND `coupon_code` = '.sqlval($_POST['gv_redeem_code']).'
			');

			if (xtc_db_num_rows($date_query) == 0)
			{
				$_SESSION['info_message'] = urlencode(ERROR_INVALID_STARTDATE_COUPON);
				xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
			}

			$date_query = xtc_db_query('
				SELECT `coupon_expire_date`
				FROM `'.TABLE_COUPONS.'`
				WHERE `coupon_expire_date` >= NOW()
				  AND `coupon_code` = '.sqlval($_POST['gv_redeem_code']).'
			');

			if (xtc_db_num_rows($date_query) == 0)
			{
				$_SESSION['info_message'] = urlencode(ERROR_INVALID_FINISDATE_COUPON);
				xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
			}

			$coupon_count = xtc_db_query('
				SELECT `coupon_id`
				FROM `'.TABLE_COUPON_REDEEM_TRACK.'`
				WHERE `coupon_id` = '.sqlval($gv_result['coupon_id']).'
			');
			$coupon_count_customer = xtc_db_query('
				SELECT `coupon_id`
				FROM `'.TABLE_COUPON_REDEEM_TRACK.'`
				WHERE `coupon_id` = '.sqlval($gv_result['coupon_id']).'
				  AND `customer_id` = '.sqlval($_SESSION['customer_id']).'
			');

			if (xtc_db_num_rows($coupon_count) >= $gv_result['uses_per_coupon'] && $gv_result['uses_per_coupon'] > 0)
			{
				$_SESSION['info_message'] = urlencode(ERROR_INVALID_USES_COUPON.$gv_result['uses_per_coupon'].TIMES);
				xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
			}

			if (xtc_db_num_rows($coupon_count_customer) >= $gv_result['uses_per_user'] && $gv_result['uses_per_user'] > 0)
			{
				$_SESSION['info_message'] = urlencode(ERROR_INVALID_USES_USER_COUPON.$gv_result['uses_per_user'].TIMES);
				xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
			}

			if ($gv_result['coupon_type'] == 'S')
				$coupon_amount = $order->info['shipping_cost'];
			else
				$coupon_amount = $gv_result['coupon_amount'].' ';

			if ($gv_result['coupon_type'] == 'P')
				$coupon_amount = $gv_result['coupon_amount'].'% ';

			if ($gv_result['coupon_minimum_order'] > 0)
				$coupon_amount .= 'on orders greater than '.$gv_result['coupon_minimum_order'];

			if (!xtc_session_is_registered('cc_id'))
				xtc_session_register('cc_id'); //Fred - this was commented out before

			$_SESSION['cc_id'] = $gv_result['coupon_id']; //Fred ADDED, set the global and session variable

			xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
		}
	}

	if ($_POST['submit_redeem_x'] && $gv_result['coupon_type'] == 'G')
		xtc_redirect(xtc_href_link(FILENAME_SHOPPING_CART, 'info_message='.urlencode(ERROR_NO_REDEEM_CODE), 'SSL'));
}