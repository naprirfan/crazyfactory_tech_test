<?php
/* -----------------------------------------------------------------------------------------
   $Id: xtc_check_for_stock_items.inc.php 

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(specials.php,v 1.5 2003/02/11); www.oscommerce.com 
   (c) 2003	 nextcommerce (xtc_check_for_stock_items.inc.php,v 1.3 2003/08/13); www.nextcommerce.org
   (c) 2011 creations media GmbH

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/
   
	// Reserves Stock Items again.
  function xtc_check_for_stock_items($orders_id) {
 	
  	// Alle Stockartikel dieser Bestellung holen (zur Sicherheit nochmal auf stock_freed checken.
		$sql = 'SELECT orders_products.products_id, orders_products.products_quantity, products.products_stock_item FROM orders_products 
					INNER JOIN products ON (orders_products.products_id = products.products_id)
					INNER JOIN orders ON (orders_products.orders_id = orders.orders_id)
				WHERE orders_products.orders_id = "'.$orders_id.'"
					AND orders.stock_freed = 1
					AND products.products_stock_item = 1
				';
		$res = xtc_db_query($sql);
		while ($products = xtc_db_fetch_array($res))
		{
			if ($products['products_stock_item'] == '1') 
			{
				$stock_query = xtc_db_query("select stock from products_stock where products_id = '".$products['products_id']."'");
	
				if (xtc_db_num_rows($stock_query) > 0) 
				{
					$stock_values = xtc_db_fetch_array($stock_query);
					$stock_left = $stock_values['stock'] - $products['products_quantity'];
		
					xtc_db_query("update products_stock SET stock = '".$stock_left."' where products_id = '".$products['products_id']."' LIMIT 1");
					if ($stock_left < 1) 
						xtc_db_query("update ".TABLE_PRODUCTS." set products_status = '0' where products_id = '".$products['products_id']."'");
					
				}
			}
		}
		
		xtc_db_query("UPDATE orders SET stock_freed = '0' WHERE orders_id = '".$orders_id."' LIMIT 1");
    return true;
  }