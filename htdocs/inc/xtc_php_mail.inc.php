<?php

/* -----------------------------------------------------------------------------------------
   $Id: xtc_php_mail.inc.php 1129 2005-08-05 11:46:11Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2003	 nextcommerce (xtc_php_mail.inc.php,v 1.17 2003/08/24); www.nextcommerce.org


   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
// include the mail classes
function xtc_php_mail($from_email_address, $from_email_name, $to_email_address, $to_name, $forwarding_to, $reply_address, $reply_address_name, $path_to_attachement, $path_to_more_attachements, $email_subject, $message_body_html, $message_body_plain, $attach_signature = null, $curr_language = null, $signature_replace = null)
{
	if (!isset($attach_signature))
		$attach_signature = true;

	if (!isset($curr_language))
		$curr_language = 'session';

	if (!isset($signature_replace))
		$signature_replace = false;

	// eMail Signatur
	$mailsmarty = new Smarty;
	$mailsmarty->compile_dir = DIR_FS_DOCUMENT_ROOT.'templates_c';

	// $adresse = xtc_db_fetch_array(xtc_db_query("SELECT entry_firstname, entry_lastname, entry_street_address, entry_postcode, entry_city FROM address_book WHERE customers_id = 1"));

	$mailsmarty->assign('SHOP_NAME', STORE_NAME);
	$mailsmarty->assign('SHOP_BESITZER', STORE_OWNER);
	/*
	$mailsmarty->assign('SHOP_ADRESSE_VNAME', '');
	$mailsmarty->assign('SHOP_ADRESSE_NNAME', '');
	$mailsmarty->assign('SHOP_ADRESSE_STRASSE', 'Via San Giulio');
	$mailsmarty->assign('SHOP_ADRESSE_PLZ', '6535');
	$mailsmarty->assign('SHOP_ADRESSE_ORT', 'Roveredo GR');
	 */
	$mailsmarty->assign('SHOP_EMAIL', STORE_OWNER_EMAIL_ADDRESS);
	$mailsmarty->assign('SHOP_NAME', HTTP_SERVER);
	if ($curr_language == 'session')
		$lang = $_SESSION['language'];
	else
		$lang = $curr_language;

	if ($attach_signature)
	{
		$html_signatur = $mailsmarty->fetch(DIR_FS_DOCUMENT_ROOT.'templates/'.CURRENT_TEMPLATE.'/mail/'.$lang.'/signatur.html');
		$txt_signatur = $mailsmarty->fetch(DIR_FS_DOCUMENT_ROOT.'templates/'.CURRENT_TEMPLATE.'/mail/'.$lang.'/signatur.txt');
	}
	else
		$html_signatur = $txt_signatur = '';

	$mail = new PHPMailer();

	if (isset ($_SESSION['language_charset']))
		$mail->CharSet = $_SESSION['language_charset'];
	else
	{
		$lang_query = "SELECT * FROM ".TABLE_LANGUAGES." WHERE code = '".DEFAULT_LANGUAGE."'";
		$lang_query = xtc_db_query($lang_query);
		$lang_data = xtc_db_fetch_array($lang_query);
		$mail->CharSet = $lang_data['language_charset'];
	}

	if (EMAIL_TRANSPORT == 'smtp')
	{
		$mail->IsSMTP();
		$mail->SMTPKeepAlive = true; // set mailer to use SMTP
		$mail->SMTPAuth = SMTP_AUTH; // turn on SMTP authentication true/false
		$mail->Username = SMTP_USERNAME; // SMTP username
		$mail->Password = SMTP_PASSWORD; // SMTP password
		$mail->Host = SMTP_MAIN_SERVER.';'.SMTP_Backup_Server; // specify main and backup server "smtp1.example.com;smtp2.example.com"
	}

	if (EMAIL_TRANSPORT == 'sendmail')
	{
		$mail->IsSendmail();
		$mail->Sendmail = SENDMAIL_PATH;
	}

	if (EMAIL_TRANSPORT == 'mail')
		$mail->IsMail();

	// Textmail von HTML befreien
	$message_body_plain = str_replace(array('<br />', '<br>', '<br/>'), "\n", $message_body_plain); //DPW Signatur erg�nzt.
	$message_body_plain = strip_tags($message_body_plain);

	if (EMAIL_USE_HTML == 'true') // set email format to HTML
	{
		$mail->IsHTML(true);

		// Ist die Signatur inmitten der struktur
		if ($signature_replace)
			$mail->Body = str_replace($signature_replace, $html_signatur, $message_body_html);
		else
			$mail->Body = $message_body_html.$html_signatur;

		$mail->AltBody = $message_body_plain.$txt_signatur;
	}
	else
	{
		$mail->IsHTML(false);
		$mail->Body = $message_body_plain.$txt_signatur;
	}

	$mail->From = $from_email_address;
	$mail->Sender = $from_email_address;
	$mail->FromName = $from_email_name;
	$mail->AddAddress($to_email_address, $to_name);
	if ($forwarding_to)
		$mail->AddBCC($forwarding_to);
	if ($reply_address)
		$mail->AddReplyTo($reply_address, $reply_address_name);

	$mail->WordWrap = 50;

	if ($path_to_attachement)
		$mail->AddAttachment($path_to_attachement);

	if ($path_to_more_attachements)
		$mail->AddAttachment($path_to_more_attachements);

	$mail->Subject = $email_subject;

	if (!$mail->Send())
	{
		echo "Message was not sent <p>";
		echo "Mailer Error: ".$mail->ErrorInfo;
		// exit;
	}
}