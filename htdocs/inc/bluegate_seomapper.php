<?PHP

/*
Extension Direct URL (c) 2007 bluegate communications
---------------------------------------------------------------------------------------
bluegate communications - http://www.bluegate.at
Author: Ing. Michael F�rst
*/

// Create Instance of bluegate seo class
require_once(DIR_FS_INC . 'bluegate_seo.inc.php');
!$bluegateSeo ? $bluegateSeo = new BluegateSeo() : false; 

// Get ID parameter
switch ($_REQUEST['bluegatemapto']) {
	case 'product':
		$productId = $bluegateSeo->getIdForURL($_GET['linkurl'],'product');
		// Get language setting from URL
		MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL=='True' && $_GET['linkurl'] && !$_GET['language'] ? $_GET['language']=substr($_GET['linkurl'],0,2) : false;
		// Set product ID
		$_GET['info']=$productId;
		break;
	case 'category':
		$categoryId = $bluegateSeo->getIdForURL($_GET['linkurl'],'category');
		// Get language setting from URL
		MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL=='True' && $_GET['linkurl'] && !$_GET['language'] ? $_GET['language']=substr($_GET['linkurl'],0,2) : false;
		// Set category ID
		$_GET['cat']=$categoryId;
		break;
	case 'content':
		$contentGroupId = $bluegateSeo->getIdForURL($_GET['linkurl'],'content');
		// Get language setting from URL
		MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL=='True' && $_GET['linkurl'] && !$_GET['language'] ? $_GET['language']=substr($_GET['linkurl'],0,2) : false;
		$_GET['coID']=$contentGroupId;
		break;
}

// Remove GET Parameter Trash
unset($_GET['bluegatemapto']);
unset($_GET['linkurl']);
?>