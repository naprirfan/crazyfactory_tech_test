<?php
/* -----------------------------------------------------------------------------------------
   $Id: xtc_buyable_coupons.inc.php

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(specials.php,v 1.5 2003/02/11); www.oscommerce.com
   (c) 2003	 nextcommerce (xtc_set_specials_status.inc.php,v 1.3 2003/08/13); www.nextcommerce.org
   (c) 2011 creations media GmbH

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  function xtc_activate_buyable_coupons($orders_id) {

  	// Wert holen und checken ob diese Bestellung evtl. schon mal crazy-cashpoints erhalten hat.
  	$sql = 'UPDATE `coupons` SET `coupon_active` = "Y", `date_modified` = NOW() WHERE `orders_id` = '.xtc_db_input($orders_id);
		if (xtc_db_query($sql))
			return true;
		else
			return false;
  }

  function xtc_deactivate_buyable_coupons($orders_id) {
  	// Checken ob dieser Gutscheincode bereits verwendet wurde.
  	$sql = 'SELECT * FROM coupons
  				LEFT JOIN coupon_redeem_track USING (coupon_id)
  				WHERE orders_id = '.xtc_db_input($orders_id);

			$res = xtc_db_query($sql);
      ;

  	while ($check = xtc_db_fetch_array($res))
  	{
  		if ($check['unique_id'])
  			return false;
  	}

		$sql = 'UPDATE `coupons` SET `coupon_active` = "N", `date_modified` = NOW() WHERE `orders_id` = '.xtc_db_input($orders_id);
		xtc_db_query($sql);
		return true;

  }