<?php
/*
Get Manufacturers Data
Copyright 2007 Southbridge.
www.southbridge.de
*/
function getManufacturersData($man_id)
{
if(!empty($man_id) && is_numeric($man_id)){
$man_array = array();
$image = '';
$url = '';
$link = '';
$manufacturer_query = xtDBquery("
SELECT
m.manufacturers_id,
m.manufacturers_name,
m.manufacturers_image,
mi.manufacturers_url
FROM
" . TABLE_MANUFACTURERS . " m,
" . TABLE_MANUFACTURERS_INFO . " mi
WHERE
m.manufacturers_id = '".$man_id."'
AND
m.manufacturers_id = mi.manufacturers_id
AND
mi.languages_id = '".(int)$_SESSION['languages_id']."'");
$manufacturer = xtc_db_fetch_array($manufacturer_query,true);
if(!empty($manufacturer['manufacturers_image']))
$image = DIR_WS_IMAGES . $manufacturer['manufacturers_image'];
if(!empty($manufacturer['manufacturers_url']))
$url = xtc_href_link(FILENAME_REDIRECT,
'action=manufacturer&'.xtc_manufacturer_link($manufacturer['manufacturers_id'],$manufacturer['manufacturers_name']));
if(!empty($manufacturer['manufacturers_name']))
$link = xtc_href_link(FILENAME_DEFAULT,
xtc_manufacturer_link($manufacturer['manufacturers_id'],$manufacturer['manufacturers_name']));
$man_array = array(
'image' => $image,
'name' => $manufacturer['manufacturers_name'],
'link' => $link,
'url' => $url
);
return $man_array;
}
}
?>
