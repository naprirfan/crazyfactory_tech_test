<?php
/* -----------------------------------------------------------------------------------------
   $Id: xtc_href_link.inc.php 804 2005-02-26 16:42:03Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(html_output.php,v 1.52 2003/03/19); www.oscommerce.com 
   (c) 2003	 nextcommerce (xtc_href_link.inc.php,v 1.3 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/
   
// The HTML href link wrapper function
  function xtc_href_link($page = '', $parameters = '', $connection = 'NONSSL', $add_session_id = true, $search_engine_safe = true) {
	global $request_type, $session_started, $http_domain, $https_domain,$truncate_session_id;

    if (!xtc_not_null($page)) {
      // WORKAROUND FOR SOME SERVERS WITH WRONG httpd.conf
	  $page='index.php';
    }
	
	$ist_blog_da = 'blog.php';
	if(file_exists($ist_blog_da)) {
		$blog_da = true;
	}
	else {
		$blog_da = false;
	}

    if ($connection == 'NONSSL') {
      $link = HTTP_SERVER . DIR_WS_CATALOG;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL == true) {
        $link = HTTPS_SERVER . DIR_WS_CATALOG;
      } else {
        $link = HTTP_SERVER . DIR_WS_CATALOG;
      }
    } else {
      die('</td></tr></table></td></tr></table><br /><br /><font color="#ff0000"><b>Error!</b></font><br /><br /><b>Unable to determine connection method on a link!<br /><br />Known methods: NONSSL SSL</b><br /><br />');
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);

    /* Create GET Parameters, if Direct URL is not in use */
	if (MODULE_BLUEGATE_SEO_INDEX_STATUS<>'True') {
		if (xtc_not_null($parameters)) {
      		$link .= $page . '?' . $parameters;
		    $separator = '&';
	  } else {
    		$link .= $page;
		    $separator = '?';
      }
	}
	
	/* xtc fake Suma friendly URL */
	if ( (SEARCH_ENGINE_FRIENDLY_URLS == 'true') && (MODULE_BLUEGATE_SEO_INDEX_STATUS<>'True')) {
      while (strstr($link, '&&')) $link = str_replace('&&', '&', $link);

      $link = str_replace('?', '/', $link);
      $link = str_replace('&', '/', $link);
      $link = str_replace('=', '/', $link);
      $separator = '?';
    }
		
	if (SEARCH_ENGINE_FRIENDLY_URLS == 'true' && MODULE_BLUEGATE_SEO_INDEX_STATUS == 'True') {
		
		
		// Create Instance of bluegate seo class
		require_once(DIR_FS_INC . 'bluegate_seo.inc.php');
		!$bluegateSeo ? $bluegateSeo = new BluegateSeo() : false;
		$realUrl = false;
		
		// *****************************************
		// Set product link if page=product_info.php
		// AND $parameters includes the string "info="
		// AND $parameters doesn't include the string "action="
		// *****************************************
		if ($page=='product_info.php' && strpos($parameters,'products_id')!==false && strpos($parameters,'action=')===false) {
			$realUrl = true;
			$link = $bluegateSeo->getProductLink($parameters,$connection,$_SESSION['languages_id']);		//generate product link
		}
		
		// *****************************************
		// Set category link if page=index.php
		// AND $parameters includes the string "cat=c"
		// *****************************************
		if ($page=='index.php' && strpos($parameters,'cPath')!==false && strpos($parameters,'action=')===false && strpos($parameters,'page=')===false) {
			$realUrl = true;
			$link = $bluegateSeo->getCategoryLink($parameters,$connection,$_SESSION['languages_id']);

		}
		
		// *****************************************
		// Set content link if page=shop_content.php
		// AND $parameters includes the string "coID"
		// *****************************************
		if ($page=='shop_content.php' && strpos($parameters,'coID')!==false && strpos($parameters,'action=')===false) {
			$realUrl = true;
			$link = $bluegateSeo->getContentLink($parameters,$connection,$_SESSION['languages_id']);
		}
		
		// *****************************************
		// Set blog link if page=blog.php
		// AND $parameters includes the string "blog_item"
		// *****************************************
		if($blog_da == true) {
			if ($page=='blog.php' && strpos($parameters,'blog_item')!==false && strpos($parameters,'action=')===false) {
				$realUrl = true;
				$link = $bluegateSeo->getBlogLink($parameters,$connection,$_SESSION['languages_id']);
			} 
			elseif	($page=='blog.php' && strpos($parameters,'blog_item')===false && strpos($parameters,'action=')===false) {
				$realUrl = true;
				$link = $bluegateSeo->getBlogCatLink($parameters,$connection,$_SESSION['languages_id']);
			} 
		}
		// *****************************************
		// Set language link 
		// AND $parameters includes the string "language="
		// *****************************************

			if (($page=='product_info.php' || $page=='index.php' || $page == 'commerce_seo_url.php' || $page=='shop_content.php' || $page=='blog.php') && MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL=='True' && strpos($parameters,'language=')!==false && strpos($parameters,'action=')===false && strpos($parameters,'page=')===false) {

				$link = $bluegateSeo->getLanguageChangeLink($page,$parameters,$connection='NONSSL');
				$realUrl = true;
			}

		// Append necessary GET Params
		$separator = '?';
		
		if (xtc_not_null($parameters)) {
      		// Append GET Parameters if it isn't a Real URL
			if (!$realUrl) {
				$link .= $page . '?' . $parameters;
      			$separator = '&';
			}		
		} else {
			// Set Standard Link if it isn't a Real URL
			if (!$realUrl) {
				$link .= $page;
      			$separator = '?';
			}
		}
	}
	
	
	// Add the session ID when moving from different HTTP and HTTPS servers, or when SID is defined
    if ( ($add_session_id == true) && ($session_started == true) && (SESSION_FORCE_COOKIE_USE == 'False') ) {
      if (defined('SID') && xtc_not_null(SID)) {
        $sid = SID;
      } elseif ( ( ($request_type == 'NONSSL') && ($connection == 'SSL') && (ENABLE_SSL == true) ) || ( ($request_type == 'SSL') && ($connection == 'NONSSL') ) ) {
        if ($http_domain != $https_domain) {
          $sid = session_name() . '=' . session_id();
        }
      }        
    }
	
	// remove session if useragent is a known Spider
    if ($truncate_session_id) $sid=NULL;
	/* NOT
    if (isset($sid)) {
      $link .= $separator . $sid;
    }
		*/
    return $link;
  }

  function xtc_href_link_admin($page = '', $parameters = '', $connection = 'NONSSL', $add_session_id = true, $search_engine_safe = true) {
    global $request_type, $session_started, $http_domain, $https_domain;

    if (!xtc_not_null($page)) {
      die('</td></tr></table></td></tr></table><br /><br /><font color="#ff0000"><b>Error!</b></font><br /><br /><b>Unable to determine the page link!<br /><br />');
    }

    if ($connection == 'NONSSL') {
      $link = HTTP_SERVER . DIR_WS_CATALOG;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL == true) {
        $link = HTTPS_SERVER . DIR_WS_CATALOG;
      } else {
        $link = HTTP_SERVER . DIR_WS_CATALOG;
      }
    } else {
      die('</td></tr></table></td></tr></table><br /><br /><font color="#ff0000"><b>Error!</b></font><br /><br /><b>Unable to determine connection method on a link!<br /><br />Known methods: NONSSL SSL</b><br /><br />');
    }

    if (xtc_not_null($parameters)) {
      $link .= $page . '?' . $parameters;
      $separator = '&';
    } else {
      $link .= $page;
      $separator = '?';
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);

// Add the session ID when moving from different HTTP and HTTPS servers, or when SID is defined
    if ( ($add_session_id == true) && ($session_started == true) && (SESSION_FORCE_COOKIE_USE == 'False') ) {
      if (defined('SID') && xtc_not_null(SID)) {
        $sid = SID;
      } elseif ( ( ($request_type == 'NONSSL') && ($connection == 'SSL') && (ENABLE_SSL == true) ) || ( ($request_type == 'SSL') && ($connection == 'NONSSL') ) ) {
        if ($http_domain != $https_domain) {
          $sid = session_name() . '=' . session_id();
        }
      }
    }

    if ($truncate_session_id) $sid=NULL;

/* NOT
    if (isset($sid)) {
      $link .= $separator . $sid;
    }
*/

    return $link;
  }

 ?>