<?php
/* -----------------------------------------------------------------------------------------
   $Id: xtc_image_button.inc.php,v 1.1 2004/10/13 10:34:26 stephan Exp $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(html_output.php,v 1.52 2003/03/19); www.oscommerce.com 
   (c) 2003	 nextcommerce (xtc_image_button.inc.php,v 1.3 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   File modified
   stspi momd_040903 2004-09-03
   
   Set the variables mouseover or mousedown to default=false or true to deactivate or activat 
   mouseover or mousedown for the whole page, or call it in a single function.
   ---------------------------------------------------------------------------------------*/
   
// Output a function button in the selected language

  function xtc_image_button($image, $alt = '', $parameters = '', $mouseover = true, $mousedown = true) 
  {
    return xtc_image('templates/'.CURRENT_TEMPLATE.'/buttons/' . $_SESSION['language'] . '/'. $image, $alt, '', '', $parameters, $mouseover, $mousedown);
  }
 ?>