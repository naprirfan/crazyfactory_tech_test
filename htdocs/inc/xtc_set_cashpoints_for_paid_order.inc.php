<?php
/* -----------------------------------------------------------------------------------------
   $Id: xtc_set_cashpoints_for_paid_order.inc.php 

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(specials.php,v 1.5 2003/02/11); www.oscommerce.com 
   (c) 2003	 nextcommerce (xtc_set_specials_status.inc.php,v 1.3 2003/08/13); www.nextcommerce.org
   (c) 2010 creations media GmbH

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/
   
	// Sets cashpoints for a paid order
  function xtc_set_cashpoints_for_paid_order($orders_id,$source) {
  	
  	// Wert holen und checken ob diese Bestellung evtl. schon mal crazy-cashpoints erhalten hat.
  	$sql = 'SELECT o.orders_id, o.customers_id, o.currency_value, ot.value , ot.class, cp.cash_points_id
  					FROM orders o INNER JOIN orders_total ot ON (o.orders_id = ot.orders_id )
  					LEFT JOIN cm_cash_points cp ON (o.orders_id = cp.orders_id AND cp.action = "order_bonus")
  	 				where o.orders_id = '.xtc_db_input($orders_id);
  	$check_query = xtc_db_query($sql);
		$raw_value = 0;
		while ($check_data = xtc_db_fetch_array($check_query))
		{
			$currency_value = $check_data['currency_value'];
			$customers_id = $check_data['customers_id'];
			$cash_points_id = $check_data['cash_points_id'];
			
			if ($check_data['class'] != 'ot_total' AND $check_data['class'] != 'ot_shipping' AND $check_data['class'] != 'ot_tax')
			$raw_value = $raw_value + $check_data['value'];
		}
		if ($raw_value > 0)
		{
			$cash_points_gained = ceil(($raw_value/$currency_value)*5);  // Vorgabe: Immer aufrunden.
			
			if ($cash_points_id === NULL && $orders_id > 0)
			{
				xtc_db_query('INSERT INTO cm_cash_points (customers_id, orders_id,  action, amount, meta_data, date_create) VALUES ("'.$customers_id.'" ,"'.$orders_id.'", "order_bonus", "'.$cash_points_gained.'","'.$source.'", NOW() )');
				return true;
			}
			elseif ($cash_points_id > 0 )
			{
				xtc_db_query('UPDATE cm_cash_points SET amount = "'.$cash_points_gained.'", meta_data = "'.$source.'", date_update = NOW() WHERE cash_points_id = "'.$cash_points_id.'" ');
				return false;
	    }
    }
    else
	    return false;
  }