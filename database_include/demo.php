<?php

require_once('database.inc.php');

// $DBDATA['host'] = ''; // Default: localhost
$DBDATA['user'] = '';
$DBDATA['pass'] = '';
$DBDATA['db'] = '';
// $DBDATA['port'] = ''; // Default: 3306
// $DBDATA['on_query_error'] = array('my_query_error_handler'); // Default: database_include_errorcallback_query
// $DBDATA['on_connect_error'] = array(); // function handler($errorstring)
// $DBDATA['on_use_error'] = array(); // function handler($errorstring)
// $DBDATA['on_error_error'] = array(); // function handler($errorstring)
// $DBDATA['on_query'] = array(); // function handler($querystring)
// $DBDATA['on_select_result'] = array(); // function handler($matrix)
// $DBDATA['on_insert_result'] = array(); // function handler($insert_id)
// $DBDATA['on_update_result'] = array(); // function handler($affected_rows)
// $DBDATA['on_replace_result'] = array(); // function handler($affected_rows)
// $DBDATA['on_delete_result'] = array(); // function handler($affected_rows)


function my_query_error_handler($errorstring,$backtrace,$querystring)
{
}


$matrix = query('SELECT ...');
$insert_id = query('INSERT INTO ...');
$affected_rows = query('UPDATE ...');
$affected_rows = query('REPLACE ...');
$affected_rows = query('DELETE FROM ...');

if (query('SELECT ...'))
	; // This will get executed only if there was no error and there are any matching rows

if (query('SELECT ...') !== false)
	; // This will get executed if there was no error, even if there are no matching rows

foreach (query('SELECT ...') as $row) // This will complain about getting passed false only when there is an error in the query
	;