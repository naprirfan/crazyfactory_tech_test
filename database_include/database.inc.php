<?php

database_include_early_init_config();

// ================================================================================

// Wird ausgef�hrt, wenn die Datei includet wird; setzt die Defaults, bevor die Konfiguration durch
// die Anwendung ge�ndert wird
function database_include_early_init_config()
{
	static $already_inited;
	if ($already_inited++)
		return;

	global $DBDATA;

	$DBDATA['on_query_error'] = array('database_include_errorcallback_query');
}

// Wird ausgef�hrt, wenn das erste Query gemacht wird; bereitet die Konfiguration auf
function database_include_late_init_config()
{
	static $already_inited;
	if ($already_inited++)
		return;

	global $DBDATA;

	if (!isset($DBDATA['host']) || strlen($DBDATA['host']) < 1) $DBDATA['host']="localhost";
	if (isset($DBDATA['username']) && strlen($DBDATA['username'])) $DBDATA['user']=$DBDATA['username'];
	if (isset($DBDATA['password']) && strlen($DBDATA['password'])) $DBDATA['pass']=$DBDATA['password'];
	if (isset($DBDATA['database']) && strlen($DBDATA['database'])) $DBDATA['db']=$DBDATA['database'];
	if ($DBDATA['port'] > 0) $DBDATA['host'] .= ':'.$DBDATA['port'];
}

function database_include_errorcallback_query($errorstring,$backtrace,$querystring)
{
	define ('LF',"\n");
	echo 'MySQL error in '.$backtrace[0]['file'].' ('.$backtrace[0]['line'].'):'.LF.$errorstring.LF.'Query:'.LF.$querystring;
}

function database_include_call_callbacks($hook)
{
	global $DBDATA;

	if (is_string($DBDATA[$hook]))
		$callbacks = array($DBDATA[$hook]);
	if (is_array($DBDATA[$hook]))
		$callbacks = $DBDATA[$hook];

	if ($callbacks)
	{
		$args = func_get_args();
		array_shift($args); // Namen des Hooks abshiften
		foreach ($callbacks as $callback)
			call_user_func_array($callback,$args); // Alle anderen Argumente werden weitergegeben
	}
}

function query($querystring)
{
	global $DBDATA;
	static $DATABASE_CONNECTION;

	if (!$DATABASE_CONNECTION)
	{
		database_include_late_init_config();

		$DATABASE_CONNECTION = @mysql_connect($DBDATA['host'],$DBDATA['user'],$DBDATA['pass']);

		if (!$DATABASE_CONNECTION)
		{
			database_include_call_callbacks('on_connect_error',mysql_error());
			database_include_call_callbacks('on_error',mysql_error());
		}

		if ($DBDATA['db'])
		{
			if (!@mysql_select_db($DBDATA['db'],$DATABASE_CONNECTION))
			{
				database_include_call_callbacks('on_use_error',mysql_error());
				database_include_call_callbacks('on_error',mysql_error());
			}
		}
	}

	$querystring = trim($querystring);

	database_include_call_callbacks('on_query',$querystring);
	$result = @mysql_query($querystring,$DATABASE_CONNECTION);

	if (!$result)
	{
		database_include_call_callbacks('on_query_error',mysql_error(),debug_backtrace(),$querystring);
		database_include_call_callbacks('on_error',mysql_error());
	}

	$O = false;

	if ($result)
	{
		$O = true;

		if (preg_match('/^\(?\s*(SELECT|SHOW)/', $querystring))
		{
			$O = array();
			if (mysql_num_rows($result) > 0)
			{
				while($line=mysql_fetch_array($result,MYSQL_ASSOC))
					$O[]=$line;
			}
			database_include_call_callbacks('on_select_result',$O);
		}

		if (substr($querystring,0,6)=="INSERT")
		{
		    $O=mysql_insert_id($DATABASE_CONNECTION);
		    database_include_call_callbacks('on_insert_result',$O);
		}

		if (substr($querystring,0,6)=="UPDATE")
		{
			$O = mysql_affected_rows($DATABASE_CONNECTION);
			database_include_call_callbacks('on_update_result',$O);
		}
		if (substr($querystring,0,7)=="REPLACE")
		{
			$O = mysql_affected_rows($DATABASE_CONNECTION);
			database_include_call_callbacks('on_replace_result',$O);
		}
		if (substr($querystring,0,11)=="DELETE FROM")
		{
			$O = mysql_affected_rows($DATABASE_CONNECTION);
			database_include_call_callbacks('on_delete_result',$O);
		}
	}

	return $O;
}

function sqlval($value) {
	return "'".addslashes($value)."'";
}

?>