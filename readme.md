#Crazy Factory Tech Test

Objective : rewrite a specific feature: Importing product data from an Excel file into the database

##Installation

1. Clone this repository to your local
2. Point webserver's document root to htdocs folder
3. Import database `crazy_import.sql`. You can do this by your favorite db importer tools or, you can do this by using : `http://localhost/adminer.php`. 
4. Change some path and credentials in config file located at : `/htdocs/admin/includes/configure.php`
5. If everything is set up correctly, go to `http://localhost/admin/csv_backend.php`
6. Click `Importieren` Button to test the new import code. Play around with it

##Explanation

- I created the new import code in `/htdocs/admin/includes/classes/custom_import`
- There are two file under the folder : `ExcelToMysqlImporter` Class and `product_dictionary.php`
- `ExcelToMysqlImporter` Class has import method, which have been implemented as instructed
- `product_dictionary.php` is an array that maps excel-import-file to table-products in database.
- When `importieren` button clicked, it will instantiate the `ExcelToMysqlImporter` class and call the import() method.
- The import process works with these table : `products`, `products_description`, `categories`, and `products_to_categories`

---

## The test :

Hi.

Please see the attached RAR with some code files. It's an excerpt from the code of the xtCommerce shop software. xtCommerce is known for being old and of very bad code quality. So the task is to rewrite a specific feature: Importing product data from an Excel file into the database. The current code is ugly, unnecessarily object oriented and cluttered with unused and broken features. However, you can use the old code to see how the Excel file is read (admin/includes/import.php, lines 708 and 979-984).

To get it running, the files from htdocs must be put somewhere on a web server, the database dump has to be applied and probably some paths and credentials have to be changed in admin/includes/configure.php and possibly includes/configure.php. If everything is set up correctly you should be able to open admin/csv_backend.php and press "Importieren" which will import the file "import/Artikelliste MASTER.xls" into the database.
I also included a database include that we use to make accessing MySQL easier. It is already integrated in the xtCommerce code, I included it only so you can see (in demo.php) how it is used.
 
The particular features of the import should be:

- There should be an array to set up the mapping between the columns in Excel and the columns in DB table "products"

- If a product is already in the database (compare the column products_model) it should be updated

- There is an Excel column "categorie", this value should be compared to a new column "name" (yet to be created) in the table "categories" and an entry in products_to_categories should be made accordingly

- The values from products_description_de, ..._en, etc. should be imported into the table products_description
