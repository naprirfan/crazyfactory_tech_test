-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             6.0.0.4027
-- Date/time:                    2012-03-09 14:51:40
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table crazy_import.address_book
DROP TABLE IF EXISTS `address_book`;
CREATE TABLE IF NOT EXISTS `address_book` (
  `address_book_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `entry_gender` char(1) CHARACTER SET utf8 NOT NULL,
  `entry_company` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `entry_firstname` varchar(32) CHARACTER SET utf8 NOT NULL,
  `entry_lastname` varchar(32) CHARACTER SET utf8 NOT NULL,
  `entry_street_address` varchar(64) CHARACTER SET utf8 NOT NULL,
  `entry_street_address2` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '2. Adressfeld.',
  `entry_suburb` varchar(64) CHARACTER SET utf8 NOT NULL,
  `entry_postcode` varchar(10) CHARACTER SET utf8 NOT NULL,
  `entry_city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `entry_state` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0',
  `address_date_added` datetime DEFAULT '0000-00-00 00:00:00',
  `address_last_modified` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`address_book_id`),
  KEY `idx_address_book_customers_id` (`customers_id`),
  KEY `entry_city` (`entry_city`),
  KEY `entry_postcode` (`entry_postcode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.address_book: 0 rows
DELETE FROM `address_book`;
/*!40000 ALTER TABLE `address_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_book` ENABLE KEYS */;


-- Dumping structure for table crazy_import.address_format
DROP TABLE IF EXISTS `address_format`;
CREATE TABLE IF NOT EXISTS `address_format` (
  `address_format_id` int(11) NOT NULL AUTO_INCREMENT,
  `address_format` varchar(128) NOT NULL,
  `address_summary` varchar(48) NOT NULL,
  PRIMARY KEY (`address_format_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.address_format: 0 rows
DELETE FROM `address_format`;
/*!40000 ALTER TABLE `address_format` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_format` ENABLE KEYS */;


-- Dumping structure for table crazy_import.admin_access
DROP TABLE IF EXISTS `admin_access`;
CREATE TABLE IF NOT EXISTS `admin_access` (
  `customers_id` varchar(32) NOT NULL DEFAULT '0',
  `configuration` int(1) NOT NULL DEFAULT '0',
  `modules` int(1) NOT NULL DEFAULT '0',
  `countries` int(1) NOT NULL DEFAULT '0',
  `currencies` int(1) NOT NULL DEFAULT '0',
  `zones` int(1) NOT NULL DEFAULT '0',
  `geo_zones` int(1) NOT NULL DEFAULT '0',
  `tax_classes` int(1) NOT NULL DEFAULT '0',
  `tax_rates` int(1) NOT NULL DEFAULT '0',
  `accounting` int(1) NOT NULL DEFAULT '0',
  `backup` int(1) NOT NULL DEFAULT '0',
  `cache` int(1) NOT NULL DEFAULT '0',
  `server_info` int(1) NOT NULL DEFAULT '0',
  `whos_online` int(1) NOT NULL DEFAULT '0',
  `languages` int(1) NOT NULL DEFAULT '0',
  `define_language` int(1) NOT NULL DEFAULT '0',
  `orders_status` int(1) NOT NULL DEFAULT '0',
  `shipping_status` int(1) NOT NULL DEFAULT '0',
  `module_export` int(1) NOT NULL DEFAULT '0',
  `customers` int(1) NOT NULL DEFAULT '0',
  `create_account` int(1) NOT NULL DEFAULT '0',
  `customers_status` int(1) NOT NULL DEFAULT '0' COMMENT 'right to change the customers group (admin, customer, guest, ...)',
  `orders` int(1) NOT NULL DEFAULT '0',
  `campaigns` int(1) NOT NULL DEFAULT '0',
  `print_packingslip` int(1) NOT NULL DEFAULT '0',
  `print_order` int(1) NOT NULL DEFAULT '0',
  `popup_memo` int(1) NOT NULL DEFAULT '0',
  `coupon_admin` int(1) NOT NULL DEFAULT '0',
  `listcategories` int(1) NOT NULL DEFAULT '0',
  `gv_queue` int(1) NOT NULL DEFAULT '0',
  `gv_mail` int(1) NOT NULL DEFAULT '0',
  `gv_sent` int(1) NOT NULL DEFAULT '0',
  `coupon_themes` tinyint(1) NOT NULL DEFAULT '0',
  `validproducts` int(1) NOT NULL DEFAULT '0',
  `validcategories` int(1) NOT NULL DEFAULT '0',
  `mail` int(1) NOT NULL DEFAULT '0',
  `categories` int(1) NOT NULL DEFAULT '0',
  `new_attributes` int(1) NOT NULL DEFAULT '0',
  `products_attributes` int(1) NOT NULL DEFAULT '0',
  `manufacturers` int(1) NOT NULL DEFAULT '0',
  `reviews` int(1) NOT NULL DEFAULT '0',
  `specials` int(1) NOT NULL DEFAULT '0',
  `stats_products_expected` int(1) NOT NULL DEFAULT '0',
  `stats_products_viewed` int(1) NOT NULL DEFAULT '0',
  `stats_products_purchased` int(1) NOT NULL DEFAULT '0',
  `stats_customers` int(1) NOT NULL DEFAULT '0',
  `stats_sales_report` int(1) NOT NULL DEFAULT '0',
  `stats_campaigns` int(1) NOT NULL DEFAULT '0',
  `banner_manager` int(1) NOT NULL DEFAULT '0',
  `banner_statistics` int(1) NOT NULL DEFAULT '0',
  `module_newsletter` int(1) NOT NULL DEFAULT '0',
  `start` int(1) NOT NULL DEFAULT '0',
  `content_manager` int(1) NOT NULL DEFAULT '0',
  `content_preview` int(1) NOT NULL DEFAULT '0',
  `credits` int(1) NOT NULL DEFAULT '0',
  `blacklist` int(1) NOT NULL DEFAULT '0',
  `orders_edit` int(1) NOT NULL DEFAULT '0',
  `popup_image` int(1) NOT NULL DEFAULT '0',
  `csv_backend` int(1) NOT NULL DEFAULT '0',
  `products_vpe` int(1) NOT NULL DEFAULT '0',
  `cross_sell_groups` int(1) NOT NULL DEFAULT '0',
  `fck_wrapper` int(1) NOT NULL DEFAULT '0',
  `econda` int(1) NOT NULL DEFAULT '0',
  `paypal` int(1) NOT NULL,
  `customers_sik` int(1) NOT NULL DEFAULT '0',
  `novalnet` int(1) NOT NULL DEFAULT '0',
  `group_prices` int(1) NOT NULL DEFAULT '0',
  `customers_aquise` int(1) NOT NULL DEFAULT '0',
  `customers_aquise_request` int(1) NOT NULL DEFAULT '0',
  `orders_overview` int(1) NOT NULL DEFAULT '0',
  `orders_overview_print` int(1) NOT NULL DEFAULT '0',
  `blog` int(1) NOT NULL DEFAULT '0',
  `pdfbill_config` int(1) NOT NULL DEFAULT '0',
  `pdfbill_display` int(1) NOT NULL DEFAULT '0',
  `recover_cart_sales` int(1) NOT NULL DEFAULT '0',
  `stats_recover_cart_sales` int(1) NOT NULL DEFAULT '0',
  `module_newsletter_products` int(1) NOT NULL DEFAULT '0',
  `close_cart_new_order` int(1) NOT NULL DEFAULT '0',
  `actionshots` tinyint(1) NOT NULL DEFAULT '0',
  `orders_export` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `orders_mass_switch` tinyint(4) NOT NULL DEFAULT '0',
  `orders_allow_export` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Amicron Updateflag setzen',
  `orders_export_in_euro` tinyint(3) unsigned NOT NULL,
  `email_manager` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Email Templates bearbeiten',
  `paypal_conflict_check` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `docdata_conflict_check` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Docdata Zahlungen von stornierten Bestellungen ',
  `variations` int(1) NOT NULL DEFAULT '0',
  `article_upload` tinyint(1) NOT NULL DEFAULT '0',
  `article_import` tinyint(1) NOT NULL DEFAULT '0',
  `article_update` tinyint(1) NOT NULL DEFAULT '0',
  `disable_list` tinyint(1) NOT NULL,
  `email_preview` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Email Manager HTML Vorschau',
  `collect_new_orders` tinyint(1) NOT NULL COMMENT 'customfeld',
  `change_adminrights` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Adminrechte bearbeiten',
  `send_order` int(1) NOT NULL DEFAULT '0',
  `email_known_typos` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `orders_invoice_print` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `stats_paymentmethods` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `transfer_check` tinyint(4) NOT NULL COMMENT 'zuordnen von nicht autom. zuordbaren Zahlungen',
  `check_missing_images` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `product_deactivation_wizard` tinyint(3) unsigned NOT NULL,
  `new_products_list` tinyint(3) unsigned NOT NULL,
  `rma` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Recht für RMA-Bearbeitung',
  `rma_refund` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Recht Rückerstattungen auszuführen',
  `stockproducts_list` tinyint(4) NOT NULL COMMENT 'customfeld',
  `rma_create` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `rma_packets` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.admin_access: 0 rows
DELETE FROM `admin_access`;
/*!40000 ALTER TABLE `admin_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_access` ENABLE KEYS */;


-- Dumping structure for table crazy_import.banktransfer
DROP TABLE IF EXISTS `banktransfer`;
CREATE TABLE IF NOT EXISTS `banktransfer` (
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `banktransfer_owner` varchar(64) DEFAULT NULL,
  `banktransfer_number` varchar(24) DEFAULT NULL,
  `banktransfer_bankname` varchar(255) DEFAULT NULL,
  `banktransfer_blz` varchar(8) DEFAULT NULL,
  `banktransfer_status` int(11) DEFAULT NULL,
  `banktransfer_prz` char(2) DEFAULT NULL,
  `banktransfer_fax` char(2) DEFAULT NULL,
  KEY `orders_id` (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.banktransfer: 0 rows
DELETE FROM `banktransfer`;
/*!40000 ALTER TABLE `banktransfer` DISABLE KEYS */;
/*!40000 ALTER TABLE `banktransfer` ENABLE KEYS */;


-- Dumping structure for table crazy_import.banners
DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `banners_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_title` varchar(64) NOT NULL,
  `banners_url` varchar(255) NOT NULL,
  `banners_image` varchar(64) NOT NULL,
  `banners_group` varchar(10) NOT NULL,
  `banners_html_text` text,
  `expires_impressions` int(7) DEFAULT '0',
  `expires_date` datetime DEFAULT NULL,
  `date_scheduled` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`banners_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.banners: 0 rows
DELETE FROM `banners`;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;


-- Dumping structure for table crazy_import.banners_history
DROP TABLE IF EXISTS `banners_history`;
CREATE TABLE IF NOT EXISTS `banners_history` (
  `banners_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_id` int(11) NOT NULL,
  `banners_shown` int(5) NOT NULL DEFAULT '0',
  `banners_clicked` int(5) NOT NULL DEFAULT '0',
  `banners_history_date` datetime NOT NULL,
  PRIMARY KEY (`banners_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.banners_history: 0 rows
DELETE FROM `banners_history`;
/*!40000 ALTER TABLE `banners_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `banners_history` ENABLE KEYS */;


-- Dumping structure for table crazy_import.blog_categories
DROP TABLE IF EXISTS `blog_categories`;
CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `titel` varchar(150) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `date` varchar(10) NOT NULL,
  `update_date` varchar(10) NOT NULL,
  `meta_title` text,
  `meta_desc` text,
  `meta_key` text,
  PRIMARY KEY (`id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.blog_categories: 0 rows
DELETE FROM `blog_categories`;
/*!40000 ALTER TABLE `blog_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_categories` ENABLE KEYS */;


-- Dumping structure for table crazy_import.blog_comment
DROP TABLE IF EXISTS `blog_comment`;
CREATE TABLE IF NOT EXISTS `blog_comment` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL DEFAULT '',
  `text` text,
  `date` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.blog_comment: 0 rows
DELETE FROM `blog_comment`;
/*!40000 ALTER TABLE `blog_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_comment` ENABLE KEYS */;


-- Dumping structure for table crazy_import.blog_items
DROP TABLE IF EXISTS `blog_items`;
CREATE TABLE IF NOT EXISTS `blog_items` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '',
  `name` varchar(200) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `date` varchar(10) NOT NULL,
  `date_update` varchar(10) NOT NULL,
  `meta_title` text,
  `meta_keywords` text,
  `meta_description` text,
  `lenght` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.blog_items: 0 rows
DELETE FROM `blog_items`;
/*!40000 ALTER TABLE `blog_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_items` ENABLE KEYS */;


-- Dumping structure for table crazy_import.blog_settings
DROP TABLE IF EXISTS `blog_settings`;
CREATE TABLE IF NOT EXISTS `blog_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `blog_key` varchar(20) DEFAULT NULL,
  `wert` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.blog_settings: 0 rows
DELETE FROM `blog_settings`;
/*!40000 ALTER TABLE `blog_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_settings` ENABLE KEYS */;


-- Dumping structure for table crazy_import.blog_start
DROP TABLE IF EXISTS `blog_start`;
CREATE TABLE IF NOT EXISTS `blog_start` (
  `id` int(1) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.blog_start: 0 rows
DELETE FROM `blog_start`;
/*!40000 ALTER TABLE `blog_start` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_start` ENABLE KEYS */;


-- Dumping structure for table crazy_import.blog_vote
DROP TABLE IF EXISTS `blog_vote`;
CREATE TABLE IF NOT EXISTS `blog_vote` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `blog_id` int(8) NOT NULL,
  `vote_nr` int(8) NOT NULL,
  `img_nr` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.blog_vote: 0 rows
DELETE FROM `blog_vote`;
/*!40000 ALTER TABLE `blog_vote` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_vote` ENABLE KEYS */;


-- Dumping structure for table crazy_import.blz_table
DROP TABLE IF EXISTS `blz_table`;
CREATE TABLE IF NOT EXISTS `blz_table` (
  `blz` char(10) NOT NULL,
  `bank_description` varchar(58) NOT NULL,
  `btx_name` char(255) NOT NULL,
  `bic` char(11) NOT NULL,
  `checksum_method` char(2) NOT NULL,
  KEY `blz` (`blz`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table crazy_import.blz_table: 0 rows
DELETE FROM `blz_table`;
/*!40000 ALTER TABLE `blz_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `blz_table` ENABLE KEYS */;


-- Dumping structure for table crazy_import.campaigns
DROP TABLE IF EXISTS `campaigns`;
CREATE TABLE IF NOT EXISTS `campaigns` (
  `campaigns_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaigns_name` varchar(32) NOT NULL DEFAULT '',
  `campaigns_refID` varchar(64) DEFAULT NULL,
  `campaigns_leads` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`campaigns_id`),
  KEY `IDX_CAMPAIGNS_NAME` (`campaigns_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.campaigns: 0 rows
DELETE FROM `campaigns`;
/*!40000 ALTER TABLE `campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigns` ENABLE KEYS */;


-- Dumping structure for table crazy_import.campaigns_ip
DROP TABLE IF EXISTS `campaigns_ip`;
CREATE TABLE IF NOT EXISTS `campaigns_ip` (
  `user_ip` varchar(15) NOT NULL,
  `time` datetime NOT NULL,
  `campaign` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.campaigns_ip: 0 rows
DELETE FROM `campaigns_ip`;
/*!40000 ALTER TABLE `campaigns_ip` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigns_ip` ENABLE KEYS */;


-- Dumping structure for table crazy_import.card_blacklist
DROP TABLE IF EXISTS `card_blacklist`;
CREATE TABLE IF NOT EXISTS `card_blacklist` (
  `blacklist_id` int(5) NOT NULL AUTO_INCREMENT,
  `blacklist_card_number` varchar(20) NOT NULL DEFAULT '',
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  KEY `blacklist_id` (`blacklist_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.card_blacklist: 0 rows
DELETE FROM `card_blacklist`;
/*!40000 ALTER TABLE `card_blacklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `card_blacklist` ENABLE KEYS */;


-- Dumping structure for table crazy_import.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_image` varchar(64) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `categories_status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `categories_template` varchar(64) DEFAULT NULL,
  `group_permission_0` tinyint(1) NOT NULL,
  `group_permission_1` tinyint(1) NOT NULL,
  `group_permission_2` tinyint(1) NOT NULL,
  `group_permission_3` tinyint(1) NOT NULL,
  `listing_template` varchar(64) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `products_sorting` varchar(32) DEFAULT NULL,
  `products_sorting2` varchar(32) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `import_key` varchar(255) NOT NULL COMMENT 'Unter welchem Begriff die Kategorie in der Artikelliste geführt wird',
  `hide_actionshots` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`categories_id`),
  KEY `idx_categories_parent_id` (`parent_id`),
  KEY `categories_status` (`categories_status`)
) ENGINE=MyISAM AUTO_INCREMENT=582 DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.categories: 580 rows
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`categories_id`, `categories_image`, `parent_id`, `categories_status`, `categories_template`, `group_permission_0`, `group_permission_1`, `group_permission_2`, `group_permission_3`, `listing_template`, `sort_order`, `products_sorting`, `products_sorting2`, `date_added`, `last_modified`, `import_key`, `hide_actionshots`) VALUES
	(1, 'cat_001.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:30:52', '', 0),
	(2, '2.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-11-08 01:43:42', '', 0),
	(3, 'cat_003.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(4, 'cat_004.jpg', 3, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(5, 'cat_005.jpg', 3, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(6, 'cat_006.jpg', 3, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(7, 'cat_007.jpg', 3, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(8, 'cat_008.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(9, 'cat_009.jpg', 8, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(10, 'cat_010.jpg', 8, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(11, 'cat_011.jpg', 8, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(12, 'cat_012.jpg', 8, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(13, 'cat_013.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(14, 'cat_014.jpg', 13, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(15, 'cat_015.jpg', 13, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(16, 'cat_016.jpg', 13, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(17, '17.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-11-08 01:45:25', '', 0),
	(18, 'cat_018.jpg', 17, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(19, 'cat_019.jpg', 17, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(20, 'cat_020.jpg', 17, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(21, 'cat_021.jpg', 17, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(22, 'cat_022.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(23, 'cat_023.jpg', 22, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(24, 'cat_024.jpg', 22, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(25, 'cat_025.jpg', 22, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(26, 'cat_026.jpg', 22, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(27, 'cat_027.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 7, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:47:29', '', 0),
	(28, '28.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 8, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-11-08 01:46:01', '', 0),
	(29, 'cat_029.jpg', 28, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(30, 'cat_030.jpg', 28, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(31, 'cat_031.jpg', 28, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(32, '32.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 9, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-11-08 01:47:03', '', 0),
	(33, 'cat_033.jpg', 32, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(34, 'cat_034.jpg', 32, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(35, 'cat_035.jpg', 32, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(36, '36.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 10, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-11-08 01:47:36', '', 0),
	(37, 'cat_037.jpg', 36, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(38, 'cat_038.jpg', 36, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(39, 'cat_039.jpg', 36, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(40, 'cat_040.jpg', 36, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:48:23', '', 0),
	(41, 'cat_041.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 11, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(42, 'cat_042.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 12, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(43, 'cat_043.jpg', 42, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(44, 'cat_044.jpg', 42, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(45, 'cat_045.jpg', 42, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(46, 'cat_046.jpg', 42, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:49:16', '', 0),
	(47, 'cat_047.jpg', 42, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(48, 'cat_048.jpg', 42, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(49, '49.jpg', 1, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 13, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-11-08 01:48:11', '', 0),
	(50, 'cat_050.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:32:00', '', 0),
	(51, 'cat_051.jpg', 50, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:56:52', '', 0),
	(52, 'cat_052.jpg', 51, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(53, 'cat_053.jpg', 51, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(54, 'cat_054.jpg', 51, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 04:00:27', '', 0),
	(55, 'cat_055.jpg', 51, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(56, 'cat_056.jpg', 50, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:57:49', '', 0),
	(57, 'cat_057.jpg', 56, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(58, 'cat_058.jpg', 56, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(59, 'cat_059.jpg', 56, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 04:01:19', '', 0),
	(60, 'cat_060.jpg', 56, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(61, 'cat_061.jpg', 50, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(62, 'cat_062.jpg', 61, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(63, 'cat_063.jpg', 61, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(64, 'cat_064.jpg', 61, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 04:02:02', '', 0),
	(65, 'cat_065.jpg', 61, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(66, 'cat_066.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:33:22', '', 0),
	(67, 'cat_067.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(68, 'cat_068.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(69, 'cat_069.jpg', 68, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(70, 'cat_070.jpg', 68, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(71, 'cat_071.jpg', 68, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(72, 'cat_072.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(73, 'cat_073.jpg', 72, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(74, 'cat_074.jpg', 72, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(75, 'cat_075.jpg', 72, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(76, 'cat_076.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(77, 'cat_077.jpg', 76, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(78, 'cat_078.jpg', 76, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(79, 'cat_079.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(80, 'cat_080.jpg', 79, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(81, 'cat_081.jpg', 79, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(82, 'cat_082.jpg', 79, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(83, 'cat_083.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(84, 'cat_084.jpg', 83, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(85, 'cat_085.jpg', 83, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(86, 'cat_086.jpg', 83, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(87, 'cat_087.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 7, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:50:38', '', 0),
	(88, 'cat_088.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 8, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(89, 'cat_089.jpg', 88, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(90, 'cat_090.jpg', 88, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(91, 'cat_091.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 9, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(92, 'cat_092.jpg', 91, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(93, 'cat_093.jpg', 91, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(94, 'cat_094.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 10, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(95, 'cat_095.jpg', 94, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(96, 'cat_096.jpg', 94, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(97, 'cat_097.jpg', 94, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(98, 'cat_098.jpg', 94, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:53:36', '', 0),
	(99, 'cat_099.jpg', 32, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(100, 'cat_100.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 11, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(101, 'cat_101.jpg', 100, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(102, 'cat_102.jpg', 100, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(103, 'cat_103.jpg', 100, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(104, 'cat_104.jpg', 100, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:54:36', '', 0),
	(105, 'cat_105.jpg', 100, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(106, 'cat_106.jpg', 100, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(107, 'cat_107.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 12, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:59:32', '', 0),
	(108, 'cat_108.jpg', 66, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 13, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(109, 'cat_109.jpg', 108, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(110, 'cat_110.jpg', 108, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(111, 'cat_111.jpg', 108, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 04:02:49', '', 0),
	(112, 'cat_112.jpg', 108, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(113, 'cat_113.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:34:41', '', 0),
	(114, 'cat_114.jpg', 113, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(115, 'cat_115.jpg', 113, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(116, 'cat_116.jpg', 113, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-18 14:59:42', '', 0),
	(117, 'cat_117.jpg', 113, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:00:48', '', 0),
	(118, 'cat_118.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(119, 'cat_119.jpg', 118, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(120, 'cat_120.jpg', 118, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(121, 'cat_121.jpg', 118, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:02:27', '', 0),
	(122, 'cat_122.jpg', 118, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(123, 'cat_123.jpg', 118, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(124, 'cat_124.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(125, 'cat_125.jpg', 124, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(126, 'cat_126.jpg', 124, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(127, 'cat_127.jpg', 124, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:03:32', '', 0),
	(128, 'cat_128.jpg', 127, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(129, 'cat_129.jpg', 127, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(130, 'cat_130.jpg', 127, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(131, 'cat_131.jpg', 127, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(132, 'cat_132.jpg', 124, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(133, 'cat_133.jpg', 124, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(134, 'cat_134.jpg', 124, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:41:19', '', 0),
	(135, 'cat_135.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 7, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(136, 'cat_136.jpg', 135, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(137, 'cat_137.jpg', 135, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(138, 'cat_138.jpg', 135, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(139, 'cat_139.jpg', 135, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(140, 'cat_140.jpg', 135, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(141, 'cat_141.jpg', 135, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:42:35', '', 0),
	(142, 'cat_142.jpg', 135, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 7, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(143, 'cat_143.jpg', 135, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 8, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(144, 'cat_144.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 8, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(145, 'cat_145.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 9, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:35:46', '', 0),
	(146, 'cat_146.jpg', 145, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:04:57', '', 0),
	(147, 'cat_147.jpg', 145, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:05:54', '', 0),
	(148, 'cat_148.jpg', 145, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:07:17', '', 0),
	(149, 'cat_149.jpg', 145, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:43:53', '', 0),
	(150, 'cat_150.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 10, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:36:54', '', 0),
	(151, 'cat_151.jpg', 150, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(152, 'cat_152.jpg', 151, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(153, 'cat_153.jpg', 151, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(154, 'cat_154.jpg', 151, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 04:05:03', '', 0),
	(155, 'cat_155.jpg', 151, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(156, 'cat_156.jpg', 150, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(157, 'cat_157.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 11, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-22 11:56:00', '', 0),
	(158, 'cat_158.jpg', 157, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(159, 'cat_159.jpg', 157, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(160, 'cat_160.jpg', 157, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:10:10', '', 0),
	(161, 'cat_161.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 12, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:13:20', '', 0),
	(162, 'cat_162.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 13, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(163, 'cat_163.jpg', 162, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(164, 'cat_164.jpg', 162, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(165, 'cat_165.jpg', 162, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(166, 'cat_166.jpg', 162, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:19:54', '', 0),
	(167, 'cat_167.jpg', 162, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(168, 'cat_168.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 14, 'p.products_price', 'DESC', '2008-06-17 15:53:42', '2010-12-31 04:22:45', '', 0),
	(169, 'cat_169.jpg', 168, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(170, 'cat_170.jpg', 168, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(171, 'cat_171.jpg', 168, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(172, 'cat_172.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 15, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(173, 'cat_173.jpg', 172, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(174, 'cat_174.jpg', 172, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(175, 'cat_175.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 16, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(176, 'cat_176.jpg', 175, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(177, 'cat_177.jpg', 175, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(178, 'cat_178.jpg', 175, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:22:42', '', 0),
	(179, 'cat_179.jpg', 175, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(180, 'cat_180.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 17, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:26:17', '', 0),
	(181, 'cat_181.jpg', 180, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(182, 'cat_182.jpg', 180, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 13:26:28', '', 0),
	(183, 'cat_183.jpg', 180, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(184, 'cat_184.jpg', 180, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(185, 'cat_185.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 18, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:28:49', '', 0),
	(186, 'cat_186.jpg', 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 19, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-25 10:37:08', '', 0),
	(187, 'cat_187.jpg', 162, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(188, 'cat_188.jpg', 91, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', '2008-06-19 12:52:42', '', 0),
	(189, 'cat_189.jpg', 135, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 9, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(190, 'cat_190.jpg', 145, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(191, 'cat_191.jpg', 120, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(192, 'cat_192.jpg', 120, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(193, 'cat_193.jpg', 121, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(194, 'cat_194.jpg', 121, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(195, 'cat_195.jpg', 118, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 7, '`p`.`products_sort`', 'DESC', '2008-06-17 15:53:42', NULL, '', 0),
	(196, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'default', 100, 'p.products_price', 'DESC', '2008-10-14 14:21:06', '2011-04-05 05:41:00', 'Balls & Attachments', 1),
	(197, '197.jpg', 196, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 08:50:35', '2008-11-13 08:15:58', '', 0),
	(198, NULL, 196, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 08:54:58', NULL, '', 0),
	(199, '199.jpg', 196, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 08:58:21', '2008-11-13 08:20:19', '', 0),
	(200, '200.jpg', 196, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 08:59:32', '2008-11-13 08:21:05', '', 0),
	(201, '201.jpg', 196, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-10-16 09:02:47', '2008-11-13 08:21:45', '', 0),
	(202, '202.jpg', 196, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-10-16 09:03:51', '2008-11-13 08:23:07', '', 0),
	(203, '203.jpg', 197, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 09:05:18', '2008-11-13 08:23:59', '', 0),
	(204, '204.jpg', 197, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 09:06:05', '2008-11-13 08:24:40', '', 0),
	(205, '205.jpg', 197, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 09:06:48', '2008-11-13 08:25:26', '', 0),
	(206, '206.jpg', 197, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 09:07:42', '2008-11-13 08:26:03', '', 0),
	(207, NULL, 197, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 09:16:55', NULL, '', 0),
	(208, '208.jpg', 204, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 09:22:32', '2008-11-13 08:26:45', '', 0),
	(209, '209.jpg', 204, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 09:23:06', '2008-11-13 08:27:30', '', 0),
	(210, '210.jpg', 204, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 09:23:46', '2008-11-13 08:32:03', '', 0),
	(211, '211.jpg', 204, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 09:24:26', '2008-11-13 08:32:59', '', 0),
	(212, '212.jpg', 205, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 09:25:53', '2008-11-13 08:33:51', '', 0),
	(213, '213.jpg', 205, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 09:26:28', '2008-11-13 08:37:44', '', 0),
	(214, '214.jpg', 205, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 09:27:09', '2008-11-13 08:38:28', '', 0),
	(215, '215.jpg', 205, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 09:27:53', '2008-11-13 08:39:08', '', 0),
	(216, NULL, 198, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 09:34:46', NULL, '', 0),
	(217, NULL, 198, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 09:54:24', NULL, '', 0),
	(218, NULL, 198, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 09:58:01', NULL, '', 0),
	(219, NULL, 198, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 09:58:43', NULL, '', 0),
	(220, NULL, 217, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 10:00:14', NULL, '', 0),
	(221, NULL, 217, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 10:01:54', NULL, '', 0),
	(222, NULL, 217, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 10:02:33', NULL, '', 0),
	(223, NULL, 217, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 10:03:02', NULL, '', 0),
	(224, NULL, 218, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 10:21:22', NULL, '', 0),
	(225, NULL, 218, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 10:21:58', NULL, '', 0),
	(226, NULL, 218, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 10:22:49', NULL, '', 0),
	(227, NULL, 218, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 10:23:41', NULL, '', 0),
	(228, '228.jpg', 199, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 10:25:14', '2008-11-13 08:39:54', '', 0),
	(229, '229.jpg', 199, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 10:26:07', '2008-11-13 08:40:31', '', 0),
	(230, '230.jpg', 199, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 10:26:46', '2008-11-13 08:41:11', '', 0),
	(231, '231.jpg', 229, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 10:44:55', '2008-11-13 08:41:45', '', 0),
	(232, '232.jpg', 229, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 10:45:31', '2008-11-13 08:42:19', '', 0),
	(233, '233.jpg', 229, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 10:46:07', '2008-11-13 08:42:59', '', 0),
	(234, '234.jpg', 229, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 10:46:46', '2008-11-13 08:43:45', '', 0),
	(235, '235.jpg', 230, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 10:47:53', '2008-11-13 08:44:24', '', 0),
	(236, '236.jpg', 230, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 10:48:27', '2008-11-13 08:45:06', '', 0),
	(237, '237.jpg', 230, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 10:49:02', '2008-11-13 08:45:53', '', 0),
	(238, '238.jpg', 230, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 10:49:38', '2008-11-13 08:46:30', '', 0),
	(239, '239.jpg', 200, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 10:50:51', '2008-11-13 08:47:08', '', 0),
	(240, '240.jpg', 200, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 10:51:25', '2008-11-13 08:47:46', '', 0),
	(241, '241.jpg', 200, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 10:52:01', '2008-11-13 08:48:46', '', 0),
	(242, '242.jpg', 240, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 10:53:24', '2008-11-13 08:51:04', '', 0),
	(243, '243.jpg', 240, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 10:54:16', '2008-11-13 08:51:44', '', 0),
	(244, '244.jpg', 240, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 10:55:15', '2008-11-13 08:52:24', '', 0),
	(245, '245.jpg', 240, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 10:55:42', '2008-11-13 08:53:04', '', 0),
	(246, '246.jpg', 241, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 10:56:24', '2008-11-13 08:53:44', '', 0),
	(247, '247.jpg', 241, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 10:57:02', '2008-11-13 08:54:27', '', 0),
	(248, '248.jpg', 241, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 10:57:30', '2008-11-13 08:55:16', '', 0),
	(249, '249.jpg', 241, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 10:58:12', '2008-11-13 08:56:04', '', 0),
	(250, '250.jpg', 201, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 10:59:21', '2008-11-13 09:13:40', '', 0),
	(251, '251.jpg', 202, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 11:02:01', '2008-11-13 09:16:12', '', 0),
	(252, '252.jpg', 202, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 11:02:34', '2008-11-13 09:16:48', '', 0),
	(253, '253.jpg', 251, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 11:03:56', '2008-11-13 09:17:38', '', 0),
	(254, '254.jpg', 251, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 11:04:28', '2008-11-13 09:18:20', '', 0),
	(255, '255.jpg', 252, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 11:05:44', '2008-11-13 09:19:17', '', 0),
	(256, '256.jpg', 252, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 11:07:14', '2008-11-13 09:20:09', '', 0),
	(257, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 101, 'p.products_price', 'DESC', '2008-10-16 11:43:24', '2010-12-31 04:15:18', 'Loose Pins', 1),
	(258, '258.jpg', 257, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 11:46:05', '2008-11-13 09:21:17', '', 0),
	(259, '259.jpg', 257, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 11:47:39', '2008-11-13 09:21:58', '', 0),
	(260, '260.jpg', 257, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 11:48:38', '2008-11-13 09:23:10', '', 0),
	(261, '261.jpg', 257, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-16 11:49:06', '2008-11-13 09:24:01', '', 0),
	(262, '262.jpg', 257, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-10-16 11:49:40', '2008-11-13 09:25:07', '', 0),
	(263, '263.jpg', 257, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-10-16 11:50:23', '2008-11-13 09:26:48', '', 0),
	(264, '264.jpg', 258, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 11:52:10', '2008-11-13 09:28:46', '', 0),
	(265, '265.jpg', 258, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 11:53:02', '2008-11-13 09:29:37', '', 0),
	(266, '266.jpg', 258, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 11:53:40', '2008-11-13 09:30:13', '', 0),
	(267, '267.jpg', 265, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 11:54:32', '2008-11-13 09:31:04', '', 0),
	(268, '268.jpg', 265, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 11:55:00', '2008-11-13 09:32:21', '', 0),
	(269, '269.jpg', 265, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 11:55:32', '2008-11-13 09:33:10', '', 0),
	(270, '270.jpg', 266, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 11:56:21', '2008-11-13 09:33:55', '', 0),
	(271, '271.jpg', 266, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 11:56:49', '2008-11-13 09:37:55', '', 0),
	(272, '272.jpg', 266, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 11:57:16', '2008-11-13 09:39:04', '', 0),
	(273, '273.jpg', 259, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 16:30:56', '2008-11-13 09:41:29', '', 0),
	(274, '274.jpg', 259, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 16:31:33', '2008-11-13 09:42:49', '', 0),
	(275, '275.jpg', 259, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 16:32:07', '2008-11-13 09:43:49', '', 0),
	(276, '276.jpg', 274, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 16:33:28', '2008-11-13 09:46:37', '', 0),
	(277, '277.jpg', 274, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 16:34:00', '2008-11-13 09:52:13', '', 0),
	(278, '278.jpg', 274, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 16:34:40', '2008-11-13 09:55:08', '', 0),
	(279, '279.jpg', 275, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 16:35:38', '2008-11-13 09:56:14', '', 0),
	(280, '280.jpg', 275, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 16:36:08', '2008-11-13 09:56:44', '', 0),
	(281, '281.jpg', 275, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 16:36:42', '2008-11-13 09:57:14', '', 0),
	(282, '282.jpg', 260, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 16:38:02', '2008-11-13 09:57:59', '', 0),
	(283, '283.jpg', 260, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 16:38:35', '2008-11-13 09:58:29', '', 0),
	(284, '284.jpg', 260, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 16:39:21', '2008-11-13 09:59:00', '', 0),
	(285, '285.jpg', 283, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 16:40:47', '2008-11-13 09:59:48', '', 0),
	(286, '286.jpg', 283, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 16:41:27', '2008-11-13 10:00:48', '', 0),
	(287, '287.jpg', 283, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 16:42:10', '2008-11-13 10:01:57', '', 0),
	(288, '288.jpg', 284, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 16:43:14', '2008-11-13 10:02:35', '', 0),
	(289, '289.jpg', 284, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 16:44:19', '2008-11-13 10:05:39', '', 0),
	(290, '290.jpg', 284, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 16:45:02', '2008-11-13 10:06:16', '', 0),
	(291, '291.jpg', 261, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 16:46:12', '2008-11-13 10:06:55', '', 0),
	(292, '292.jpg', 261, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 16:46:56', '2008-11-13 10:07:31', '', 0),
	(293, '293.jpg', 261, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 16:47:28', '2008-11-13 10:08:03', '', 0),
	(294, '294.jpg', 292, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 16:48:08', '2008-11-13 10:09:20', '', 0),
	(295, '295.jpg', 292, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 16:48:44', '2008-11-13 10:09:59', '', 0),
	(296, '296.jpg', 292, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 16:49:14', '2008-11-13 10:11:31', '', 0),
	(297, '297.jpg', 293, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-16 16:50:08', '2008-11-13 10:17:19', '', 0),
	(298, '298.jpg', 293, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-16 16:50:46', '2008-11-13 10:18:35', '', 0),
	(299, '299.jpg', 293, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-16 16:51:27', '2008-11-13 10:19:04', '', 0),
	(300, '300.jpg', 262, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 06:45:00', '2008-11-13 10:19:47', '', 0),
	(301, '301.jpg', 262, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 06:45:47', '2008-11-13 10:20:19', '', 0),
	(302, '302.jpg', 262, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 06:46:52', '2008-11-13 10:20:52', '', 0),
	(303, '303.jpg', 301, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 06:49:12', '2008-11-13 10:21:34', '', 0),
	(304, '304.jpg', 301, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 06:49:42', '2008-11-13 10:21:59', '', 0),
	(305, '305.jpg', 301, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 06:50:39', '2008-11-13 10:22:32', '', 0),
	(306, '306.jpg', 302, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 06:51:23', '2008-11-13 10:23:23', '', 0),
	(307, '307.jpg', 302, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 06:51:49', '2008-11-13 10:23:57', '', 0),
	(308, '308.jpg', 302, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 06:52:19', '2008-11-13 10:24:45', '', 0),
	(309, '309.jpg', 263, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 07:44:44', '2008-11-13 10:25:28', '', 0),
	(310, '310.jpg', 263, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 07:45:33', '2008-11-13 10:25:54', '', 0),
	(311, '311.jpg', 309, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 07:46:19', '2008-11-13 10:27:21', '', 0),
	(312, '312.jpg', 309, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 07:47:03', '2008-11-13 10:29:25', '', 0),
	(313, '313.jpg', 310, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 07:47:48', '2008-11-13 10:31:38', '', 0),
	(314, '314.jpg', 310, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 07:48:16', '2008-11-13 10:33:15', '', 0),
	(315, '315.jpg', 257, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 7, '`p`.`products_sort`', 'DESC', '2008-10-17 07:53:49', '2008-11-13 09:27:46', '', 0),
	(316, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 102, 'p.products_price', 'DESC', '2008-10-17 08:01:23', '2010-12-31 04:16:53', 'Piercing Rings', 0),
	(317, '317.jpg', 316, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 08:02:40', '2008-11-13 10:49:54', '', 0),
	(318, '318.jpg', 316, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 08:03:28', '2008-11-13 10:50:30', '', 0),
	(319, '319.jpg', 316, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 08:04:00', '2008-11-13 10:51:00', '', 0),
	(320, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 103, 'p.products_price', 'DESC', '2008-10-17 08:16:31', '2010-12-31 04:18:19', 'Barbells', 0),
	(321, '321.jpg', 320, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 08:17:51', '2008-11-13 10:51:58', '', 0),
	(322, '322.jpg', 320, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 08:18:18', '2008-11-13 10:53:04', '', 0),
	(323, '323.jpg', 320, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 08:18:42', '2008-11-13 10:52:27', '', 0),
	(324, '324.jpg', 320, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 08:19:07', '2008-11-13 10:52:40', '', 0),
	(325, '325.jpg', 322, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 08:21:01', '2008-11-13 10:54:47', '', 0),
	(326, '326.jpg', 322, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 08:23:38', '2008-11-13 10:55:31', '', 0),
	(327, '327.jpg', 322, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 08:24:50', '2008-11-13 10:55:37', '', 0),
	(328, '328.jpg', 322, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 08:25:30', '2008-11-13 10:55:45', '', 0),
	(329, '329.jpg', 325, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 08:28:54', '2008-11-13 10:57:16', '', 0),
	(330, '330.jpg', 325, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 08:29:20', '2008-11-13 10:58:17', '', 0),
	(331, '331.jpg', 325, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 08:29:51', '2008-11-13 10:58:25', '', 0),
	(332, '332.jpg', 325, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 08:30:21', '2008-11-13 10:58:30', '', 0),
	(333, '333.jpg', 326, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 08:32:05', '2008-11-13 10:57:32', '', 0),
	(334, '334.jpg', 326, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 08:32:32', '2008-11-13 10:58:50', '', 0),
	(335, '335.jpg', 326, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 08:33:06', '2008-11-13 10:58:59', '', 0),
	(336, '336.jpg', 326, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 08:33:37', '2008-11-13 10:59:38', '', 0),
	(337, '337.jpg', 327, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 08:34:31', '2008-11-13 10:57:46', '', 0),
	(338, '338.jpg', 327, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 08:34:56', '2008-11-13 10:59:19', '', 0),
	(339, '339.jpg', 323, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 08:38:16', '2008-11-13 10:54:09', '', 0),
	(340, '340.jpg', 323, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 08:38:48', '2008-11-13 10:54:38', '', 0),
	(341, '341.jpg', 323, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 08:39:23', '2008-11-13 10:54:30', '', 0),
	(342, '342.jpg', 339, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 08:40:27', '2008-11-13 11:00:54', '', 0),
	(343, '343.jpg', 339, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 08:40:49', '2008-11-13 11:01:27', '', 0),
	(344, '344.jpg', 339, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 08:41:19', '2008-11-13 11:01:33', '', 0),
	(345, '345.jpg', 339, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-10-17 08:41:44', '2008-11-13 11:02:19', '', 0),
	(346, '346.jpg', 340, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 09:00:38', '2008-11-13 11:01:06', '', 0),
	(347, '347.jpg', 340, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 09:01:02', '2008-11-13 11:02:30', '', 0),
	(348, '348.jpg', 340, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 09:01:32', '2008-11-13 11:02:38', '', 0),
	(349, '349.jpg', 340, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 09:01:55', '2008-11-13 11:02:45', '', 0),
	(350, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 104, 'p.products_price', 'DESC', '2008-10-17 09:50:11', '2010-10-25 13:34:12', 'Bananas', 0),
	(351, '351.jpg', 350, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 09:51:33', '2008-11-13 11:03:37', '', 0),
	(352, '352.jpg', 350, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 09:52:22', '2008-11-13 11:03:50', '', 0),
	(353, '353.jpg', 350, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 09:56:40', '2008-11-13 11:03:59', '', 0),
	(354, '354.jpg', 350, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 09:57:15', '2008-11-13 11:04:06', '', 0),
	(355, '355.jpg', 352, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 09:57:55', '2008-11-13 11:26:46', '', 0),
	(356, '356.jpg', 352, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 09:58:28', '2008-11-13 11:27:28', '', 0),
	(357, '357.jpg', 352, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 09:59:01', '2008-11-13 11:27:33', '', 0),
	(358, '358.jpg', 355, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 10:00:00', '2008-11-13 11:29:29', '', 0),
	(359, '359.jpg', 355, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 10:00:29', '2008-11-13 11:30:04', '', 0),
	(360, '360.jpg', 355, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 10:01:07', '2008-11-13 11:30:15', '', 0),
	(361, '361.jpg', 355, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 10:01:42', '2008-11-13 11:30:30', '', 0),
	(362, '362.jpg', 356, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 10:02:32', '2008-11-13 11:29:51', '', 0),
	(363, '363.jpg', 356, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 10:03:20', '2008-11-13 11:30:31', '', 0),
	(364, '364.jpg', 356, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 10:03:50', '2008-11-13 11:31:00', '', 0),
	(365, '365.jpg', 353, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 10:04:50', '2008-11-13 11:31:44', '', 0),
	(366, '366.jpg', 353, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 10:05:26', '2008-11-13 11:32:03', '', 0),
	(367, '367.jpg', 353, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 10:06:06', '2008-11-13 11:32:16', '', 0),
	(368, '368.jpg', 365, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 10:25:26', '2008-11-13 11:34:54', '', 0),
	(369, '369.jpg', 365, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 10:25:58', '2008-11-13 11:36:30', '', 0),
	(370, '370.jpg', 365, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 10:26:35', '2008-11-13 11:36:41', '', 0),
	(371, '371.jpg', 365, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 10:27:04', '2008-11-13 11:36:41', '', 0),
	(372, '372.jpg', 366, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 10:27:58', '2008-11-13 11:33:58', '', 0),
	(373, '373.jpg', 366, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 10:28:36', '2008-11-13 11:35:24', '', 0),
	(374, '374.jpg', 366, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 10:29:28', '2008-11-13 11:35:38', '', 0),
	(375, '375.jpg', 366, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 10:30:01', '2008-11-13 11:35:26', '', 0),
	(376, '376.jpg', 366, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-10-17 10:30:43', '2008-11-13 11:36:53', '', 0),
	(377, '377.jpg', 366, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-10-17 10:31:24', '2008-11-13 11:35:48', '', 0),
	(378, '378.jpg', 366, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 7, '`p`.`products_sort`', 'DESC', '2008-10-17 10:32:10', '2008-11-13 11:36:12', '', 0),
	(379, '379.jpg', 366, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 8, '`p`.`products_sort`', 'DESC', '2008-10-17 10:33:01', '2008-11-13 11:36:05', '', 0),
	(380, '380.jpg', 366, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 9, '`p`.`products_sort`', 'DESC', '2008-10-17 10:33:39', '2008-11-13 11:36:22', '', 0),
	(381, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 105, '`p`.`products_sort`', 'DESC', '2008-10-17 10:43:13', NULL, 'Labrets', 0),
	(382, '382.jpg', 381, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 10:48:09', '2008-11-13 11:37:58', '', 0),
	(383, '383.jpg', 381, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 10:48:45', '2008-11-13 11:38:20', '', 0),
	(384, '384.jpg', 381, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 10:49:29', '2008-11-13 11:38:22', '', 0),
	(385, '385.jpg', 383, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 10:50:50', '2008-11-13 11:41:27', '', 0),
	(386, '386.jpg', 383, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 10:51:22', '2008-11-13 11:42:33', '', 0),
	(387, '387.jpg', 383, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 10:51:57', '2008-11-13 11:42:42', '', 0),
	(388, '388.jpg', 383, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 10:52:32', '2008-11-13 11:42:46', '', 0),
	(389, '389.jpg', 383, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-10-17 10:53:20', '2008-11-13 11:42:54', '', 0),
	(390, '390.jpg', 385, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 10:55:33', '2008-11-13 11:47:06', '', 0),
	(391, '391.jpg', 385, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 11:05:32', '2008-11-13 11:49:02', '', 0),
	(392, '392.jpg', 385, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 11:06:20', '2008-11-13 11:49:03', '', 0),
	(393, '393.jpg', 385, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 11:07:37', '2008-11-13 11:49:11', '', 0),
	(394, '394.jpg', 386, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 11:08:52', '2008-11-13 11:47:25', '', 0),
	(395, '395.jpg', 386, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 11:09:27', '2008-11-13 11:49:18', '', 0),
	(396, '396.jpg', 386, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 11:09:56', '2008-11-13 11:49:23', '', 0),
	(397, '397.jpg', 386, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 11:10:54', '2008-11-13 11:49:36', '', 0),
	(398, '398.jpg', 387, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 11:11:53', '2008-11-13 11:47:50', '', 0),
	(399, '399.jpg', 387, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 11:12:22', '2008-11-13 11:49:50', '', 0),
	(400, '400.jpg', 384, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 11:13:42', '2008-11-13 11:41:59', '', 0),
	(401, '401.jpg', 384, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 11:14:22', '2008-11-13 11:42:59', '', 0),
	(402, '402.jpg', 384, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 11:15:29', '2008-11-13 11:43:08', '', 0),
	(403, '403.jpg', 384, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 11:16:16', '2008-11-13 11:43:11', '', 0),
	(404, '404.jpg', 384, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-10-17 12:38:32', '2008-11-13 11:43:26', '', 0),
	(405, '405.jpg', 400, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 12:50:08', '2008-11-13 11:48:05', '', 0),
	(406, '406.jpg', 400, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 12:50:47', '2008-11-13 11:49:57', '', 0),
	(407, '407.jpg', 400, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 12:51:18', '2008-11-13 11:49:30', '', 0),
	(408, '408.jpg', 400, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 12:52:23', '2008-11-13 11:50:07', '', 0),
	(409, '409.jpg', 401, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 12:53:49', '2008-11-13 11:48:24', '', 0),
	(410, '410.jpg', 401, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 12:54:23', '2008-11-13 11:50:11', '', 0),
	(411, '411.jpg', 401, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 12:54:58', '2008-11-13 11:50:24', '', 0),
	(412, '412.jpg', 401, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 12:55:36', '2008-11-13 11:50:27', '', 0),
	(413, '413.jpg', 402, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 12:56:35', '2008-11-13 11:48:52', '', 0),
	(414, '414.jpg', 402, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 12:57:14', '2008-11-13 11:50:33', '', 0),
	(415, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 106, 'p.products_price', 'DESC', '2008-10-17 13:00:51', '2010-12-31 04:18:49', 'Circular Barbells', 0),
	(416, '416.jpg', 415, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 13:01:56', '2008-11-13 11:51:50', '', 0),
	(417, '417.jpg', 415, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 13:02:32', '2008-11-13 11:52:49', '', 0),
	(418, '418.jpg', 415, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 13:03:14', '2008-11-13 11:53:03', '', 0),
	(419, '419.jpg', 415, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 13:04:01', '2008-11-13 11:53:02', '', 0),
	(420, '420.jpg', 417, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 13:07:51', '2008-11-13 11:53:20', '', 0),
	(421, '421.jpg', 417, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 13:10:22', '2008-11-13 11:54:15', '', 0),
	(422, '422.jpg', 417, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 13:10:59', '2008-11-13 11:54:27', '', 0),
	(423, '423.jpg', 417, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 13:11:28', '2008-11-13 11:54:28', '', 0),
	(424, '424.jpg', 418, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-17 13:18:26', '2008-11-13 11:53:50', '', 0),
	(425, '425.jpg', 418, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-17 13:18:52', '2008-11-13 11:54:37', '', 0),
	(426, '426.jpg', 418, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-17 13:19:37', '2008-11-13 11:54:41', '', 0),
	(427, '427.jpg', 418, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-17 13:20:08', '2008-11-13 11:54:42', '', 0),
	(428, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 107, 'p.products_price', 'DESC', '2008-10-19 03:42:50', '2010-10-25 13:35:29', 'Spirals', 0),
	(429, '429.jpg', 428, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 03:49:39', '2008-11-13 11:56:04', '', 0),
	(430, '430.jpg', 428, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 03:51:03', '2008-11-13 11:57:03', '', 0),
	(431, '431.jpg', 428, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 03:51:39', '2008-11-13 11:57:08', '', 0),
	(432, '432.jpg', 428, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-19 03:52:29', '2008-11-13 11:57:24', '', 0),
	(433, '433.jpg', 430, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 03:53:38', '2008-11-13 11:57:27', '', 0),
	(434, '434.jpg', 430, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 03:54:27', '2008-11-13 16:05:26', '', 0),
	(435, '435.jpg', 430, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 03:55:00', '2008-11-13 11:58:07', '', 0),
	(436, '436.jpg', 431, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 03:56:15', '2008-11-13 11:57:46', '', 0),
	(437, '437.jpg', 431, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 03:56:57', '2008-11-13 11:58:18', '', 0),
	(438, '438.jpg', 431, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 03:57:29', '2008-11-13 11:58:19', '', 0),
	(439, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'default', 108, 'p.products_price', 'DESC', '2008-10-19 04:38:57', '2012-02-13 09:08:34', 'Tunnel, Plug, Tube etc.', 0),
	(440, NULL, 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 109, '`p`.`products_sort`', 'DESC', '2008-10-19 04:39:41', NULL, '', 0),
	(441, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 110, 'p.products_price', 'DESC', '2008-10-19 04:40:14', '2010-12-31 04:21:28', 'Nose Piercings', 0),
	(442, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 111, 'p.products_price', 'DESC', '2008-10-19 04:40:59', '2010-12-31 04:22:14', 'Nipple Piercings', 0),
	(443, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'default', 112, 'p.products_price', 'DESC', '2008-10-19 04:41:42', '2011-05-16 09:49:20', 'Fashion Jewelry', 0),
	(444, NULL, 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 113, '`p`.`products_sort`', 'DESC', '2008-10-19 04:42:26', NULL, '', 0),
	(445, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 114, 'p.products_price', 'DESC', '2008-10-19 04:51:43', '2010-12-31 04:23:12', 'Fake Piercings', 0),
	(446, NULL, 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 115, '`p`.`products_sort`', 'DESC', '2008-10-19 04:52:23', NULL, '', 0),
	(447, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 116, 'p.products_price', 'DESC', '2008-10-19 04:53:15', '2010-12-10 04:10:07', 'Sterilized Piercings', 0),
	(448, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 117, 'p.products_price', 'DESC', '2008-10-19 04:53:59', '2010-12-31 04:24:03', 'Piercing Tools', 1),
	(449, NULL, 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 118, '`p`.`products_sort`', 'DESC', '2008-10-19 04:54:50', NULL, '', 0),
	(450, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 119, 'p.products_price', 'DESC', '2008-10-19 04:55:48', '2010-12-31 04:24:29', 'Wholesale Packs', 1),
	(451, '451.jpg', 439, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 04:58:19', '2008-11-13 11:59:47', '', 0),
	(452, '452.jpg', 439, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 04:58:53', '2008-11-13 12:00:52', '', 0),
	(453, '453.jpg', 439, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 04:59:22', '2008-11-13 12:01:44', '', 0),
	(454, '454.jpg', 439, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-19 05:00:04', '2008-11-13 12:00:55', '', 0),
	(455, '455.jpg', 439, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-10-19 05:00:35', '2008-11-13 12:01:07', '', 0),
	(456, '456.jpg', 439, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-10-19 05:01:25', '2008-11-13 12:01:11', '', 0),
	(457, '457.jpg', 451, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:02:15', '2008-11-13 12:01:21', '', 0),
	(458, '458.jpg', 451, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:02:47', '2008-11-13 12:01:51', '', 0),
	(459, '459.jpg', 451, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:03:25', '2008-11-13 12:01:57', '', 0),
	(460, '460.jpg', 453, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:04:13', '2008-11-13 12:01:32', '', 0),
	(461, '461.jpg', 453, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:04:41', '2008-11-13 12:02:03', '', 0),
	(462, '462.jpg', 453, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:05:16', '2008-11-13 12:02:07', '', 0),
	(463, '463.jpg', 440, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:06:22', '2008-11-13 12:03:20', '', 0),
	(464, '464.jpg', 440, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:07:03', '2008-11-13 12:03:47', '', 0),
	(465, '465.jpg', 440, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:07:45', '2008-11-13 12:03:50', '', 0),
	(466, '466.jpg', 440, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-19 05:08:26', '2008-11-13 12:04:02', '', 0),
	(467, '467.jpg', 441, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:09:50', '2008-11-13 12:04:54', '', 0),
	(468, '468.jpg', 441, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:11:00', '2008-11-13 12:05:42', '', 0),
	(469, '469.jpg', 441, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:11:48', '2008-11-13 12:05:43', '', 0),
	(470, '470.jpg', 441, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-19 05:12:25', '2008-11-13 12:05:49', '', 0),
	(471, '471.jpg', 467, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:13:35', '2008-11-13 12:05:52', '', 0),
	(472, '472.jpg', 467, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:14:11', '2008-11-13 12:06:23', '', 0),
	(473, '473.jpg', 467, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:14:51', '2008-11-13 12:06:30', '', 0),
	(474, '474.jpg', 468, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:15:54', '2008-11-13 12:06:08', '', 0),
	(475, '475.jpg', 468, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:16:23', '2008-11-13 12:06:43', '', 0),
	(476, '476.jpg', 468, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:16:55', '2008-11-13 12:06:48', '', 0),
	(477, '477.jpg', 442, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:22:17', '2008-11-13 12:07:48', '', 0),
	(478, '478.jpg', 442, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:22:50', '2008-11-13 12:08:36', '', 0),
	(479, '479.jpg', 442, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:23:32', '2008-11-13 12:08:25', '', 0),
	(480, '480.jpg', 443, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:25:18', '2008-11-13 12:08:39', '', 0),
	(481, '481.jpg', 443, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:25:52', '2008-11-13 12:09:11', '', 0),
	(482, '482.jpg', 444, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:26:51', '2008-11-13 15:56:47', '', 0),
	(483, '483.jpg', 444, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:27:20', '2008-11-13 15:59:36', '', 0),
	(484, '484.jpg', 447, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:28:10', '2008-11-13 12:10:00', '', 0),
	(485, '485.jpg', 447, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:28:57', '2008-11-13 12:10:32', '', 0),
	(486, '486.jpg', 447, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:29:34', '2008-11-13 12:10:39', '', 0),
	(487, '487.jpg', 447, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-19 05:30:06', '2008-11-13 12:10:48', '', 0),
	(488, '488.jpg', 449, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:30:56', '2008-11-13 12:11:32', '', 0),
	(489, '489.jpg', 449, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:31:54', '2008-11-13 12:12:00', '', 0),
	(490, '490.jpg', 449, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:32:33', '2008-11-13 12:12:03', '', 0),
	(491, '491.jpg', 450, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:38:14', '2008-11-07 07:59:43', '', 0),
	(492, '492.jpg', 450, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:38:50', '2008-11-07 07:58:40', '', 0),
	(493, '493.jpg', 450, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:39:22', '2008-11-07 08:15:20', '', 0),
	(494, '494.jpg', 450, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-19 05:40:01', '2008-11-07 08:14:27', '', 0),
	(495, '495.jpg', 450, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2008-10-19 05:40:41', '2008-11-07 08:18:16', '', 0),
	(496, '496.jpg', 450, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2008-10-19 05:41:36', '2008-11-07 08:20:31', '', 0),
	(497, NULL, 492, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:42:50', NULL, '', 0),
	(498, NULL, 492, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:43:18', NULL, '', 0),
	(499, NULL, 493, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:44:21', NULL, '', 0),
	(500, NULL, 493, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:45:12', NULL, '', 0),
	(501, NULL, 493, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:45:45', NULL, '', 0),
	(502, NULL, 495, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2008-10-19 05:46:33', NULL, '', 0),
	(503, NULL, 495, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 05:47:02', NULL, '', 0),
	(504, NULL, 495, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 05:47:30', NULL, '', 0),
	(505, '505.jpg', 200, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-19 07:54:21', '2008-11-13 08:50:09', '', 0),
	(506, '506.jpg', 201, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2008-10-19 08:49:31', '2008-11-13 09:15:26', '', 0),
	(507, '507.jpg', 201, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2008-10-19 08:54:44', '2008-11-13 09:14:34', '', 0),
	(508, '508.jpg', 449, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-19 08:57:10', '2008-11-13 12:12:17', '', 0),
	(509, NULL, 339, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-19 10:48:11', NULL, '', 0),
	(510, '510.jpg', 352, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2008-10-19 11:12:53', '2008-11-13 11:27:42', '', 0),
	(511, '511.jpg', 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 90, 'p.products_price', 'DESC', '2009-04-16 05:56:39', '2010-11-27 06:55:11', '', 0),
	(512, NULL, 0, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 90, '`p`.`products_sort`', 'DESC', '2009-04-16 05:57:27', NULL, '', 0),
	(513, '513.jpg', 511, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 100, '`p`.`products_sort`', 'DESC', '2009-04-16 06:00:26', '2009-04-28 11:48:54', '', 0),
	(514, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 109, 'p.products_price', 'DESC', '2009-04-16 07:00:43', '2010-10-25 13:38:37', 'Implants', 0),
	(515, '515.jpg', 450, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 75, '`p`.`products_sort`', 'DESC', '2009-04-27 09:31:17', '2009-04-28 04:58:03', '', 0),
	(516, '516.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2009-04-28 05:01:57', NULL, '', 0),
	(517, '517.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2009-04-28 05:03:12', NULL, '', 0),
	(518, '518.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2009-04-28 05:19:59', NULL, '', 0),
	(519, '519.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2009-04-28 05:39:10', NULL, '', 0),
	(520, '520.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2009-04-28 05:40:09', NULL, '', 0),
	(521, '521.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2009-04-28 05:42:14', NULL, '', 0),
	(522, '522.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 7, '`p`.`products_sort`', 'DESC', '2009-04-28 05:42:54', '2009-04-28 05:44:40', '', 0),
	(523, '523.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 8, '`p`.`products_sort`', 'DESC', '2009-04-28 05:45:26', NULL, '', 0),
	(524, '524.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 9, '`p`.`products_sort`', 'DESC', '2009-04-28 05:46:11', NULL, '', 0),
	(525, '525.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 10, '`p`.`products_sort`', 'DESC', '2009-04-28 05:47:00', NULL, '', 0),
	(526, '526.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 11, '`p`.`products_sort`', 'DESC', '2009-04-28 05:47:53', NULL, '', 0),
	(527, '527.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 12, '`p`.`products_sort`', 'DESC', '2009-04-28 05:48:41', NULL, '', 0),
	(528, '528.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 13, '`p`.`products_sort`', 'DESC', '2009-04-28 05:49:25', NULL, '', 0),
	(529, '529.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 14, '`p`.`products_sort`', 'DESC', '2009-04-28 05:50:16', NULL, '', 0),
	(530, '530.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 15, '`p`.`products_sort`', 'DESC', '2009-04-28 05:51:10', NULL, '', 0),
	(531, '531.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 16, '`p`.`products_sort`', 'DESC', '2009-04-28 05:52:05', NULL, '', 0),
	(532, '532.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 17, '`p`.`products_sort`', 'DESC', '2009-04-28 05:52:47', NULL, '', 0),
	(533, '533.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 18, '`p`.`products_sort`', 'DESC', '2009-04-28 05:53:32', NULL, '', 0),
	(534, '534.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 19, '`p`.`products_sort`', 'DESC', '2009-04-28 05:54:13', NULL, '', 0),
	(535, '535.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 20, '`p`.`products_sort`', 'DESC', '2009-04-28 05:57:47', NULL, '', 0),
	(536, '536.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 21, '`p`.`products_sort`', 'DESC', '2009-04-28 05:58:21', NULL, '', 0),
	(537, '537.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 22, '`p`.`products_sort`', 'DESC', '2009-04-28 05:59:14', NULL, '', 0),
	(538, '538.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 23, '`p`.`products_sort`', 'DESC', '2009-04-28 06:00:32', NULL, '', 0),
	(539, '539.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 24, '`p`.`products_sort`', 'DESC', '2009-04-28 06:01:09', NULL, '', 0),
	(540, '540.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 25, '`p`.`products_sort`', 'DESC', '2009-04-28 06:01:53', NULL, '', 0),
	(541, '541.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 26, '`p`.`products_sort`', 'DESC', '2009-04-28 06:02:40', NULL, '', 0),
	(542, '542.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 27, '`p`.`products_sort`', 'DESC', '2009-04-28 06:03:29', NULL, '', 0),
	(543, '543.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 28, '`p`.`products_sort`', 'DESC', '2009-04-28 06:23:20', NULL, '', 0),
	(544, '544.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 29, '`p`.`products_sort`', 'DESC', '2009-04-28 06:24:46', NULL, '', 0),
	(545, '545.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 30, '`p`.`products_sort`', 'DESC', '2009-04-28 06:25:49', NULL, '', 0),
	(546, '546.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 31, '`p`.`products_sort`', 'DESC', '2009-04-28 06:29:37', NULL, '', 0),
	(547, '547.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 32, '`p`.`products_sort`', 'DESC', '2009-04-28 06:30:43', NULL, '', 0),
	(548, '548.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 33, '`p`.`products_sort`', 'DESC', '2009-04-28 06:31:34', NULL, '', 0),
	(549, '549.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 34, '`p`.`products_sort`', 'DESC', '2009-04-28 06:32:50', NULL, '', 0),
	(550, '550.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 35, '`p`.`products_sort`', 'DESC', '2009-04-28 06:33:42', NULL, '', 0),
	(551, '551.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 36, '`p`.`products_sort`', 'DESC', '2009-04-28 06:34:36', NULL, '', 0),
	(552, '552.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 37, '`p`.`products_sort`', 'DESC', '2009-04-28 06:35:29', NULL, '', 0),
	(553, '553.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 38, '`p`.`products_sort`', 'DESC', '2009-04-28 06:36:32', NULL, '', 0),
	(554, '554.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 39, '`p`.`products_sort`', 'DESC', '2009-04-28 06:37:31', NULL, '', 0),
	(555, '555.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 40, '`p`.`products_sort`', 'DESC', '2009-04-28 06:38:16', NULL, '', 0),
	(556, '556.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 41, '`p`.`products_sort`', 'DESC', '2009-04-28 06:39:15', NULL, '', 0),
	(557, '557.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 42, '`p`.`products_sort`', 'DESC', '2009-04-28 06:40:19', NULL, '', 0),
	(558, '558.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 43, '`p`.`products_sort`', 'DESC', '2009-04-28 06:41:13', NULL, '', 0),
	(559, '559.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 44, '`p`.`products_sort`', 'DESC', '2009-04-28 06:42:11', NULL, '', 0),
	(560, '560.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 45, '`p`.`products_sort`', 'DESC', '2009-04-28 06:43:50', NULL, '', 0),
	(561, '561.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 46, '`p`.`products_sort`', 'DESC', '2009-04-28 06:46:41', NULL, '', 0),
	(562, '562.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 47, '`p`.`products_sort`', 'DESC', '2009-04-28 06:47:38', NULL, '', 0),
	(563, '563.jpg', 515, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 50, '`p`.`products_sort`', 'DESC', '2009-04-28 06:49:31', NULL, '', 0),
	(564, '564.jpg', 511, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 110, '`p`.`products_sort`', 'DESC', '2009-06-05 08:54:10', '2009-12-09 02:30:34', '', 0),
	(565, '565.jpg', 197, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2009-06-05 10:10:25', NULL, '', 0),
	(566, '566.JPG', 201, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2009-06-06 09:48:46', '2009-06-17 08:48:41', '', 0),
	(567, '567.JPG', 566, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2009-06-06 09:53:05', '2009-06-17 08:49:31', '', 0),
	(568, '568.JPG', 566, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2009-06-06 09:53:41', '2009-06-17 08:50:23', '', 0),
	(569, '569.JPG', 566, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2009-06-06 09:54:09', '2009-06-17 08:50:03', '', 0),
	(570, '570.JPG', 444, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2009-06-17 08:53:34', NULL, '', 0),
	(571, '571.JPG', 570, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 1, '`p`.`products_sort`', 'DESC', '2009-06-17 08:54:48', NULL, '', 0),
	(572, '572.JPG', 570, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 2, '`p`.`products_sort`', 'DESC', '2009-06-17 08:55:08', '2009-06-17 08:56:29', '', 0),
	(573, '573.JPG', 570, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 3, '`p`.`products_sort`', 'DESC', '2009-06-17 08:57:21', NULL, '', 0),
	(574, '574.JPG', 570, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 4, '`p`.`products_sort`', 'DESC', '2009-06-17 08:58:01', NULL, '', 0),
	(575, '575.JPG', 570, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 5, '`p`.`products_sort`', 'DESC', '2009-06-17 08:59:16', NULL, '', 0),
	(576, '576.JPG', 570, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 6, '`p`.`products_sort`', 'DESC', '2009-06-17 08:59:58', NULL, '', 0),
	(577, '577.jpg', 511, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 120, '`p`.`products_sort`', 'DESC', '2009-11-30 04:40:49', '2010-01-06 08:05:14', '', 0),
	(578, '578.jpg', 511, 0, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 130, '`p`.`products_sort`', 'DESC', '2010-04-29 05:20:55', '2010-04-29 05:21:55', '', 0),
	(579, NULL, 0, 1, 'categorie_listing.html', 0, 0, 0, 0, 'product_listing_v1.html', 80, 'p.products_price', 'ASC', '2010-09-15 10:04:30', '2010-12-31 04:12:37', '', 0),
	(581, NULL, 0, 1, 'default', 0, 0, 0, 0, 'default', 95, 'p.products_price', 'ASC', '2011-12-07 15:31:55', '2011-12-07 15:34:06', '', 0);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;


-- Dumping structure for table crazy_import.categories_description
DROP TABLE IF EXISTS `categories_description`;
CREATE TABLE IF NOT EXISTS `categories_description` (
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `categories_name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `categories_heading_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `categories_description` text CHARACTER SET utf8 NOT NULL,
  `categories_meta_title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `categories_meta_description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `categories_meta_keywords` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`categories_id`,`language_id`),
  KEY `idx_categories_name` (`categories_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.categories_description: 0 rows
DELETE FROM `categories_description`;
/*!40000 ALTER TABLE `categories_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories_description` ENABLE KEYS */;


-- Dumping structure for table crazy_import.centralised_buying_color_codes
DROP TABLE IF EXISTS `centralised_buying_color_codes`;
CREATE TABLE IF NOT EXISTS `centralised_buying_color_codes` (
  `centralised_buying_color_codes_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color_code` char(6) NOT NULL,
  PRIMARY KEY (`centralised_buying_color_codes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.centralised_buying_color_codes: 0 rows
DELETE FROM `centralised_buying_color_codes`;
/*!40000 ALTER TABLE `centralised_buying_color_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `centralised_buying_color_codes` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_actionshotrating_to_user
DROP TABLE IF EXISTS `cm_actionshotrating_to_user`;
CREATE TABLE IF NOT EXISTS `cm_actionshotrating_to_user` (
  `actionshotrating_to_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actionshot_id` int(10) unsigned NOT NULL,
  `customers_id` int(10) unsigned NOT NULL,
  `rating` tinyint(10) unsigned NOT NULL,
  PRIMARY KEY (`actionshotrating_to_user_id`),
  KEY `customers_id` (`customers_id`),
  KEY `actionshot_id` (`actionshot_id`),
  KEY `top27coolpics` (`actionshot_id`,`rating`),
  KEY `nextcoolpic` (`actionshot_id`,`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_actionshotrating_to_user: 0 rows
DELETE FROM `cm_actionshotrating_to_user`;
/*!40000 ALTER TABLE `cm_actionshotrating_to_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_actionshotrating_to_user` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_actionshots
DROP TABLE IF EXISTS `cm_actionshots`;
CREATE TABLE IF NOT EXISTS `cm_actionshots` (
  `actionshot_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `rating` tinyint(3) unsigned NOT NULL,
  `upload_time` datetime NOT NULL,
  `confirmation_time` datetime NOT NULL,
  `confirmed_from` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL COMMENT '0=hochgeladen;1=vom Admin bestätigt',
  `reported` tinyint(1) unsigned NOT NULL COMMENT 'Vom Kunden gemeldet (1)',
  `valentine` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'wird nicht mehr genutzt.',
  `special` enum('default','valentine_2011','valentine_2012','coolpics') NOT NULL DEFAULT 'default',
  `special_editing_finished` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Wird auf 1 gesetzt sobald der User den special ActionShot als "fertig" markiert hat - davor sind die Daten (E-Mail, Rahmen) unvollständig!',
  `deleted` tinyint(1) NOT NULL COMMENT 'gelöschte Actionshots',
  `attributes` varchar(255) NOT NULL COMMENT 'Attribute des gekauften Produkts',
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`actionshot_id`),
  KEY `products_id` (`products_id`),
  KEY `reported` (`reported`),
  KEY `active` (`active`),
  KEY `customers_id` (`customers_id`),
  KEY `valentine` (`valentine`),
  KEY `valentine_editing_finished` (`special_editing_finished`),
  KEY `deleted` (`deleted`),
  KEY `special` (`special`),
  KEY `top27coolpics` (`active`,`reported`,`deleted`,`special`,`special_editing_finished`,`actionshot_id`),
  KEY `nextcoolpic` (`actionshot_id`,`customers_id`,`active`,`reported`,`deleted`,`special`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_actionshots: 0 rows
DELETE FROM `cm_actionshots`;
/*!40000 ALTER TABLE `cm_actionshots` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_actionshots` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_actionshots_denial_reasons
DROP TABLE IF EXISTS `cm_actionshots_denial_reasons`;
CREATE TABLE IF NOT EXISTS `cm_actionshots_denial_reasons` (
  `denial_reason_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `languages_id` int(11) NOT NULL,
  `reason` enum('wrong_article','quality-size','article_visibility','inappropriate','duplicate','too_dark','worn_wrong','overexposed','technical_problem','copyright') NOT NULL,
  `translated_text` text CHARACTER SET utf8 NOT NULL,
  `email_subject` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`denial_reason_id`),
  UNIQUE KEY `languages_id_cause` (`languages_id`,`reason`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_actionshots_denial_reasons: 0 rows
DELETE FROM `cm_actionshots_denial_reasons`;
/*!40000 ALTER TABLE `cm_actionshots_denial_reasons` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_actionshots_denial_reasons` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_add_cart_log
DROP TABLE IF EXISTS `cm_add_cart_log`;
CREATE TABLE IF NOT EXISTS `cm_add_cart_log` (
  `add_cart_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `debug_data` longtext NOT NULL,
  `function` varchar(255) NOT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`add_cart_log_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table crazy_import.cm_add_cart_log: 0 rows
DELETE FROM `cm_add_cart_log`;
/*!40000 ALTER TABLE `cm_add_cart_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_add_cart_log` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_also_purchased_cache
DROP TABLE IF EXISTS `cm_also_purchased_cache`;
CREATE TABLE IF NOT EXISTS `cm_also_purchased_cache` (
  `products_id` int(11) NOT NULL,
  `also_purchased_products_id` int(11) NOT NULL,
  `date_purchased` datetime NOT NULL,
  PRIMARY KEY (`products_id`,`also_purchased_products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_also_purchased_cache: 0 rows
DELETE FROM `cm_also_purchased_cache`;
/*!40000 ALTER TABLE `cm_also_purchased_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_also_purchased_cache` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_book_inpayment_import
DROP TABLE IF EXISTS `cm_book_inpayment_import`;
CREATE TABLE IF NOT EXISTS `cm_book_inpayment_import` (
  `book_inpayment_import_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL DEFAULT '0',
  `book_inpayment_id` int(10) unsigned NOT NULL DEFAULT '0',
  `book_inpayment_type` enum('transfer','paypal','moneybookers','infin','docdata') CHARACTER SET latin1 NOT NULL DEFAULT 'transfer',
  `book_inpayment_status` varchar(100) CHARACTER SET latin1 NOT NULL,
  `currency` enum('EUR','USD','GBP','CAD','AUD','JPY','DKK','NOK','CHF','SEK') CHARACTER SET latin1 NOT NULL DEFAULT 'EUR',
  `amount` float(10,2) NOT NULL DEFAULT '0.00',
  `paypal_txn_type` varchar(50) CHARACTER SET latin1 NOT NULL,
  `paypal_fee_amount` float(10,2) DEFAULT '0.00',
  `from_name` varchar(255) DEFAULT NULL,
  `from_email` varchar(255) NOT NULL,
  `to_email` varchar(255) NOT NULL COMMENT 'Paypal Empfängermail',
  `transactioncode` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `bank_subject` text NOT NULL,
  `date_receive` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `controlstring` text NOT NULL COMMENT 'Volltext Importinformationen',
  `error` enum('NONE','VALUE_MISSMATCH','ORDERID_PARSE','NO_WEBACCEPT_FOUND','PAID','CANCELED','NO_PAYMENTCLUSTERID_FOUND') CHARACTER SET latin1 NOT NULL DEFAULT 'NONE',
  `hidden` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `unclear` tinyint(4) NOT NULL COMMENT 'Liste fuer Unklare Faelle',
  `amount_found` float(10,2) DEFAULT NULL,
  `payment_difference` float(10,2) NOT NULL COMMENT 'zuviel oder zuwenig bezahlt',
  `manually_matched` tinyint(1) NOT NULL COMMENT 'Wurde manuell zugewiesen im Admininterface',
  PRIMARY KEY (`book_inpayment_import_id`),
  KEY `Kundennummer` (`order_id`),
  KEY `transaktionscode` (`transactioncode`),
  KEY `error` (`error`),
  KEY `hide_pp_info_stati` (`book_inpayment_status`,`hidden`),
  KEY `pp_check_send_money` (`book_inpayment_status`,`paypal_txn_type`,`error`),
  KEY `transfer_precheck` (`order_id`,`book_inpayment_type`,`error`,`hidden`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table crazy_import.cm_book_inpayment_import: 0 rows
DELETE FROM `cm_book_inpayment_import`;
/*!40000 ALTER TABLE `cm_book_inpayment_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_book_inpayment_import` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_cash_points
DROP TABLE IF EXISTS `cm_cash_points`;
CREATE TABLE IF NOT EXISTS `cm_cash_points` (
  `cash_points_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customers_id` int(10) unsigned NOT NULL,
  `orders_id` int(10) unsigned NOT NULL,
  `coupon_id` int(10) unsigned NOT NULL,
  `rma_id` int(10) unsigned NOT NULL DEFAULT '0',
  `action` enum('actionshot','comment','newsletter','special','order_bonus','used_to_order','coupon','storno','valentine2011','cancellation','rma','rma_bonus_reduce','store_credit') NOT NULL,
  `actionshots_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_comment` varchar(255) NOT NULL,
  `meta_data` varchar(64) NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`cash_points_id`),
  KEY `customers_id` (`customers_id`),
  KEY `actionshots_id` (`actionshots_id`),
  KEY `orders_id` (`orders_id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `order_action` (`orders_id`,`action`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_cash_points: 0 rows
DELETE FROM `cm_cash_points`;
/*!40000 ALTER TABLE `cm_cash_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_cash_points` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_cash_points_bak
DROP TABLE IF EXISTS `cm_cash_points_bak`;
CREATE TABLE IF NOT EXISTS `cm_cash_points_bak` (
  `cash_points_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customers_id` int(10) unsigned NOT NULL,
  `orders_id` int(10) unsigned NOT NULL,
  `coupon_id` int(10) unsigned NOT NULL,
  `rma_id` int(10) unsigned NOT NULL DEFAULT '0',
  `action` enum('actionshot','comment','newsletter','special','order_bonus','used_to_order','coupon','storno','valentine2011','cancellation','rma','rma_bonus_reduce','store_credit') NOT NULL,
  `actionshots_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_comment` varchar(255) NOT NULL,
  `meta_data` varchar(64) NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`cash_points_id`),
  KEY `customers_id` (`customers_id`),
  KEY `actionshots_id` (`actionshots_id`),
  KEY `orders_id` (`orders_id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `order_action` (`orders_id`,`action`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_cash_points_bak: 0 rows
DELETE FROM `cm_cash_points_bak`;
/*!40000 ALTER TABLE `cm_cash_points_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_cash_points_bak` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_cash_point_coupons
DROP TABLE IF EXISTS `cm_cash_point_coupons`;
CREATE TABLE IF NOT EXISTS `cm_cash_point_coupons` (
  `coupon_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(64) NOT NULL COMMENT 'PHP-Funktion zum generieren -> generate_cash_points_coupon. Sollte zum erkennen mit "CCP" beginnen',
  `expire_date` date NOT NULL,
  `one_time_usage` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Wenn dieses Flag gesetzt ist kann der Gutschein nur ein einziges mal benutzt werden',
  `used_by_customers_id` int(11) NOT NULL COMMENT 'Wenn one_time_usage steht hier die customers_id drin von dem Einlöser',
  `amount` int(10) unsigned NOT NULL,
  `internal_description` varchar(64) NOT NULL,
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `code` (`code`),
  KEY `expire_date` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_cash_point_coupons: 0 rows
DELETE FROM `cm_cash_point_coupons`;
/*!40000 ALTER TABLE `cm_cash_point_coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_cash_point_coupons` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_feedback
DROP TABLE IF EXISTS `cm_feedback`;
CREATE TABLE IF NOT EXISTS `cm_feedback` (
  `feedback_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customers_id` int(10) unsigned NOT NULL,
  `type` enum('bug','feedback','suggestion') NOT NULL,
  `bug_choice` enum('image','text','typo','technical') NOT NULL,
  `feedback_choice` enum('good','okay','bad') NOT NULL,
  `text` text NOT NULL,
  `pinned` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_metadata` text NOT NULL,
  PRIMARY KEY (`feedback_id`),
  KEY `deleted` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_feedback: 0 rows
DELETE FROM `cm_feedback`;
/*!40000 ALTER TABLE `cm_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_feedback` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_feedback_ng
DROP TABLE IF EXISTS `cm_feedback_ng`;
CREATE TABLE IF NOT EXISTS `cm_feedback_ng` (
  `feedback_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('bug','feedback','suggestions') NOT NULL,
  `text` text NOT NULL,
  `additional_info` varchar(255) NOT NULL COMMENT 'Zusätzliche Checkboxen, etc.',
  `language` varchar(255) NOT NULL,
  `user_metadata` text NOT NULL COMMENT 'Useragent, Referrer, etc.',
  `anonymously` tinyint(1) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `email_status` enum('undue','pending','done') NOT NULL COMMENT 'Steht auf undue wenn keine E-Mail nötig ist (z.B. Feedback ohne Text)',
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`feedback_id`),
  KEY `email_status` (`email_status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table crazy_import.cm_feedback_ng: 0 rows
DELETE FROM `cm_feedback_ng`;
/*!40000 ALTER TABLE `cm_feedback_ng` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_feedback_ng` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_feedback_tags
DROP TABLE IF EXISTS `cm_feedback_tags`;
CREATE TABLE IF NOT EXISTS `cm_feedback_tags` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feedback_id` int(10) unsigned NOT NULL,
  `tag` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `feedback_id_tag` (`feedback_id`,`tag`),
  KEY `tag` (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_feedback_tags: 0 rows
DELETE FROM `cm_feedback_tags`;
/*!40000 ALTER TABLE `cm_feedback_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_feedback_tags` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_file_flags
DROP TABLE IF EXISTS `cm_file_flags`;
CREATE TABLE IF NOT EXISTS `cm_file_flags` (
  `file_flag` int(11) NOT NULL,
  `file_flag_name` varchar(32) NOT NULL,
  PRIMARY KEY (`file_flag`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_file_flags: 0 rows
DELETE FROM `cm_file_flags`;
/*!40000 ALTER TABLE `cm_file_flags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_file_flags` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_filters
DROP TABLE IF EXISTS `cm_filters`;
CREATE TABLE IF NOT EXISTS `cm_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `sort_order` int(10) unsigned NOT NULL,
  `show_in_searches` tinyint(1) NOT NULL,
  `presentation` enum('default') NOT NULL,
  `image_priority` int(10) unsigned NOT NULL,
  `language_dependent` tinyint(1) unsigned NOT NULL,
  `products_field` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_filters: 0 rows
DELETE FROM `cm_filters`;
/*!40000 ALTER TABLE `cm_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_filters` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_filter_values
DROP TABLE IF EXISTS `cm_filter_values`;
CREATE TABLE IF NOT EXISTS `cm_filter_values` (
  `filter_value_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filter_id` int(10) unsigned NOT NULL,
  `sort_order` int(10) unsigned NOT NULL,
  `import_key` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`filter_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_filter_values: 0 rows
DELETE FROM `cm_filter_values`;
/*!40000 ALTER TABLE `cm_filter_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_filter_values` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_header_graphic_html
DROP TABLE IF EXISTS `cm_header_graphic_html`;
CREATE TABLE IF NOT EXISTS `cm_header_graphic_html` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grafik` varchar(255) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `inhalt` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `is_external_link` tinyint(4) NOT NULL,
  `sort_order` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grafik_languages_id` (`grafik`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_header_graphic_html: 0 rows
DELETE FROM `cm_header_graphic_html`;
/*!40000 ALTER TABLE `cm_header_graphic_html` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_header_graphic_html` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_header_graphic_html_bak
DROP TABLE IF EXISTS `cm_header_graphic_html_bak`;
CREATE TABLE IF NOT EXISTS `cm_header_graphic_html_bak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grafik` varchar(255) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `inhalt` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `is_external_link` tinyint(4) NOT NULL,
  `sort_order` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `grafik_languages_id` (`grafik`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_header_graphic_html_bak: 0 rows
DELETE FROM `cm_header_graphic_html_bak`;
/*!40000 ALTER TABLE `cm_header_graphic_html_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_header_graphic_html_bak` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_invoices
DROP TABLE IF EXISTS `cm_invoices`;
CREATE TABLE IF NOT EXISTS `cm_invoices` (
  `cm_invoice_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` varchar(16) NOT NULL,
  `invoice_number` varchar(255) NOT NULL,
  `invoice_date` date NOT NULL,
  `filename` varchar(255) NOT NULL,
  `retry_counter` int(10) unsigned NOT NULL,
  `download_status` enum('undue','pending','done') NOT NULL DEFAULT 'undue',
  `import_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cm_invoice_id`),
  UNIQUE KEY `orders_id` (`orders_id`),
  KEY `download_status` (`download_status`,`retry_counter`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Importierte Rechnungen';

-- Dumping data for table crazy_import.cm_invoices: 0 rows
DELETE FROM `cm_invoices`;
/*!40000 ALTER TABLE `cm_invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_invoices` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_invoices_bak
DROP TABLE IF EXISTS `cm_invoices_bak`;
CREATE TABLE IF NOT EXISTS `cm_invoices_bak` (
  `cm_invoice_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` varchar(16) NOT NULL,
  `invoice_number` varchar(255) NOT NULL,
  `invoice_date` date NOT NULL,
  `filename` varchar(255) NOT NULL,
  `retry_counter` int(10) unsigned NOT NULL,
  `download_status` enum('undue','pending','done') NOT NULL DEFAULT 'undue',
  `import_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cm_invoice_id`),
  UNIQUE KEY `orders_id` (`orders_id`),
  KEY `download_status` (`download_status`,`retry_counter`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Importierte Rechnungen';

-- Dumping data for table crazy_import.cm_invoices_bak: 0 rows
DELETE FROM `cm_invoices_bak`;
/*!40000 ALTER TABLE `cm_invoices_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_invoices_bak` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_log
DROP TABLE IF EXISTS `cm_log`;
CREATE TABLE IF NOT EXISTS `cm_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session` longtext NOT NULL,
  `server` longtext NOT NULL,
  `backtrace` longtext NOT NULL,
  `post` longtext NOT NULL,
  `customers_id` int(10) unsigned NOT NULL,
  `entry_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table crazy_import.cm_log: 0 rows
DELETE FROM `cm_log`;
/*!40000 ALTER TABLE `cm_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_log` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_missing_colorcharts
DROP TABLE IF EXISTS `cm_missing_colorcharts`;
CREATE TABLE IF NOT EXISTS `cm_missing_colorcharts` (
  `missing_colorcharts_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`missing_colorcharts_id`),
  UNIQUE KEY `image` (`image`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='fehlende Produkt-Variationsbilder';

-- Dumping data for table crazy_import.cm_missing_colorcharts: 0 rows
DELETE FROM `cm_missing_colorcharts`;
/*!40000 ALTER TABLE `cm_missing_colorcharts` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_missing_colorcharts` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_missing_images
DROP TABLE IF EXISTS `cm_missing_images`;
CREATE TABLE IF NOT EXISTS `cm_missing_images` (
  `missing_images_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`missing_images_id`),
  UNIQUE KEY `image` (`image`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Fehlende Artikelbilder';

-- Dumping data for table crazy_import.cm_missing_images: 0 rows
DELETE FROM `cm_missing_images`;
/*!40000 ALTER TABLE `cm_missing_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_missing_images` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_missing_variantimages
DROP TABLE IF EXISTS `cm_missing_variantimages`;
CREATE TABLE IF NOT EXISTS `cm_missing_variantimages` (
  `cm_missing_variantimages_id` int(11) NOT NULL AUTO_INCREMENT,
  `attributes` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`cm_missing_variantimages_id`),
  UNIQUE KEY `image` (`image`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_missing_variantimages: 0 rows
DELETE FROM `cm_missing_variantimages`;
/*!40000 ALTER TABLE `cm_missing_variantimages` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_missing_variantimages` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_order_pool_history
DROP TABLE IF EXISTS `cm_order_pool_history`;
CREATE TABLE IF NOT EXISTS `cm_order_pool_history` (
  `order_pool_history_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_create` datetime NOT NULL,
  `mode` enum('old','new') NOT NULL DEFAULT 'new' COMMENT 'alt oder neu..',
  `order_ids_count` int(10) unsigned NOT NULL,
  `order_ids_switched` mediumtext NOT NULL,
  PRIMARY KEY (`order_pool_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_order_pool_history: 0 rows
DELETE FROM `cm_order_pool_history`;
/*!40000 ALTER TABLE `cm_order_pool_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_order_pool_history` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_reported_actionshots
DROP TABLE IF EXISTS `cm_reported_actionshots`;
CREATE TABLE IF NOT EXISTS `cm_reported_actionshots` (
  `reported_actionshot_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actionshot_id` int(10) unsigned NOT NULL,
  `customers_id` int(10) unsigned NOT NULL,
  `ip` varchar(255) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`reported_actionshot_id`),
  KEY `customers_id` (`customers_id`),
  KEY `actionshot_id` (`actionshot_id`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_reported_actionshots: 0 rows
DELETE FROM `cm_reported_actionshots`;
/*!40000 ALTER TABLE `cm_reported_actionshots` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_reported_actionshots` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_rma
DROP TABLE IF EXISTS `cm_rma`;
CREATE TABLE IF NOT EXISTS `cm_rma` (
  `rma_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customers_id` int(10) unsigned NOT NULL,
  `orders_id` int(10) unsigned NOT NULL,
  `rma_mail_subject` varchar(255) NOT NULL DEFAULT 'Crazy Factory RMA' COMMENT 'keine Lust dafür eine extra Tabelle zu machen.',
  `rma_bag_id` int(10) unsigned NOT NULL,
  `refund_type` enum('paypal','moneybookers','bank_details') DEFAULT NULL,
  `account_number` varchar(255) NOT NULL,
  `bank_code` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `account_holder` varchar(255) NOT NULL,
  `payment_email` varchar(255) NOT NULL COMMENT 'hier steht entweder die paypal- oder moneybookers emailadresse',
  `status_id` tinyint(3) unsigned NOT NULL,
  `edited_by` int(10) unsigned NOT NULL,
  `date_created` date NOT NULL,
  `date_confirmed` datetime NOT NULL,
  `refund_done` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `refunded_by` int(11) NOT NULL,
  `date_refund` date NOT NULL,
  `comment` text NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`rma_id`),
  KEY `customers_id` (`customers_id`,`orders_id`,`status_id`),
  KEY `rma_packet_id_status_id` (`status_id`),
  KEY `rma_bag_id` (`rma_bag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table crazy_import.cm_rma: 0 rows
DELETE FROM `cm_rma`;
/*!40000 ALTER TABLE `cm_rma` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_rma` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_rma_bags
DROP TABLE IF EXISTS `cm_rma_bags`;
CREATE TABLE IF NOT EXISTS `cm_rma_bags` (
  `rma_bag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rma_packet_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `location` varchar(250) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `finished_datetime` datetime NOT NULL,
  `export_status` enum('undue','pending','done') NOT NULL,
  PRIMARY KEY (`rma_bag_id`),
  KEY `rma_packet_id` (`rma_packet_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_rma_bags: 0 rows
DELETE FROM `cm_rma_bags`;
/*!40000 ALTER TABLE `cm_rma_bags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_rma_bags` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_rma_cause
DROP TABLE IF EXISTS `cm_rma_cause`;
CREATE TABLE IF NOT EXISTS `cm_rma_cause` (
  `rma_cause_id` smallint(5) unsigned NOT NULL,
  `language_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  UNIQUE KEY `rma_cause_index` (`rma_cause_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='moegliche RMA Gruende';

-- Dumping data for table crazy_import.cm_rma_cause: 0 rows
DELETE FROM `cm_rma_cause`;
/*!40000 ALTER TABLE `cm_rma_cause` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_rma_cause` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_rma_packets
DROP TABLE IF EXISTS `cm_rma_packets`;
CREATE TABLE IF NOT EXISTS `cm_rma_packets` (
  `rma_packet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(10) unsigned NOT NULL,
  `create_datetime` datetime NOT NULL,
  `finished_datetime` datetime NOT NULL,
  PRIMARY KEY (`rma_packet_id`),
  KEY `created_by` (`created_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table crazy_import.cm_rma_packets: 0 rows
DELETE FROM `cm_rma_packets`;
/*!40000 ALTER TABLE `cm_rma_packets` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_rma_packets` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_rma_products
DROP TABLE IF EXISTS `cm_rma_products`;
CREATE TABLE IF NOT EXISTS `cm_rma_products` (
  `rma_products_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rma_id` int(10) unsigned NOT NULL,
  `orders_products_id` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `cause` enum('no_cause','quality','damaged_by_carrier','switcheroo','unwanted','unauthorised','bad_description','item_missing','cause_other','wrong_size') NOT NULL,
  `comment` text NOT NULL,
  `rma_type` enum('refund','replacement','refund_credit_voucher') NOT NULL DEFAULT 'refund_credit_voucher',
  `confirmed_by` int(10) unsigned NOT NULL,
  `date_created` date NOT NULL,
  `date_edited` date NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `in_bag` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Ob das Produkt mit zurückgeschickt wird und nicht etwa weggeworfen',
  PRIMARY KEY (`rma_products_id`),
  KEY `rma_id` (`rma_id`,`orders_products_id`),
  KEY `in_packet` (`in_bag`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_rma_products: 0 rows
DELETE FROM `cm_rma_products`;
/*!40000 ALTER TABLE `cm_rma_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_rma_products` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_rma_refund
DROP TABLE IF EXISTS `cm_rma_refund`;
CREATE TABLE IF NOT EXISTS `cm_rma_refund` (
  `rma_refund_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime_created` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `refund_sum` decimal(15,4) NOT NULL,
  `refund_count` int(11) NOT NULL,
  `refund_array` mediumtext NOT NULL,
  `pp_refund_transaction_id` varchar(128) NOT NULL COMMENT 'Transaktions-ID der Erstattung',
  `pp_refunded_amount` decimal(10,4) NOT NULL COMMENT 'Betrag, der auf dem Konto des Kunden angekommen ist',
  `pp_amount_from_account` decimal(10,4) NOT NULL COMMENT 'Betrag der dafür vom Crazy-Factory-Konto abgezogen wurde',
  `pp_amount_from_paypal_fee` decimal(10,4) NOT NULL COMMENT 'Betrag der nicht vom Crazy-Factory-Konto abgezogen wurde, sondern von PayPal als Gebührenrückerstattung beigesteuert wurde',
  `pp_total_refunds` decimal(10,4) NOT NULL COMMENT 'Gesamtbetrag aller Erstattungen, die nach dieser Erstattung für die ursprüngliche Zahlung alle gemacht wurden',
  `pp_from_account` varchar(128) NOT NULL COMMENT 'Paypal Account',
  PRIMARY KEY (`rma_refund_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table crazy_import.cm_rma_refund: 0 rows
DELETE FROM `cm_rma_refund`;
/*!40000 ALTER TABLE `cm_rma_refund` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_rma_refund` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_rma_status
DROP TABLE IF EXISTS `cm_rma_status`;
CREATE TABLE IF NOT EXISTS `cm_rma_status` (
  `rma_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`rma_status_id`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_rma_status: 0 rows
DELETE FROM `cm_rma_status`;
/*!40000 ALTER TABLE `cm_rma_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_rma_status` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_rma_type
DROP TABLE IF EXISTS `cm_rma_type`;
CREATE TABLE IF NOT EXISTS `cm_rma_type` (
  `rma_type_id` smallint(5) unsigned NOT NULL,
  `language_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  UNIQUE KEY `rma_type_index` (`rma_type_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='moegliche RMA Gruende';

-- Dumping data for table crazy_import.cm_rma_type: 0 rows
DELETE FROM `cm_rma_type`;
/*!40000 ALTER TABLE `cm_rma_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_rma_type` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_shopping_cart_ng_entries
DROP TABLE IF EXISTS `cm_shopping_cart_ng_entries`;
CREATE TABLE IF NOT EXISTS `cm_shopping_cart_ng_entries` (
  `cart_entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `attributes` varchar(255) NOT NULL COMMENT 'Attribute als simples Array',
  `quantity` int(10) unsigned NOT NULL,
  `centralised_buying_group_id` int(10) unsigned NOT NULL,
  `price_when_last_seen` decimal(15,4) NOT NULL COMMENT 'Zuletzt im Warenkorb betrachteter Preis (für Preisänderungsmeldungen)',
  PRIMARY KEY (`cart_entry_id`),
  UNIQUE KEY `Identifiziert ein Produkt eindeutig im Warenkorb eines Kunden` (`customers_id`,`products_id`,`attributes`,`centralised_buying_group_id`),
  KEY `customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_shopping_cart_ng_entries: 0 rows
DELETE FROM `cm_shopping_cart_ng_entries`;
/*!40000 ALTER TABLE `cm_shopping_cart_ng_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_shopping_cart_ng_entries` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_shopping_cart_ng_merklisten_debug
DROP TABLE IF EXISTS `cm_shopping_cart_ng_merklisten_debug`;
CREATE TABLE IF NOT EXISTS `cm_shopping_cart_ng_merklisten_debug` (
  `customers_id` int(10) NOT NULL,
  `cart_entries_before` text NOT NULL,
  `broken_products` text NOT NULL,
  `product_data` text NOT NULL,
  `product_data_prepared` text NOT NULL,
  `datetime_added` datetime NOT NULL,
  KEY `customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_shopping_cart_ng_merklisten_debug: 0 rows
DELETE FROM `cm_shopping_cart_ng_merklisten_debug`;
/*!40000 ALTER TABLE `cm_shopping_cart_ng_merklisten_debug` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_shopping_cart_ng_merklisten_debug` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_shopping_cart_ng_options
DROP TABLE IF EXISTS `cm_shopping_cart_ng_options`;
CREATE TABLE IF NOT EXISTS `cm_shopping_cart_ng_options` (
  `customers_id` int(10) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  UNIQUE KEY `customers_id_key` (`customers_id`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_shopping_cart_ng_options: 0 rows
DELETE FROM `cm_shopping_cart_ng_options`;
/*!40000 ALTER TABLE `cm_shopping_cart_ng_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_shopping_cart_ng_options` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_special_mails
DROP TABLE IF EXISTS `cm_special_mails`;
CREATE TABLE IF NOT EXISTS `cm_special_mails` (
  `special_mail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actionshot_id` int(10) unsigned NOT NULL,
  `customers_id` int(10) unsigned NOT NULL,
  `recipients` text NOT NULL,
  `message` text NOT NULL,
  `special` enum('default','valentine_2011','valentine_2012','coolpics') NOT NULL DEFAULT 'default',
  `wait_for_valentines_day` tinyint(1) unsigned NOT NULL,
  `date_inserted` date NOT NULL,
  `status` enum('pending','waiting_for_file','processing','sent') NOT NULL,
  `last_try_datetime` datetime NOT NULL,
  `date_mail_sent` date NOT NULL,
  `js_data` text NOT NULL,
  PRIMARY KEY (`special_mail_id`),
  UNIQUE KEY `actionshot_id_customers_id` (`actionshot_id`,`customers_id`),
  KEY `wait_for_valentines_day` (`wait_for_valentines_day`),
  KEY `date_inserted` (`date_inserted`),
  KEY `date_mail_sent` (`date_mail_sent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_special_mails: 0 rows
DELETE FROM `cm_special_mails`;
/*!40000 ALTER TABLE `cm_special_mails` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_special_mails` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_transfer_pool_history
DROP TABLE IF EXISTS `cm_transfer_pool_history`;
CREATE TABLE IF NOT EXISTS `cm_transfer_pool_history` (
  `order_pool_history_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_create` datetime NOT NULL,
  `mode` enum('old','new') NOT NULL DEFAULT 'new' COMMENT 'alt oder neu..',
  `order_ids_count` int(10) unsigned NOT NULL,
  `order_ids_switched` text NOT NULL,
  PRIMARY KEY (`order_pool_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_transfer_pool_history: 0 rows
DELETE FROM `cm_transfer_pool_history`;
/*!40000 ALTER TABLE `cm_transfer_pool_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_transfer_pool_history` ENABLE KEYS */;


-- Dumping structure for table crazy_import.cm_uploadify_tickets
DROP TABLE IF EXISTS `cm_uploadify_tickets`;
CREATE TABLE IF NOT EXISTS `cm_uploadify_tickets` (
  `ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(40) NOT NULL COMMENT 'sha1',
  `session_id` char(32) NOT NULL,
  `data` text NOT NULL,
  `datetime_added` datetime NOT NULL,
  PRIMARY KEY (`ticket_id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.cm_uploadify_tickets: 0 rows
DELETE FROM `cm_uploadify_tickets`;
/*!40000 ALTER TABLE `cm_uploadify_tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `cm_uploadify_tickets` ENABLE KEYS */;


-- Dumping structure for table crazy_import.configuration
DROP TABLE IF EXISTS `configuration`;
CREATE TABLE IF NOT EXISTS `configuration` (
  `configuration_id` int(11) NOT NULL AUTO_INCREMENT,
  `configuration_key` varchar(255) NOT NULL,
  `configuration_value` varchar(1024) NOT NULL,
  `configuration_group_id` int(11) NOT NULL,
  `sort_order` int(5) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `use_function` varchar(255) DEFAULT NULL,
  `set_function` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`configuration_id`),
  KEY `idx_configuration_group_id` (`configuration_group_id`),
  KEY `configuration_key` (`configuration_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.configuration: 0 rows
DELETE FROM `configuration`;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;


-- Dumping structure for table crazy_import.configuration_group
DROP TABLE IF EXISTS `configuration_group`;
CREATE TABLE IF NOT EXISTS `configuration_group` (
  `configuration_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `configuration_group_title` varchar(64) NOT NULL,
  `configuration_group_description` varchar(255) NOT NULL,
  `sort_order` int(5) DEFAULT NULL,
  `visible` int(1) DEFAULT '1',
  PRIMARY KEY (`configuration_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.configuration_group: 0 rows
DELETE FROM `configuration_group`;
/*!40000 ALTER TABLE `configuration_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `configuration_group` ENABLE KEYS */;


-- Dumping structure for table crazy_import.content_manager
DROP TABLE IF EXISTS `content_manager`;
CREATE TABLE IF NOT EXISTS `content_manager` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `group_ids` text,
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `content_title` text CHARACTER SET utf8 NOT NULL,
  `content_heading` text CHARACTER SET utf8 NOT NULL,
  `content_text` text CHARACTER SET utf8 NOT NULL,
  `sort_order` int(4) NOT NULL DEFAULT '0',
  `file_flag` int(1) NOT NULL DEFAULT '0',
  `content_file` varchar(64) CHARACTER SET utf8 NOT NULL,
  `content_status` int(1) NOT NULL DEFAULT '0',
  `content_group` int(11) NOT NULL,
  `content_delete` int(1) NOT NULL DEFAULT '1',
  `content_meta_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `content_meta_description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `content_meta_keywords` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`content_id`),
  KEY `content_group` (`content_group`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.content_manager: 0 rows
DELETE FROM `content_manager`;
/*!40000 ALTER TABLE `content_manager` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_manager` ENABLE KEYS */;


-- Dumping structure for table crazy_import.counter
DROP TABLE IF EXISTS `counter`;
CREATE TABLE IF NOT EXISTS `counter` (
  `startdate` char(8) DEFAULT NULL,
  `counter` int(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.counter: 0 rows
DELETE FROM `counter`;
/*!40000 ALTER TABLE `counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `counter` ENABLE KEYS */;


-- Dumping structure for table crazy_import.counter_history
DROP TABLE IF EXISTS `counter_history`;
CREATE TABLE IF NOT EXISTS `counter_history` (
  `month` char(8) DEFAULT NULL,
  `counter` int(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.counter_history: 0 rows
DELETE FROM `counter_history`;
/*!40000 ALTER TABLE `counter_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `counter_history` ENABLE KEYS */;


-- Dumping structure for table crazy_import.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `countries_id` int(11) NOT NULL AUTO_INCREMENT,
  `countries_name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `countries_iso_code_2` char(2) CHARACTER SET utf8 NOT NULL,
  `countries_iso_code_3` char(3) CHARACTER SET utf8 NOT NULL,
  `address_format_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `check_country` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_prioritised` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`countries_id`),
  KEY `IDX_COUNTRIES_NAME` (`countries_name`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.countries: 0 rows
DELETE FROM `countries`;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;


-- Dumping structure for table crazy_import.coupons
DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(10) unsigned NOT NULL COMMENT 'for bought coupons',
  `coupon_type` char(1) NOT NULL DEFAULT 'F',
  `coupon_code` varchar(32) NOT NULL DEFAULT '',
  `coupon_amount` decimal(8,4) NOT NULL DEFAULT '0.0000',
  `coupon_amount_in_target_currency` decimal(8,4) NOT NULL DEFAULT '0.0000',
  `currency` char(3) NOT NULL DEFAULT 'EUR',
  `coupon_minimum_order` decimal(8,4) NOT NULL DEFAULT '0.0000',
  `coupon_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `coupon_expire_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uses_per_coupon` int(5) NOT NULL DEFAULT '1',
  `uses_per_user` int(5) NOT NULL DEFAULT '0',
  `restrict_to_products` varchar(255) DEFAULT NULL,
  `restrict_to_categories` varchar(255) DEFAULT NULL,
  `restrict_to_customers` text,
  `coupon_active` char(1) NOT NULL DEFAULT 'Y',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customer_description_header` varchar(64) CHARACTER SET utf8 NOT NULL,
  `customer_description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `customer_mail_sent` tinyint(1) NOT NULL DEFAULT '0',
  `coupon_theme` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ausgewähltes Motivbild',
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `coupon_code` (`coupon_code`),
  KEY `order_id` (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.coupons: 0 rows
DELETE FROM `coupons`;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;


-- Dumping structure for table crazy_import.coupons_description
DROP TABLE IF EXISTS `coupons_description`;
CREATE TABLE IF NOT EXISTS `coupons_description` (
  `coupon_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `coupon_name` varchar(32) NOT NULL DEFAULT '',
  `coupon_description` text,
  KEY `coupon_id` (`coupon_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.coupons_description: 0 rows
DELETE FROM `coupons_description`;
/*!40000 ALTER TABLE `coupons_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons_description` ENABLE KEYS */;


-- Dumping structure for table crazy_import.coupon_email_track
DROP TABLE IF EXISTS `coupon_email_track`;
CREATE TABLE IF NOT EXISTS `coupon_email_track` (
  `unique_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL DEFAULT '0',
  `customer_id_sent` int(11) NOT NULL DEFAULT '0',
  `sent_firstname` varchar(32) DEFAULT NULL,
  `sent_lastname` varchar(32) DEFAULT NULL,
  `emailed_to` varchar(32) DEFAULT NULL,
  `date_sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`unique_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.coupon_email_track: 0 rows
DELETE FROM `coupon_email_track`;
/*!40000 ALTER TABLE `coupon_email_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_email_track` ENABLE KEYS */;


-- Dumping structure for table crazy_import.coupon_gv_customer
DROP TABLE IF EXISTS `coupon_gv_customer`;
CREATE TABLE IF NOT EXISTS `coupon_gv_customer` (
  `customer_id` int(5) NOT NULL DEFAULT '0',
  `amount` decimal(8,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`customer_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.coupon_gv_customer: 0 rows
DELETE FROM `coupon_gv_customer`;
/*!40000 ALTER TABLE `coupon_gv_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_gv_customer` ENABLE KEYS */;


-- Dumping structure for table crazy_import.coupon_gv_queue
DROP TABLE IF EXISTS `coupon_gv_queue`;
CREATE TABLE IF NOT EXISTS `coupon_gv_queue` (
  `unique_id` int(5) NOT NULL AUTO_INCREMENT,
  `customer_id` int(5) NOT NULL DEFAULT '0',
  `order_id` int(5) NOT NULL DEFAULT '0',
  `amount` decimal(8,4) NOT NULL DEFAULT '0.0000',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ipaddr` varchar(32) NOT NULL DEFAULT '',
  `release_flag` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`unique_id`),
  KEY `uid` (`unique_id`,`customer_id`,`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.coupon_gv_queue: 0 rows
DELETE FROM `coupon_gv_queue`;
/*!40000 ALTER TABLE `coupon_gv_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_gv_queue` ENABLE KEYS */;


-- Dumping structure for table crazy_import.coupon_redeem_track
DROP TABLE IF EXISTS `coupon_redeem_track`;
CREATE TABLE IF NOT EXISTS `coupon_redeem_track` (
  `unique_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `redeem_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `redeem_ip` varchar(32) NOT NULL DEFAULT '',
  `order_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`unique_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.coupon_redeem_track: 0 rows
DELETE FROM `coupon_redeem_track`;
/*!40000 ALTER TABLE `coupon_redeem_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_redeem_track` ENABLE KEYS */;


-- Dumping structure for table crazy_import.coupon_themes
DROP TABLE IF EXISTS `coupon_themes`;
CREATE TABLE IF NOT EXISTS `coupon_themes` (
  `coupon_themes_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_theme_categories_id` int(11) NOT NULL,
  `status` enum('pending','active','inactive') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`coupon_themes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.coupon_themes: 0 rows
DELETE FROM `coupon_themes`;
/*!40000 ALTER TABLE `coupon_themes` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_themes` ENABLE KEYS */;


-- Dumping structure for table crazy_import.coupon_theme_categories
DROP TABLE IF EXISTS `coupon_theme_categories`;
CREATE TABLE IF NOT EXISTS `coupon_theme_categories` (
  `coupon_theme_categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupon_theme_categories_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.coupon_theme_categories: 0 rows
DELETE FROM `coupon_theme_categories`;
/*!40000 ALTER TABLE `coupon_theme_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_theme_categories` ENABLE KEYS */;


-- Dumping structure for table crazy_import.coupon_theme_category_names
DROP TABLE IF EXISTS `coupon_theme_category_names`;
CREATE TABLE IF NOT EXISTS `coupon_theme_category_names` (
  `coupon_theme_categories_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`coupon_theme_categories_id`,`language_id`),
  KEY `voucher_theme_categories_id` (`coupon_theme_categories_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.coupon_theme_category_names: 0 rows
DELETE FROM `coupon_theme_category_names`;
/*!40000 ALTER TABLE `coupon_theme_category_names` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_theme_category_names` ENABLE KEYS */;


-- Dumping structure for table crazy_import.currencies
DROP TABLE IF EXISTS `currencies`;
CREATE TABLE IF NOT EXISTS `currencies` (
  `currencies_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) DEFAULT NULL,
  `symbol_right` varchar(12) DEFAULT NULL,
  `decimal_point` varchar(1) DEFAULT NULL,
  `thousands_point` varchar(1) DEFAULT NULL,
  `decimal_places` varchar(1) DEFAULT NULL,
  `value` float(13,8) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`currencies_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.currencies: 0 rows
DELETE FROM `currencies`;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;


-- Dumping structure for table crazy_import.currencies_history
DROP TABLE IF EXISTS `currencies_history`;
CREATE TABLE IF NOT EXISTS `currencies_history` (
  `currencies_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `currencies_id` int(11) NOT NULL,
  `date_create` date NOT NULL,
  `value` float(13,8) NOT NULL DEFAULT '0.00000000',
  PRIMARY KEY (`currencies_history_id`),
  KEY `date_create` (`date_create`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='History der Umrechnungsfaktoren der Fremdwährungen in Euro';

-- Dumping data for table crazy_import.currencies_history: 0 rows
DELETE FROM `currencies_history`;
/*!40000 ALTER TABLE `currencies_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `currencies_history` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers
DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_cid` varchar(32) DEFAULT NULL,
  `customers_vat_id` varchar(20) DEFAULT NULL,
  `customers_vat_id_status` int(2) NOT NULL DEFAULT '0',
  `customers_warning` varchar(32) DEFAULT NULL,
  `customers_status` int(5) NOT NULL DEFAULT '1',
  `customers_gender` char(1) NOT NULL,
  `customers_firstname` varchar(32) CHARACTER SET utf8 NOT NULL,
  `customers_lastname` varchar(32) CHARACTER SET utf8 NOT NULL,
  `nickname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `customers_dob` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customers_email_address` varchar(96) CHARACTER SET utf8 NOT NULL,
  `invalid_email` varchar(96) CHARACTER SET utf8 NOT NULL COMMENT 'invalide emailadressen',
  `customers_default_address_id` int(11) NOT NULL,
  `customers_telephone` varchar(32) NOT NULL,
  `customers_fax` varchar(32) DEFAULT NULL,
  `customers_password` varchar(40) NOT NULL,
  `customers_newsletter` char(1) DEFAULT NULL,
  `customers_newsletter_mode` char(1) NOT NULL DEFAULT '0',
  `member_flag` char(1) NOT NULL DEFAULT '0',
  `delete_user` char(1) NOT NULL DEFAULT '1' COMMENT '1 = löschen möglich',
  `account_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Gast',
  `password_request_key` varchar(32) NOT NULL,
  `payment_unallowed` varchar(255) NOT NULL,
  `shipping_unallowed` varchar(255) NOT NULL,
  `refferers_id` int(5) NOT NULL DEFAULT '0',
  `customers_date_added` datetime DEFAULT '0000-00-00 00:00:00',
  `customers_last_modified` datetime DEFAULT '0000-00-00 00:00:00',
  `datensg` datetime DEFAULT NULL,
  `login_tries` int(10) unsigned NOT NULL,
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `centralised_buying_active` tinyint(3) unsigned NOT NULL COMMENT 'Sammelbesteller',
  PRIMARY KEY (`customers_id`),
  KEY `customers_email_address` (`customers_email_address`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers: 0 rows
DELETE FROM `customers`;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers_basket
DROP TABLE IF EXISTS `customers_basket`;
CREATE TABLE IF NOT EXISTS `customers_basket` (
  `customers_basket_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `products_id` tinytext NOT NULL,
  `customers_basket_quantity` int(2) NOT NULL,
  `final_price` decimal(15,4) NOT NULL,
  `customers_basket_date_added` char(8) DEFAULT NULL,
  `checkout_site` enum('cart','shipping','payment','confirm') NOT NULL DEFAULT 'cart',
  `language` varchar(32) DEFAULT NULL,
  `centralised_buying_group_id` int(10) unsigned NOT NULL DEFAULT '1',
  `combinedAttributes` varchar(255) NOT NULL,
  `attributes` text NOT NULL,
  `price_when_last_seen` decimal(15,4) NOT NULL,
  `to_discard` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'benötigt während des logins',
  `added_datetime` datetime NOT NULL,
  `changed_datetime` datetime NOT NULL,
  PRIMARY KEY (`customers_basket_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers_basket: 0 rows
DELETE FROM `customers_basket`;
/*!40000 ALTER TABLE `customers_basket` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_basket` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers_basket_attributes
DROP TABLE IF EXISTS `customers_basket_attributes`;
CREATE TABLE IF NOT EXISTS `customers_basket_attributes` (
  `customers_basket_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `products_id` tinytext NOT NULL,
  `products_options_id` int(11) NOT NULL,
  `products_options_value_id` int(11) NOT NULL,
  `centralised_buying_group_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`customers_basket_attributes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers_basket_attributes: 0 rows
DELETE FROM `customers_basket_attributes`;
/*!40000 ALTER TABLE `customers_basket_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_basket_attributes` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers_info
DROP TABLE IF EXISTS `customers_info`;
CREATE TABLE IF NOT EXISTS `customers_info` (
  `customers_info_id` int(11) NOT NULL,
  `customers_info_date_of_last_logon` datetime DEFAULT NULL,
  `customers_info_number_of_logons` int(5) DEFAULT NULL,
  `customers_info_date_account_created` datetime DEFAULT NULL,
  `customers_info_date_account_last_modified` datetime DEFAULT NULL,
  `global_product_notifications` int(1) DEFAULT '0',
  PRIMARY KEY (`customers_info_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers_info: 0 rows
DELETE FROM `customers_info`;
/*!40000 ALTER TABLE `customers_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_info` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers_ip
DROP TABLE IF EXISTS `customers_ip`;
CREATE TABLE IF NOT EXISTS `customers_ip` (
  `customers_ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_ip` varchar(15) NOT NULL DEFAULT '',
  `customers_ip_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customers_host` varchar(255) NOT NULL DEFAULT '',
  `customers_advertiser` varchar(30) DEFAULT NULL,
  `customers_referer_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`customers_ip_id`),
  KEY `customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers_ip: 0 rows
DELETE FROM `customers_ip`;
/*!40000 ALTER TABLE `customers_ip` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_ip` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers_list
DROP TABLE IF EXISTS `customers_list`;
CREATE TABLE IF NOT EXISTS `customers_list` (
  `customers_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `list_type` enum('watchlist','wishlist') NOT NULL,
  `date_created` date NOT NULL,
  `date_changed` date NOT NULL,
  PRIMARY KEY (`customers_list_id`),
  KEY `die_liste` (`list_type`,`customers_id`,`customers_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers_list: 0 rows
DELETE FROM `customers_list`;
/*!40000 ALTER TABLE `customers_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_list` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers_list_products
DROP TABLE IF EXISTS `customers_list_products`;
CREATE TABLE IF NOT EXISTS `customers_list_products` (
  `customers_list_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_list_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `attributes` text NOT NULL,
  `datetime_added` datetime NOT NULL,
  PRIMARY KEY (`customers_list_products_id`),
  KEY `customers_list_id` (`customers_list_id`),
  KEY `letztes_produkt` (`datetime_added`,`customers_list_id`),
  KEY `remove_from_list` (`products_id`,`customers_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers_list_products: 0 rows
DELETE FROM `customers_list_products`;
/*!40000 ALTER TABLE `customers_list_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_list_products` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers_memo
DROP TABLE IF EXISTS `customers_memo`;
CREATE TABLE IF NOT EXISTS `customers_memo` (
  `memo_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `memo_date` date NOT NULL DEFAULT '0000-00-00',
  `memo_title` text NOT NULL,
  `memo_text` text NOT NULL,
  `poster_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`memo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers_memo: 0 rows
DELETE FROM `customers_memo`;
/*!40000 ALTER TABLE `customers_memo` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_memo` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers_sik
DROP TABLE IF EXISTS `customers_sik`;
CREATE TABLE IF NOT EXISTS `customers_sik` (
  `customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_cid` varchar(32) DEFAULT NULL,
  `customers_vat_id` varchar(20) DEFAULT NULL,
  `customers_vat_id_status` int(2) NOT NULL DEFAULT '0',
  `customers_warning` varchar(32) DEFAULT NULL,
  `customers_status` int(5) NOT NULL DEFAULT '1',
  `customers_gender` char(1) NOT NULL,
  `customers_firstname` varchar(32) NOT NULL,
  `customers_lastname` varchar(32) NOT NULL,
  `customers_dob` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customers_email_address` varchar(96) NOT NULL,
  `customers_default_address_id` int(11) NOT NULL,
  `customers_telephone` varchar(32) NOT NULL,
  `customers_fax` varchar(32) DEFAULT NULL,
  `customers_password` varchar(40) NOT NULL,
  `customers_newsletter` char(1) DEFAULT NULL,
  `customers_newsletter_mode` char(1) NOT NULL DEFAULT '0',
  `member_flag` char(1) NOT NULL DEFAULT '0',
  `delete_user` char(1) NOT NULL DEFAULT '1',
  `account_type` int(1) NOT NULL DEFAULT '0',
  `password_request_key` varchar(32) NOT NULL,
  `payment_unallowed` varchar(255) NOT NULL,
  `shipping_unallowed` varchar(255) NOT NULL,
  `refferers_id` int(5) NOT NULL DEFAULT '0',
  `customers_date_added` datetime DEFAULT '0000-00-00 00:00:00',
  `customers_last_modified` datetime DEFAULT '0000-00-00 00:00:00',
  `datensg` datetime DEFAULT NULL,
  `login_tries` varchar(2) NOT NULL DEFAULT '0',
  `login_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers_sik: 0 rows
DELETE FROM `customers_sik`;
/*!40000 ALTER TABLE `customers_sik` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_sik` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers_status
DROP TABLE IF EXISTS `customers_status`;
CREATE TABLE IF NOT EXISTS `customers_status` (
  `customers_status_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `customers_status_name` varchar(32) NOT NULL DEFAULT '',
  `customers_status_public` int(1) NOT NULL DEFAULT '1',
  `customers_status_min_order` int(7) DEFAULT NULL,
  `customers_status_max_order` int(7) DEFAULT NULL,
  `customers_status_image` varchar(64) DEFAULT NULL,
  `customers_status_discount` decimal(4,2) DEFAULT '0.00',
  `customers_status_ot_discount_flag` char(1) NOT NULL DEFAULT '0',
  `customers_status_ot_discount` decimal(4,2) DEFAULT '0.00',
  `customers_status_graduated_prices` varchar(1) NOT NULL DEFAULT '0',
  `customers_status_show_price` int(1) NOT NULL DEFAULT '1',
  `customers_status_show_price_tax` int(1) NOT NULL DEFAULT '1',
  `customers_status_add_tax_ot` int(1) NOT NULL DEFAULT '0',
  `customers_status_payment_unallowed` varchar(255) NOT NULL,
  `customers_status_shipping_unallowed` varchar(255) NOT NULL,
  `customers_status_discount_attributes` int(1) NOT NULL DEFAULT '0',
  `customers_fsk18` int(1) NOT NULL DEFAULT '1',
  `customers_fsk18_display` int(1) NOT NULL DEFAULT '1',
  `customers_status_write_reviews` int(1) NOT NULL DEFAULT '1',
  `customers_status_read_reviews` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`customers_status_id`,`language_id`),
  KEY `idx_orders_status_name` (`customers_status_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers_status: 0 rows
DELETE FROM `customers_status`;
/*!40000 ALTER TABLE `customers_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_status` ENABLE KEYS */;


-- Dumping structure for table crazy_import.customers_status_history
DROP TABLE IF EXISTS `customers_status_history`;
CREATE TABLE IF NOT EXISTS `customers_status_history` (
  `customers_status_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `new_value` int(5) NOT NULL DEFAULT '0',
  `old_value` int(5) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `customer_notified` int(1) DEFAULT '0',
  PRIMARY KEY (`customers_status_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.customers_status_history: 0 rows
DELETE FROM `customers_status_history`;
/*!40000 ALTER TABLE `customers_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_status_history` ENABLE KEYS */;


-- Dumping structure for table crazy_import.database_version
DROP TABLE IF EXISTS `database_version`;
CREATE TABLE IF NOT EXISTS `database_version` (
  `version` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.database_version: 0 rows
DELETE FROM `database_version`;
/*!40000 ALTER TABLE `database_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `database_version` ENABLE KEYS */;


-- Dumping structure for table crazy_import.edn
DROP TABLE IF EXISTS `edn`;
CREATE TABLE IF NOT EXISTS `edn` (
  `delivery_note_id` int(10) unsigned NOT NULL,
  `barcode` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` enum('pending','done','scanned') NOT NULL DEFAULT 'pending',
  `last_export` datetime NOT NULL,
  `last_scanned_datetime` datetime NOT NULL,
  `retry_counter` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `update_erp` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`delivery_note_id`),
  KEY `status` (`status`),
  KEY `status_last_export_retry_counter` (`status`,`last_export`,`retry_counter`),
  KEY `barcode` (`barcode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table crazy_import.edn: 0 rows
DELETE FROM `edn`;
/*!40000 ALTER TABLE `edn` DISABLE KEYS */;
/*!40000 ALTER TABLE `edn` ENABLE KEYS */;


-- Dumping structure for table crazy_import.erp_delivery_notes_for_ratingen
DROP TABLE IF EXISTS `erp_delivery_notes_for_ratingen`;
CREATE TABLE IF NOT EXISTS `erp_delivery_notes_for_ratingen` (
  `delivery_note_id` int(10) unsigned NOT NULL,
  `barcode` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` enum('pending','done','scanned') NOT NULL DEFAULT 'pending',
  `last_export` datetime NOT NULL,
  `last_scanned_datetime` datetime NOT NULL,
  `retry_counter` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `update_erp` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`delivery_note_id`),
  KEY `status` (`status`),
  KEY `status_last_export_retry_counter` (`status`,`last_export`,`retry_counter`),
  KEY `barcode` (`barcode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.erp_delivery_notes_for_ratingen: 0 rows
DELETE FROM `erp_delivery_notes_for_ratingen`;
/*!40000 ALTER TABLE `erp_delivery_notes_for_ratingen` DISABLE KEYS */;
/*!40000 ALTER TABLE `erp_delivery_notes_for_ratingen` ENABLE KEYS */;


-- Dumping structure for table crazy_import.geo_zones
DROP TABLE IF EXISTS `geo_zones`;
CREATE TABLE IF NOT EXISTS `geo_zones` (
  `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_zone_name` varchar(32) NOT NULL,
  `geo_zone_description` varchar(255) NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`geo_zone_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.geo_zones: 0 rows
DELETE FROM `geo_zones`;
/*!40000 ALTER TABLE `geo_zones` DISABLE KEYS */;
/*!40000 ALTER TABLE `geo_zones` ENABLE KEYS */;


-- Dumping structure for table crazy_import.languages
DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `languages_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 NOT NULL,
  `code` char(2) CHARACTER SET utf8 NOT NULL,
  `locale` char(5) CHARACTER SET utf8 NOT NULL,
  `image` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `directory` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `sort_order` int(3) DEFAULT NULL,
  `language_charset` text CHARACTER SET utf8 NOT NULL,
  `contact_email` varchar(64) CHARACTER SET utf8 NOT NULL,
  `date_format` varchar(255) NOT NULL,
  `time_format` enum('12','24') NOT NULL,
  PRIMARY KEY (`languages_id`),
  KEY `IDX_LANGUAGES_NAME` (`name`),
  KEY `fuer den export an die ERP` (`directory`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.languages: 0 rows
DELETE FROM `languages`;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;


-- Dumping structure for table crazy_import.languages_bak
DROP TABLE IF EXISTS `languages_bak`;
CREATE TABLE IF NOT EXISTS `languages_bak` (
  `languages_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `code` char(2) NOT NULL,
  `image` varchar(64) DEFAULT NULL,
  `directory` varchar(32) DEFAULT NULL,
  `sort_order` int(3) DEFAULT NULL,
  `language_charset` text NOT NULL,
  PRIMARY KEY (`languages_id`),
  KEY `IDX_LANGUAGES_NAME` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.languages_bak: 0 rows
DELETE FROM `languages_bak`;
/*!40000 ALTER TABLE `languages_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `languages_bak` ENABLE KEYS */;


-- Dumping structure for table crazy_import.mailaddress_check
DROP TABLE IF EXISTS `mailaddress_check`;
CREATE TABLE IF NOT EXISTS `mailaddress_check` (
  `mailaddress_check_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `check_string` varchar(200) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`mailaddress_check_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.mailaddress_check: 0 rows
DELETE FROM `mailaddress_check`;
/*!40000 ALTER TABLE `mailaddress_check` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailaddress_check` ENABLE KEYS */;


-- Dumping structure for table crazy_import.manufacturers
DROP TABLE IF EXISTS `manufacturers`;
CREATE TABLE IF NOT EXISTS `manufacturers` (
  `manufacturers_id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturers_name` varchar(32) NOT NULL,
  `manufacturers_image` varchar(64) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`manufacturers_id`),
  KEY `IDX_MANUFACTURERS_NAME` (`manufacturers_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.manufacturers: 0 rows
DELETE FROM `manufacturers`;
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;


-- Dumping structure for table crazy_import.manufacturers_info
DROP TABLE IF EXISTS `manufacturers_info`;
CREATE TABLE IF NOT EXISTS `manufacturers_info` (
  `manufacturers_id` int(11) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `manufacturers_meta_title` varchar(100) NOT NULL,
  `manufacturers_meta_description` varchar(255) NOT NULL,
  `manufacturers_meta_keywords` varchar(255) NOT NULL,
  `manufacturers_url` varchar(255) NOT NULL,
  `url_clicked` int(5) NOT NULL DEFAULT '0',
  `date_last_click` datetime DEFAULT NULL,
  PRIMARY KEY (`manufacturers_id`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.manufacturers_info: 0 rows
DELETE FROM `manufacturers_info`;
/*!40000 ALTER TABLE `manufacturers_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturers_info` ENABLE KEYS */;


-- Dumping structure for table crazy_import.media_content
DROP TABLE IF EXISTS `media_content`;
CREATE TABLE IF NOT EXISTS `media_content` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `old_filename` text NOT NULL,
  `new_filename` text NOT NULL,
  `file_comment` text NOT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.media_content: 0 rows
DELETE FROM `media_content`;
/*!40000 ALTER TABLE `media_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_content` ENABLE KEYS */;


-- Dumping structure for table crazy_import.module_newsletter
DROP TABLE IF EXISTS `module_newsletter`;
CREATE TABLE IF NOT EXISTS `module_newsletter` (
  `newsletter_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `bc` text NOT NULL,
  `cc` text NOT NULL,
  `date` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `body` text NOT NULL,
  `personalize` char(3) NOT NULL DEFAULT '0',
  `greeting` int(1) NOT NULL DEFAULT '0',
  `gift` char(3) NOT NULL DEFAULT '0',
  `ammount` varchar(10) NOT NULL DEFAULT '0',
  `product_list` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`newsletter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.module_newsletter: 0 rows
DELETE FROM `module_newsletter`;
/*!40000 ALTER TABLE `module_newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_newsletter` ENABLE KEYS */;


-- Dumping structure for table crazy_import.newsletters
DROP TABLE IF EXISTS `newsletters`;
CREATE TABLE IF NOT EXISTS `newsletters` (
  `newsletters_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `module` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_sent` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `locked` int(1) DEFAULT '0',
  PRIMARY KEY (`newsletters_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.newsletters: 0 rows
DELETE FROM `newsletters`;
/*!40000 ALTER TABLE `newsletters` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletters` ENABLE KEYS */;


-- Dumping structure for table crazy_import.newsletters_history
DROP TABLE IF EXISTS `newsletters_history`;
CREATE TABLE IF NOT EXISTS `newsletters_history` (
  `news_hist_id` int(11) NOT NULL DEFAULT '0',
  `news_hist_cs` int(11) NOT NULL DEFAULT '0',
  `news_hist_cs_date_sent` date DEFAULT NULL,
  PRIMARY KEY (`news_hist_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.newsletters_history: 0 rows
DELETE FROM `newsletters_history`;
/*!40000 ALTER TABLE `newsletters_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletters_history` ENABLE KEYS */;


-- Dumping structure for table crazy_import.newsletter_products
DROP TABLE IF EXISTS `newsletter_products`;
CREATE TABLE IF NOT EXISTS `newsletter_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accessories_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.newsletter_products: 0 rows
DELETE FROM `newsletter_products`;
/*!40000 ALTER TABLE `newsletter_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_products` ENABLE KEYS */;


-- Dumping structure for table crazy_import.newsletter_product_list
DROP TABLE IF EXISTS `newsletter_product_list`;
CREATE TABLE IF NOT EXISTS `newsletter_product_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_name` char(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.newsletter_product_list: 0 rows
DELETE FROM `newsletter_product_list`;
/*!40000 ALTER TABLE `newsletter_product_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_product_list` ENABLE KEYS */;


-- Dumping structure for table crazy_import.newsletter_recipients
DROP TABLE IF EXISTS `newsletter_recipients`;
CREATE TABLE IF NOT EXISTS `newsletter_recipients` (
  `mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_email_address` varchar(96) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_status` int(5) NOT NULL DEFAULT '0',
  `customers_firstname` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `customers_lastname` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mail_status` int(1) NOT NULL DEFAULT '0',
  `mail_key` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `customers_language` varchar(255) CHARACTER SET utf8 NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `temp` tinyint(4) NOT NULL,
  PRIMARY KEY (`mail_id`),
  UNIQUE KEY `customers_email_address` (`customers_email_address`),
  KEY `customers_language` (`customers_language`),
  KEY `mail_key` (`mail_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.newsletter_recipients: 0 rows
DELETE FROM `newsletter_recipients`;
/*!40000 ALTER TABLE `newsletter_recipients` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_recipients` ENABLE KEYS */;


-- Dumping structure for table crazy_import.newsletter_recipients_03_01_2012
DROP TABLE IF EXISTS `newsletter_recipients_03_01_2012`;
CREATE TABLE IF NOT EXISTS `newsletter_recipients_03_01_2012` (
  `mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_email_address` varchar(96) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_status` int(5) NOT NULL DEFAULT '0',
  `customers_firstname` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `customers_lastname` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mail_status` int(1) NOT NULL DEFAULT '0',
  `mail_key` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `customers_language` varchar(255) CHARACTER SET utf8 NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `temp` tinyint(4) NOT NULL,
  PRIMARY KEY (`mail_id`),
  UNIQUE KEY `customers_email_address` (`customers_email_address`),
  KEY `customers_language` (`customers_language`),
  KEY `mail_key` (`mail_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.newsletter_recipients_03_01_2012: 0 rows
DELETE FROM `newsletter_recipients_03_01_2012`;
/*!40000 ALTER TABLE `newsletter_recipients_03_01_2012` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_recipients_03_01_2012` ENABLE KEYS */;


-- Dumping structure for table crazy_import.orders
DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `customers_cid` varchar(32) DEFAULT NULL,
  `customers_vat_id` varchar(20) DEFAULT NULL,
  `customers_status` int(11) DEFAULT NULL,
  `customers_status_name` varchar(32) NOT NULL,
  `customers_status_image` varchar(64) DEFAULT NULL,
  `customers_status_discount` decimal(4,2) DEFAULT NULL,
  `customers_name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `customers_firstname` varchar(64) CHARACTER SET utf8 NOT NULL,
  `customers_lastname` varchar(64) CHARACTER SET utf8 NOT NULL,
  `customers_company` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `customers_street_address` varchar(64) CHARACTER SET utf8 NOT NULL,
  `customers_suburb` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `customers_city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `customers_postcode` varchar(10) CHARACTER SET utf8 NOT NULL,
  `customers_state` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `customers_country` varchar(32) CHARACTER SET utf8 NOT NULL,
  `customers_telephone` varchar(32) CHARACTER SET utf8 NOT NULL,
  `customers_email_address` varchar(96) CHARACTER SET utf8 NOT NULL,
  `invalid_email` varchar(96) CHARACTER SET utf8 NOT NULL COMMENT 'invalide emailadressen',
  `customers_address_format_id` int(5) NOT NULL,
  `delivery_name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `delivery_firstname` varchar(64) CHARACTER SET utf8 NOT NULL,
  `delivery_lastname` varchar(64) CHARACTER SET utf8 NOT NULL,
  `delivery_company` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `delivery_street_address` varchar(64) CHARACTER SET utf8 NOT NULL,
  `delivery_suburb` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '2. Addressfeld',
  `delivery_city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `delivery_postcode` varchar(10) CHARACTER SET utf8 NOT NULL,
  `delivery_state` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `delivery_country` varchar(32) CHARACTER SET utf8 NOT NULL,
  `delivery_country_iso_code_2` char(2) CHARACTER SET utf8 NOT NULL,
  `delivery_address_format_id` int(5) NOT NULL,
  `billing_name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `billing_firstname` varchar(64) CHARACTER SET utf8 NOT NULL,
  `billing_lastname` varchar(64) CHARACTER SET utf8 NOT NULL,
  `billing_company` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `billing_street_address` varchar(64) CHARACTER SET utf8 NOT NULL,
  `billing_suburb` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `billing_city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `billing_postcode` varchar(10) NOT NULL,
  `billing_state` varchar(32) DEFAULT NULL,
  `billing_country` varchar(32) NOT NULL,
  `billing_country_iso_code_2` char(2) NOT NULL,
  `billing_address_format_id` int(5) NOT NULL,
  `payment_method` varchar(32) NOT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `cc_owner` varchar(64) DEFAULT NULL,
  `cc_number` varchar(64) DEFAULT NULL,
  `cc_expires` varchar(4) DEFAULT NULL,
  `cc_start` varchar(4) DEFAULT NULL,
  `cc_issue` varchar(3) DEFAULT NULL,
  `cc_cvv` varchar(4) DEFAULT NULL,
  `comments` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_status` int(5) NOT NULL,
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `account_type` int(1) NOT NULL DEFAULT '0',
  `payment_class` varchar(32) NOT NULL,
  `shipping_method` varchar(32) NOT NULL,
  `shipping_class` varchar(32) NOT NULL,
  `customers_ip` varchar(32) NOT NULL,
  `language` varchar(32) NOT NULL,
  `afterbuy_success` int(1) NOT NULL DEFAULT '0',
  `afterbuy_id` int(32) NOT NULL DEFAULT '0',
  `refferers_id` varchar(32) NOT NULL,
  `conversion_type` int(1) NOT NULL DEFAULT '0',
  `orders_ident_key` varchar(128) DEFAULT NULL,
  `edebit_transaction_id` varchar(32) DEFAULT NULL,
  `edebit_gutid` varchar(32) DEFAULT NULL,
  `ibn_billnr` int(11) NOT NULL,
  `ibn_billdate` date NOT NULL,
  `ibn_pdfnotifydate` date NOT NULL,
  `allow_export` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Extra Freigabe zum Export zum Amicron',
  `allow_export_manuell` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `remind_mail` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'für paypal_pending cron',
  `payment_cluster_key` varchar(255) NOT NULL COMMENT 'für Docdata Payments',
  `order_billing_country` char(2) NOT NULL COMMENT 'für Docdata Payments',
  `payment_profile` varchar(255) NOT NULL COMMENT 'für Docdata Payments',
  `extra_products` text CHARACTER SET utf8 COMMENT 'Für nachträglich hinzugefüge Produkte.',
  `used_cashpoints` int(11) unsigned NOT NULL DEFAULT '0',
  `rma_orders_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Enthält die orders_id für die die Ersatzendung gemacht wurde',
  `rma_id` int(10) unsigned NOT NULL DEFAULT '0',
  `stock_freed` tinyint(4) NOT NULL COMMENT 'Stockartikel wurden dem Stock wieder aufaddiert, da der Kunde nicht bezahlt hat.',
  `pp_mode` tinyint(1) NOT NULL DEFAULT '0',
  `exported_to_erp` tinyint(1) NOT NULL,
  `force_export_to_erp` tinyint(1) NOT NULL,
  `prevent_export_to_erp` tinyint(1) NOT NULL,
  `filename` varchar(255) NOT NULL COMMENT 'Bis die Datei committet ist',
  PRIMARY KEY (`orders_id`),
  KEY `customers_id` (`customers_id`),
  KEY `date_purchased` (`date_purchased`),
  KEY `allow_export` (`allow_export`),
  KEY `orders_status` (`orders_status`),
  KEY `remind_mail` (`remind_mail`),
  KEY `customers_firstname` (`customers_firstname`),
  KEY `customers_lastname` (`customers_lastname`),
  KEY `stock_status` (`stock_freed`),
  KEY `orders_status_2` (`orders_status`,`exported_to_erp`),
  KEY `exported_to_test_erp` (`exported_to_erp`),
  KEY `customers_email_address` (`customers_email_address`),
  KEY `rma_orders_id` (`rma_orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.orders: 0 rows
DELETE FROM `orders`;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


-- Dumping structure for table crazy_import.orders_products
DROP TABLE IF EXISTS `orders_products`;
CREATE TABLE IF NOT EXISTS `orders_products` (
  `orders_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `products_model` varchar(64) DEFAULT NULL,
  `products_model_erp` varchar(64) NOT NULL,
  `products_name` varchar(64) NOT NULL,
  `products_price` decimal(15,4) NOT NULL,
  `products_discount_made` decimal(4,2) DEFAULT NULL,
  `products_shipping_time` varchar(255) DEFAULT NULL,
  `final_price` decimal(15,4) NOT NULL,
  `products_tax` decimal(7,4) NOT NULL,
  `products_quantity` int(2) NOT NULL,
  `allow_tax` int(1) NOT NULL,
  `centralised_buying_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attributes` text NOT NULL,
  PRIMARY KEY (`orders_products_id`),
  KEY `orders_id` (`orders_id`),
  KEY `products_id` (`products_id`),
  KEY `products_model_erp` (`products_model_erp`),
  KEY `products_model` (`products_model`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.orders_products: 0 rows
DELETE FROM `orders_products`;
/*!40000 ALTER TABLE `orders_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_products` ENABLE KEYS */;


-- Dumping structure for table crazy_import.orders_products_attributes
DROP TABLE IF EXISTS `orders_products_attributes`;
CREATE TABLE IF NOT EXISTS `orders_products_attributes` (
  `orders_products_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `orders_products_id` int(11) NOT NULL,
  `products_options` varchar(32) NOT NULL,
  `products_options_values` varchar(64) NOT NULL,
  `options_values_price` decimal(15,4) NOT NULL,
  `price_prefix` char(1) NOT NULL,
  `centralised_buying_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_products_attributes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.orders_products_attributes: 0 rows
DELETE FROM `orders_products_attributes`;
/*!40000 ALTER TABLE `orders_products_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_products_attributes` ENABLE KEYS */;


-- Dumping structure for table crazy_import.orders_products_download
DROP TABLE IF EXISTS `orders_products_download`;
CREATE TABLE IF NOT EXISTS `orders_products_download` (
  `orders_products_download_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `orders_products_id` int(11) NOT NULL DEFAULT '0',
  `orders_products_filename` varchar(255) NOT NULL DEFAULT '',
  `download_maxdays` int(2) NOT NULL DEFAULT '0',
  `download_count` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_products_download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.orders_products_download: 0 rows
DELETE FROM `orders_products_download`;
/*!40000 ALTER TABLE `orders_products_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_products_download` ENABLE KEYS */;


-- Dumping structure for table crazy_import.orders_recalculate
DROP TABLE IF EXISTS `orders_recalculate`;
CREATE TABLE IF NOT EXISTS `orders_recalculate` (
  `orders_recalculate_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `n_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `b_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax_rate` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `class` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`orders_recalculate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.orders_recalculate: 0 rows
DELETE FROM `orders_recalculate`;
/*!40000 ALTER TABLE `orders_recalculate` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_recalculate` ENABLE KEYS */;


-- Dumping structure for table crazy_import.orders_sent_out
DROP TABLE IF EXISTS `orders_sent_out`;
CREATE TABLE IF NOT EXISTS `orders_sent_out` (
  `orders_sent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(10) unsigned NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`orders_sent_id`),
  KEY `date_added` (`date_added`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.orders_sent_out: 0 rows
DELETE FROM `orders_sent_out`;
/*!40000 ALTER TABLE `orders_sent_out` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_sent_out` ENABLE KEYS */;


-- Dumping structure for table crazy_import.orders_status
DROP TABLE IF EXISTS `orders_status`;
CREATE TABLE IF NOT EXISTS `orders_status` (
  `orders_status_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `orders_status_name` varchar(32) NOT NULL,
  PRIMARY KEY (`orders_status_id`,`language_id`),
  KEY `idx_orders_status_name` (`orders_status_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.orders_status: 0 rows
DELETE FROM `orders_status`;
/*!40000 ALTER TABLE `orders_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_status` ENABLE KEYS */;


-- Dumping structure for table crazy_import.orders_status_history
DROP TABLE IF EXISTS `orders_status_history`;
CREATE TABLE IF NOT EXISTS `orders_status_history` (
  `orders_status_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `orders_status_id` int(5) NOT NULL,
  `date_added` datetime NOT NULL,
  `customer_notified` int(1) DEFAULT '0',
  `supporter_did_it` int(10) unsigned NOT NULL,
  `comments` text,
  PRIMARY KEY (`orders_status_history_id`),
  KEY `get_last_inserted_by_status` (`orders_status_id`,`date_added`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.orders_status_history: 0 rows
DELETE FROM `orders_status_history`;
/*!40000 ALTER TABLE `orders_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_status_history` ENABLE KEYS */;


-- Dumping structure for table crazy_import.orders_total
DROP TABLE IF EXISTS `orders_total`;
CREATE TABLE IF NOT EXISTS `orders_total` (
  `orders_total_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` decimal(15,4) NOT NULL,
  `class` varchar(32) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`orders_total_id`),
  KEY `idx_orders_total_orders_id` (`orders_id`),
  KEY `class` (`class`),
  KEY `fuer den export an die ERP` (`orders_id`,`class`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.orders_total: 0 rows
DELETE FROM `orders_total`;
/*!40000 ALTER TABLE `orders_total` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_total` ENABLE KEYS */;


-- Dumping structure for table crazy_import.payment_moneybookers
DROP TABLE IF EXISTS `payment_moneybookers`;
CREATE TABLE IF NOT EXISTS `payment_moneybookers` (
  `mb_TRID` varchar(255) NOT NULL DEFAULT '',
  `mb_ERRNO` smallint(3) unsigned NOT NULL DEFAULT '0',
  `mb_ERRTXT` varchar(255) NOT NULL DEFAULT '',
  `mb_DATE` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mb_MBTID` bigint(18) unsigned NOT NULL DEFAULT '0',
  `mb_STATUS` tinyint(1) NOT NULL DEFAULT '0',
  `mb_ORDERID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`mb_TRID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.payment_moneybookers: 0 rows
DELETE FROM `payment_moneybookers`;
/*!40000 ALTER TABLE `payment_moneybookers` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_moneybookers` ENABLE KEYS */;


-- Dumping structure for table crazy_import.payment_moneybookers_countries
DROP TABLE IF EXISTS `payment_moneybookers_countries`;
CREATE TABLE IF NOT EXISTS `payment_moneybookers_countries` (
  `osc_cID` int(11) NOT NULL DEFAULT '0',
  `mb_cID` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`osc_cID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.payment_moneybookers_countries: 0 rows
DELETE FROM `payment_moneybookers_countries`;
/*!40000 ALTER TABLE `payment_moneybookers_countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_moneybookers_countries` ENABLE KEYS */;


-- Dumping structure for table crazy_import.payment_moneybookers_currencies
DROP TABLE IF EXISTS `payment_moneybookers_currencies`;
CREATE TABLE IF NOT EXISTS `payment_moneybookers_currencies` (
  `mb_currID` char(3) NOT NULL DEFAULT '',
  `mb_currName` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`mb_currID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.payment_moneybookers_currencies: 0 rows
DELETE FROM `payment_moneybookers_currencies`;
/*!40000 ALTER TABLE `payment_moneybookers_currencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_moneybookers_currencies` ENABLE KEYS */;


-- Dumping structure for table crazy_import.payment_qenta
DROP TABLE IF EXISTS `payment_qenta`;
CREATE TABLE IF NOT EXISTS `payment_qenta` (
  `q_TRID` varchar(255) NOT NULL DEFAULT '',
  `q_DATE` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `q_QTID` bigint(18) unsigned NOT NULL DEFAULT '0',
  `q_ORDERDESC` varchar(255) NOT NULL DEFAULT '',
  `q_STATUS` tinyint(1) NOT NULL DEFAULT '0',
  `q_ORDERID` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`q_TRID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.payment_qenta: 0 rows
DELETE FROM `payment_qenta`;
/*!40000 ALTER TABLE `payment_qenta` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_qenta` ENABLE KEYS */;


-- Dumping structure for table crazy_import.paypal
DROP TABLE IF EXISTS `paypal`;
CREATE TABLE IF NOT EXISTS `paypal` (
  `paypal_ipn_id` int(11) NOT NULL AUTO_INCREMENT,
  `xtc_order_id` int(11) NOT NULL DEFAULT '0',
  `txn_type` varchar(32) NOT NULL DEFAULT '',
  `reason_code` varchar(15) DEFAULT NULL,
  `payment_type` varchar(7) NOT NULL DEFAULT '',
  `payment_status` varchar(17) NOT NULL DEFAULT '',
  `pending_reason` varchar(14) DEFAULT NULL,
  `invoice` varchar(64) DEFAULT NULL,
  `mc_currency` char(3) NOT NULL DEFAULT '',
  `first_name` varchar(32) NOT NULL DEFAULT '',
  `last_name` varchar(32) NOT NULL DEFAULT '',
  `payer_business_name` varchar(64) DEFAULT NULL,
  `address_name` varchar(32) DEFAULT NULL,
  `address_street` varchar(64) DEFAULT NULL,
  `address_city` varchar(32) DEFAULT NULL,
  `address_state` varchar(32) DEFAULT NULL,
  `address_zip` varchar(10) DEFAULT NULL,
  `address_country` varchar(64) DEFAULT NULL,
  `address_status` varchar(11) DEFAULT NULL,
  `payer_email` varchar(96) NOT NULL DEFAULT '',
  `payer_id` varchar(32) NOT NULL DEFAULT '',
  `payer_status` varchar(10) NOT NULL DEFAULT '',
  `payment_date` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `business` varchar(96) NOT NULL DEFAULT '',
  `receiver_email` varchar(96) NOT NULL DEFAULT '',
  `receiver_id` varchar(32) NOT NULL DEFAULT '',
  `txn_id` varchar(40) NOT NULL DEFAULT '',
  `parent_txn_id` varchar(17) DEFAULT NULL,
  `num_cart_items` tinyint(4) NOT NULL DEFAULT '1',
  `mc_gross` decimal(7,2) NOT NULL DEFAULT '0.00',
  `mc_fee` decimal(7,2) NOT NULL DEFAULT '0.00',
  `mc_shipping` decimal(7,2) NOT NULL DEFAULT '0.00',
  `payment_gross` decimal(7,2) DEFAULT NULL,
  `payment_fee` decimal(7,2) DEFAULT NULL,
  `settle_amount` decimal(7,2) DEFAULT NULL,
  `settle_currency` char(3) DEFAULT NULL,
  `exchange_rate` decimal(4,2) DEFAULT NULL,
  `notify_version` decimal(2,1) NOT NULL DEFAULT '0.0',
  `verify_sign` varchar(128) NOT NULL DEFAULT '',
  `last_modified` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `date_added` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `memo` text,
  `mc_authorization` decimal(7,2) NOT NULL,
  `mc_captured` decimal(7,2) NOT NULL,
  PRIMARY KEY (`paypal_ipn_id`,`txn_id`),
  KEY `xtc_order_id` (`xtc_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.paypal: 0 rows
DELETE FROM `paypal`;
/*!40000 ALTER TABLE `paypal` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal` ENABLE KEYS */;


-- Dumping structure for table crazy_import.paypal_status_history
DROP TABLE IF EXISTS `paypal_status_history`;
CREATE TABLE IF NOT EXISTS `paypal_status_history` (
  `payment_status_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `paypal_ipn_id` int(11) NOT NULL DEFAULT '0',
  `txn_id` varchar(64) NOT NULL DEFAULT '',
  `parent_txn_id` varchar(64) NOT NULL DEFAULT '',
  `payment_status` varchar(17) NOT NULL DEFAULT '',
  `pending_reason` varchar(64) DEFAULT NULL,
  `mc_amount` decimal(7,2) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  PRIMARY KEY (`payment_status_history_id`),
  KEY `paypal_ipn_id` (`paypal_ipn_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.paypal_status_history: 0 rows
DELETE FROM `paypal_status_history`;
/*!40000 ALTER TABLE `paypal_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_status_history` ENABLE KEYS */;


-- Dumping structure for table crazy_import.pdfbill_profile
DROP TABLE IF EXISTS `pdfbill_profile`;
CREATE TABLE IF NOT EXISTS `pdfbill_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(255) NOT NULL DEFAULT '',
  `profile_parameter` text NOT NULL,
  `profile_categories` text NOT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.pdfbill_profile: 0 rows
DELETE FROM `pdfbill_profile`;
/*!40000 ALTER TABLE `pdfbill_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `pdfbill_profile` ENABLE KEYS */;


-- Dumping structure for table crazy_import.personal_offers_by_customers_status_0
DROP TABLE IF EXISTS `personal_offers_by_customers_status_0`;
CREATE TABLE IF NOT EXISTS `personal_offers_by_customers_status_0` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `personal_offer` decimal(15,4) DEFAULT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.personal_offers_by_customers_status_0: 0 rows
DELETE FROM `personal_offers_by_customers_status_0`;
/*!40000 ALTER TABLE `personal_offers_by_customers_status_0` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_offers_by_customers_status_0` ENABLE KEYS */;


-- Dumping structure for table crazy_import.personal_offers_by_customers_status_1
DROP TABLE IF EXISTS `personal_offers_by_customers_status_1`;
CREATE TABLE IF NOT EXISTS `personal_offers_by_customers_status_1` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `personal_offer` decimal(15,4) DEFAULT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.personal_offers_by_customers_status_1: 0 rows
DELETE FROM `personal_offers_by_customers_status_1`;
/*!40000 ALTER TABLE `personal_offers_by_customers_status_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_offers_by_customers_status_1` ENABLE KEYS */;


-- Dumping structure for table crazy_import.personal_offers_by_customers_status_2
DROP TABLE IF EXISTS `personal_offers_by_customers_status_2`;
CREATE TABLE IF NOT EXISTS `personal_offers_by_customers_status_2` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `personal_offer` decimal(15,4) DEFAULT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.personal_offers_by_customers_status_2: 0 rows
DELETE FROM `personal_offers_by_customers_status_2`;
/*!40000 ALTER TABLE `personal_offers_by_customers_status_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_offers_by_customers_status_2` ENABLE KEYS */;


-- Dumping structure for table crazy_import.personal_offers_by_customers_status_3
DROP TABLE IF EXISTS `personal_offers_by_customers_status_3`;
CREATE TABLE IF NOT EXISTS `personal_offers_by_customers_status_3` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `personal_offer` decimal(15,4) DEFAULT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.personal_offers_by_customers_status_3: 0 rows
DELETE FROM `personal_offers_by_customers_status_3`;
/*!40000 ALTER TABLE `personal_offers_by_customers_status_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_offers_by_customers_status_3` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `current_products_popularity` int(10) unsigned NOT NULL COMMENT 'Produkte des Monats sortieren.',
  `products_ean` varchar(128) DEFAULT NULL,
  `products_quantity` int(4) NOT NULL,
  `products_shippingtime` int(4) NOT NULL,
  `products_catalog_page` int(11) unsigned DEFAULT NULL COMMENT 'customfield',
  `colorchart` varchar(128) DEFAULT NULL COMMENT 'customfield',
  `product_info1` varchar(128) DEFAULT NULL COMMENT 'customfield',
  `product_info2` varchar(128) DEFAULT NULL COMMENT 'customfield',
  `products_model` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `group_permission_0` tinyint(1) NOT NULL,
  `group_permission_1` tinyint(1) NOT NULL,
  `group_permission_2` tinyint(1) NOT NULL,
  `group_permission_3` tinyint(1) NOT NULL,
  `products_sort` int(11) NOT NULL DEFAULT '0',
  `products_image` varchar(64) DEFAULT NULL,
  `products_price` decimal(10,6) NOT NULL,
  `products_discount_allowed` decimal(3,2) NOT NULL DEFAULT '0.00',
  `products_date_added` datetime NOT NULL,
  `products_last_modified` datetime DEFAULT NULL,
  `products_date_available` datetime DEFAULT NULL,
  `products_weight` decimal(5,2) NOT NULL,
  `products_status` tinyint(1) NOT NULL COMMENT 'Ob das Produkt aktiv ist; wird aus verschiedenen Bedingungen berechnet',
  `deactivated_by_formula_parser` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deactivated_by_formula_parser_modified` datetime NOT NULL,
  `products_new` tinyint(1) NOT NULL COMMENT 'Ob das Produkt neu ist',
  `products_new_activated` tinyint(1) NOT NULL COMMENT 'Ob das neue Produkt aktiv sein soll',
  `out_of_stock` tinyint(1) NOT NULL COMMENT 'Ob das Produkt laut ERP deaktiviert sein soll',
  `manually_deactivated` tinyint(1) NOT NULL COMMENT 'Ob das Produkt laut Import oder Artikelmaske deaktiviert sein soll',
  `products_stock_item` tinyint(1) NOT NULL COMMENT 'ist Bestadsartikel',
  `products_tax_class_id` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `product_template` varchar(64) DEFAULT NULL,
  `options_template` varchar(64) DEFAULT NULL,
  `manufacturers_id` int(11) DEFAULT NULL,
  `products_ordered` int(11) NOT NULL DEFAULT '0',
  `products_fsk18` int(1) NOT NULL DEFAULT '0',
  `products_vpe` int(11) NOT NULL,
  `products_vpe_status` int(1) NOT NULL DEFAULT '0',
  `products_vpe_value` decimal(15,4) NOT NULL,
  `products_startpage` int(1) NOT NULL DEFAULT '0',
  `products_startpage_sort` int(4) NOT NULL DEFAULT '0',
  `products_discountable` int(1) NOT NULL DEFAULT '1',
  `same_texts` tinyint(1) NOT NULL COMMENT 'Gibt an ob eine Sprache für alles verwendet wird',
  `formel_loeschmich` mediumtext NOT NULL,
  `formel_copy_loeschmich` mediumtext NOT NULL,
  `formula_main` mediumtext NOT NULL,
  `formula_deactivation` mediumtext NOT NULL,
  `formel_array` mediumtext NOT NULL,
  `formula_array` mediumtext NOT NULL,
  `formula_needs_processing` tinyint(1) NOT NULL,
  `export_to_erp` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Wird auf 1 gesetzt wenn der Produktimport vom ERP dieses Produkt neu importieren muss',
  `formula_processing_time` decimal(13,3) unsigned NOT NULL,
  `konstruktor` varchar(255) NOT NULL,
  `image_forced_attributes` varchar(255) NOT NULL,
  `f_cid` varchar(64) NOT NULL COMMENT 'Gewindetyp',
  `has_price_changes` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `special_price` tinyint(1) unsigned NOT NULL,
  `color_chart` varchar(255) NOT NULL,
  `filter_style` varchar(255) NOT NULL,
  `filter_for_which_material_thickness` varchar(255) NOT NULL,
  `filter_material` varchar(255) NOT NULL,
  `filter_thread_type` varchar(255) NOT NULL,
  `filter_material_thickness` varchar(255) NOT NULL,
  `filter_product_type` varchar(255) NOT NULL,
  `filter_has_diameter` varchar(255) NOT NULL,
  `filter_is_picture` tinyint(1) NOT NULL,
  `measurement_info` varchar(255) NOT NULL,
  PRIMARY KEY (`products_id`),
  UNIQUE KEY `products_model` (`products_model`),
  KEY `idx_products_date_added` (`products_date_added`),
  KEY `products_ordered` (`products_ordered`),
  KEY `products_sort` (`products_sort`),
  KEY `f_cid` (`f_cid`),
  KEY `products_status` (`products_status`),
  KEY `neue_produkte_oder_so` (`current_products_popularity`),
  KEY `meistgekaufte_oder_so` (`products_ordered`),
  KEY `deactivated_by_formula_parser` (`deactivated_by_formula_parser`),
  KEY `export_to_erp` (`export_to_erp`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products: 100 rows
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`products_id`, `current_products_popularity`, `products_ean`, `products_quantity`, `products_shippingtime`, `products_catalog_page`, `colorchart`, `product_info1`, `product_info2`, `products_model`, `group_permission_0`, `group_permission_1`, `group_permission_2`, `group_permission_3`, `products_sort`, `products_image`, `products_price`, `products_discount_allowed`, `products_date_added`, `products_last_modified`, `products_date_available`, `products_weight`, `products_status`, `deactivated_by_formula_parser`, `deactivated_by_formula_parser_modified`, `products_new`, `products_new_activated`, `out_of_stock`, `manually_deactivated`, `products_stock_item`, `products_tax_class_id`, `product_template`, `options_template`, `manufacturers_id`, `products_ordered`, `products_fsk18`, `products_vpe`, `products_vpe_status`, `products_vpe_value`, `products_startpage`, `products_startpage_sort`, `products_discountable`, `same_texts`, `formel_loeschmich`, `formel_copy_loeschmich`, `formula_main`, `formula_deactivation`, `formel_array`, `formula_array`, `formula_needs_processing`, `export_to_erp`, `formula_processing_time`, `konstruktor`, `image_forced_attributes`, `f_cid`, `has_price_changes`, `special_price`, `color_chart`, `filter_style`, `filter_for_which_material_thickness`, `filter_material`, `filter_thread_type`, `filter_material_thickness`, `filter_product_type`, `filter_has_diameter`, `filter_is_picture`, `measurement_info`) VALUES
	(1, 0, NULL, 0, 0, 0, NULL, '', '', '18J1', 0, 0, 0, 0, 0, '', 7.280000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[size][stonecolor]', '', 'A-BOLBS', 0, 0, '', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(2, 0, NULL, 0, 0, 0, NULL, '', '', '18J2', 0, 0, 0, 0, 0, '', 5.670000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[size][stonecolor]', '', 'A-BOLBS', 0, 0, '', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(3, 0, NULL, 0, 0, 0, NULL, '', '', '18J5', 0, 0, 0, 0, 0, '', 8.050000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[size][stonecolor]', '', 'A-BOLBS', 0, 0, '', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(4, 0, NULL, 0, 0, 0, NULL, '', '', '18JS-02', 0, 0, 0, 0, 0, '', 8.190000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[stonecolor:AQ;CR;PI]', '', '', '', 1, 1, 0.000, '[stonecolor]', '', 'A-BOLBS', 0, 0, 'S', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(5, 0, NULL, 0, 0, 0, NULL, '', '', '18JS-03', 0, 0, 0, 0, 0, '', 13.300000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[stonecolor:AQ;CR;PI]', '', '', '', 1, 1, 0.000, '[stonecolor]', '', 'A-BOLBS', 0, 0, 'S', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(6, 0, NULL, 0, 0, 0, NULL, '', '', '18JS-06', 0, 0, 0, 0, 0, '', 7.350000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '[stonecolor]', '', 'A-BOLBS', 0, 0, '', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(7, 0, NULL, 0, 0, 0, NULL, '', '', '18JS-07', 0, 0, 0, 0, 0, '', 7.420000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '[stonecolor]', '', 'A-BOLBS', 0, 0, '', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(8, 0, NULL, 0, 0, 0, NULL, '', '', '18JS-08', 0, 0, 0, 0, 0, '', 26.600000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '[stonecolor]', '', 'A-BOLBS', 0, 0, '', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(9, 0, NULL, 0, 0, 0, NULL, '', '', '18JS-09', 0, 0, 0, 0, 0, '', 12.530000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '[stonecolor]', '', 'A-BOLBS', 0, 0, '', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(10, 0, NULL, 0, 0, 0, NULL, '', '', '18NO1B20', 0, 0, 0, 0, 0, '', 9.100000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(11, 0, NULL, 0, 0, 0, NULL, '', '', '18NO1C20', 0, 0, 0, 0, 0, '', 13.300000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(12, 0, NULL, 0, 0, 0, NULL, '', '', '18NO1C22', 0, 0, 0, 0, 0, '', 10.500000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(13, 0, NULL, 0, 0, 0, NULL, '', '', '18NO1S20', 0, 0, 0, 0, 0, '', 13.300000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:15]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(14, 0, NULL, 0, 0, 0, NULL, '', '', '18NO1S22', 0, 0, 0, 0, 0, '', 10.500000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:15]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(15, 0, NULL, 0, 0, 0, NULL, '', '', '18NO2C20', 0, 0, 0, 0, 0, '', 12.600000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(16, 0, NULL, 0, 0, 0, NULL, '', '', '18NO2C22', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(17, 0, NULL, 0, 0, 0, NULL, '', '', '18NO2S20', 0, 0, 0, 0, 0, '', 12.600000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:15]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(18, 0, NULL, 0, 0, 0, NULL, '', '', '18NO2S22', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:15]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(19, 0, NULL, 0, 0, 0, NULL, '', '', '18NO3B20', 0, 0, 0, 0, 0, '', 8.750000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[attachment:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][attachment]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(20, 0, NULL, 0, 0, 0, NULL, '', '', '18NO3C20', 0, 0, 0, 0, 0, '', 12.600000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(21, 0, NULL, 0, 0, 0, NULL, '', '', '18NO3C22', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(22, 0, NULL, 0, 0, 0, NULL, '', '', '18NO3S20', 0, 0, 0, 0, 0, '', 12.600000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:15]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(23, 0, NULL, 0, 0, 0, NULL, '', '', '18NO3S22', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:15]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(24, 0, NULL, 0, 0, 0, NULL, '', '', '18NO4B20', 0, 0, 0, 0, 0, '', 8.750000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[attachment:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][attachment]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(25, 0, NULL, 0, 0, 0, NULL, '', '', '18NO4C20', 0, 0, 0, 0, 0, '', 12.600000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(26, 0, NULL, 0, 0, 0, NULL, '', '', '18NO4C22', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(27, 0, NULL, 0, 0, 0, NULL, '', '', '18NO4S20', 0, 0, 0, 0, 0, '', 12.600000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:15]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(28, 0, NULL, 0, 0, 0, NULL, '', '', '18NO4S22', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:15]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(29, 0, NULL, 0, 0, 0, NULL, '', '', '18NO5C22', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(30, 0, NULL, 0, 0, 0, NULL, '', '', '18NO5S22', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:15]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(31, 0, NULL, 0, 0, 0, NULL, '', '', '18NO6C22', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(32, 0, NULL, 0, 0, 0, NULL, '', '', '18NO6S22', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:15]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(33, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ1B18', 0, 0, 0, 0, 0, '', 9.450000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.0]\n+[length:6.5]\n+[ball:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(34, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ1B20', 0, 0, 0, 0, 0, '', 8.050000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(35, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ1B22', 0, 0, 0, 0, 0, '', 7.350000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(36, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ1C18', 0, 0, 0, 0, 0, '', 15.400000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.0]\n+[length:6.5]\n+[ball:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(37, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ1C20', 0, 0, 0, 0, 0, '', 12.950000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(38, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ1C22', 0, 0, 0, 0, 0, '', 9.450000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(39, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ1S18', 0, 0, 0, 0, 0, '', 15.400000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.0]\n+[length:15]\n+[ball:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(40, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ1S20', 0, 0, 0, 0, 0, '', 12.950000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:15]\n+[ball:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(41, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ1S22', 0, 0, 0, 0, 0, '', 9.450000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:15]\n+[ball:2]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(42, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ2B20', 0, 0, 0, 0, 0, '', 8.050000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(43, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ2B22', 0, 0, 0, 0, 0, '', 7.350000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(44, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ2C18', 0, 0, 0, 0, 0, '', 15.400000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.0]\n+[length:6.5]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(45, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ2C20', 0, 0, 0, 0, 0, '', 12.950000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(46, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ2C22', 0, 0, 0, 0, 0, '', 9.450000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(47, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ2S18', 0, 0, 0, 0, 0, '', 15.400000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.0]\n+[length:15]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(48, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ2S20', 0, 0, 0, 0, 0, '', 12.950000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:15]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(49, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ2S22', 0, 0, 0, 0, 0, '', 9.450000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:15]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(50, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ3B20', 0, 0, 0, 0, 0, '', 12.950000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.35]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(51, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ3B22', 0, 0, 0, 0, 0, '', 11.200000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:1.35]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(52, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ3C20', 0, 0, 0, 0, 0, '', 16.450000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.35]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(53, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ3C22', 0, 0, 0, 0, 0, '', 13.300000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[ball:1.35]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(54, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ3S20', 0, 0, 0, 0, 0, '', 16.450000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:15]\n+[ball:1.35]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(55, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ3S22', 0, 0, 0, 0, 0, '', 13.300000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:15]\n+[ball:1.35]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(56, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ4B18', 0, 0, 0, 0, 0, '', 9.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.0]\n+[length:6.5]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(57, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ4C18', 0, 0, 0, 0, 0, '', 15.750000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.0]\n+[length:6.5]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(58, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ4C20', 0, 0, 0, 0, 0, '', 12.950000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(59, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ4S18', 0, 0, 0, 0, 0, '', 15.750000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.0]\n+[length:15]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(60, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ4S20', 0, 0, 0, 0, 0, '', 12.950000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:15]\n+[ball:1.5]\n+[stonecolor:CR]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball][stonecolor]', '', '', 0, 0, '', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(61, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ5C22', 0, 0, 0, 0, 0, '', 9.100000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:6.5]\n+[stonecolor:AB;AQ;CR;PI;SA]', '', '', '', 1, 1, 0.000, '*[gauge][length][stonecolor]', '', '', 0, 0, 'S', 'Curved Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(62, 0, NULL, 0, 0, 0, NULL, '', '', '18NOJ5S22', 0, 0, 0, 0, 0, '', 9.100000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.6]\n+[length:15]\n+[stonecolor:AB;AQ;CR;PI;SA]', '', '', '', 1, 1, 0.000, '*[gauge][length][stonecolor]', '', '', 0, 0, 'S', 'Straight Nose Studs', '', 'Gold', '', '', '', '', 0, ''),
	(63, 0, NULL, 0, 0, 0, NULL, '', '', '18PS-01', 0, 0, 0, 0, 0, '', 13.650000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '', '', '', '', 1, 1, 0.000, '', '', 'A-BOLBS', 0, 0, '', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(64, 0, NULL, 0, 0, 0, NULL, '', '', '18PS-02', 0, 0, 0, 0, 0, '', 12.530000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '', '', '', '', 1, 1, 0.000, '', '', 'A-BOLBS', 0, 0, '', 'for Internal Bioflex Pins', 'Items without thread', 'Other Materials', '', '', '', '', 0, ''),
	(65, 0, NULL, 0, 0, 0, NULL, '', '', '24NOJ4B20', 0, 0, 0, 0, 0, '', 14.000000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:0.8]\n+[length:6.5]\n+[ball:1.5]', '', '', '', 1, 1, 0.000, '*[gauge][length][ball]', '', '', 0, 0, '', 'Nose Bones', '', 'Gold', '', '', '', '', 0, ''),
	(66, 0, NULL, 0, 0, 0, NULL, '', '', '4FB', 0, 0, 0, 0, 0, '', 0.840000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:4]', '', '', '', 1, 1, 0.000, '[size]', '', 'A-S16', 0, 0, '', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(67, 0, NULL, 0, 0, 0, NULL, '', '', '4FC', 0, 0, 0, 0, 0, '', 1.120000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:4]', '', '', '', 1, 1, 0.000, '[size]', '', 'A-S16', 0, 0, '', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(68, 0, NULL, 0, 0, 0, NULL, '', '', '5FA', 0, 0, 0, 0, 0, '', 0.700000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:5]', '', '', '', 1, 1, 0.000, '[size]', '', 'A-S16', 0, 0, '', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(69, 0, NULL, 0, 0, 0, NULL, '', '', '5FB', 0, 0, 0, 0, 0, '', 0.840000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:5]', '', '', '', 1, 1, 0.000, '[size]', '', 'A-S16', 0, 0, '', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(70, 0, NULL, 0, 0, 0, NULL, '', '', '5FC', 0, 0, 0, 0, 0, '', 1.120000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:5]', '', '', '', 1, 1, 0.000, '[size]', '', 'A-S16', 0, 0, '', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(71, 0, NULL, 0, 0, 0, NULL, '', '', '6FA', 0, 0, 0, 0, 0, '', 0.700000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:6]', '', '', '', 1, 1, 0.000, '[size]', '', 'A-S16', 0, 0, '', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(72, 0, NULL, 0, 0, 0, NULL, '', '', '6FB', 0, 0, 0, 0, 0, '', 0.840000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:6]', '', '', '', 1, 1, 0.000, '[size]', '', 'A-S16', 0, 0, '', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(73, 0, NULL, 0, 0, 0, NULL, '', '', 'AB', 0, 0, 0, 0, 0, '', 0.770000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[diameter:4;5;6;8]\n+[color:BL;CO;GR;PU;RN;YE]', '', '', '', 1, 1, 0.000, '[diameter][color]', 'diameter:8', 'A-S16', 0, 0, 'T', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(74, 0, NULL, 0, 0, 0, NULL, '', '', 'ABBC', 0, 0, 0, 0, 0, '', 0.770000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[diameter:3;4;5;6;8]\n+[color:BL;CO;GR;PU;RN]', '', '', '', 1, 1, 0.000, '[diameter][color]', 'diameter:8', 'A-BC', 0, 0, 'T', 'for Ball Closure Rings', 'Items without thread', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(75, 0, NULL, 0, 0, 0, NULL, '', '', 'ABC', 0, 0, 0, 0, 0, '', 1.540000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.2;1.6;2.5|diameter:8;10;12|ball:4;5;6]\n-[gauge:1.2|diameter:12]\n-[gauge:1.2|ball:5;6]\n-[gauge:1.6|diameter:12]\n-[gauge:1.6|ball:5;6]\n-[gauge:2.5|diameter:8;10]\n-[gauge:2.5|ball:4;5]\n=[gauge:2.5](Preis:+1,40)\n+[color:BL;CO;GR;PU;RN;YE]', '', '', '', 1, 1, 0.000, '[gauge][diameter][ball][color]', 'gauge:12&diameter:8&ball:4', '', 0, 1, 'T', 'Ball Closure Rings', '', 'Surgical Steel 316L', '', '', '', '', 0, '01_ball_closure_ring.jpg'),
	(76, 0, NULL, 0, 0, 0, NULL, '', '', 'ABCS', 0, 0, 0, 0, 0, '', 5.250000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.2;1.6;2.0;2.5;3.2;4;5|diameter:8;10;12;14]\n-[gauge:1.2|diameter:14]\n-[gauge:1.6|diameter:14]\n-[gauge:2.0|diameter:8;14]\n-[gauge:2.5|diameter:8;14]\n-[gauge:3.2|diameter:8;10]\n-[gauge:4|diameter:8;10]\n-[gauge:5|diameter:8;10]\n=[gauge:2.0](Preis:+1,05)\n=[gauge:2.5](Preis:+2,10)\n=[gauge:3.2](Preis:+3,15)\n=[gauge:4](Preis:+5,25)\n=[gauge:5](Preis:+7,35)\n+[color:BL;CO;GR;PU;RN]', '', '', '', 1, 1, 0.000, '[gauge][diameter][color]', 'gauge:16&diameter:10', '', 0, 1, '', 'Segment Rings', '', 'Surgical Steel 316L', '', '', '', '', 0, '02_normal_ring.jpg'),
	(77, 0, NULL, 0, 0, 0, NULL, '', '', 'ABNAD', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.6]\n+[length:6;8;10;11.2;12;14]\n+[ball:5/8]\n+[pincolor:BL;CO;GR;PU;RN;YE]\n+[color:BL;CO;GR;PU;RN;YE]', '', '', '', 1, 1, 0.000, '[gauge][length][ball][pincolor][color]', 'length:10', '', 0, 0, 'T', 'Fashion Bananas', '', 'Surgical Steel 316L', '', '1.6mm', '', '', 0, '06_banana.jpg'),
	(78, 0, NULL, 0, 0, 0, NULL, '', '', 'ABNASC', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.6]\n+[length:6;8;10;11.2;12;14]\n+[ball:5/8]\n+[pincolor:BL;CO;GR;PU;RN;YE]\n+[color:BL;CO;GR;PU;RN;YE]', '', '', '', 1, 1, 0.000, '[gauge][length][ball][pincolor][color]', 'length:10', '', 0, 0, 'T', 'Fashion Bananas', '', 'Surgical Steel 316L', '', '1.6mm', '', '', 0, '06_banana.jpg'),
	(79, 0, NULL, 0, 0, 0, NULL, '', '', 'ABNDJ', 0, 0, 0, 0, 0, '', 3.500000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.6]\n+[length:10]\n+[ball:5/8]\n+[color:BL/CR;CO/CR;GR/CR;PU/CR;RN/CR;YE/CR]', '', '', '', 1, 1, 0.000, '[gauge][length][ball][color]', '', '', 0, 1, 'TJ', 'Basic Bananas', '', 'Surgical Steel 316L', '', '1.6mm', '', '', 0, '06_banana.jpg'),
	(80, 0, NULL, 0, 0, 0, NULL, '', '', 'ABNJ', 0, 0, 0, 0, 0, '', 2.940000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.6]\n+[length:10]\n+[ball:5/8]\n+[color:BL/CR;CO/CR;GR/CR;PU/CR;RN/CR;YE/CR]', '', '', '', 1, 1, 0.000, '[gauge][length][ball][color]', '', '', 0, 0, 'TJ', 'Basic Bananas', '', 'Surgical Steel 316L', '', '1.6mm', '', '', 0, '06_banana.jpg'),
	(81, 0, NULL, 0, 0, 0, NULL, '', '', 'ABOJD', 0, 0, 0, 0, 0, '', 1.120000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[diameter:2.35]\n+[color:BL/CR;CO/CR;GR/CR;PU/CR;RN/CR;YE/CR]', '', '', '', 1, 1, 0.000, '[diameter][color]', '', 'A-BOLBS', 0, 0, 'TJ', 'for Internal Bioflex Pins', 'Items without thread', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(82, 0, NULL, 0, 0, 0, NULL, '', '', 'AB-PACK', 0, 0, 0, 0, 0, '', 30.800000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 0, 0, '', '', '+[pack_size:50;100;250;500|diameter:MIX;4;5;6]\n=[pack_size:100](Preis:+23,10)\n=[pack_size:250](Preis:+84,70)\n=[pack_size:500](Preis:+161,70)', '', '', '', 1, 1, 0.000, '*[pack_size][diameter]', '', '', 0, 0, '', '', '', 'Surgical Steel 316L', '', '', 'Balls & Attachments', '', 0, ''),
	(83, 0, NULL, 0, 0, 0, NULL, '', '', 'ABT', 0, 0, 0, 0, 0, '', 1.540000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:6]\n+[color:BL;GR;PU;RN]', '', '', '', 1, 1, 0.000, '[size][color]', '', 'A-S16', 0, 1, 'T', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(84, 0, NULL, 0, 0, 0, NULL, '', '', 'ABUL', 0, 0, 0, 0, 0, '', 1.190000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:6]\n+[color:BL;GR;PU;RN]', '', '', '', 1, 1, 0.000, '[size][color]', '', 'A-S16', 0, 1, 'T', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(85, 0, NULL, 0, 0, 0, NULL, '', '', 'AC', 0, 0, 0, 0, 0, '', 0.980000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[size:4;5;6]\n+[color:BL;CO;GR;PU;RN;YE]', '', '', '', 1, 1, 0.000, '[size][color]', 'size:5', 'A-S16', 0, 0, 'T', 'for Normal Threaded Pins', 'for 1.6mm', 'Surgical Steel 316L', '', '', '', '', 0, ''),
	(86, 0, NULL, 0, 0, 0, NULL, '', '', 'ACB', 0, 0, 0, 0, 0, '', 2.450000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.6]\n+[diameter:10;12]\n+[ball:4;5;6]\n+[color:BL;CO;GR;PU;RN;YE]', '', '', '', 1, 1, 0.000, '[gauge][diameter][ball][color]', 'diameter:10&ball:4', '', 0, 0, 'T', '', '', 'Surgical Steel 316L', '', '1.6mm', '', '', 0, '08_circular_barbell.jpg'),
	(87, 0, NULL, 0, 0, 0, NULL, '', '', 'ACBC', 0, 0, 0, 0, 0, '', 2.870000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.6]\n+[diameter:10;12]\n+[attachment:4;5]\n+[color:BL;CO;GR;PU;RN;YE]', '', '', '', 1, 1, 0.000, '[gauge][diameter][attachment][color]', 'diameter:10&attachment:4', '', 0, 0, 'T', '', '', 'Surgical Steel 316L', '', '1.6mm', '', '', 0, '08_circular_barbell.jpg'),
	(88, 0, NULL, 0, 0, 0, NULL, '', '', 'ACBLC', 0, 0, 0, 0, 0, '', 2.870000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.6]\n+[diameter:10;12]\n+[attachment:3x7.5]\n+[color:BL;CO;GR;PU;RN;YE]', '', '', '', 1, 1, 0.000, '[gauge][diameter][attachment][color]', 'diameter:10', '', 0, 0, 'T', '', '', 'Surgical Steel 316L', '', '1.6mm', '', '', 0, '08_circular_barbell.jpg'),
	(89, 0, NULL, 0, 0, 0, NULL, '', '', 'ACB-PIN', 0, 0, 0, 0, 0, '', 0.910000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:1.6]\n+[diameter:10;12]\n+[color:BL;CO;GR;PU;RN;YE]', '', '', '', 1, 1, 0.000, '[gauge][diameter][color]', 'diameter:10', 'P-S16', 0, 0, 'T', '', '', 'Surgical Steel 316L', '', '1.6mm', 'Circular Barbell', '', 0, '08_circular_barbell.jpg'),
	(90, 0, NULL, 0, 0, 0, NULL, '', '', 'ACB-PIN-PACK', 0, 0, 0, 0, 0, '', 36.400000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 0, 0, '', '', '+[pack_size:50;100|diameter:MIX]\n=[pack_size:100](Preis:+27,30)', '', '', '', 1, 1, 0.000, '*[pack_size][diameter]', '', '', 0, 0, '', '', '', 'Surgical Steel 316L', '', '', 'Loose Pins', '', 0, ''),
	(91, 0, NULL, 0, 0, 0, NULL, '', '', 'ACFLP11', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:5;6;8;10;12;14;16;18;20]\n=[gauge:10](Preis:+0.14)\n=[gauge:12](Preis:+0.49)\n=[gauge:14](Preis:+0.91)\n=[gauge:16](Preis:+1.82)\n=[gauge:18](Preis:+1.96)\n=[gauge:20](Preis:+2.10)', '', '', '', 1, 1, 0.000, '*[gauge]', '', '', 0, 0, '', '', '', 'UV Acryl', '', '', 'Plugs', '', 0, '11_plug_tube.jpg'),
	(92, 0, NULL, 0, 0, 0, NULL, '', '', 'ACFLP12', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:5;6;8;10;12;14;16;18;20]\n=[gauge:10](Preis:+0.14)\n=[gauge:12](Preis:+0.49)\n=[gauge:14](Preis:+0.91)\n=[gauge:16](Preis:+1.82)\n=[gauge:18](Preis:+1.96)\n=[gauge:20](Preis:+2.10)', '', '', '', 1, 1, 0.000, '*[gauge]', '', '', 0, 0, '', '', '', 'UV Acryl', '', '', 'Plugs', '', 0, '11_plug_tube.jpg'),
	(93, 0, NULL, 0, 0, 0, NULL, '', '', 'ACFLP13', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:5;6;8;10;12;14;16;18;20]\n=[gauge:10](Preis:+0.14)\n=[gauge:12](Preis:+0.49)\n=[gauge:14](Preis:+0.91)\n=[gauge:16](Preis:+1.82)\n=[gauge:18](Preis:+1.96)\n=[gauge:20](Preis:+2.10)', '', '', '', 1, 1, 0.000, '*[gauge]', '', '', 0, 0, '', '', '', 'UV Acryl', '', '', 'Plugs', '', 0, '11_plug_tube.jpg'),
	(94, 0, NULL, 0, 0, 0, NULL, '', '', 'ACFLP14', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:5;6;8;10;12;14;16;18;20]\n=[gauge:10](Preis:+0.14)\n=[gauge:12](Preis:+0.49)\n=[gauge:14](Preis:+0.91)\n=[gauge:16](Preis:+1.82)\n=[gauge:18](Preis:+1.96)\n=[gauge:20](Preis:+2.10)', '', '', '', 1, 1, 0.000, '*[gauge]', '', '', 0, 0, '', '', '', 'UV Acryl', '', '', 'Plugs', '', 0, '11_plug_tube.jpg'),
	(95, 0, NULL, 0, 0, 0, NULL, '', '', 'ACFLP15', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:5;6;8;10;12;14;16;18;20]\n=[gauge:10](Preis:+0.14)\n=[gauge:12](Preis:+0.49)\n=[gauge:14](Preis:+0.91)\n=[gauge:16](Preis:+1.82)\n=[gauge:18](Preis:+1.96)\n=[gauge:20](Preis:+2.10)', '', '', '', 1, 1, 0.000, '*[gauge]', '', '', 0, 0, '', '', '', 'UV Acryl', '', '', 'Plugs', '', 0, '11_plug_tube.jpg'),
	(96, 0, NULL, 0, 0, 0, NULL, '', '', 'ACFLP16', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:5;6;8;10;12;14;16;18;20]\n=[gauge:10](Preis:+0.14)\n=[gauge:12](Preis:+0.49)\n=[gauge:14](Preis:+0.91)\n=[gauge:16](Preis:+1.82)\n=[gauge:18](Preis:+1.96)\n=[gauge:20](Preis:+2.10)', '', '', '', 1, 1, 0.000, '*[gauge]', '', '', 0, 0, '', '', '', 'UV Acryl', '', '', 'Plugs', '', 0, '11_plug_tube.jpg'),
	(97, 0, NULL, 0, 0, 0, NULL, '', '', 'ACFLP17', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:5;6;8;10;12;14;16;18;20;22;24;26;28;30;32;34;36;38;40;42;44;46;48;50]\n=[gauge:10](Preis:+0.14)\n=[gauge:12](Preis:+0.49)\n=[gauge:14](Preis:+0.91)\n=[gauge:16](Preis:+1.82)\n=[gauge:18](Preis:+1.96)\n=[gauge:20](Preis:+2.10)\n=[gauge:22](Preis:+4.48)\n=[gauge:24](Preis:+5.04)\n=[gauge:26](Preis:+5.32)\n=[gauge:28](Preis:+5.53)\n=[gauge:30](Preis:+5.74)\n=[gauge:32](Preis:+6.02)\n=[gauge:34](Preis:+6.23)\n=[gauge:36](Preis:+6.72)\n=[gauge:38](Preis:+7.14)\n=[gauge:40](Preis:+7.63)\n=[gauge:42](Preis:+8.12)\n=[gauge:44](Preis:+8.82)\n=[gauge:46](Preis:+9.52)\n=[gauge:48](Preis:+10.43)\n=[gauge:50](Preis:+11.34)', '', '', '', 1, 1, 0.000, '*[gauge]', '', '', 0, 0, '', '', '', 'UV Acryl', '', '', 'Plugs', '', 0, '11_plug_tube.jpg'),
	(98, 0, NULL, 0, 0, 0, NULL, '', '', 'ACFLP23', 0, 0, 0, 0, 0, '', 6.440000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:8;10;12;14;16;18;20]\n=[gauge:12](Preis:+0.63)\n=[gauge:14](Preis:+1.33)\n=[gauge:16](Preis:+2.10)\n=[gauge:18](Preis:+2.80)\n=[gauge:20](Preis:+3.36)', '', '', '', 1, 1, 0.000, '*[gauge]', '', '', 0, 0, '', '', '', 'UV Acryl', '', '', 'Plugs', '', 0, '11_plug_tube.jpg'),
	(99, 0, NULL, 0, 0, 0, NULL, '', '', 'ACFLP6', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:5;6;8;10;12;14;16;18;20|color:BL;RE;WH]\n=[gauge:10](Preis:+0.14)\n=[gauge:12](Preis:+0.49)\n=[gauge:14](Preis:+0.91)\n=[gauge:16](Preis:+1.82)\n=[gauge:18](Preis:+1.96)\n=[gauge:20](Preis:+2.10)', '', '', '', 1, 1, 0.000, '*[gauge][color]', '', '', 0, 0, '', '', '', 'UV Acryl', '', '', 'Plugs', '', 0, '11_plug_tube.jpg'),
	(100, 0, NULL, 0, 0, 0, NULL, '', '', 'ACFLP9', 0, 0, 0, 0, 0, '', 4.060000, 0.00, '0000-00-00 00:00:00', NULL, NULL, 0.00, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1, 0, 1, '', '', 0, 0, 0, 0, 0, 0.0000, 0, 0, 1, 0, '', '', '+[gauge:5;6;8;10;12;14;16;18;20|color:OR;PI;PU]\n=[gauge:10](Preis:+0.14)\n=[gauge:12](Preis:+0.49)\n=[gauge:14](Preis:+0.91)\n=[gauge:16](Preis:+1.82)\n=[gauge:18](Preis:+1.96)\n=[gauge:20](Preis:+2.10)', '', '', '', 1, 1, 0.000, '*[gauge][color]', '', '', 0, 0, '', '', '', 'UV Acryl', '', '', 'Plugs', '', 0, '11_plug_tube.jpg');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_attributes
DROP TABLE IF EXISTS `products_attributes`;
CREATE TABLE IF NOT EXISTS `products_attributes` (
  `products_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `attributes_name` varchar(50) NOT NULL,
  `attributes_value` varchar(50) NOT NULL,
  PRIMARY KEY (`products_attributes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_attributes: 0 rows
DELETE FROM `products_attributes`;
/*!40000 ALTER TABLE `products_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_attributes` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_attributes_download
DROP TABLE IF EXISTS `products_attributes_download`;
CREATE TABLE IF NOT EXISTS `products_attributes_download` (
  `products_attributes_id` int(11) NOT NULL,
  `products_attributes_filename` varchar(255) NOT NULL DEFAULT '',
  `products_attributes_maxdays` int(2) DEFAULT '0',
  `products_attributes_maxcount` int(2) DEFAULT '0',
  PRIMARY KEY (`products_attributes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_attributes_download: 0 rows
DELETE FROM `products_attributes_download`;
/*!40000 ALTER TABLE `products_attributes_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_attributes_download` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_bak
DROP TABLE IF EXISTS `products_bak`;
CREATE TABLE IF NOT EXISTS `products_bak` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `current_products_popularity` int(10) unsigned NOT NULL COMMENT 'Produkte des Monats sortieren.',
  `products_ean` varchar(128) DEFAULT NULL,
  `products_quantity` int(4) NOT NULL,
  `products_shippingtime` int(4) NOT NULL,
  `products_catalog_page` int(11) unsigned DEFAULT NULL COMMENT 'customfield',
  `colorchart` varchar(128) DEFAULT NULL COMMENT 'customfield',
  `product_info1` varchar(128) DEFAULT NULL COMMENT 'customfield',
  `product_info2` varchar(128) DEFAULT NULL COMMENT 'customfield',
  `products_model` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `group_permission_0` tinyint(1) NOT NULL,
  `group_permission_1` tinyint(1) NOT NULL,
  `group_permission_2` tinyint(1) NOT NULL,
  `group_permission_3` tinyint(1) NOT NULL,
  `products_sort` int(11) NOT NULL DEFAULT '0',
  `products_image` varchar(64) DEFAULT NULL,
  `products_price` decimal(10,6) NOT NULL,
  `products_discount_allowed` decimal(3,2) NOT NULL DEFAULT '0.00',
  `products_date_added` datetime NOT NULL,
  `products_last_modified` datetime DEFAULT NULL,
  `products_date_available` datetime DEFAULT NULL,
  `products_weight` decimal(5,2) NOT NULL,
  `products_status` tinyint(1) NOT NULL,
  `deactivated_by_formula_parser` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deactivated_by_formula_parser_modified` datetime NOT NULL,
  `products_new` tinyint(3) unsigned NOT NULL COMMENT 'Produktflag fuer neue Produkte',
  `products_stock_item` tinyint(1) unsigned NOT NULL COMMENT 'ist Bestadsartikel',
  `products_tax_class_id` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `product_template` varchar(64) DEFAULT NULL,
  `options_template` varchar(64) DEFAULT NULL,
  `manufacturers_id` int(11) DEFAULT NULL,
  `products_ordered` int(11) NOT NULL DEFAULT '0',
  `products_fsk18` int(1) NOT NULL DEFAULT '0',
  `products_vpe` int(11) NOT NULL,
  `products_vpe_status` int(1) NOT NULL DEFAULT '0',
  `products_vpe_value` decimal(15,4) NOT NULL,
  `products_startpage` int(1) NOT NULL DEFAULT '0',
  `products_startpage_sort` int(4) NOT NULL DEFAULT '0',
  `products_discountable` int(1) NOT NULL DEFAULT '1',
  `same_texts` tinyint(1) NOT NULL COMMENT 'Gibt an ob eine Sprache für alles verwendet wird',
  `formel_loeschmich` mediumtext NOT NULL,
  `formel_copy_loeschmich` mediumtext NOT NULL,
  `formula_main` mediumtext NOT NULL,
  `formula_deactivation` mediumtext NOT NULL,
  `formel_array` mediumtext NOT NULL,
  `formula_array` mediumtext NOT NULL,
  `formula_needs_processing` tinyint(1) NOT NULL,
  `export_to_erp` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Wird auf 1 gesetzt wenn der Produktimport vom ERP dieses Produkt neu importieren muss',
  `formula_processing_time` decimal(13,3) unsigned NOT NULL,
  `konstruktor` varchar(255) NOT NULL,
  `image_forced_attributes` varchar(255) NOT NULL,
  `f_cid` varchar(64) NOT NULL COMMENT 'Gewindetyp',
  `has_price_changes` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `special_price` tinyint(1) unsigned NOT NULL,
  `color_chart` varchar(255) NOT NULL,
  `filter_style` varchar(255) NOT NULL,
  `filter_for_which_material_thickness` varchar(255) NOT NULL,
  `filter_material` varchar(255) NOT NULL,
  `filter_thread_type` varchar(255) NOT NULL,
  `filter_material_thickness` varchar(255) NOT NULL,
  `filter_product_type` varchar(255) NOT NULL,
  `filter_has_diameter` varchar(255) NOT NULL,
  `filter_is_picture` tinyint(1) NOT NULL,
  `measurement_info` varchar(255) NOT NULL,
  PRIMARY KEY (`products_id`),
  UNIQUE KEY `products_model` (`products_model`),
  KEY `idx_products_date_added` (`products_date_added`),
  KEY `products_ordered` (`products_ordered`),
  KEY `products_sort` (`products_sort`),
  KEY `f_cid` (`f_cid`),
  KEY `products_status` (`products_status`),
  KEY `neue_produkte_oder_so` (`current_products_popularity`),
  KEY `meistgekaufte_oder_so` (`products_ordered`),
  KEY `deactivated_by_formula_parser` (`deactivated_by_formula_parser`),
  KEY `export_to_erp` (`export_to_erp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_bak: 0 rows
DELETE FROM `products_bak`;
/*!40000 ALTER TABLE `products_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_bak` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_content
DROP TABLE IF EXISTS `products_content`;
CREATE TABLE IF NOT EXISTS `products_content` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `group_ids` text,
  `content_name` varchar(32) NOT NULL DEFAULT '',
  `content_file` varchar(64) NOT NULL,
  `content_link` text NOT NULL,
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `content_read` int(11) NOT NULL DEFAULT '0',
  `file_comment` text NOT NULL,
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_content: 0 rows
DELETE FROM `products_content`;
/*!40000 ALTER TABLE `products_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_content` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_description
DROP TABLE IF EXISTS `products_description`;
CREATE TABLE IF NOT EXISTS `products_description` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `products_description` text CHARACTER SET utf8,
  `products_short_description` text CHARACTER SET utf8,
  `products_keywords` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `products_meta_title` text CHARACTER SET utf8 NOT NULL,
  `products_meta_description` text CHARACTER SET utf8 NOT NULL,
  `products_meta_keywords` text CHARACTER SET utf8 NOT NULL,
  `products_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `products_viewed` int(5) DEFAULT '0',
  PRIMARY KEY (`products_id`,`language_id`),
  KEY `products_name` (`products_name`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_description: 0 rows
DELETE FROM `products_description`;
/*!40000 ALTER TABLE `products_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_description` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_graduated_prices
DROP TABLE IF EXISTS `products_graduated_prices`;
CREATE TABLE IF NOT EXISTS `products_graduated_prices` (
  `products_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `unitprice` decimal(15,4) NOT NULL DEFAULT '0.0000',
  KEY `products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_graduated_prices: 0 rows
DELETE FROM `products_graduated_prices`;
/*!40000 ALTER TABLE `products_graduated_prices` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_graduated_prices` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_images
DROP TABLE IF EXISTS `products_images`;
CREATE TABLE IF NOT EXISTS `products_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `image_nr` smallint(6) NOT NULL,
  `image_name` varchar(254) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_images: 0 rows
DELETE FROM `products_images`;
/*!40000 ALTER TABLE `products_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_images` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_notifications
DROP TABLE IF EXISTS `products_notifications`;
CREATE TABLE IF NOT EXISTS `products_notifications` (
  `products_id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`products_id`,`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_notifications: 0 rows
DELETE FROM `products_notifications`;
/*!40000 ALTER TABLE `products_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_notifications` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_options
DROP TABLE IF EXISTS `products_options`;
CREATE TABLE IF NOT EXISTS `products_options` (
  `products_options_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_options_name` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`products_options_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_options: 0 rows
DELETE FROM `products_options`;
/*!40000 ALTER TABLE `products_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_options` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_options_values
DROP TABLE IF EXISTS `products_options_values`;
CREATE TABLE IF NOT EXISTS `products_options_values` (
  `products_options_values_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_options_values_name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`products_options_values_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_options_values: 0 rows
DELETE FROM `products_options_values`;
/*!40000 ALTER TABLE `products_options_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_options_values` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_options_values_to_products_options
DROP TABLE IF EXISTS `products_options_values_to_products_options`;
CREATE TABLE IF NOT EXISTS `products_options_values_to_products_options` (
  `products_options_values_to_products_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_options_id` int(11) NOT NULL,
  `products_options_values_id` int(11) NOT NULL,
  PRIMARY KEY (`products_options_values_to_products_options_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_options_values_to_products_options: 0 rows
DELETE FROM `products_options_values_to_products_options`;
/*!40000 ALTER TABLE `products_options_values_to_products_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_options_values_to_products_options` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_restored
DROP TABLE IF EXISTS `products_restored`;
CREATE TABLE IF NOT EXISTS `products_restored` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `current_products_popularity` int(10) unsigned NOT NULL COMMENT 'Produkte des Monats sortieren.',
  `products_ean` varchar(128) DEFAULT NULL,
  `products_quantity` int(4) NOT NULL,
  `products_shippingtime` int(4) NOT NULL,
  `products_catalog_page` int(11) unsigned DEFAULT NULL COMMENT 'customfield',
  `colorchart` varchar(128) DEFAULT NULL COMMENT 'customfield',
  `product_info1` varchar(128) DEFAULT NULL COMMENT 'customfield',
  `product_info2` varchar(128) DEFAULT NULL COMMENT 'customfield',
  `products_model` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `group_permission_0` tinyint(1) NOT NULL,
  `group_permission_1` tinyint(1) NOT NULL,
  `group_permission_2` tinyint(1) NOT NULL,
  `group_permission_3` tinyint(1) NOT NULL,
  `products_sort` int(11) NOT NULL DEFAULT '0',
  `products_image` varchar(64) DEFAULT NULL,
  `products_price` decimal(10,6) NOT NULL,
  `products_discount_allowed` decimal(3,2) NOT NULL DEFAULT '0.00',
  `products_date_added` datetime NOT NULL,
  `products_last_modified` datetime DEFAULT NULL,
  `products_date_available` datetime DEFAULT NULL,
  `products_weight` decimal(5,2) NOT NULL,
  `products_status` tinyint(1) NOT NULL,
  `products_new` tinyint(3) unsigned NOT NULL COMMENT 'Produktflag fuer neue Produkte',
  `products_tax_class_id` tinyint(11) unsigned NOT NULL,
  `product_template` varchar(64) DEFAULT NULL,
  `options_template` varchar(64) DEFAULT NULL,
  `manufacturers_id` int(11) DEFAULT NULL,
  `products_ordered` int(11) NOT NULL DEFAULT '0',
  `products_fsk18` int(1) NOT NULL DEFAULT '0',
  `products_vpe` int(11) NOT NULL,
  `products_vpe_status` int(1) NOT NULL DEFAULT '0',
  `products_vpe_value` decimal(15,4) NOT NULL,
  `products_startpage` int(1) NOT NULL DEFAULT '0',
  `products_startpage_sort` int(4) NOT NULL DEFAULT '0',
  `products_discountable` int(1) NOT NULL DEFAULT '1',
  `same_texts` tinyint(1) NOT NULL COMMENT 'Gibt an ob eine Sprache für alles verwendet wird',
  `formel` mediumtext NOT NULL,
  `formel_copy` mediumtext NOT NULL COMMENT 'Wenn temporäre Aenderungen gemacht wurden, ist dieses Feld gefuellt',
  `formel_array` mediumtext NOT NULL,
  `konstruktor` varchar(255) NOT NULL,
  `image_forced_attributes` varchar(255) NOT NULL,
  `f_cid` varchar(64) NOT NULL COMMENT 'Gewindetyp',
  `has_price_changes` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `special_price` tinyint(1) unsigned NOT NULL,
  `color_chart` varchar(255) NOT NULL,
  `filter_style` varchar(255) NOT NULL,
  `filter_for_which_material_thickness` varchar(255) NOT NULL,
  `filter_material` varchar(255) NOT NULL,
  `filter_thread_type` varchar(255) NOT NULL,
  `filter_material_thickness` varchar(255) NOT NULL,
  `filter_product_type` varchar(255) NOT NULL,
  `measurement_info` varchar(255) NOT NULL,
  PRIMARY KEY (`products_id`),
  KEY `idx_products_date_added` (`products_date_added`),
  KEY `products_ordered` (`products_ordered`),
  KEY `products_model` (`products_model`),
  KEY `Für deaktivierte Varianten` (`products_id`,`products_status`),
  KEY `products_sort` (`products_sort`),
  KEY `f_cid` (`f_cid`),
  KEY `products_status` (`products_status`),
  KEY `neue_produkte_oder_so` (`products_status`,`current_products_popularity`),
  KEY `meistgekaufte_oder_so` (`products_status`,`products_ordered`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_restored: 0 rows
DELETE FROM `products_restored`;
/*!40000 ALTER TABLE `products_restored` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_restored` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_stock
DROP TABLE IF EXISTS `products_stock`;
CREATE TABLE IF NOT EXISTS `products_stock` (
  `products_stock_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `products_id` int(10) unsigned NOT NULL,
  `stock` int(10) NOT NULL,
  PRIMARY KEY (`products_stock_id`),
  UNIQUE KEY `products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Artikelbestaende';

-- Dumping data for table crazy_import.products_stock: 0 rows
DELETE FROM `products_stock`;
/*!40000 ALTER TABLE `products_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_stock` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_stock_raw
DROP TABLE IF EXISTS `products_stock_raw`;
CREATE TABLE IF NOT EXISTS `products_stock_raw` (
  `products_stock_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `products_id` int(10) unsigned NOT NULL,
  `stock` int(10) NOT NULL,
  PRIMARY KEY (`products_stock_id`),
  UNIQUE KEY `products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Artikelbestaende';

-- Dumping data for table crazy_import.products_stock_raw: 0 rows
DELETE FROM `products_stock_raw`;
/*!40000 ALTER TABLE `products_stock_raw` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_stock_raw` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_to_categories
DROP TABLE IF EXISTS `products_to_categories`;
CREATE TABLE IF NOT EXISTS `products_to_categories` (
  `products_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  PRIMARY KEY (`products_id`,`categories_id`),
  KEY `nach_kategorie` (`categories_id`,`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_to_categories: 100 rows
DELETE FROM `products_to_categories`;
/*!40000 ALTER TABLE `products_to_categories` DISABLE KEYS */;
INSERT INTO `products_to_categories` (`products_id`, `categories_id`) VALUES
	(1, 196),
	(2, 196),
	(3, 196),
	(4, 196),
	(5, 196),
	(6, 196),
	(7, 196),
	(8, 196),
	(9, 196),
	(10, 441),
	(11, 441),
	(12, 441),
	(13, 441),
	(14, 441),
	(15, 441),
	(16, 441),
	(17, 441),
	(18, 441),
	(19, 441),
	(20, 441),
	(21, 441),
	(22, 441),
	(23, 441),
	(24, 441),
	(25, 441),
	(26, 441),
	(27, 441),
	(28, 441),
	(29, 441),
	(30, 441),
	(31, 441),
	(32, 441),
	(33, 441),
	(34, 441),
	(35, 441),
	(36, 441),
	(37, 441),
	(38, 441),
	(39, 441),
	(40, 441),
	(41, 441),
	(42, 441),
	(43, 441),
	(44, 441),
	(45, 441),
	(46, 441),
	(47, 441),
	(48, 441),
	(49, 441),
	(50, 441),
	(51, 441),
	(52, 441),
	(53, 441),
	(54, 441),
	(55, 441),
	(56, 441),
	(57, 441),
	(58, 441),
	(59, 441),
	(60, 441),
	(61, 441),
	(62, 441),
	(63, 196),
	(64, 196),
	(65, 441),
	(66, 196),
	(67, 196),
	(68, 196),
	(69, 196),
	(70, 196),
	(71, 196),
	(72, 196),
	(73, 196),
	(74, 196),
	(75, 316),
	(76, 316),
	(77, 350),
	(78, 350),
	(79, 350),
	(80, 350),
	(81, 196),
	(82, 450),
	(83, 196),
	(84, 196),
	(85, 196),
	(86, 415),
	(87, 415),
	(88, 415),
	(89, 257),
	(90, 450),
	(91, 439),
	(92, 439),
	(93, 439),
	(94, 439),
	(95, 439),
	(96, 439),
	(97, 439),
	(98, 439),
	(99, 439),
	(100, 439);
/*!40000 ALTER TABLE `products_to_categories` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_vpe
DROP TABLE IF EXISTS `products_vpe`;
CREATE TABLE IF NOT EXISTS `products_vpe` (
  `products_vpe_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `products_vpe_name` varchar(32) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_vpe: 0 rows
DELETE FROM `products_vpe`;
/*!40000 ALTER TABLE `products_vpe` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_vpe` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_xsell
DROP TABLE IF EXISTS `products_xsell`;
CREATE TABLE IF NOT EXISTS `products_xsell` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `products_id` int(10) unsigned NOT NULL DEFAULT '1',
  `products_xsell_grp_name_id` int(10) unsigned NOT NULL DEFAULT '1',
  `xsell_id` int(10) unsigned NOT NULL DEFAULT '1',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_xsell: 0 rows
DELETE FROM `products_xsell`;
/*!40000 ALTER TABLE `products_xsell` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_xsell` ENABLE KEYS */;


-- Dumping structure for table crazy_import.products_xsell_grp_name
DROP TABLE IF EXISTS `products_xsell_grp_name`;
CREATE TABLE IF NOT EXISTS `products_xsell_grp_name` (
  `products_xsell_grp_name_id` int(10) NOT NULL,
  `xsell_sort_order` int(10) NOT NULL DEFAULT '0',
  `language_id` smallint(6) NOT NULL DEFAULT '0',
  `groupname` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.products_xsell_grp_name: 0 rows
DELETE FROM `products_xsell_grp_name`;
/*!40000 ALTER TABLE `products_xsell_grp_name` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_xsell_grp_name` ENABLE KEYS */;


-- Dumping structure for table crazy_import.reviews
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `reviews_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `customers_id` int(11) DEFAULT NULL,
  `customers_name` varchar(64) NOT NULL,
  `reviews_rating` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `reviews_read` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reviews_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.reviews: 0 rows
DELETE FROM `reviews`;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;


-- Dumping structure for table crazy_import.reviews_description
DROP TABLE IF EXISTS `reviews_description`;
CREATE TABLE IF NOT EXISTS `reviews_description` (
  `reviews_id` int(11) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `reviews_text` text NOT NULL,
  PRIMARY KEY (`reviews_id`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.reviews_description: 0 rows
DELETE FROM `reviews_description`;
/*!40000 ALTER TABLE `reviews_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews_description` ENABLE KEYS */;


-- Dumping structure for table crazy_import.scart
DROP TABLE IF EXISTS `scart`;
CREATE TABLE IF NOT EXISTS `scart` (
  `scartid` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `dateadded` varchar(8) NOT NULL,
  `datemodified` varchar(8) NOT NULL,
  PRIMARY KEY (`scartid`),
  UNIQUE KEY `customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.scart: 0 rows
DELETE FROM `scart`;
/*!40000 ALTER TABLE `scart` DISABLE KEYS */;
/*!40000 ALTER TABLE `scart` ENABLE KEYS */;


-- Dumping structure for table crazy_import.sessions
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `sesskey` varchar(32) NOT NULL,
  `expiry` int(11) unsigned NOT NULL,
  `value` mediumtext CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`sesskey`),
  KEY `expiry` (`expiry`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.sessions: ~1 rows (approximately)
DELETE FROM `sessions`;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` (`sesskey`, `expiry`, `value`) VALUES
	('ls7ch4u3tsavun2cejvrv54uk7', 1331302445, 'language|s:6:"german";languages_id|N;customers_status|a:20:{s:19:"customers_status_id";s:33:"DEFAULT_CUSTOMERS_STATUS_ID_GUEST";s:21:"customers_status_name";N;s:22:"customers_status_image";N;s:25:"customers_status_discount";N;s:23:"customers_status_public";N;s:26:"customers_status_min_order";N;s:26:"customers_status_max_order";N;s:33:"customers_status_ot_discount_flag";N;s:28:"customers_status_ot_discount";N;s:33:"customers_status_graduated_prices";N;s:27:"customers_status_show_price";N;s:31:"customers_status_show_price_tax";N;s:27:"customers_status_add_tax_ot";N;s:34:"customers_status_payment_unallowed";N;s:35:"customers_status_shipping_unallowed";N;s:36:"customers_status_discount_attributes";N;s:15:"customers_fsk18";N;s:23:"customers_fsk18_display";N;s:30:"customers_status_write_reviews";N;s:29:"customers_status_read_reviews";N;}user_info|a:4:{s:7:"user_ip";s:3:"::1";s:9:"user_host";s:8:"andre-PC";s:10:"advertiser";N;s:11:"referer_url";s:51:"http://localhost/crazy_import/admin/csv_backend.php";}selected_box|s:13:"configuration";');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;


-- Dumping structure for table crazy_import.shipping_status
DROP TABLE IF EXISTS `shipping_status`;
CREATE TABLE IF NOT EXISTS `shipping_status` (
  `shipping_status_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `shipping_status_name` varchar(32) NOT NULL,
  `shipping_status_image` varchar(32) NOT NULL,
  PRIMARY KEY (`shipping_status_id`,`language_id`),
  KEY `idx_shipping_status_name` (`shipping_status_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.shipping_status: 0 rows
DELETE FROM `shipping_status`;
/*!40000 ALTER TABLE `shipping_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping_status` ENABLE KEYS */;


-- Dumping structure for table crazy_import.specials
DROP TABLE IF EXISTS `specials`;
CREATE TABLE IF NOT EXISTS `specials` (
  `specials_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `specials_quantity` int(4) NOT NULL,
  `specials_sold` int(10) unsigned NOT NULL,
  `specials_new_products_price` decimal(15,4) NOT NULL,
  `discount_in_percent` int(11) NOT NULL,
  `specials_date_added` datetime DEFAULT NULL,
  `specials_last_modified` datetime DEFAULT NULL,
  `start_date` date NOT NULL,
  `expires_date` date DEFAULT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`specials_id`),
  KEY `status` (`status`),
  KEY `start_date` (`start_date`,`expires_date`),
  KEY `products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.specials: 0 rows
DELETE FROM `specials`;
/*!40000 ALTER TABLE `specials` DISABLE KEYS */;
/*!40000 ALTER TABLE `specials` ENABLE KEYS */;


-- Dumping structure for table crazy_import.sphinx_intermediate
DROP TABLE IF EXISTS `sphinx_intermediate`;
CREATE TABLE IF NOT EXISTS `sphinx_intermediate` (
  `product_id` int(10) unsigned NOT NULL,
  `filter_value_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.sphinx_intermediate: 0 rows
DELETE FROM `sphinx_intermediate`;
/*!40000 ALTER TABLE `sphinx_intermediate` DISABLE KEYS */;
/*!40000 ALTER TABLE `sphinx_intermediate` ENABLE KEYS */;


-- Dumping structure for table crazy_import.sphinx_orphans
DROP TABLE IF EXISTS `sphinx_orphans`;
CREATE TABLE IF NOT EXISTS `sphinx_orphans` (
  `product_id` int(10) unsigned NOT NULL,
  `filter_id` int(10) unsigned NOT NULL,
  `filter_value` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`,`filter_id`,`filter_value`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.sphinx_orphans: 0 rows
DELETE FROM `sphinx_orphans`;
/*!40000 ALTER TABLE `sphinx_orphans` DISABLE KEYS */;
/*!40000 ALTER TABLE `sphinx_orphans` ENABLE KEYS */;


-- Dumping structure for table crazy_import.staffel_templates
DROP TABLE IF EXISTS `staffel_templates`;
CREATE TABLE IF NOT EXISTS `staffel_templates` (
  `template_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) NOT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.staffel_templates: 0 rows
DELETE FROM `staffel_templates`;
/*!40000 ALTER TABLE `staffel_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `staffel_templates` ENABLE KEYS */;


-- Dumping structure for table crazy_import.staffel_to_templates
DROP TABLE IF EXISTS `staffel_to_templates`;
CREATE TABLE IF NOT EXISTS `staffel_to_templates` (
  `template_id` int(5) unsigned NOT NULL,
  `quantity` int(5) DEFAULT NULL,
  `personal_offer` decimal(15,4) DEFAULT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.staffel_to_templates: 0 rows
DELETE FROM `staffel_to_templates`;
/*!40000 ALTER TABLE `staffel_to_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `staffel_to_templates` ENABLE KEYS */;


-- Dumping structure for table crazy_import.tax_class
DROP TABLE IF EXISTS `tax_class`;
CREATE TABLE IF NOT EXISTS `tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_title` varchar(32) NOT NULL,
  `tax_class_description` varchar(255) NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`tax_class_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.tax_class: 0 rows
DELETE FROM `tax_class`;
/*!40000 ALTER TABLE `tax_class` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_class` ENABLE KEYS */;


-- Dumping structure for table crazy_import.tax_rates
DROP TABLE IF EXISTS `tax_rates`;
CREATE TABLE IF NOT EXISTS `tax_rates` (
  `tax_rates_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_zone_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_priority` int(5) DEFAULT '1',
  `tax_rate` decimal(7,4) NOT NULL,
  `tax_description` varchar(255) NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`tax_rates_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.tax_rates: 0 rows
DELETE FROM `tax_rates`;
/*!40000 ALTER TABLE `tax_rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_rates` ENABLE KEYS */;


-- Dumping structure for table crazy_import.variation_attributes
DROP TABLE IF EXISTS `variation_attributes`;
CREATE TABLE IF NOT EXISTS `variation_attributes` (
  `variation_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(30) NOT NULL,
  PRIMARY KEY (`variation_attribute_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Mögliche\r\nVarianteneigenschaften';

-- Dumping data for table crazy_import.variation_attributes: 0 rows
DELETE FROM `variation_attributes`;
/*!40000 ALTER TABLE `variation_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `variation_attributes` ENABLE KEYS */;


-- Dumping structure for table crazy_import.variation_attributes_description
DROP TABLE IF EXISTS `variation_attributes_description`;
CREATE TABLE IF NOT EXISTS `variation_attributes_description` (
  `variation_attribute_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`variation_attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.variation_attributes_description: 0 rows
DELETE FROM `variation_attributes_description`;
/*!40000 ALTER TABLE `variation_attributes_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `variation_attributes_description` ENABLE KEYS */;


-- Dumping structure for table crazy_import.voucher_mail_text
DROP TABLE IF EXISTS `voucher_mail_text`;
CREATE TABLE IF NOT EXISTS `voucher_mail_text` (
  `voucher_mail_text_id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(32) NOT NULL,
  `voucher_mail_subject` varchar(64) NOT NULL,
  `pdf_name` varchar(64) NOT NULL,
  PRIMARY KEY (`voucher_mail_text_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table crazy_import.voucher_mail_text: 0 rows
DELETE FROM `voucher_mail_text`;
/*!40000 ALTER TABLE `voucher_mail_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `voucher_mail_text` ENABLE KEYS */;


-- Dumping structure for table crazy_import.whos_online
DROP TABLE IF EXISTS `whos_online`;
CREATE TABLE IF NOT EXISTS `whos_online` (
  `customer_id` int(11) DEFAULT NULL,
  `full_name` varchar(64) NOT NULL,
  `session_id` varchar(128) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `time_entry` varchar(14) NOT NULL,
  `time_last_click` varchar(14) NOT NULL,
  `last_page_url` varchar(255) NOT NULL,
  `http_referer` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  KEY `session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.whos_online: ~0 rows (approximately)
DELETE FROM `whos_online`;
/*!40000 ALTER TABLE `whos_online` DISABLE KEYS */;
/*!40000 ALTER TABLE `whos_online` ENABLE KEYS */;


-- Dumping structure for table crazy_import.zones
DROP TABLE IF EXISTS `zones`;
CREATE TABLE IF NOT EXISTS `zones` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL,
  `zone_code` varchar(32) CHARACTER SET utf8 NOT NULL,
  `zone_name` varchar(32) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.zones: 0 rows
DELETE FROM `zones`;
/*!40000 ALTER TABLE `zones` DISABLE KEYS */;
/*!40000 ALTER TABLE `zones` ENABLE KEYS */;


-- Dumping structure for table crazy_import.zones_bak
DROP TABLE IF EXISTS `zones_bak`;
CREATE TABLE IF NOT EXISTS `zones_bak` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL,
  `zone_code` varchar(32) CHARACTER SET utf8 NOT NULL,
  `zone_name` varchar(32) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.zones_bak: 0 rows
DELETE FROM `zones_bak`;
/*!40000 ALTER TABLE `zones_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `zones_bak` ENABLE KEYS */;


-- Dumping structure for table crazy_import.zones_to_geo_zones
DROP TABLE IF EXISTS `zones_to_geo_zones`;
CREATE TABLE IF NOT EXISTS `zones_to_geo_zones` (
  `association_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `geo_zone_id` int(11) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`association_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table crazy_import.zones_to_geo_zones: 0 rows
DELETE FROM `zones_to_geo_zones`;
/*!40000 ALTER TABLE `zones_to_geo_zones` DISABLE KEYS */;
/*!40000 ALTER TABLE `zones_to_geo_zones` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

ALTER TABLE `categories`
ADD `name` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `import_key`;